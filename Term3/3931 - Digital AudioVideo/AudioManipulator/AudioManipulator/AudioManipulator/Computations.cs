﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace AudioManipulator
{
    public class Computations
    {
        /*
         * 
         * This class is intended to run most of the major math computations, including fourier, inverse fourier, pythagoras, and various windowing methods
         * This class contains the real and imaginary amplitudes 
         * 
         */
        public enum WindowType{ NOWIN, TRIWIN, HANNWIN};//Enum is for selecting which windowing technique we want to use
        public WindowType type = WindowType.NOWIN;
        public List<double> aRe = new List<double>();//the Real values of the sample aplitudes
        public List<double> aIm = new List<double>();//the Imaginary values of the sample amplitudes
        //public double[] filter;
        public double[] pnt;//the amplitude points for graphing on the frequency domain
        public double[] sampleArry;//samples for the example generator
        public Complex[] compFilter;//the filter mask, contains 1's and 0's

        /*
         * generateExample; Generates example samples to apply to the frequency graph
         * n: number of samples
         * freq: the frequencies you want to include
         * phase: the phases you want to offset things by
         */ 
        public void generateExample(double n, int[] freq, double[] phase)
        {
            sampleArry = samples(n, freq, phase);
            dft(n, sampleArry);
            pnt = pyth();
        }

        /*
         * generate: Applies dft and pythagoras to provided samples
         * samps: samples you wish analyzed
         */ 
        public void generate(double[] samps)
        {
            dft(samps.Length, samps);
            pnt = pyth();
        }

        /*
         * samples: generates samples based on provided data
         * n: number of samples
         * freq: number of frequencies
         * phase: number of phases
         * returns a double array of samples
         */ 
        public double[] samples(double n, int[] freq, double[] phase)
        {
            double[] samps = new double[(int)n];

            for (int f = 0; f < freq.Length; f++)
            {
                for (int t = 1; t <= n - 1; t++)
                {
                    samps[t] += (double)phase[f] + Math.Cos(2 * Math.PI * t * freq[f] / n);
                }
            }

            return samps;
        } 

        /*
         * dft: calculates the fourier transform for provided samples
         * n: number of samples
         * s: provided samples to analyze
         */ 
        public void dft(double n, double[] s)
        {
            
            aRe.Clear();
            aIm.Clear();

            double[] windowWeights = new double[s.Length];

            for(int f = 0; f < n; f++)
            {
                aRe.Add(0);
                aIm.Add(0);

                switch (type)
                {
                    case WindowType.NOWIN:
                        for (int jjj = 0; jjj < windowWeights.Length; jjj++)
                            windowWeights[jjj] = 1;
                        break;
                    case WindowType.TRIWIN:
                        windowWeights = triangularWindow(s);
                        break;
                    case WindowType.HANNWIN:
                        windowWeights = hannWindow(s);
                        break;
                    default:
                        for (int jjj = 0; jjj < windowWeights.Length; jjj++)
                            windowWeights[jjj] = 1;
                        break;
                }

                for (int t = 0; t < n; t++)
                {
                    //for now, if S = n then
                    aRe[f] += (double)((s[t]*windowWeights[t])*(Math.Cos(2 * Math.PI * t * f / n)));
                    aIm[f] -= (double)((s[t]*windowWeights[t])*(Math.Sin(2 * Math.PI * (t * f) / n)));
                }

                aRe[f] /= n;
                aIm[f] /= n;
            }
        }

        /*
         * dft: This is an override method for the previous dft, which accounts for if the user inputs an int array
         * n: number of samples
         * s: int array of samples
         */ 

        public void dft(double n, int[] s)
        {
            double[] smp = new double[s.Length];

            for(int iii = 0; iii < s.Length; iii++)
            {
                smp[iii] = s[iii];
            }

            dft(n, smp);
        }

        /*
         * pyth: Runs pythagoras on the aRe and aIm lists. This cannot be run until dft is run at least once.
         * returns a double array of amplitudes.
         */ 

        public double[] pyth ()
        {
            double[] tri = new double[aRe.Count];

            //Pythagorean theorem for this is theta=tan^-1(img/real)
            for(int lll = 0; lll < aRe.Count; lll++)
            {
                tri[lll] = (double)Math.Sqrt(Math.Pow(aIm[lll], 2) + Math.Pow(aRe[lll], 2));
            }
            //filter = tri;
            return tri;
        }

        /*
         * createFilterMask: fills a complex array with 0's and 1's to make a mask to apply to the frequency domain
         * cutOff: the maximum cut-off frequency bin to apply to the mask. anything lower than it gets a 1, 
         *         anything higher than the total bins minus it gets a 1, the rest is zeroes
         * freq: number of frequency bins
         */ 
        public void createFilterMask(int cutOff, int freq)
        {
            compFilter = new Complex[freq];

            for(int iii = 0;  iii < compFilter.Length; iii++)
            {
                if(iii < cutOff || iii > (compFilter.Length - cutOff))
                {
                    compFilter[iii] = new Complex(1, 1);
                }else
                {
                    compFilter[iii] = new Complex(0, 0);
                }
            }
        }

        /*
         * inverseFourier: runs inverse fourier on the already calculated fourier and provides the sample weights
         * returns: a double array of weights to apply the filter to the samples in the time domain
         */ 
        public double[] inverseFourier()
        {
            int n = compFilter.Length;
            double[] weights = new double[n];

            for (int t = 0; t < n; t++)
            {
                for (int f = 0; f < n; f++)
                {
                    weights[t] += compFilter[f].Real * Math.Cos(2 * Math.PI * t * f / n) - compFilter[f].Imaginary * Math.Sin(2 * Math.PI * t * f / n);
                }
                weights[t] /= compFilter.Length;
            }

            return weights;
        }

        /*
         * convoluteSamples: applies convolution to samples in order to apply the filter in the time domain
         * samps: provided samples to filter
         * weights: weights provided from inverseFourier
         * returns: a new double array of modified samples
         */ 
        public double[] convoluteSamples(double[] samps, double[] weights)
        {
            double[] convolutedSamples = new double[samps.Length];

            for(int iii = 0; iii < samps.Length; iii++)
            {
                for (int jjj = 0; jjj < weights.Length; jjj++)
                {
                    double tmp_sample = ((iii + jjj) >= samps.Length) ? 0 : samps[iii + jjj];
                    convolutedSamples[iii] += (weights[jjj] * tmp_sample);
                }
            }

            return convolutedSamples;
        }

        /*
         * triangularWindow: creates weights to apply to dft to display frequencies with less predicted repetition
         * samps: provided samples
         * returns: double array of new sample points to run dft on
         */ 
        public double[] triangularWindow(double[] samps)
        {
            double[] windowWeights = new double[samps.Length];
            int L = samps.Length + 1;

            for (int n = 0; n < windowWeights.Length; n++)
            {
                windowWeights[n] = 1 - Math.Abs((samps[n] - ((samps.Length - 1) / 2)) / (L / 2));
            }

            return windowWeights;
        }

        /*
         * hannWindow: creates weights to apply to dft to display frequencies with less predicted repetition
         * samps: provided samples
         * returns: double array of new sample points to run dft on
         */
        public double[] hannWindow(double[] samps)
        {
            double[] windowWeights = new double[samps.Length];
            int L = samps.Length + 1;

            for (int n = 0; n < windowWeights.Length; n++)
            {
                windowWeights[n] = 0.5 * (1-Math.Cos((2*Math.PI*samps[n])/(samps.Length - 1)));
            }

            return windowWeights;
        }
    }
}
