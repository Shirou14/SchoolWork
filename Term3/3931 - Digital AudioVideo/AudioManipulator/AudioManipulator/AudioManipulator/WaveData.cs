﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Media;

namespace AudioManipulator
{
    /*
     * 
     * WaveData class: designed to hold and manage the wave file data, including bytes, samples, and header data.
     * 
     */ 
    public class WaveData : Stream
    {
        /*
         * DLL imported methods. I didn't end up using most of these
         * I tried to make it so they would take a string for the directory, but it was not having it.
         */ 
        [DllImport("D:\\~User Data\\Projects\\SchoolWork\\Term3\\3931 - Digital AudioVideo\\AudioManipulator\\AudioManipulator\\Debug\\PetzoldRecordDll.dll", CharSet = CharSet.Auto)]
        static extern void SetWavDetails(int wfTage, int nChan, int nSamPS, int nAvgBytesPS, int nBlkAln, int wbPS, int cbSz);
        [DllImport("D:\\~User Data\\Projects\\SchoolWork\\Term3\\3931 - Digital AudioVideo\\AudioManipulator\\AudioManipulator\\Debug\\PetzoldRecordDll.dll", CharSet = CharSet.Auto)]
        static extern IntPtr getBuffer();
        [DllImport("D:\\~User Data\\Projects\\SchoolWork\\Term3\\3931 - Digital AudioVideo\\AudioManipulator\\AudioManipulator\\Debug\\PetzoldRecordDll.dll", CharSet = CharSet.Auto)]
        static extern UInt32 getBufferSize();
        [DllImport("D:\\~User Data\\Projects\\SchoolWork\\Term3\\3931 - Digital AudioVideo\\AudioManipulator\\AudioManipulator\\Debug\\PetzoldRecordDll.dll", CharSet = CharSet.Auto)]
        static extern void setBlockAlign(int ba);
        [DllImport("D:\\~User Data\\Projects\\SchoolWork\\Term3\\3931 - Digital AudioVideo\\AudioManipulator\\AudioManipulator\\Debug\\PetzoldRecordDll.dll", CharSet = CharSet.Auto)]
        static extern int getBlockAlign();
        [DllImport("D:\\~User Data\\Projects\\SchoolWork\\Term3\\3931 - Digital AudioVideo\\AudioManipulator\\AudioManipulator\\Debug\\PetzoldRecordDll.dll", CharSet = CharSet.Auto)]
        static extern void setAvgBytesPerSec(int abps);
        [DllImport("D:\\~User Data\\Projects\\SchoolWork\\Term3\\3931 - Digital AudioVideo\\AudioManipulator\\AudioManipulator\\Debug\\PetzoldRecordDll.dll", CharSet = CharSet.Auto)]
        static extern int getAvgBytesPerSec();
        [DllImport("D:\\~User Data\\Projects\\SchoolWork\\Term3\\3931 - Digital AudioVideo\\AudioManipulator\\AudioManipulator\\Debug\\PetzoldRecordDll.dll", CharSet = CharSet.Auto)]
        static extern void setSamplesPerSec(int sps);
        [DllImport("D:\\~User Data\\Projects\\SchoolWork\\Term3\\3931 - Digital AudioVideo\\AudioManipulator\\AudioManipulator\\Debug\\PetzoldRecordDll.dll", CharSet = CharSet.Auto)]
        static extern int getSamplesPerSec();
        [DllImport("D:\\~User Data\\Projects\\SchoolWork\\Term3\\3931 - Digital AudioVideo\\AudioManipulator\\AudioManipulator\\Debug\\PetzoldRecordDll.dll", CharSet = CharSet.Auto)]
        static extern void setBitsPerSample(int bps);
        [DllImport("D:\\~User Data\\Projects\\SchoolWork\\Term3\\3931 - Digital AudioVideo\\AudioManipulator\\AudioManipulator\\Debug\\PetzoldRecordDll.dll", CharSet = CharSet.Auto)]
        static extern int getBitsPerSample();
        [DllImport("D:\\~User Data\\Projects\\SchoolWork\\Term3\\3931 - Digital AudioVideo\\AudioManipulator\\AudioManipulator\\Debug\\PetzoldRecordDll.dll", CharSet = CharSet.Auto)]
        static extern void setChannels(int chnnl);
        [DllImport("D:\\~User Data\\Projects\\SchoolWork\\Term3\\3931 - Digital AudioVideo\\AudioManipulator\\AudioManipulator\\Debug\\PetzoldRecordDll.dll", CharSet = CharSet.Auto)]
        static extern int getChannels();

        private string dir;

        private BinaryReader br;

        private BinaryWriter bw;

        private MemoryStream ms;

        unsafe static IntPtr ptr;

        private FileInfo fInfo;

        double[] sampleData = null;
        public double[] sampleCopy;

        private byte[] byteData = null, extraData;
        public byte[] copyData, headerData = new byte [40], dataSizeData = new byte[4];

        public int RIFF { get; set; }
        public int FILESIZE_4 { get; set; }
        public int WAVE { get; set; }
        public int fmt_ { get; set; }
        public int fmt_size { get; set; }
        public int samples_per_sec { get; set; }
        public int avg_bytes_per_sec { get; set; }
        public int data { get; set; }
        public int data_size { get; set; }
        private int editStart, editEnd;

        public short format_tag { get; set; }
        public short nchannels { get; set; }
        public short nblock_align { get; set; }
        public short bits_per_sample { get; set; }

        public long position { get; set; }

        public SoundPlayer player;

        /*
         * WaveData: constructor for the waveData object. This variant does not open a file
         */ 
        public WaveData()
        {
            dir = "";
            byteData = new byte[1];
            byteData[0] = new byte();
        }

        /*
         * WaveData: constructor for wavedata object. this variant opens a file
         * directory: directory to a file
         */ 
        public WaveData(string directory)
        {
            dir = directory;
            readFile(dir);
        }

        /*
         * SetDirectory: reads from a directory and holds the location.
         * directory: directory to a file
         */
        public void setDirectory(string directory)
        {
            dir = directory;
            readFile(dir);
        }

        /*
         * readFromRecord: reads from the petzoldRecording DLL
         */
        public void readFromRecord()
        {
            byte[] samps;

            UInt32 bufferSize = getBufferSize();

            samps = new byte[bufferSize];

            IntPtr buffer_ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(IntPtr)));
            buffer_ptr = getBuffer();

            var buffer_pptr = (IntPtr)Marshal.PtrToStructure(buffer_ptr, typeof(IntPtr));

            try
            {
                unsafe
                {
                    byte* pByte = (byte*)buffer_pptr.ToPointer();
                    for (int i = 0; i < bufferSize; i++)
                    {
                        samps[i] = pByte[i];
                    }

                    nblock_align = (short)getBlockAlign();
                    avg_bytes_per_sec = getAvgBytesPerSec(); 
                    samples_per_sec = getSamplesPerSec();
                    bits_per_sample = (short)getBitsPerSample();
                    nchannels = (short)getChannels();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return;
            }

            /*
            if(samps.Length != 0)
            {
                
                byte[] tempByte, tmp;
                tmp = createBytes(samps);
                if (byteData != null)
                {
                    tempByte = new byte[byteData.Length + tmp.Length];
                    Array.ConstrainedCopy(byteData, 0, tempByte, 0, byteData.Length);
                    Array.ConstrainedCopy(tmp, 0, tempByte, byteData.Length, tmp.Length);
                    byteData = tempByte;
                }
                else*/
            byteData = samps;// tmp;

               /* if (sampleData != null)
                {
                    double[] tempSamp = new double[sampleData.Length + samps.Length];
                    Array.ConstrainedCopy(sampleData, 0, tempSamp, 0, sampleData.Length);
                    Array.ConstrainedCopy(samps, 0, tempSamp, sampleData.Length, samps.Length);
                }
                else
                    sampleData = samps;*/


            //}
            Console.WriteLine("dog dog dog");

        }

        /*
         * gets the directory file
         */
        public string getDirectory()
        {
            return dir;
        }

        /*
         * readFile: reads the wav file provided
         * file: the file directory we wish to read
         */ 
        public void readFile(string file)
        {
            br = new BinaryReader(File.OpenRead(file));

            headerData = br.ReadBytes(40);
            br.BaseStream.Seek(0, SeekOrigin.Begin);

            //header information
            RIFF = br.ReadInt32();
            FILESIZE_4 = br.ReadInt32();
            WAVE = br.ReadInt32();
            fmt_ = br.ReadInt32(); ;
            fmt_size = br.ReadInt32();
            format_tag = br.ReadInt16();
            nchannels = br.ReadInt16();
            samples_per_sec = br.ReadInt32();
            avg_bytes_per_sec = br.ReadInt32();
            nblock_align = br.ReadInt16();
            bits_per_sample = br.ReadInt16();
            data = br.ReadInt32();
            data_size = br.ReadInt32();

            sampleData = new double[(data_size / (bits_per_sample / 8))];
            byteData = new byte[(data_size)];/// (bits_per_sample / 8))];
            extraData = new byte[Math.Max(GetFileLength() - 44 - byteData.Length, 0)];

            for (int iii = 0; iii < sampleData.Length; iii++)
            {
                //switch statement accounts for different file sizes.
                switch (bits_per_sample)
                {
                    case 8:
                        try
                        {
                            sampleData[iii] = br.ReadByte();
                        }catch(EndOfStreamException eos)
                        {
                            break;
                        }
                        break;
                    case 16:
                        try
                        {
                            sampleData[iii] = br.ReadInt16();
                        }catch(EndOfStreamException eos)
                        {
                            break;
                        }
                        break;
                    case 32:
                        try
                        {
                            sampleData[iii] = br.ReadInt32();
                        }catch(EndOfStreamException eos)
                        {
                            break;
                        }
                        break;
                    default:
                        throw new ArgumentException("bits_per_sample is out of range for currently handled types. Please read in a 8, 16, or 32 bit per sample file.");
                }
            }

            //returns us to a point in the stream to read the byte data.
            br.BaseStream.Seek(44, SeekOrigin.Begin);

            for(int jjj = 0; jjj < byteData.Length; jjj++)
            {
                try
                {
                    byteData[jjj] = br.ReadByte();
                }catch(EndOfStreamException eos)
                {
                    break;
                }
            }
            for(int LLL = 0; LLL < extraData.Length; LLL++)
            {
                try
                {
                    extraData[LLL] = br.ReadByte();
                }catch(EndOfStreamException eos)
                {
                    break;
                }
                
            }

            br.BaseStream.Seek(44, SeekOrigin.Begin);
            br.Close();
        }

        /*
         * getSamples: returns sample array
         */
        public double[] getSamples()
        {
            return sampleData;
        }

        /*
         * setSamples: sets a sample array from elsewhere
         * samps: new sample array
         */
        public void setSamples(double[] samps)
        {
            sampleData = samps;
        }

        /*
         * createSamples: makes samples from a byte array. 
         * b: byte array to make samples from
         */ 
        public double[] createSamples(byte[] b)
        {
            double[] samps = new double[b.Length/(bits_per_sample / 8)];
            byte[] tmp = new byte[bits_per_sample / 8];

            for (int iii = 0; iii < samps.Length; iii++)
            {
                Array.ConstrainedCopy(b, (bits_per_sample/8) * iii, tmp, 0, tmp.Length);

                switch (bits_per_sample)
                {
                    case 8:
                        samps[iii] = tmp[0];
                        break;
                    case 16:
                        samps[iii] = BitConverter.ToInt16(tmp, 0);
                        break;
                    case 32:
                        samps[iii] = BitConverter.ToInt32(tmp, 0);
                        break;
                    default:
                        throw new ArgumentException("bits_per_sample is out of range for currently handled types. Please read in a 8, 16, or 32 bit per sample file.");
                }
            }

            return samps;
        }

        /*
         * createSamples: creates samples selected from the wave form
         */
        public int[] createSamples()
        {
            int[] tmp;
            int tmpStrt = (editStart/(bits_per_sample/8)), tmpEnd = (editEnd/(bits_per_sample/8));

            tmp = new int[tmpEnd - tmpStrt];
            Array.Copy(sampleData, tmpStrt, tmp, 0, (tmpEnd - tmpStrt) - 1);

            return tmp;
        }

        /*
         * createBytes: makes bytes from a sample array
         * samps: samples to make bytes out of
         */ 
        public byte[] createBytes(double[] samps)
        {
            byte[] newBytes = new byte[samps.Length * (bits_per_sample/8) * nchannels], tmp;
            if(newBytes.Length % 2 == 1)
                newBytes = new byte[newBytes.Length + 1];

            for (int iii = 0; iii < samps.Length; iii++)
            {
                switch (bits_per_sample)
                {
                    case 8:
                        tmp = BitConverter.GetBytes((byte)samps[iii]);
                        break;
                    case 16:
                        tmp = BitConverter.GetBytes((short)samps[iii]);
                        break;
                    case 32:
                        tmp = BitConverter.GetBytes((int)samps[iii]);
                        break;
                    default:
                        throw new ArgumentException("bits_per_sample is out of range for currently handled types. Please read in a 8, 16, or 32 bit per sample file.");
                }

                Array.ConstrainedCopy(tmp, 0, newBytes, iii* (bits_per_sample / 8), (bits_per_sample/8));
            }

            return newBytes;
        }

        /*
         * setBytes: sets new bytes
         * newBytes: you get the idea
         */
        public void setBytes(byte[] newBytes)
        {
            byteData = newBytes;
        }

        /*
         * getBytes: returns bytes
         */ 
        public byte[] getBytes()
        {
            return byteData;
        }

        /*
         * setEditPeriod: sets the period that has been selected based on the mouse location
         * beg: where the selection starts
         * end: where the selection ends
         */ 
        public void setEditPeriod(int beg, int end)
        {
            editStart = Math.Max(0, beg);
            if(editStart % (bits_per_sample/8) != 0)
            {
                editStart -= editStart % (bits_per_sample / 8);
            }
            editEnd = Math.Min(byteData.Length, end);
            if(editEnd % (bits_per_sample / 8) != 0)
            {
                editEnd -= editEnd % (bits_per_sample / 8);
            }
        }

        /*
         * editIncAmp: increases the amplitude of the selected section
         * inc: how much to increase it by
         */
        public void editIncAmp(int inc)
        {
            byte[] bytesPerSamp = new byte[bits_per_sample / 8];

            for(int samp = editStart; samp <= editEnd; samp += 4)
            {

                Array.ConstrainedCopy(byteData, samp, bytesPerSamp, 0, bytesPerSamp.Length - 1);

                int sample = BitConverter.ToInt32(byteData, samp);
                sample *= inc;
                bytesPerSamp = BitConverter.GetBytes(sample);
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(bytesPerSamp);

                Array.ConstrainedCopy(bytesPerSamp, 0, byteData, samp, bytesPerSamp.Length - 1);
            }
        }

        /*
         * Save: saves the file to the desired directory
         * dir: desired directory
         */ 
        public void Save(string dir)
        {
            //we are assuming the same number of bytes are being used to save as when it was opened
            //Method will have to be improved to for increased sound samples when overriding
//            br = new BinaryReader(File.OpenRead(dir));
            bw = new BinaryWriter(File.OpenWrite(dir));

            bw.Write(RIFF);
            bw.Write(FILESIZE_4);
            bw.Write(WAVE);
            bw.Write(fmt_);
            bw.Write(fmt_size);
            bw.Write(format_tag);
            bw.Write(nchannels);
            bw.Write(samples_per_sec);
            bw.Write(avg_bytes_per_sec);
            bw.Write(nblock_align);
            bw.Write(bits_per_sample);
            bw.Write(data);
            bw.Write(data_size);
            bw.Write(byteData, 0, byteData.Length);

            bw.Write(extraData, 0, extraData.Length);

            bw.Close();
        }   

        /*
         * overriden method, not used
         */
        public virtual bool HasData(int count)
        {
            return Position < Length;
        }

        /*
         * CreateCopy: intended to copy a selected section of the binary sample data 
         */
        public void CreateCopy()
        {
            //here we make sure the edit selection areas match up to samples
            if(editStart % (bits_per_sample/8) != 0)
            {
                editStart -= editStart % (bits_per_sample / 8);
            }

            if(editEnd % (bits_per_sample/8) != 0)
            {
                editEnd -= editEnd % (bits_per_sample / 8);
            }

            int diff = editEnd - editStart;
            if ((diff) > 0 && editStart < byteData.Length && editEnd < byteData.Length)
            {
                copyData = new byte[diff];
                Array.ConstrainedCopy(byteData, editStart, copyData, 0, diff - 1);
                sampleCopy = createSamples(copyData);
            }else
            {
                editStart = 0;
                editEnd = 0;
                Console.WriteLine("No selection to copy");
            }
        }

        /*
         * PasteCopy: where we paste the copy of the selected data in the wave file
         */ 
        public void PasteCopy()
        {
            if (copyData.Length <= 0) return;

            int diff = copyData.Length;

            if (copyData != null && copyData.Length > 0 && diff > 0 && editStart > 0)
            {
                byte[] tmp = new byte[byteData.Length + diff];
                Array.ConstrainedCopy(byteData, 0, tmp, 0, editStart - 1);
                Array.ConstrainedCopy(copyData, 0, tmp, editStart, copyData.Length - 1);
                Array.ConstrainedCopy(byteData, editStart, tmp, editStart + diff, byteData.Length - (editStart) - 1);

                byteData = tmp;
                //Array.ConstrainedCopy(copyData, 0, byteData, editStart, diff - 1);
            }else
            {
                Console.WriteLine("No data to paste.");
            }
        }

        /*
         * CutCopy: removes selected area from waveform
         */ 
        public void CutCopy()
        {
            if ((editEnd - editStart) <= 0) return;

            CreateCopy();

            int diff = editEnd - editStart;
            byte[] tmp = new byte[byteData.Length - diff];
            //copyData = new byte[diff];
            //Array.ConstrainedCopy(byteData, editStart, copyData, 0, diff - 1);
            Array.ConstrainedCopy(byteData, 0, tmp, 0, Math.Max(editStart - 1, 0));
            Array.ConstrainedCopy(byteData, editEnd, tmp, editStart, byteData.Length - editEnd - 1);
            editEnd = Math.Min(editStart, byteData.Length - 1);
            editStart = Math.Min(editStart, byteData.Length - 1);
            byteData = tmp;
            //byteData = new byte[byteData.Length - diff];
        }

        /*
         * plays the waveform
         */ 
        public void playStream()
        {
            if(byteData != null && byteData.Length > 0)
            {
                dataSizeData = BitConverter.GetBytes((Int32)byteData.Length);

                byte[] fullFile = new byte[44 + byteData.Length + extraData.Length];
                Array.ConstrainedCopy(headerData, 0, fullFile, 0, 40);
                Array.ConstrainedCopy(dataSizeData, 0, fullFile, 40, 4);
                Array.ConstrainedCopy(byteData, 0, fullFile, 44, byteData.Length);
                if (extraData != null && extraData.Length > 0)
                    Array.ConstrainedCopy(extraData, 0, fullFile, 44 + byteData.Length, extraData.Length);

                ms = new MemoryStream(fullFile);
                player = new SoundPlayer(ms);
                player.Play();
            }
        }

        /*
         * Read: reads the byte array and draws it to the screen
         * buffer: byte array
         * offset: where to start. default 0
         * count: samplesPerPixel*bytesPerSample
         */ 
        public override int Read(byte[] buffer, int offset, int count)
        {
            int counter = 0;

            for(int iii = offset + (int)position; iii < count + (int)position; iii++)
            {
                ++counter;
                if (iii < byteData.Length)
                    buffer[iii - (int)position] = byteData[iii];
                else
                    buffer[iii - (int)position] = 0;
            }
            position += buffer.Length;
            return counter;
        }


        /*
         * the following are overridden methods inherited from the control class we extend. they do not do anything.
         */
        public override bool CanRead { get { return true; } }

        public override bool CanSeek { get { return true; } }

        public override bool CanWrite { get { return true; } }

        public override void Flush() { }

        /*
         * Seek: seeks along the waveform in theory. not used.
         */
        public override long Seek(long offset, SeekOrigin origin)
        {
            if (origin == SeekOrigin.Begin)
                Position = offset;
            else if (origin == SeekOrigin.Current)
                Position += offset;
            else
                Position = Length + offset;
            return Position;
        }

        /*
         * another overriden method we do not use.
         */
        public override void SetLength(long length)
        {
            throw new NotSupportedException("Can't set length of a WaveFormatString");
        }

        /*
         * Length: returns byteData length
         */
        public override long Length
        {
            get
            {
                return byteData.Length;
            }
        }

        /*
         * getFileLength: returns the file length
         */
        public long GetFileLength()
        {
            using (var file = new FileStream(dir, FileMode.Open, FileAccess.Read))
            {
                return file.Length;
            }
        }

        /*
         * Write: another overriden method we do not use
         */
        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException("Can't write to a WaveFormatString");
        }

        /*
         * BlockAlign: returns the block align value
         */
        public virtual int BlockAlign
        {
            get
            {
                return nblock_align;
            }
        }

        /*
         * Skip: in theory skips forward somewhere. not used. required for override.
         */
        public void Skip(int seconds)
        {
            long newPosition = Position + avg_bytes_per_sec * seconds;
            if (newPosition > Length)
                Position = Length;
            else if (newPosition < 0)
                Position = 0;
            else
                Position = newPosition;
        }

        /*
         * overriden method, not used
         */
        public virtual TimeSpan CurrentTime
        {
            get
            {
                return TimeSpan.FromSeconds((double)Position / avg_bytes_per_sec);
            }
            set
            {
                Position = (long)(value.TotalSeconds * avg_bytes_per_sec);
            }
        }

        /*
         * overriden method, not used
         */
        public virtual TimeSpan TotalTime
        {
            get
            {
                return TimeSpan.FromSeconds((double)Length / avg_bytes_per_sec);
            }
        }

        /*
         * overriden method, not used
         */
        public override long Position
        {
            get
            {
                return position;
            }

            set
            {
                position = value;
            }
        }

    }
}
