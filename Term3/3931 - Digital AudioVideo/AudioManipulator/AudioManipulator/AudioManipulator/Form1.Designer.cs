﻿using System.Windows.Forms;

namespace AudioManipulator
{
    partial class auManip
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.auChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.increaseAmpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.petzoldRecordingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pullRecordDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.win3 = new System.Windows.Forms.RadioButton();
            this.win2 = new System.Windows.Forms.RadioButton();
            this.win1 = new System.Windows.Forms.RadioButton();
            this.playButton = new System.Windows.Forms.Button();
            this.filterButton1 = new System.Windows.Forms.Button();
            this.AnalyzeFourierButton = new System.Windows.Forms.Button();
            this.zoomButton = new System.Windows.Forms.Button();
            this.resetButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.customWaveViewer1 = new AudioManipulator.CustomWaveViewer();
            this.hoverSampleLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.masterPanel = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.threadStripLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.nonThreadStripLabel = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.auChart)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.masterPanel.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // auChart
            // 
            chartArea1.Name = "ChartArea1";
            this.auChart.ChartAreas.Add(chartArea1);
            this.auChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.auChart.Location = new System.Drawing.Point(0, 0);
            this.auChart.Name = "auChart";
            series1.ChartArea = "ChartArea1";
            series1.Name = "Series1";
            this.auChart.Series.Add(series1);
            this.auChart.Size = new System.Drawing.Size(942, 265);
            this.auChart.TabIndex = 0;
            this.auChart.Text = "Audio";
            this.auChart.MouseDown += new System.Windows.Forms.MouseEventHandler(this.auChart_MouseDown);
            this.auChart.MouseUp += new System.Windows.Forms.MouseEventHandler(this.auChart_MouseUp);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.recordToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(942, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.toolStripSeparator1,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(111, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.increaseAmpToolStripMenuItem,
            this.toolStripSeparator2,
            this.copyToolStripMenuItem,
            this.cutToolStripMenuItem,
            this.pasteToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // increaseAmpToolStripMenuItem
            // 
            this.increaseAmpToolStripMenuItem.Name = "increaseAmpToolStripMenuItem";
            this.increaseAmpToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.increaseAmpToolStripMenuItem.Text = "Increase Amp.";
            this.increaseAmpToolStripMenuItem.Click += new System.EventHandler(this.increaseAmpToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(149, 6);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cutToolStripMenuItem.Text = "Cut";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click_1);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click_1);
            // 
            // recordToolStripMenuItem
            // 
            this.recordToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.petzoldRecordingToolStripMenuItem,
            this.pullRecordDataToolStripMenuItem});
            this.recordToolStripMenuItem.Name = "recordToolStripMenuItem";
            this.recordToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.recordToolStripMenuItem.Text = "Record";
            // 
            // petzoldRecordingToolStripMenuItem
            // 
            this.petzoldRecordingToolStripMenuItem.Name = "petzoldRecordingToolStripMenuItem";
            this.petzoldRecordingToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.petzoldRecordingToolStripMenuItem.Text = "PetzoldRecording";
            this.petzoldRecordingToolStripMenuItem.Click += new System.EventHandler(this.petzoldRecordingToolStripMenuItem_Click);
            // 
            // pullRecordDataToolStripMenuItem
            // 
            this.pullRecordDataToolStripMenuItem.Name = "pullRecordDataToolStripMenuItem";
            this.pullRecordDataToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.pullRecordDataToolStripMenuItem.Text = "Pull Record Data";
            this.pullRecordDataToolStripMenuItem.Click += new System.EventHandler(this.pullRecordDataToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.playButton);
            this.panel1.Controls.Add(this.filterButton1);
            this.panel1.Controls.Add(this.AnalyzeFourierButton);
            this.panel1.Controls.Add(this.zoomButton);
            this.panel1.Controls.Add(this.resetButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(742, 0);
            this.panel1.MaximumSize = new System.Drawing.Size(200, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 226);
            this.panel1.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(5, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(192, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Analyze Fourier (Non-Threaded)";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.win3);
            this.groupBox1.Controls.Add(this.win2);
            this.groupBox1.Controls.Add(this.win1);
            this.groupBox1.Location = new System.Drawing.Point(4, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(193, 63);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Window Mode";
            // 
            // win3
            // 
            this.win3.AutoSize = true;
            this.win3.Location = new System.Drawing.Point(88, 43);
            this.win3.Name = "win3";
            this.win3.Size = new System.Drawing.Size(93, 17);
            this.win3.TabIndex = 2;
            this.win3.Text = "Hann Window";
            this.win3.UseVisualStyleBackColor = true;
            this.win3.CheckedChanged += new System.EventHandler(this.win3_CheckedChanged);
            // 
            // win2
            // 
            this.win2.AutoSize = true;
            this.win2.Location = new System.Drawing.Point(88, 20);
            this.win2.Name = "win2";
            this.win2.Size = new System.Drawing.Size(105, 17);
            this.win2.TabIndex = 1;
            this.win2.Text = "Triangle Window";
            this.win2.UseVisualStyleBackColor = true;
            this.win2.CheckedChanged += new System.EventHandler(this.win2_CheckedChanged);
            // 
            // win1
            // 
            this.win1.AutoSize = true;
            this.win1.Location = new System.Drawing.Point(6, 30);
            this.win1.Name = "win1";
            this.win1.Size = new System.Drawing.Size(81, 17);
            this.win1.TabIndex = 0;
            this.win1.Text = "No Window";
            this.win1.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.win1.UseVisualStyleBackColor = true;
            this.win1.CheckedChanged += new System.EventHandler(this.win1_CheckedChanged);
            // 
            // playButton
            // 
            this.playButton.Location = new System.Drawing.Point(4, 163);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(193, 23);
            this.playButton.TabIndex = 6;
            this.playButton.Text = "Play";
            this.playButton.UseVisualStyleBackColor = true;
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            // 
            // filterButton1
            // 
            this.filterButton1.Enabled = false;
            this.filterButton1.Location = new System.Drawing.Point(4, 134);
            this.filterButton1.Name = "filterButton1";
            this.filterButton1.Size = new System.Drawing.Size(193, 23);
            this.filterButton1.TabIndex = 3;
            this.filterButton1.Text = "Filter 1";
            this.filterButton1.UseVisualStyleBackColor = true;
            this.filterButton1.Click += new System.EventHandler(this.filterButton1_Click);
            // 
            // AnalyzeFourierButton
            // 
            this.AnalyzeFourierButton.Location = new System.Drawing.Point(4, 3);
            this.AnalyzeFourierButton.Name = "AnalyzeFourierButton";
            this.AnalyzeFourierButton.Size = new System.Drawing.Size(193, 23);
            this.AnalyzeFourierButton.TabIndex = 2;
            this.AnalyzeFourierButton.Text = "Analyze Fourier (Threaded)";
            this.AnalyzeFourierButton.UseVisualStyleBackColor = true;
            this.AnalyzeFourierButton.Click += new System.EventHandler(this.AnalyzeFourierButton_Click);
            // 
            // zoomButton
            // 
            this.zoomButton.Location = new System.Drawing.Point(5, 192);
            this.zoomButton.Name = "zoomButton";
            this.zoomButton.Size = new System.Drawing.Size(92, 23);
            this.zoomButton.TabIndex = 4;
            this.zoomButton.Text = "Zoom";
            this.zoomButton.UseVisualStyleBackColor = true;
            this.zoomButton.Click += new System.EventHandler(this.zoomButton_Click);
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(103, 192);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(94, 23);
            this.resetButton.TabIndex = 5;
            this.resetButton.Text = "Reset Zoom";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.customWaveViewer1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(942, 226);
            this.panel2.TabIndex = 4;
            // 
            // customWaveViewer1
            // 
            this.customWaveViewer1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.customWaveViewer1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.customWaveViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.customWaveViewer1.Location = new System.Drawing.Point(0, 0);
            this.customWaveViewer1.Name = "customWaveViewer1";
            this.customWaveViewer1.SamplesPerPixel = 128;
            this.customWaveViewer1.Size = new System.Drawing.Size(942, 226);
            this.customWaveViewer1.StartPosition = ((long)(0));
            this.customWaveViewer1.TabIndex = 2;
            // 
            // hoverSampleLabel
            // 
            this.hoverSampleLabel.Name = "hoverSampleLabel";
            this.hoverSampleLabel.Size = new System.Drawing.Size(58, 17);
            this.hoverSampleLabel.Text = "Sample: 0";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.auChart);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 226);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(942, 265);
            this.panel3.TabIndex = 5;
            // 
            // masterPanel
            // 
            this.masterPanel.Controls.Add(this.panel3);
            this.masterPanel.Controls.Add(this.panel2);
            this.masterPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.masterPanel.Location = new System.Drawing.Point(0, 24);
            this.masterPanel.Name = "masterPanel";
            this.masterPanel.Size = new System.Drawing.Size(942, 491);
            this.masterPanel.TabIndex = 1;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hoverSampleLabel,
            this.threadStripLabel,
            this.nonThreadStripLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 493);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(942, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // threadStripLabel
            // 
            this.threadStripLabel.Name = "threadStripLabel";
            this.threadStripLabel.Size = new System.Drawing.Size(132, 17);
            this.threadStripLabel.Text = "Threaded Benchmark: 0";
            // 
            // nonThreadStripLabel
            // 
            this.nonThreadStripLabel.Name = "nonThreadStripLabel";
            this.nonThreadStripLabel.Size = new System.Drawing.Size(158, 17);
            this.nonThreadStripLabel.Text = "Non Threaded Benchmark: 0";
            // 
            // auManip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(942, 515);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.masterPanel);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "auManip";
            this.Text = "Audio Manipulator";
            ((System.ComponentModel.ISupportInitialize)(this.auChart)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.masterPanel.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.Windows.Forms.DataVisualization.Charting.Chart auChart;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private CustomWaveViewer customWaveViewer1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem increaseAmpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem petzoldRecordingToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button AnalyzeFourierButton;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.Button filterButton1;
        private Button resetButton;
        private Button zoomButton;
        private Panel panel2;
        private Panel panel3;
        private Panel masterPanel;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel hoverSampleLabel;
        private Button playButton;
        private GroupBox groupBox1;
        private RadioButton win3;
        private RadioButton win2;
        private RadioButton win1;
        private ToolStripMenuItem pullRecordDataToolStripMenuItem;
        private Button button1;
        private ToolStripStatusLabel threadStripLabel;
        private ToolStripStatusLabel nonThreadStripLabel;
    }
}
