﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Media;
using AudioManipulator;

namespace AudioManipulator
{
    /*
     * 
     * The CustomWaveViewer class is the windows object that displays the samples in a waveformat. it is a smaller part of the larger program.
     * This class is a modified version of the NAudio class, following this tutorial: www.youtube.com/watch?v=BP2MhB2KQe0
     * 
     */
    public class CustomWaveViewer : System.Windows.Forms.UserControl
    {
        private IContainer components;

        private int penWidth { get; set; }
        private int samplesPerPixel = 128, bytesPerSample, leftSample, rightSample, rectStartX = 0;
        private int zoomAmount { get; set; }

        private long startPosition, endPosition;

        public bool isZoom = false;
        private bool mouseDrag = false;

        private Point mousePos, startPos, rectPos;

        private Rectangle fillRect;// = new Rectangle();

        private Color penColor { get; set; }
  
        private WaveData wd { get; set; }

        public ToolStripStatusLabel sampleStatusLabel;

        public SoundPlayer player;
        
        private System.Drawing.Brush boxBrush = new System.Drawing.SolidBrush(Color.Black);
        private System.Drawing.Graphics boxGraphics;

        public string sampleLabel;

        /*
         * CustomeWaveViewer: the constructor for the CustomWaveViewer object. 
         */ 
        public CustomWaveViewer()
        {
            penColor = Color.Crimson;
            penWidth = 1;
            InitializeComponent();
            this.DoubleBuffered = true;

            rectPos = new Point(0,0);
            rectPos = PointToScreen(rectPos);

            fillRect = new Rectangle(0, rectPos.Y, 0, this.Height);
            boxGraphics = this.CreateGraphics();
            
        }

        /*
         * fitToScreen: fits the wav's sample data to the CustomWaveViewer viewport. Used to reset zoom and set up new open files
         */ 
        public void fitToScreen()
        {
            if (wd == null) return;

            isZoom = false;//we're not zoomed in
            int samples = (int)(wd.Length/bytesPerSample);
            startPosition = 0;
            endPosition = 0;
            SamplesPerPixel = samples / Math.Max(1, this.Width);
        }

        /*
         * zoom: zooms in on a selection of samples
         */ 
        public void zoom()
        {
            if (wd == null) return;

            isZoom = true;//we are zoomed in
            startPosition = leftSample;
            endPosition = rightSample;
            SamplesPerPixel = (int)((endPosition - startPosition) / this.Width);

        }

        /*
         * drawVerticalLine: draws a verticle xor line where desired.
         * x: where desired.
         */ 
        public void drawVerticalLine(int x)
        {
            ControlPaint.DrawReversibleLine(PointToScreen(new Point(Math.Min(Math.Max(x, 0), this.Width), 0)), PointToScreen(new Point(Math.Min(Math.Max(x, 0), this.Width), this.Height)), Color.Azure);
        }

        /*
         * OnMouseDown: Override for OnMouseDown command. Informs us if the mouse is being dragged, and draws a line where it exists.
         * e: mouse event argument
         */ 
        protected override void OnMouseDown(MouseEventArgs e)
        {
            startPos = e.Location;

            if (e.Button == MouseButtons.Left)
            {
                startPos = e.Location;
                rectStartX = e.Location.X;
                mousePos = new Point(-1, -1);//so our first line isn't immediately erased. 
                mouseDrag = true;
                drawVerticalLine(-1);

                int firstSample = (int)(Math.Max(0, startPos.X) * samplesPerPixel * bytesPerSample);
                int lastSample = (int)(Math.Min(Math.Max(e.X, 0), this.Width) * samplesPerPixel * bytesPerSample);

                //this if statement will organize the two samples into "left" and "right" samples
                if (firstSample <= lastSample)
                {
                    leftSample = firstSample;
                    rightSample = lastSample;
                }
                else
                {
                    leftSample = lastSample;
                    rightSample = firstSample;
                }
                //sets the edit period to be the samples selected
                wd.setEditPeriod(leftSample, rightSample);
            }

            base.OnMouseDown(e);
        }

        /*
         * OnMouseUp: Override for OnMouseDown command. Informs us if the mouse is no longer being dragged.
         * e: mouse event argument
         */
        protected override void OnMouseUp(MouseEventArgs e)
        {
            if(mouseDrag && e.Button == MouseButtons.Left)
            {
                if (wd == null) return;

                mouseDrag = false;
                drawVerticalLine(startPos.X);

                if (mousePos.X == -1) return;
                drawVerticalLine(mousePos.X);

                //this code gets us the samples collected.
                int firstSample = (int)(Math.Max(0, startPos.X) * samplesPerPixel * bytesPerSample);
                int lastSample = (int)(Math.Min(Math.Max(e.X, 0), this.Width) * samplesPerPixel * bytesPerSample);

                //this if statement will organize the two samples into "left" and "right" samples
                if(firstSample <= lastSample)
                {
                    leftSample = firstSample;
                    rightSample = lastSample;
                }
                else
                {
                    leftSample = lastSample;
                    rightSample = firstSample;
                }
                //sets the edit period to be the samples selected
                wd.setEditPeriod(leftSample, rightSample);
            }

            base.OnMouseUp(e);
        }

        /*
         * OnMouseMove: Override for OnMouseMove command. Changes the sample status bar.
         * e: mouse event argument
         */
        protected override void OnMouseMove(MouseEventArgs e)
        {
            sampleLabel = "Sample: " + ((Math.Max(MousePosition.X, 0) * samplesPerPixel * bytesPerSample) + StartPosition);
            Console.WriteLine(sampleLabel);

            if (mouseDrag)
            {
                //We draw a line in the new position, then we draw a second line in the old position,
                //over -writing it, then we set the old position to this position.
                drawVerticalLine(e.X);
                if (mousePos.X != -1)
                    drawVerticalLine(mousePos.X);

                mousePos = e.Location;
            }

            if (sampleStatusLabel != null)
                sampleStatusLabel.Text = sampleLabel;

            base.OnMouseMove(e);
        }

        /*
         * OnResize: overrides OnResize method. make's sure the waveform adjusts with the window size. 
         *           if we're zoomed in, it uses the zoom method, if not, it resizes to fit the full thing.
         * e: event argument
         */ 
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            this.Invalidate(true);
            if (!isZoom)
            {
                fitToScreen();
            }else
            {
                zoom();
            }
        }

        /*
         * getWaveData: returns the WaveData object this object uses.
         */ 
        public WaveData getWaveData()
        {
            return wd;
        }

        /*
         * setWaveData: sets the wave data object this object should be using
         * nwd: new wave data object
         */ 
        public void setWaveData(WaveData nwd)
        {
            wd = nwd;

            if (wd != null)
            {
                bytesPerSample = (wd.bits_per_sample / 8) * wd.nchannels;
            }

            this.Invalidate();
        }

        /*
         * SamplesPerPixel: gets and sets the samplesPerPixel data member
         */
        public int SamplesPerPixel
        {
            get
            {
                return samplesPerPixel;
            }
            set
            {
                samplesPerPixel = Math.Max(1, value);
                this.Invalidate();
            }
        }

        /*
         * filter: filters the sample data drawn on the customWaveViewer
         * filterCutOff: the frequency bin cut off, selected from the frequency graph
         * comp: the Computations object this object uses
         */ 
        public void filter(int filterCutoff, Computations comp)
        {
            //create filter
            comp.createFilterMask(filterCutoff, comp.pnt.Length);
            //run inverse DFT on it
            double[] weights = comp.inverseFourier();
            //replace WaveData samples with this
            wd.setSamples(comp.convoluteSamples(wd.getSamples(), weights));
            //change waveData samples to bits and redraw the wave
            wd.setBytes(wd.createBytes(wd.getSamples()));
            //read the bytes and draw them
            wd.Read(wd.getBytes(), 0, samplesPerPixel * bytesPerSample);
            setWaveData(wd);

            if (!isZoom)
            {
                fitToScreen();
            }
            else
            {
                zoom();
            }

            this.Invalidate();
            //party?
            //definitely party.
        }

        /*
         * StartPosition: sets and gets the startPosition object
         */
        public long StartPosition
        {
            get
            {
                return startPosition;
            }
            set
            {
                startPosition = value;
            }
        }

        /*
         * Dispose: overrides the Dispose method
         * disposing: I... honestly don't know what this is for
         */ 
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /*
         * OnPaint: overrides the OnPaint method (well, probably just augments it) this is where all the logic 
         *          for using the byte array to draw the waveform.
         * e: Paint event arguments
         */ 
        protected override void OnPaint(PaintEventArgs e)
        {
            //if our wd isn't null. we don't want to draw something that doesn't exist.
            if (wd != null)
            {
                wd.position = 0;
                int bytesRead;
                byte[] waveData = new byte[samplesPerPixel * bytesPerSample];
                wd.position = startPosition + (e.ClipRectangle.Left * bytesPerSample * samplesPerPixel);

                using (Pen linePen = new Pen (penColor, penWidth))
                {
                    for (float x = e.ClipRectangle.X; x < e.ClipRectangle.Right; x += 1)
                    {
                        short low = 0;
                        short high = 0;
                        bytesRead = wd.Read(waveData, 0, samplesPerPixel * bytesPerSample);

                        if (bytesRead == 0)
                            break;

                        for (int n = 0; n < bytesRead; n += 2)
                        {
                            short sample = BitConverter.ToInt16(waveData, n);
                            if (sample < low) low = sample;
                            if (sample > high) high = sample;
                        }

                        float lowPercent = ((((float)low) - short.MinValue) / ushort.MaxValue);
                        float highPercent = ((((float)high) - short.MinValue) / ushort.MaxValue);
                        e.Graphics.DrawLine(linePen, x, this.Height * lowPercent, x, this.Height * highPercent);
                    }
                }
            }
            base.OnPaint(e);
        }


        #region Component Designer generated code
        /*
         * InitializeComponent: this is generated by C#. I don't touch this.
         */ 
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SuspendLayout();
            // 
            // CustomWaveViewer
            // 
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.DoubleBuffered = true;
            this.Name = "CustomWaveViewer";
            this.Size = new System.Drawing.Size(403, 203);
            this.ResumeLayout(false);

        }
        #endregion
    }
}