AudioManipulator
Author: Jay Coughlan, a00897647

How to run:
>Compile the Record.C and Record.RC into a new .DLL and .LIB
>Open WaveData.cs and Form1.cs and change their .DLL locations to where you have compiled your DLL 
    (I tried making it so they accepted a single string, but C# was not having it.)
>compile WaveData.cs, Form1.cs, Form1.designer.cs, CustomWaveViewer.cs, Computations.cs, and Program.cs
>run the executable.

How to use:
>The file menu allows you to open any 8, 16, or 32 bit wave file.
>to select a section of the wave file, click and drag along the desired section.
>To zoom, you must have a portion selected. the reset button will un-zoom.
>You can cut, copy, or paste any selection you've made by using the edit drop down menu. You may also increase the amplitude.
>You can record using the PetzoldRecord button in the record menu. 
>You may pull the recorded data WHILE THE RECORD WINDOW IS STILL OPEN by clicking the Pull Record Data in the record drop down.
>>>>Record data currently pulls properly, but does not display properly, and you cannot play it outside of the record window.

I tried to fix the threading benchmarking issue using another method, but that didn't work at all. so... that's a thing.

Thank you
Jay Coughlan