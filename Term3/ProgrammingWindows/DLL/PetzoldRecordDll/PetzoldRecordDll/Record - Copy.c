#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <strsafe.h>
#include "resource.h"

#define INP_BUFFER_SIZE 16384

BOOL CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);

TCHAR szAppName[] = TEXT("Assignment3");

HMODULE handle;

WAVEFORMATEX expWav;

PBYTE *buffer;

DWORD vol, *bufferSize;

int iVol, iwFormatTag = WAVE_FORMAT_PCM, inChannels = 1, inSamplesPerSec = 11025, 
		inAvgBytesPerSec = 11025, inBlockAlign = 1, iwBitsPerSample = 8, icbSize = 0;

HWND dlgHwnd = NULL;

HANDLE recordEvent;

BOOL APIENTRY DllMain(
	HANDLE hModule,	   // Handle to DLL module 
	DWORD ul_reason_for_call,
	LPVOID lpReserved)     // Reserved
{
	handle = hModule;
	return TRUE;
}

EXPORT void MakeWindow()
{
	//HINSTANCE hInstance = GetModuleHandle("andreidll_v2.dll");
	//printf("we are making the window all right!");
	//MessageBox(NULL, TEXT("Making a super window!"), TEXT("window"), MB_ICONHAND);

	if (-1 == CreateDialog(handle, TEXT("RECORD"), NULL, DlgProc))
	{
		//ErrorExit(TEXT("GetProcessId"));
		MessageBox(NULL, TEXT("This program asdfasd requires Windows NT!"),
			szAppName, MB_ICONERROR);
	}
}

EXPORT PBYTE* GetBuffer() 
{
	if (buffer != NULL)
		return buffer;
	else
		return -1;
}

EXPORT DWORD* GetBufferSize() 
{ 
	if (bufferSize != NULL)
		return bufferSize;
	else
		return -1;
}

EXPORT void SetBufferPtr(PBYTE csharp)
{
	buffer = (PBYTE)malloc(sizeof(byte));//&csharp;
}

EXPORT void SetWavDetails(int wfTage, int nChan, int nSamPS, int nAvgBytesPS, int nBlkAln, int wbPS, int cbSz)
{
	iwFormatTag = wfTage;
	inChannels = nChan;
	inSamplesPerSec = nSamPS;
	inAvgBytesPerSec = nAvgBytesPS;
	inBlockAlign = nBlkAln;
	iwBitsPerSample = wbPS;
	icbSize = cbSz;
}

EXPORT int GetWaveForm()
{
	if (&expWav != NULL)
		return &expWav;
	else
		return 0;
}

EXPORT int GetWaveFormSize()
{
	if (&expWav != NULL)
	{
		int size = sizeof(expWav);// = (44 + expWave.wBitsPerSample*);

		return size;
	}
	return 0;
}

EXPORT void SetVolume(double v)
{
	int tmp = (v / 10) * 16;
	iVol = (v / 10) * 2;
	byte btmp = tmp;
	vol = (btmp << 16) | btmp;
	if(dlgHwnd != NULL)
		SendMessage(dlgHwnd, WM_COMMAND, 0, IDC_PLAY_VOL_ADJST);
}

void RecordVolume(BYTE * pBuffer, int iLength)
{
	int i, tmp;
	for (i = 0; i < iLength; i++) {
		//tmp = 
		pBuffer[i] = pBuffer[i] * iVol;
	}
}

void ReverseMemory(BYTE * pBuffer, int iLength)
{
	BYTE b;
	int  i;

	for (i = 0; i < iLength / 2; i++)
	{
		b = pBuffer[i];
		pBuffer[i] = pBuffer[iLength - i - 1];
		pBuffer[iLength - i - 1] = b;
	}
}

BOOL CALLBACK DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static BOOL         bRecording, bPlaying, bReverse, bPaused,
		bEnding, bTerminating;
	static DWORD        dwDataLength, dwRepetitions = 1;
	static HWAVEIN      hWaveIn;
	static HWAVEOUT     hWaveOut;
	static PBYTE        pBuffer1, pBuffer2, pNewBuffer, *pSaveBuffer, pTempBuffer;
	static PWAVEHDR     pWaveHdr1, pWaveHdr2;
	static TCHAR        szOpenError[] = TEXT("Error opening waveform audio!");
	static TCHAR        szMemError[] = TEXT("Error allocating memory!");
	static WAVEFORMATEX waveform;

	//buffer = &pSaveBuffer;
	bufferSize = &dwDataLength;

	switch (message)
	{
	case WM_INITDIALOG:
		// Allocate memory for wave header
		//recordEvent = CreateEvent(NULL, TRUE, FALSE, "jCoughlan_recordStart");
		if (!(pWaveHdr1 = malloc(sizeof(WAVEHDR))))
		{
			MessageBox(NULL, TEXT("pWaveHdr1 has failed!!"), TEXT("window"), MB_ICONHAND);

		}
		if (!(pWaveHdr2 = malloc(sizeof(WAVEHDR)))) 
		{
			MessageBox(NULL, TEXT("pWaveHdr2 has failed!!"), TEXT("window"), MB_ICONHAND);
		}

		// Allocate memory for save buffer

		//pTempBuffer = malloc(1);
		
		if (!(**pSaveBuffer = malloc(1)))
		{
			MessageBox(NULL, TEXT("pSaveBuffer has failed!!"), TEXT("window"), MB_ICONHAND);
		}// &pTempBuffer;
		//MessageBox(NULL, TEXT("Making a super window! 222"), TEXT("window"), MB_ICONHAND);
		/*if (buffer != NULL)
		{
			//MessageBox(NULL, TEXT("Making a super window! 333"), TEXT("window"), MB_ICONHAND);
			pSaveBuffer = buffer;
			//memcopy(*pSaveBuffer, *buffer, sizeof(PBYTE));
		}
		else {
			//MessageBox(NULL, TEXT("Making a super window! 666"), TEXT("window"), MB_ICONHAND);
			buffer = pSaveBuffer;
		}*/
		///////////////

		return TRUE;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_RECORD_BEG:
			dlgHwnd = hwnd;
			
			// Allocate buffer memory
			MessageBox(NULL, TEXT("XXXXXXXXXXXXXX!!"), TEXT("window"), MB_ICONHAND);
			if (!(pBuffer1 = malloc(INP_BUFFER_SIZE)))
			{
				MessageBox(NULL, TEXT("pBuffer1 has failed!!"), TEXT("window"), MB_ICONHAND);
			}
			if (!(pBuffer2 = malloc(INP_BUFFER_SIZE)))
			{
				MessageBox(NULL, TEXT("pBuffer2 has failed!!"), TEXT("window"), MB_ICONHAND);
			}

			MessageBox(NULL, TEXT("YYYYYYYYYYYYYYYYY!!"), TEXT("window"), MB_ICONHAND);
			//MessageBox(NULL, TEXT("Making a super window! 444"), TEXT("window"), MB_ICONHAND);
			if (!pBuffer1 || !pBuffer2)
			{
				if (pBuffer1) free(pBuffer1);
				if (pBuffer2) free(pBuffer2);

				MessageBeep(MB_ICONEXCLAMATION);
				MessageBox(hwnd, szMemError, szAppName,
					MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			MessageBox(NULL, TEXT("ZZZZZZZZZZZZZZZZZZ!!"), TEXT("window"), MB_ICONHAND);
			// Open waveform audio for input

			waveform.wFormatTag = iwFormatTag;
			waveform.nChannels = inChannels;
			waveform.nSamplesPerSec = inSamplesPerSec;
			waveform.nAvgBytesPerSec = inAvgBytesPerSec;
			waveform.nBlockAlign = inBlockAlign;
			waveform.wBitsPerSample = iwBitsPerSample;
			waveform.cbSize = icbSize;

			if (waveInOpen(&hWaveIn, WAVE_MAPPER, &waveform,
				(DWORD)hwnd, 0, CALLBACK_WINDOW))
			{
				free(pBuffer1);
				free(pBuffer2);
				MessageBeep(MB_ICONEXCLAMATION);
				MessageBox(hwnd, szOpenError, szAppName,
					MB_ICONEXCLAMATION | MB_OK);
			}
			// Set up headers and prepare them

			pWaveHdr1->lpData = pBuffer1;
			pWaveHdr1->dwBufferLength = INP_BUFFER_SIZE;
			pWaveHdr1->dwBytesRecorded = 0;
			pWaveHdr1->dwUser = 0;
			pWaveHdr1->dwFlags = 0;
			pWaveHdr1->dwLoops = 1;
			pWaveHdr1->lpNext = NULL;
			pWaveHdr1->reserved = 0;

			waveInPrepareHeader(hWaveIn, pWaveHdr1, sizeof(WAVEHDR));

			pWaveHdr2->lpData = pBuffer2;
			pWaveHdr2->dwBufferLength = INP_BUFFER_SIZE;
			pWaveHdr2->dwBytesRecorded = 0;
			pWaveHdr2->dwUser = 0;
			pWaveHdr2->dwFlags = 0;
			pWaveHdr2->dwLoops = 1;
			pWaveHdr2->lpNext = NULL;
			pWaveHdr2->reserved = 0;

			waveInPrepareHeader(hWaveIn, pWaveHdr2, sizeof(WAVEHDR));
			return TRUE;

		case IDC_RECORD_END:
			// Reset input to return last buffer

			bEnding = TRUE;
			expWav = waveform;
			waveInReset(hWaveIn);
			return TRUE;

		case IDC_PLAY_BEG:
			// Open waveform audio for output

			waveform.wFormatTag = iwFormatTag;
			waveform.nChannels = inChannels;
			waveform.nSamplesPerSec = inSamplesPerSec;
			waveform.nAvgBytesPerSec = inAvgBytesPerSec;
			waveform.nBlockAlign = inBlockAlign;
			waveform.wBitsPerSample = iwBitsPerSample;
			waveform.cbSize = icbSize;

			if (waveOutOpen(&hWaveOut, WAVE_MAPPER, &waveform,
				(DWORD)hwnd, 0, CALLBACK_WINDOW))
			{
				MessageBeep(MB_ICONEXCLAMATION);
				MessageBox(hwnd, szOpenError, szAppName,
					MB_ICONEXCLAMATION | MB_OK);
			}

			waveOutSetVolume(&hWaveOut, vol);
			return TRUE;

		case IDC_PLAY_PAUSE:
			// Pause or restart output

			if (!bPaused)
			{
				waveOutPause(hWaveOut);
				SetDlgItemText(hwnd, IDC_PLAY_PAUSE, TEXT("Resume"));
				bPaused = TRUE;
			}
			else
			{
				waveOutRestart(hWaveOut);
				SetDlgItemText(hwnd, IDC_PLAY_PAUSE, TEXT("Pause"));
				bPaused = FALSE;
			}
			return TRUE;

		case IDC_PLAY_END:
			// Reset output for close preparation

			bEnding = TRUE;
			waveOutReset(hWaveOut);
			return TRUE;

		case IDC_PLAY_REV:
			// Reverse save buffer and play

			bReverse = TRUE;
			ReverseMemory(*pSaveBuffer, dwDataLength);

			SendMessage(hwnd, WM_COMMAND, IDC_PLAY_BEG, 0);
			return TRUE;

		case IDC_PLAY_REP:
			// Set infinite repetitions and play

			dwRepetitions = -1;
			SendMessage(hwnd, WM_COMMAND, IDC_PLAY_BEG, 0);
			return TRUE;

		case IDC_PLAY_VOL_ADJST:
			waveOutSetVolume(&hWaveOut, vol);
			return TRUE;

		case IDC_PLAY_SPEED:
			// Open waveform audio for fast output

			waveform.wFormatTag = iwFormatTag;
			waveform.nChannels = inChannels;
			waveform.nSamplesPerSec = inSamplesPerSec;
			waveform.nAvgBytesPerSec = inAvgBytesPerSec;
			waveform.nBlockAlign = inBlockAlign;
			waveform.wBitsPerSample = iwBitsPerSample;
			waveform.cbSize = icbSize;

			if (waveOutOpen(&hWaveOut, 0, &waveform, (DWORD)hwnd, 0,
				CALLBACK_WINDOW))
			{
				MessageBeep(MB_ICONEXCLAMATION);
				MessageBox(hwnd, szOpenError, szAppName,
					MB_ICONEXCLAMATION | MB_OK);
			}
			return TRUE;
		}
		break;

	case MM_WIM_OPEN:
		// Shrink down the save buffer
		*pSaveBuffer = realloc(*pSaveBuffer, 1);

		// Enable and disable Buttons

		EnableWindow(GetDlgItem(hwnd, IDC_RECORD_BEG), FALSE);
		EnableWindow(GetDlgItem(hwnd, IDC_RECORD_END), TRUE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_BEG), TRUE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_PAUSE), FALSE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_END), FALSE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_REV), FALSE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_REP), FALSE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_SPEED), FALSE);
		SetFocus(GetDlgItem(hwnd, IDC_RECORD_END));

		// Add the buffers

		waveInAddBuffer(hWaveIn, pWaveHdr1, sizeof(WAVEHDR));
		waveInAddBuffer(hWaveIn, pWaveHdr2, sizeof(WAVEHDR));

		// Begin sampling

		bRecording = TRUE;
		bEnding = FALSE;
		dwDataLength = 0;
		waveInStart(hWaveIn);
		return TRUE;

	case MM_WIM_DATA:

		// Reallocate save buffer memory

		pNewBuffer = realloc(*pSaveBuffer, dwDataLength +
			((PWAVEHDR)lParam)->dwBytesRecorded);

		if (pNewBuffer == NULL)
		{
			waveInClose(hWaveIn);
			MessageBeep(MB_ICONEXCLAMATION);
			MessageBox(hwnd, szMemError, szAppName,
				MB_ICONEXCLAMATION | MB_OK);
			return TRUE;
		}

		*pSaveBuffer = pNewBuffer;
		CopyMemory(*pSaveBuffer + dwDataLength, ((PWAVEHDR)lParam)->lpData,
			((PWAVEHDR)lParam)->dwBytesRecorded);

		dwDataLength += ((PWAVEHDR)lParam)->dwBytesRecorded;

		if (bEnding)
		{
			waveInClose(hWaveIn);
			return TRUE;
		}

		// Send out a new buffer

		waveInAddBuffer(hWaveIn, (PWAVEHDR)lParam, sizeof(WAVEHDR));
		return TRUE;

	case MM_WIM_CLOSE:
		// Free the buffer memory

		waveInUnprepareHeader(hWaveIn, pWaveHdr1, sizeof(WAVEHDR));
		waveInUnprepareHeader(hWaveIn, pWaveHdr2, sizeof(WAVEHDR));

		free(pBuffer1);
		free(pBuffer2);

		// Enable and disable buttons

		EnableWindow(GetDlgItem(hwnd, IDC_RECORD_BEG), TRUE);
		EnableWindow(GetDlgItem(hwnd, IDC_RECORD_END), FALSE);
		SetFocus(GetDlgItem(hwnd, IDC_RECORD_BEG));

		if (dwDataLength > 0)
		{
			EnableWindow(GetDlgItem(hwnd, IDC_PLAY_BEG), TRUE);
			EnableWindow(GetDlgItem(hwnd, IDC_PLAY_PAUSE), FALSE);
			EnableWindow(GetDlgItem(hwnd, IDC_PLAY_END), FALSE);
			EnableWindow(GetDlgItem(hwnd, IDC_PLAY_REP), TRUE);
			EnableWindow(GetDlgItem(hwnd, IDC_PLAY_REV), TRUE);
			EnableWindow(GetDlgItem(hwnd, IDC_PLAY_SPEED), TRUE);
			SetFocus(GetDlgItem(hwnd, IDC_PLAY_BEG));
		}
		bRecording = FALSE;

		if (bTerminating)
			SendMessage(hwnd, WM_SYSCOMMAND, SC_CLOSE, 0L);

		return TRUE;

	case MM_WOM_OPEN:
		// Enable and disable buttons

		EnableWindow(GetDlgItem(hwnd, IDC_RECORD_BEG), FALSE);
		EnableWindow(GetDlgItem(hwnd, IDC_RECORD_END), FALSE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_BEG), TRUE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_PAUSE), TRUE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_END), TRUE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_REP), FALSE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_REV), FALSE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_SPEED), FALSE);
		SetFocus(GetDlgItem(hwnd, IDC_PLAY_END));

		// Set up header

		pWaveHdr1->lpData = *pSaveBuffer;
		pWaveHdr1->dwBufferLength = dwDataLength;
		pWaveHdr1->dwBytesRecorded = 0;
		pWaveHdr1->dwUser = 0;
		pWaveHdr1->dwFlags = WHDR_BEGINLOOP | WHDR_ENDLOOP;
		pWaveHdr1->dwLoops = dwRepetitions;
		pWaveHdr1->lpNext = NULL;
		pWaveHdr1->reserved = 0;

		// Prepare and write

		waveOutPrepareHeader(hWaveOut, pWaveHdr1, sizeof(WAVEHDR));
		waveOutWrite(hWaveOut, pWaveHdr1, sizeof(WAVEHDR));

		bEnding = FALSE;
		bPlaying = TRUE;
		return TRUE;

	case MM_WOM_DONE:
		waveOutUnprepareHeader(hWaveOut, pWaveHdr1, sizeof(WAVEHDR));
		waveOutClose(hWaveOut);
		return TRUE;

	case MM_WOM_CLOSE:
		// Enable and disable buttons

		EnableWindow(GetDlgItem(hwnd, IDC_RECORD_BEG), TRUE);
		EnableWindow(GetDlgItem(hwnd, IDC_RECORD_END), TRUE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_BEG), TRUE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_PAUSE), FALSE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_END), FALSE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_REV), TRUE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_REP), TRUE);
		EnableWindow(GetDlgItem(hwnd, IDC_PLAY_SPEED), TRUE);
		SetFocus(GetDlgItem(hwnd, IDC_PLAY_BEG));

		SetDlgItemText(hwnd, IDC_PLAY_PAUSE, TEXT("Pause"));
		bPaused = FALSE;
		dwRepetitions = 1;
		bPlaying = FALSE;

		if (bReverse)
		{
			ReverseMemory(*pSaveBuffer, dwDataLength);
			bReverse = FALSE;
		}

		if (bTerminating)
			SendMessage(hwnd, WM_SYSCOMMAND, SC_CLOSE, 0L);

		return TRUE;

	case WM_SYSCOMMAND:
		switch (LOWORD(wParam))
		{
		case SC_CLOSE:
			if (bRecording)
			{
				bTerminating = TRUE;
				bEnding = TRUE;
				waveInReset(hWaveIn);
				return TRUE;
			}

			if (bPlaying)
			{
				bTerminating = TRUE;
				bEnding = TRUE;
				waveOutReset(hWaveOut);
				return TRUE;
			}

			free(pWaveHdr1);
			free(pWaveHdr2);
			//free(*pSaveBuffer);
			EndDialog(hwnd, 0);
			return TRUE;
		}
		break;
	}
	return FALSE;
}