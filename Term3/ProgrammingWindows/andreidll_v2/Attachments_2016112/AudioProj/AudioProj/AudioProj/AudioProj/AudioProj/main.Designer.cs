﻿namespace AudioProj
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.output = new System.Windows.Forms.RichTextBox();
            this.h_textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.h_textBox2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.drawButton = new System.Windows.Forms.Button();
            this.h_textBox13 = new System.Windows.Forms.TextBox();
            this.h_textBox12 = new System.Windows.Forms.TextBox();
            this.h_textBox11 = new System.Windows.Forms.TextBox();
            this.h_textBox10 = new System.Windows.Forms.TextBox();
            this.h_textBox9 = new System.Windows.Forms.TextBox();
            this.h_textBox7 = new System.Windows.Forms.TextBox();
            this.h_textBox6 = new System.Windows.Forms.TextBox();
            this.h_textBox5 = new System.Windows.Forms.TextBox();
            this.h_textBox8 = new System.Windows.Forms.TextBox();
            this.h_textBox4 = new System.Windows.Forms.TextBox();
            this.h_textBox3 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea3.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea3);
            this.chart1.DataSource = this.chart2.Titles;
            legend4.Name = "Legend1";
            this.chart1.Legends.Add(legend4);
            this.chart1.Location = new System.Drawing.Point(12, 31);
            this.chart1.Name = "chart1";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.chart1.Series.Add(series4);
            this.chart1.Size = new System.Drawing.Size(1102, 276);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // chart2
            // 
            chartArea4.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea4);
            legend3.Name = "Legend1";
            this.chart2.Legends.Add(legend3);
            this.chart2.Location = new System.Drawing.Point(12, 311);
            this.chart2.Name = "chart2";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.chart2.Series.Add(series3);
            this.chart2.Size = new System.Drawing.Size(1102, 301);
            this.chart2.TabIndex = 1;
            this.chart2.Text = "chart2";
            // 
            // output
            // 
            this.output.Location = new System.Drawing.Point(12, 618);
            this.output.Name = "output";
            this.output.Size = new System.Drawing.Size(1102, 280);
            this.output.TabIndex = 2;
            this.output.Text = "";
            // 
            // h_textBox1
            // 
            this.h_textBox1.Location = new System.Drawing.Point(154, 24);
            this.h_textBox1.Name = "h_textBox1";
            this.h_textBox1.Size = new System.Drawing.Size(167, 22);
            this.h_textBox1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Header Information";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "RIFF";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Filesize -4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "WAVE";
            // 
            // h_textBox2
            // 
            this.h_textBox2.Location = new System.Drawing.Point(154, 50);
            this.h_textBox2.Name = "h_textBox2";
            this.h_textBox2.Size = new System.Drawing.Size(167, 22);
            this.h_textBox2.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Format";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "Format Size";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "Format Tag";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 192);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 17);
            this.label8.TabIndex = 14;
            this.label8.Text = "Num Channels";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 220);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(115, 17);
            this.label9.TabIndex = 15;
            this.label9.Text = "Samples per Sec";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 247);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 17);
            this.label10.TabIndex = 16;
            this.label10.Text = "Avg Byte per Sec";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 273);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 17);
            this.label11.TabIndex = 17;
            this.label11.Text = "Nblock Align";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 299);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 17);
            this.label12.TabIndex = 18;
            this.label12.Text = "Bits per Sample";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.drawButton);
            this.panel1.Controls.Add(this.h_textBox13);
            this.panel1.Controls.Add(this.h_textBox12);
            this.panel1.Controls.Add(this.h_textBox11);
            this.panel1.Controls.Add(this.h_textBox10);
            this.panel1.Controls.Add(this.h_textBox9);
            this.panel1.Controls.Add(this.h_textBox7);
            this.panel1.Controls.Add(this.h_textBox6);
            this.panel1.Controls.Add(this.h_textBox5);
            this.panel1.Controls.Add(this.h_textBox8);
            this.panel1.Controls.Add(this.h_textBox4);
            this.panel1.Controls.Add(this.h_textBox3);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.h_textBox2);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.h_textBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(1124, 43);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(332, 449);
            this.panel1.TabIndex = 19;
            // 
            // drawButton
            // 
            this.drawButton.Location = new System.Drawing.Point(107, 397);
            this.drawButton.Name = "drawButton";
            this.drawButton.Size = new System.Drawing.Size(107, 32);
            this.drawButton.TabIndex = 35;
            this.drawButton.Text = "Draw";
            this.drawButton.UseVisualStyleBackColor = true;
            this.drawButton.Click += new System.EventHandler(this.drawButton_Click);
            // 
            // h_textBox13
            // 
            this.h_textBox13.Location = new System.Drawing.Point(154, 346);
            this.h_textBox13.Name = "h_textBox13";
            this.h_textBox13.Size = new System.Drawing.Size(167, 22);
            this.h_textBox13.TabIndex = 34;
            // 
            // h_textBox12
            // 
            this.h_textBox12.Location = new System.Drawing.Point(154, 320);
            this.h_textBox12.Name = "h_textBox12";
            this.h_textBox12.Size = new System.Drawing.Size(167, 22);
            this.h_textBox12.TabIndex = 33;
            // 
            // h_textBox11
            // 
            this.h_textBox11.Location = new System.Drawing.Point(154, 294);
            this.h_textBox11.Name = "h_textBox11";
            this.h_textBox11.Size = new System.Drawing.Size(167, 22);
            this.h_textBox11.TabIndex = 32;
            // 
            // h_textBox10
            // 
            this.h_textBox10.Location = new System.Drawing.Point(154, 268);
            this.h_textBox10.Name = "h_textBox10";
            this.h_textBox10.Size = new System.Drawing.Size(167, 22);
            this.h_textBox10.TabIndex = 31;
            // 
            // h_textBox9
            // 
            this.h_textBox9.Location = new System.Drawing.Point(154, 242);
            this.h_textBox9.Name = "h_textBox9";
            this.h_textBox9.Size = new System.Drawing.Size(167, 22);
            this.h_textBox9.TabIndex = 30;
            // 
            // h_textBox7
            // 
            this.h_textBox7.Location = new System.Drawing.Point(154, 187);
            this.h_textBox7.Name = "h_textBox7";
            this.h_textBox7.Size = new System.Drawing.Size(167, 22);
            this.h_textBox7.TabIndex = 29;
            // 
            // h_textBox6
            // 
            this.h_textBox6.Location = new System.Drawing.Point(154, 160);
            this.h_textBox6.Name = "h_textBox6";
            this.h_textBox6.Size = new System.Drawing.Size(167, 22);
            this.h_textBox6.TabIndex = 28;
            // 
            // h_textBox5
            // 
            this.h_textBox5.Location = new System.Drawing.Point(154, 134);
            this.h_textBox5.Name = "h_textBox5";
            this.h_textBox5.Size = new System.Drawing.Size(167, 22);
            this.h_textBox5.TabIndex = 27;
            // 
            // h_textBox8
            // 
            this.h_textBox8.Location = new System.Drawing.Point(154, 215);
            this.h_textBox8.Name = "h_textBox8";
            this.h_textBox8.Size = new System.Drawing.Size(167, 22);
            this.h_textBox8.TabIndex = 26;
            // 
            // h_textBox4
            // 
            this.h_textBox4.Location = new System.Drawing.Point(154, 106);
            this.h_textBox4.Name = "h_textBox4";
            this.h_textBox4.Size = new System.Drawing.Size(167, 22);
            this.h_textBox4.TabIndex = 22;
            // 
            // h_textBox3
            // 
            this.h_textBox3.Location = new System.Drawing.Point(154, 78);
            this.h_textBox3.Name = "h_textBox3";
            this.h_textBox3.Size = new System.Drawing.Size(167, 22);
            this.h_textBox3.TabIndex = 21;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 351);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 17);
            this.label14.TabIndex = 20;
            this.label14.Text = "Data Size";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 323);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 17);
            this.label13.TabIndex = 19;
            this.label13.Text = "DATA";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1467, 28);
            this.menuStrip1.TabIndex = 20;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(44, 24);
            this.toolStripMenuItem1.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(120, 26);
            this.openToolStripMenuItem.Text = "New";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(120, 26);
            this.saveToolStripMenuItem.Text = "Open";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1467, 939);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.output);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "main";
            this.Text = "main";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.RichTextBox output;
        private System.Windows.Forms.TextBox h_textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox h_textBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox h_textBox4;
        private System.Windows.Forms.TextBox h_textBox3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox h_textBox13;
        private System.Windows.Forms.TextBox h_textBox12;
        private System.Windows.Forms.TextBox h_textBox11;
        private System.Windows.Forms.TextBox h_textBox10;
        private System.Windows.Forms.TextBox h_textBox9;
        private System.Windows.Forms.TextBox h_textBox7;
        private System.Windows.Forms.TextBox h_textBox6;
        private System.Windows.Forms.TextBox h_textBox5;
        private System.Windows.Forms.TextBox h_textBox8;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.Button drawButton;
    }
}

