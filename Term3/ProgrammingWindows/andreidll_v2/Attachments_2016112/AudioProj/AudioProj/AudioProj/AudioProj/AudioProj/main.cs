﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace AudioProj
{

    public struct WAV_HEADER
    {
        public int RIFF;
        public int fileSize;
        public int WAVE;
        public int fmt;
        public int fmt_size;
        public Int16 fmt_tag;
        public Int16 n_channels;
        public int sampleRate;
        public int fmtAvgBPS;
        public Int16 fmtBlockAlign;
        public Int16 bitDepth;
        public int DATA;
        public int data_size;
    };


    public partial class main : Form
    {

        OpenFileDialog ofd = new OpenFileDialog();
        WAV_HEADER fheader = new WAV_HEADER(); 
        
        int i, j, k;

        List<int> samples;
        List<Complex> dft_result;
        List<double> idft_result;

        public main()
        {
            InitializeComponent();

            int sampleSize = 10;
            double[] frequencies = {  3, 6 };
            List<double> samples_s = this.genSampFreq();
            List<Complex> amplitude = this.fourier(samples_s, sampleSize);
            List<double> samples_re = this.ifourier(amplitude, sampleSize);

            for ( i = 0; i < sampleSize; i++)
            {
                output.Text += "Samples output " + samples_re[i] + System.Environment.NewLine;
            }
            
            for (i = 0; i < sampleSize; i++)
            {
                output.Text += "Amplitude output Real / Imaginary " + amplitude[i].Real + "    " + amplitude[i].Imaginary + System.Environment.NewLine;
            }

            for (i = 0; i < sampleSize; i++)
            {
                if ( amplitude[i].Magnitude > 0.000001 )
                    output.Text += "Total amplitude for sample [" + i + "]: " + amplitude[i].Magnitude + " Phase shift: " + amplitude[i].Phase + System.Environment.NewLine;
                else
                    output.Text += "Total amplitude for sample [" + i + "]: 0" +  System.Environment.NewLine;
            }

            for ( i = 0; i < 250; i++)
            {
                //this.chart1.Series["Series1"].Points.AddXY(i, samples_re[i]);
                //this.chart2.Series["Series1"].Points.AddXY(i, amplitude[i].Magnitude);
            }

        }

        public List<Complex> fourier(List<double> samples, int sampleSize)
        {

            List<Complex> amplitude = new List<Complex>();
            double n = sampleSize;

            for (int f = 0; f < n; f++)
            {
                double real = 0, imaginary = 0;
                
                for (int t = 0; t < n; t++)
                {
                    real += samples[t] * Math.Cos(2 * Math.PI * t * f / n);
                    imaginary -= samples[t] * Math.Sin(2 * Math.PI * t * f / n);
                }
                real /= n;
                imaginary /= n;

                amplitude.Add(new Complex(real, imaginary));
            }

            return amplitude;
        }

        public List<double> ifourier(List<Complex> amplitude, int sampleSize)
        {
            double n = (double)sampleSize;
            List<double> samples = new List<double>();

            for (int t = 0; t < n; t++)
            {
                samples.Add(new Double());
                for (int f = 0; f < n; f++)
                {
                    samples[t] += amplitude[f].Real * Math.Cos(2 * Math.PI * t * f / n) + amplitude[f].Imaginary * Math.Sin(2 * Math.PI * t * f / n);
                    
                }
            }

            return samples;
        }

        public List<double> genfreq(int n, double[] freq)
        {
            
            List<double> samples = new List<double>();

            for (int t = 0; t < n; t++)
            {
                samples.Add(new double());
                for ( int f = 0; f < freq.Length; f++)
                {
                    if ( f == 0)
                        samples[t] += Math.Cos(2 * Math.PI * t * freq[f] / (double)(n) + Math.PI / 4);
                    else
                        samples[t] += Math.Cos(2 * Math.PI * t * freq[f] / (double)(n) + Math.PI / 3);
                }
                
            }

            return samples;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ( ofd.ShowDialog() == DialogResult.OK )
            {
                readHeader(ofd.SafeFileName, ofd.FileName);
            }
        }

        private void drawButton_Click(object sender, EventArgs e)
        {
            for (i = 0; i < 250; i++)
            {

                this.chart1.Series["Series1"].Points.AddXY(i, samples[i]);
                
            }
        }

        public List<double> genSampFreq()
        {
            List<double> samples = new List<double>();

            for ( int t = 0; t < 40; t++)
            {
                samples.Add(new double());
                samples[t] = 2 * Math.Cos(2 * Math.PI * 3 * t / 40) + 3 * Math.Sin(2 * Math.PI * 14 * t / 40); 
            }
            /*
            List<double> tmp = new List<double>();
            for ( int i = 0; i < 10; i++)
            {
                tmp.Add(new double());
                tmp[i] = samples[i * 4];
            }
             */   

            return samples;
        }

        public void readHeader(String fileName, String filePath)
        {

            BinaryReader reader = new BinaryReader(File.Open(filePath, FileMode.Open));
            fheader.RIFF = reader.ReadInt32();
            h_textBox1.Text = fheader.RIFF.ToString(); 
            fheader.fileSize = reader.ReadInt32();
            h_textBox2.Text = fheader.fileSize.ToString();
            fheader.WAVE = reader.ReadInt32();
            h_textBox3.Text = fheader.WAVE.ToString();
            fheader.fmt = reader.ReadInt32();
            h_textBox4.Text = fheader.fmt.ToString();
            fheader.fmt_size = reader.ReadInt32();
            h_textBox5.Text = fheader.fmt_size.ToString();
            fheader.fmt_tag = reader.ReadInt16();
            h_textBox6.Text = fheader.fmt_tag.ToString();
            fheader.n_channels = reader.ReadInt16();
            h_textBox7.Text = fheader.n_channels.ToString();
            fheader.sampleRate = reader.ReadInt32();
            h_textBox8.Text = fheader.sampleRate.ToString();
            fheader.fmtAvgBPS = reader.ReadInt32();
            h_textBox9.Text = fheader.fmtAvgBPS.ToString();
            fheader.fmtBlockAlign = reader.ReadInt16();
            h_textBox10.Text = fheader.fmtBlockAlign.ToString();
            fheader.bitDepth = reader.ReadInt16();
            h_textBox11.Text = fheader.bitDepth.ToString();
            fheader.DATA = reader.ReadInt32();
            h_textBox12.Text = fheader.DATA.ToString();
            fheader.data_size = reader.ReadInt32();
            h_textBox13.Text = fheader.data_size.ToString();

            int n = fheader.data_size / fheader.fmt_size;
            samples = new List<int>(n);
            for ( i = 0; i < n; i++)
            {
                samples.Add(reader.ReadInt16());
            }


        }



    }
}
