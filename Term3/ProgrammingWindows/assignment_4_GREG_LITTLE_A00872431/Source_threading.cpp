#include <windows.h>
#include <mmsystem.h>
# include <stdio.h>
#include<cstdio>
#include<cwchar>
#include<wchar.h>
#include <stdarg.h>
LRESULT CALLBACK HelloWndProc(HWND, UINT, WPARAM, LPARAM);
static int i = 0;
bool race = false;
HWND PurpleButton = NULL, OrangeButton = NULL, GreenButton = NULL, BLUEButton = NULL, hwndButton;
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	PSTR szCMLine, int iCmdShow) {
	static TCHAR szAppName[] = TEXT("HelloApplication");//name of app
	static TCHAR s1Appname[]=TEXT("RACE CALLED BY : BUILD 1");
	static TCHAR s2Appname[] = TEXT("RACE CALLED BY : BUILD 2");
	HWND	hwnd;//holds handle to the main window
	MSG		msg;//holds any message retrieved from the msg queue
	WNDCLASS wndclass;//wnd class for registration
	int w;
	HANDLE hEvent[2];
	hEvent[0] = CreateEvent(NULL, TRUE, FALSE, TEXT("bob"));
	hEvent[1] = CreateEvent(NULL, TRUE, FALSE, TEXT("john"));
	DWORD waiting = WaitForMultipleObjects(2, hEvent, FALSE, INFINITE);


					  //defn wndclass attributes for this application
	wndclass.style = CS_HREDRAW | CS_VREDRAW;//redraw on refresh both directions
	wndclass.lpfnWndProc = HelloWndProc;//wnd proc to handle windows msgs/commands
	wndclass.cbClsExtra = 0;//class space for expansion/info carrying
	wndclass.cbWndExtra = 0;//wnd space for info carrying
	wndclass.hInstance = hInstance;//application instance handle
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);//set icon for window
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);//set cursor for window
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);//set background
	wndclass.lpszMenuName = NULL;//set menu
	wndclass.lpszClassName = szAppName;//set application name

									   //register wndclass to O/S so approp. wnd msg are sent to application
	if (!RegisterClass(&wndclass)) {
		MessageBox(NULL, TEXT("This program requires Windows 95/98/NT"),
			szAppName, MB_ICONERROR);//if unable to be registered
		return 0;
	}
	switch (waiting)
	{
	case WAIT_OBJECT_0 + 0:
		hwnd = CreateWindow(szAppName,		//window class name
			s2Appname, // window caption
			WS_OVERLAPPEDWINDOW,	//window style
			CW_USEDEFAULT,		//initial x position
			CW_USEDEFAULT,		//initial y position
			CW_USEDEFAULT,		//initial x size
			CW_USEDEFAULT,		//initial y size
			NULL,				//parent window handle
			NULL,				//window menu handle
			hInstance,			//program instance handle
			NULL);				//creation parameters
		break;
	case WAIT_OBJECT_0 + 1:
		hwnd = CreateWindow(szAppName,		//window class name
			s1Appname, // window caption
			WS_OVERLAPPEDWINDOW,	//window style
			CW_USEDEFAULT,		//initial x position
			CW_USEDEFAULT,		//initial y position
			CW_USEDEFAULT,		//initial x size
			CW_USEDEFAULT,		//initial y size
			NULL,				//parent window handle
			NULL,				//window menu handle
			hInstance,			//program instance handle
			NULL);				//creation parameters
		break;
	default:
		break;
	}
	//create the main window and get it's handle for future reference


	BLUEButton = CreateWindow(
		L"BUTTON",  // Predefined class; Unicode assumed 
		L"BLUE CART",      // Button text 
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles 
		0,         // x position 
		300,         // y position 
		100,        // Button width
		25,        // Button height
		hwnd,     // Parent window
		NULL,       // No menu.
		(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
		NULL);      // Pointer not needed.

	hwndButton = CreateWindow(
		L"BUTTON",  // Predefined class; Unicode assumed 
		L"RED CART",      // Button text 
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles 
		0,         // x position 
		100,         // y position 
		100,        // Button width
		25,        // Button height
		hwnd,     // Parent window
		NULL,       // No menu.
		(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
		NULL);      // Pointer not needed.

	GreenButton = CreateWindow(
		L"BUTTON",  // Predefined class; Unicode assumed 
		L"GREEN CART",      // Button text 
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles 
		0,         // x position 
		200,         // y position 
		100,        // Button width
		25,        // Button height
		hwnd,     // Parent window
		NULL,       // No menu.
		(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
		NULL);

	OrangeButton = CreateWindow(
		L"BUTTON",  // Predefined class; Unicode assumed 
		L"ORANGE CART",      // Button text 
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles 
		0,         // x position 
		400,         // y position 
		100,        // Button width
		25,        // Button height
		hwnd,     // Parent window
		NULL,       // No menu.
		(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
		NULL);

	PurpleButton = CreateWindow(
		L"BUTTON",  // Predefined class; Unicode assumed 
		L"PURPLE CART",      // Button text 
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles 
		0,         // x position 
		0,         // y position 
		100,        // Button width
		25,        // Button height
		hwnd,     // Parent window
		NULL,       // No menu.
		(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
		NULL);


	ShowWindow(hwnd, iCmdShow);//set window to be shown
	UpdateWindow(hwnd);//force an update so window is drawn

					   //messgae loop
	while (GetMessage(&msg, NULL, 0, 0)) {//get message from queue
		TranslateMessage(&msg);//for keystroke translation
		DispatchMessage(&msg);//pass msg back to windows for processing
							  //note that this is to put windows o/s in control, rather than this app
	}

	return msg.wParam;
}


DWORD WINAPI Threadproc(LPVOID lpParam) {
	while (i < 10000) {
		i++;
	if(race == true)
		Sleep(1);
	}
	return i;
}


LRESULT CALLBACK HelloWndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	HDC		hdc;
	PAINTSTRUCT ps;
	RECT	rect;
	static int xpos = 0;
	static int  cxClient, cyClient;
	HPEN pen, pen2;
	SCROLLINFO si;
	HRGN viewRegion;
	LPDWORD blue = 0, red = 0, orange = 0, green = 0, purple = 0;
	HANDLE Colors[5];
	DWORD waiter;
	HANDLE hEvent2 = CreateEvent(NULL, true, false, TEXT("time to close"));
	HANDLE hEvent3 = CreateEvent(NULL, true, false, TEXT("ok"));
	HANDLE hEventrace = CreateEvent(NULL, true, false, TEXT("race"));
	HANDLE close[3];
	LPDWORD B = NULL,R = NULL, O = NULL, G = NULL, P = NULL;
	// find how to wait but at any time then send out destroy move to destroy method 
	switch (message) {
	case WM_CREATE://additional things to do when window is created


		return 0;
	case WM_SIZE:
		cxClient = LOWORD(lParam);
		cyClient = HIWORD(lParam);
		return 0;

	case WM_RBUTTONUP:
		//MessageBox(hwnd, TEXT("this is a test of \n the message box working"), TEXT("TEST"), MB_OK);
		race = false;
		for (int k = 0; k < 5; k++) {
			Colors[k] = CreateThread(NULL, 0, Threadproc, NULL, 0, NULL);
		}
		waiter = WaitForMultipleObjects(5, Colors, FALSE, INFINITE);
		MoveWindow(PurpleButton, (cxClient / 2) + 20, 0, 100, 25, TRUE);
		MoveWindow(hwndButton, (cxClient / 2) + 20, 100, 100, 25, TRUE);
		MoveWindow(GreenButton, (cxClient / 2) + 20, 200, 100, 25, TRUE);
		MoveWindow(BLUEButton, (cxClient / 2) + 20, 300, 100, 25, TRUE);
		MoveWindow(OrangeButton, (cxClient / 2) + 20, 400, 100, 25, TRUE);
		switch (waiter)
		{
		case WAIT_OBJECT_0 + 0:
			MessageBox(hwnd, TEXT("congratulations to \n BLUE"), TEXT("THE WINNER IS!!"), MB_OK);
			break;
		case WAIT_OBJECT_0 + 1:
			MessageBox(hwnd, TEXT("congratulations to \n RED"), TEXT("THE WINNER IS!!"), MB_OK);
			break;
		case WAIT_OBJECT_0 + 2:
			MessageBox(hwnd, TEXT("congratulations to \n GREEN"), TEXT("THE WINNER IS!!"), MB_OK);
			break;
		case WAIT_OBJECT_0 + 3:
			MessageBox(hwnd, TEXT("congratulations to \n ORANGE"), TEXT("THE WINNER IS!!"), MB_OK);
			break;
		case WAIT_OBJECT_0 + 4:
			MessageBox(hwnd, TEXT("congratulations to \n PURPLE"), TEXT("THE WINNER IS!!"), MB_OK);
			break;
		default:
			break;
		}

		return 0;
	case WM_LBUTTONUP:
		//MessageBox(hwnd, TEXT("this is a test of \n the message box working \n but for the left mouse click"), TEXT("TEST"), MB_OK);
		race = true;
		for (int k = 0; k < 5; k++) {
			Colors[k] = CreateThread(NULL, 0, Threadproc, NULL, 0, NULL);
		}
		waiter = WaitForMultipleObjects(5, Colors, FALSE, INFINITE);
		MoveWindow(PurpleButton, (cxClient / 2) + 20, 0, 100, 25, TRUE);
		MoveWindow(hwndButton, (cxClient / 2) + 20, 100, 100, 25, TRUE);
		MoveWindow(GreenButton, (cxClient / 2) + 20, 200, 100, 25, TRUE);
		MoveWindow(BLUEButton, (cxClient / 2) + 20, 300, 100, 25, TRUE);
		MoveWindow(OrangeButton, (cxClient / 2) + 20, 400, 100, 25, TRUE);
		switch (waiter)
		{
		case WAIT_OBJECT_0 + 0:
			MessageBox(hwnd, TEXT("congratulations to \n BLUE"), TEXT("THE WINNER IS!!"), MB_OK);
			break;
		case WAIT_OBJECT_0 + 1:
			MessageBox(hwnd, TEXT("congratulations to \n RED"), TEXT("THE WINNER IS!!"), MB_OK);
			break;
		case WAIT_OBJECT_0 + 2:
			MessageBox(hwnd, TEXT("congratulations to \n GREEN"), TEXT("THE WINNER IS!!"), MB_OK);
			break;
		case WAIT_OBJECT_0 + 3:
			MessageBox(hwnd, TEXT("congratulations to \n ORANGE"), TEXT("THE WINNER IS!!"), MB_OK);
			break;
		case WAIT_OBJECT_0 + 4:
			MessageBox(hwnd, TEXT("congratulations to \n PURPLE"), TEXT("THE WINNER IS!!"), MB_OK);
			break;
		default:
			break;
		}

		
		/*
		if (GetExitCodeThread(Colors[0], B) != STILL_ACTIVE ||
			GetExitCodeThread(Colors[1], R) != STILL_ACTIVE ||
			GetExitCodeThread(Colors[2], G) != STILL_ACTIVE ||
			GetExitCodeThread(Colors[3], O) != STILL_ACTIVE ||
			GetExitCodeThread(Colors[4], P) != STILL_ACTIVE) {
			if (*B!=STILL_ACTIVE) {
				if (B == LPDWORD(10000)) {
					MessageBox(hwnd, TEXT("congratulations to \n BLUE"), TEXT("THE WINNER IS!!"), MB_OK);
				}
			}
			/*
			MessageBox(hwnd, TEXT("congratulations to \n BLUE"), TEXT("THE WINNER IS!!"), MB_OK);
			if (R == LPDWORD(10000)) {
				MessageBox(hwnd, TEXT("congratulations to \n RED"), TEXT("THE WINNER IS!!"), MB_OK);
			}
			if (G == LPDWORD(10000)) {
				MessageBox(hwnd, TEXT("congratulations to \n GREEN"), TEXT("THE WINNER IS!!"), MB_OK);
			}
			if (O == LPDWORD(10000)) {
				MessageBox(hwnd, TEXT("congratulations to \n ORANGE"), TEXT("THE WINNER IS!!"), MB_OK);
			}
			if (P == LPDWORD(10000)) {
				MessageBox(hwnd, TEXT("congratulations to \n PURPLE"), TEXT("THE WINNER IS!!"), MB_OK);
			}*/
		//}
		return 0;
	case WM_PAINT://what to do when a paint msg occurs
		hdc = BeginPaint(hwnd, &ps);//get a handle to a device context for drawing

		pen = CreatePen(PS_SOLID, 2, RGB(96, 111, 247));
		SelectObject(hdc, pen);
		Rectangle(hdc, cxClient / 2, 0, (cxClient / 2)+10, cyClient);
		EndPaint(hwnd, &ps);//release the device context
		return 0;

	case WM_DESTROY://how to handle a destroy (close window app) msg
		SetEvent(hEvent2);
		close[0] = CreateEvent(NULL, TRUE, FALSE, TEXT("close1"));
		close[1] = CreateEvent(NULL, TRUE, FALSE, TEXT("close2"));
		close[2] = CreateEvent(NULL, TRUE, FALSE, TEXT("close3"));
		SetEvent(close[2]);
		WaitForMultipleObjects(3, close, TRUE, INFINITE);
		PostQuitMessage(0);
		return 0;
	}
	//return the message to windows for further processing
	return DefWindowProc(hwnd, message, wParam, lParam);
}