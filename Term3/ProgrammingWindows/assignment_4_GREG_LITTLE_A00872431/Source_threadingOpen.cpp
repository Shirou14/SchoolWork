#include <windows.h>
#include <mmsystem.h>
# include <stdio.h>
# define START 101

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	PSTR szCMLine, int iCmdShow) {
	static TCHAR szAppName[] = TEXT("build 1");//name of app
	HWND	hwnd;//holds handle to the main window
	MSG		msg;//holds any message retrieved from the msg queue
	WNDCLASS wndclass;//wnd class for registration

					  //defn wndclass attributes for this application
	wndclass.style = CS_HREDRAW | CS_VREDRAW;//redraw on refresh both directions
	wndclass.lpfnWndProc = WndProc;//wnd proc to handle windows msgs/commands
	wndclass.cbClsExtra = 0;//class space for expansion/info carrying
	wndclass.cbWndExtra = 0;//wnd space for info carrying
	wndclass.hInstance = hInstance;//application instance handle
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);//set icon for window
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);//set cursor for window
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);//set background
	wndclass.lpszMenuName = NULL;//set menu
	wndclass.lpszClassName = szAppName;//set application name

									   //register wndclass to O/S so approp. wnd msg are sent to application
	if (!RegisterClass(&wndclass)) {
		MessageBox(NULL, TEXT("This program requires Windows 95/98/NT"),
			szAppName, MB_ICONERROR);//if unable to be registered
		return 0;
	}

	//create the main window and get it's handle for future reference
	hwnd = CreateWindow(szAppName,		//window class name
		TEXT("BUILD 2"), // window caption
		WS_OVERLAPPEDWINDOW,	//window style
		0,		//initial x position
		0,		//initial y position
		400,		//initial x size
		200,		//initial y size
		NULL,				//parent window handle
		NULL,				//window menu handle
		hInstance,			//program instance handle
		NULL);				//creation parameters



	ShowWindow(hwnd, iCmdShow);//set window to be shown
	UpdateWindow(hwnd);//force an update so window is drawn

					   //messgae loop
	while (GetMessage(&msg, NULL, 0, 0)) {//get message from queue
		TranslateMessage(&msg);//for keystroke translation
		DispatchMessage(&msg);//pass msg back to windows for processing
							  //note that this is to put windows o/s in control, rather than this app
	}

	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	HDC		hdc;
	PAINTSTRUCT ps;
	RECT	rect;
	static int  cxClient, cyClient;
	HWND hwndButton;
	HANDLE hEvent;
	HANDLE	close[3];
	HANDLE hEvent2 = CreateEvent(NULL, true, false, TEXT("time to close"));
	HANDLE hEvent3 = CreateEvent(NULL, true, false, TEXT("ok"));
	switch (message) {
	case WM_CREATE://additional things to do when window is created
		hwndButton = CreateWindow(
			L"BUTTON",  // Predefined class; Unicode assumed 
			L"start",      // Button text 
			WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles 
			150,         // x position 
			50,         // y position 
			100,        // Button width
			25,        // Button height
			hwnd,     // Parent window
			(HMENU)START,       // No menu.
			(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
			NULL);      // Pointer not needed.
		return 0;
	case WM_SIZE:
		cxClient = LOWORD(lParam);
		cyClient = HIWORD(lParam);
		return 0;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case START:
		//	MessageBox(hwnd, TEXT("this is a test of \n the start button is working \n but for the left mouse click"), TEXT("TEST"), MB_OK);
			hEvent = CreateEvent(NULL, true, false, TEXT("bob"));
			SetEvent(hEvent);
			close[0] = CreateEvent(NULL, TRUE, FALSE, TEXT("close1"));
			close[1] = CreateEvent(NULL, TRUE, FALSE, TEXT("close2"));
			close[2] = CreateEvent(NULL, TRUE, FALSE, TEXT("close3"));
			WaitForMultipleObjects(3, close, FALSE, INFINITE);
			SetEvent(close[1]);
			PostQuitMessage(0);
			return 0;
		}
		return 0;
	case WM_PAINT://what to do when a paint msg occurs
		hdc = BeginPaint(hwnd, &ps);//get a handle to a device context for drawing
		EndPaint(hwnd, &ps);//release the device context
		return 0;
	case WM_DESTROY://how to handle a destroy (close window app) msg
					close[0] = CreateEvent(NULL, TRUE, FALSE, TEXT("close1"));
					close[1] = CreateEvent(NULL, TRUE, FALSE, TEXT("close2"));
					close[2] = CreateEvent(NULL, TRUE, FALSE, TEXT("close3"));
					SetEvent(close[1]);
					WaitForMultipleObjects(3, close, TRUE, INFINITE);
		PostQuitMessage(0);
		return 0;
	}
	//return the message to windows for further processing

	return DefWindowProc(hwnd, message, wParam, lParam);
}