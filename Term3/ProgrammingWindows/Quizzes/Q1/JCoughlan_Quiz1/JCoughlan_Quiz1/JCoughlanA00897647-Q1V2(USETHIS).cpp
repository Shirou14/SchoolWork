/*--------------------------------------
Purpose: Test knowledge of basic win32

Description: You are to modify the code provided.
The original code used a buffer (allocated during a
size change) to store text a user typed and to display
the text being typed in real time. A left click on the
window forces the buffer to be renewed, all text gone.

The following changes are requried:

1. create a 2nd window and neatly arrange them
2. remove the STATIC on pBuffer
3. store the pointer to the buffer (pBuffer) in a location
BOTH windows can access. Make sure there is room provided to
store this.
4. retrieve and store as necessary the buffer during any
updates to the text. The result should be that both windows will display
whatever is typed in the other. A timer has been provided to force
refreshes on the passive window.
5. on a left click, store the current buffer in THAT window (the window
that was clicked on) own window memory accessible only by it. The code
needs to be placed where the WM_SIZE:WM_LBUTTONDOWN
6. on a right click, retrieve the buffer for just that window. The text drawing
code has been provided already. The result should be that a temporary display
of the previous buffered text will show. The timer will cause that text to
clear and the shared buffered text will return until the user right clicks again

0 MARKS FOR USE OF GLOBAL VARIABLES OR USING pBuffer as a STATIC

Marking Guide

Function							Mark
2 windows neatly done				1
shared location of pbuffer			1
windows display same text live		4
left click stores pbuffer locally	2
right click displays old text		2

--------------------------------------*/

#include <windows.h>

#define BUFFER(x,y) *(pBuffer + y * cxBuffer + x)

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	PSTR szCmdLine, int iCmdShow)
{
	static TCHAR szAppName[] = TEXT("Typer");
	HWND         hwnd;
	HWND		 hwnd2;
	MSG          msg;
	WNDCLASS     wndclass;
	static int     cxChar, cyChar, cxClient, cyClient, cxBuffer, cyBuffer,
		xCaret, yCaret;
	TCHAR * pBuffer = NULL;

	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = sizeof(TCHAR*);
	wndclass.cbWndExtra = sizeof(TCHAR*);
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;

	if (!RegisterClass(&wndclass))
	{
		MessageBox(NULL, TEXT("This program requires Windows NT!"),
			szAppName, MB_ICONERROR);
		return 0;
	}

	hwnd = CreateWindow(szAppName, TEXT("Typing Program"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		NULL, NULL, hInstance, NULL);

	SetWindowPos(hwnd, (HWND)-1, 0, 0, 500, 400, SWP_NOZORDER | SWP_FRAMECHANGED);

	hwnd2 = CreateWindow(szAppName, TEXT("Typing Program"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		NULL, NULL, hInstance, NULL);

	SetWindowPos(hwnd2, (HWND)-1, 500, 0, 500, 400, SWP_NOZORDER | SWP_FRAMECHANGED);

	ShowWindow(hwnd2, iCmdShow);
	UpdateWindow(hwnd2);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);



	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

/*
	This is the Window Proccess. 
*/

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static DWORD   dwCharSet = DEFAULT_CHARSET;
	static int     cxChar, cyChar, cxClient, cyClient, cxBuffer, cyBuffer,
		xCaret, yCaret;
	TCHAR * pBuffer = NULL;
	HDC            hdc;
	int            x, y, i;
	PAINTSTRUCT    ps;
	TEXTMETRIC     tm;

	switch (message)
	{
	case WM_INPUTLANGCHANGE:
		dwCharSet = wParam;
		// fall through
	case WM_CREATE:
		hdc = GetDC(hwnd);
		SetTimer(hwnd, 1, 1000, NULL);
		SelectObject(hdc, CreateFont(0, 0, 0, 0, 0, 0, 0, 0,
			dwCharSet, 0, 0, 0, FIXED_PITCH, NULL));

		GetTextMetrics(hdc, &tm);
		cxChar = tm.tmAveCharWidth;
		cyChar = tm.tmHeight;

		DeleteObject(SelectObject(hdc, GetStockObject(SYSTEM_FONT)));
		ReleaseDC(hwnd, hdc);
		// fall through  
	case WM_LBUTTONDOWN:
		//pBuffer = (TCHAR *)malloc(cxBuffer * cyBuffer * sizeof(TCHAR));

		SetWindowLongPtr(hwnd, 0, GetClassLongPtr(hwnd, 0));
	case WM_SIZE:

		// obtain window size in pixels

		if (message == WM_SIZE)
		{
			cxClient = LOWORD(lParam);
			cyClient = HIWORD(lParam);
		}
		// calculate window size in characters

		cxBuffer = max(1, cxClient / cxChar);
		cyBuffer = max(1, cyClient / cyChar);

		//You need to remember/store previous text here before creating a new buffer for new text
		pBuffer = (TCHAR *)malloc(cxBuffer * cyBuffer * sizeof(TCHAR));
		//Since both windows SHARE the current text buffer, put it where all windows can recieve it
		SetClassLongPtr(hwnd, 0, (LONG)pBuffer);


		for (y = 0; y < cyBuffer; y++)
			for (x = 0; x < cxBuffer; x++)
				BUFFER(x, y) = ' ';

		// set caret to upper left corner

		xCaret = 0;
		yCaret = 0;

		if (hwnd == GetFocus())
			SetCaretPos(xCaret * cxChar, yCaret * cyChar);

		InvalidateRect(hwnd, NULL, TRUE);
		return 0;

	case WM_TIMER://forces refresh of both windows so new text will be displayed in passive window
		InvalidateRect(hwnd, NULL, TRUE);
		UpdateWindow(hwnd);
		return 0;

	case WM_SETFOCUS:
		// create and show the caret

		CreateCaret(hwnd, NULL, cxChar, cyChar);
		SetCaretPos(xCaret * cxChar, yCaret * cyChar);
		ShowCaret(hwnd);
		return 0;
	case WM_RBUTTONDOWN:
		hdc = GetDC(hwnd);

		SelectObject(hdc, CreateFont(0, 0, 0, 0, 0, 0, 0, 0,
			dwCharSet, 0, 0, 0, FIXED_PITCH, NULL));
		//Get OLD text stored in buffer for THIS window (not the buffer both are currently using)
		pBuffer = (TCHAR *)GetWindowLongPtr(hwnd, 0);
		for (y = 0; y < cyBuffer; y++)//displays OLD (previous) text - the timer refresh will revert to new buffer text
			TextOut(hdc, 0, y * cyChar, &BUFFER(0, y), cxBuffer);
		DeleteObject(SelectObject(hdc, GetStockObject(SYSTEM_FONT)));
		ReleaseDC(hwnd, hdc);
		return 0;

	case WM_KILLFOCUS:
		// hide and destroy the caret

		HideCaret(hwnd);
		DestroyCaret();
		return 0;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_HOME:
			xCaret = 0;
			break;

		case VK_END:
			xCaret = cxBuffer - 1;
			break;

		case VK_PRIOR:
			yCaret = 0;
			break;

		case VK_NEXT:
			yCaret = cyBuffer - 1;
			break;

		case VK_LEFT:
			xCaret = max(xCaret - 1, 0);
			break;

		case VK_RIGHT:
			xCaret = min(xCaret + 1, cxBuffer - 1);
			break;

		case VK_UP:
			yCaret = max(yCaret - 1, 0);
			break;

		case VK_DOWN:
			yCaret = min(yCaret + 1, cyBuffer - 1);
			break;

		case VK_DELETE:
			//Get current buffer so it can be updated
			pBuffer = (TCHAR *)GetClassLongPtr(hwnd, 0);
			for (x = xCaret; x < cxBuffer - 1; x++)
				BUFFER(x, yCaret) = BUFFER(x + 1, yCaret);

			BUFFER(cxBuffer - 1, yCaret) = ' ';

			HideCaret(hwnd);
			hdc = GetDC(hwnd);

			SelectObject(hdc, CreateFont(0, 0, 0, 0, 0, 0, 0, 0,
				dwCharSet, 0, 0, 0, FIXED_PITCH, NULL));

			TextOut(hdc, xCaret * cxChar, yCaret * cyChar,
				&BUFFER(xCaret, yCaret),
				cxBuffer - xCaret);

			DeleteObject(SelectObject(hdc, GetStockObject(SYSTEM_FONT)));
			ReleaseDC(hwnd, hdc);
			ShowCaret(hwnd);
			break;
		}
		SetCaretPos(xCaret * cxChar, yCaret * cyChar);
		return 0;

	case WM_CHAR:
		//Get current buffer so it can be updated
		pBuffer = (TCHAR *)GetClassLongPtr(hwnd, 0);
		for (i = 0; i < (int)LOWORD(lParam); i++)
		{
			switch (wParam)
			{
			case '\b':                    // backspace
				if (xCaret > 0)
				{
					xCaret--;
					SendMessage(hwnd, WM_KEYDOWN, VK_DELETE, 1);
				}
				break;

			case '\t':                    // tab
				do
				{
					SendMessage(hwnd, WM_CHAR, ' ', 1);
				} while (xCaret % 8 != 0);
				break;

			case '\n':                    // line feed
				if (++yCaret == cyBuffer)
					yCaret = 0;
				break;

			case '\r':                    // carriage return
				xCaret = 0;

				if (++yCaret == cyBuffer)
					yCaret = 0;
				break;

			case '\x1B':                  // escape
				for (y = 0; y < cyBuffer; y++)
					for (x = 0; x < cxBuffer; x++)
						BUFFER(x, y) = ' ';

				xCaret = 0;
				yCaret = 0;

				InvalidateRect(hwnd, NULL, FALSE);
				break;

			default:
				//Get current buffer to allow changes to be recorded
				pBuffer = (TCHAR *)GetClassLongPtr(hwnd,0);
				BUFFER(xCaret, yCaret) = (TCHAR)wParam;

				HideCaret(hwnd);
				hdc = GetDC(hwnd);

				SelectObject(hdc, CreateFont(0, 0, 0, 0, 0, 0, 0, 0,
					dwCharSet, 0, 0, 0, FIXED_PITCH, NULL));

				TextOut(hdc, xCaret * cxChar, yCaret * cyChar,
					&BUFFER(xCaret, yCaret), 1);

				DeleteObject(
					SelectObject(hdc, GetStockObject(SYSTEM_FONT)));
				ReleaseDC(hwnd, hdc);
				ShowCaret(hwnd);

				if (++xCaret == cxBuffer)
				{
					xCaret = 0;

					if (++yCaret == cyBuffer)
						yCaret = 0;
				}
				break;
			}
		}
		//Store any changes if necessary
		SetClassLongPtr(hwnd, 0, (LONG)pBuffer);
		SetCaretPos(xCaret * cxChar, yCaret * cyChar);
		return 0;

	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		//Get current buffer to display current text changes
		pBuffer = (TCHAR *)GetClassLongPtr(hwnd, 0);
		SelectObject(hdc, CreateFont(0, 0, 0, 0, 0, 0, 0, 0,
			dwCharSet, 0, 0, 0, FIXED_PITCH, NULL));

		for (y = 0; y < cyBuffer; y++)
			TextOut(hdc, 0, y * cyChar, &BUFFER(0, y), cxBuffer);

		DeleteObject(SelectObject(hdc, GetStockObject(SYSTEM_FONT)));
		EndPaint(hwnd, &ps);
		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(hwnd, message, wParam, lParam);
}
