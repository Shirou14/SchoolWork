/********************************************************************
Instructions:

Hand in JUST your .cpp files
You need to complete main1.cpp and you need to create TWO OTHER
programs.
Program2 can start right away. It counts/dispalys from 5 to 1. It signals
upon completion that Program3 can start. It waits to be told it can end.
Program3 must wait for Program2 to signal. It then counts/displays from 3
to 1. It then signals it's completion. It waits to be told it can end.
Program1 (main1.cpp) must wait for BOTH Program2 AND Program3 to signal
BEFORE it can start.
Program1 then signals Program2 and Program3 that they can END .
Program1 creates TWO (2) threads in addition to the default main thread.
Each thread tries to add values to a global array. Thread1 adds odd
values 1,3,5 and Thread2 adds even values 2,4,6. Although they add their
numbers in that order the resulting order in the array may not reflect that
order. Indeed, some values might get LOST.
You need to create an addValue() function that allows values to be added
to the global array. Note you also have a global index "index".
During regular running of Program1 a RACE CONDITION is present causing
at least one value to be not added to the array (which value is unknown).
YOU MUST CREATE THE RACE CONDITION TO OCCUR EVERY TIME WHEN UNSYNCRONIZED!
Finally, your program must allow the user to prevent the race condition
from occuring using proper syncronization coding. DO NOT CREATE TWO VERSIONS
OF YOUR FUNCTIONS! A flag is provided.
Displaying of the array is provided. You will need to WAIT for the threads
to complete their filling of the array BEFORE display() is called in MAIN().

Marking

Program3 waits for Program2											2
Program1 waits for Program2 & Program3								2
Program1 waits for threads to complete before calling display()		1
Program2 & Program3 wait for Program1								1
Program1 creates a race condition properly							2
Program1 prevents a race condition properly							2

***************************************************************************************/

#include <windows.h>//needed for threading functions
#include <stdio.h>//needed for printf
#include <conio.h>//needed for _getch() function

void display();

void addValue(int value);

//names of the events
LPCTSTR name1 = L"One";
LPCTSTR name2 = L"Two";
LPCTSTR name3 = L"Three";
int data[6];//global array
int index = 0;//global index
int sync=0;//flag indicating if to use synchronization or not

//main()
//Purpose: see above description
//argc - holds number of arguments passed
//argv - holds the arguments passed
//returns 0 meaning success, anything else indicates an error (codes not used here)
int main(int argc, char* argv[])
{
	DWORD id1, id2;//thread ids
	if (argc < 2)//if an argument is not passed
		sync = 0;//no synchronization
	else
		sync = (*argv[1] == 'y' ? 1 : 0);//if "y" sync flag is set to true

	display();
	_getch();//wait for a key press - necessary if you run on ".net" instead of VS 6.0
	return 0;//what could go wrong?? :-)  Well lots actually, thread may not be created, ....
}

void addValue(int value) {
	//your code here
}

//display
//displays the values in the array
void display(){
	for(int i=0;i<6;i++)
		printf("[%d]=%d ",i,data[i]);
}