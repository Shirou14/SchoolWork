/*******************************************************************
Q2 fall 2015

Purpose: To explore scrolling

Description:

Users can draw any where on the window however the scrollable area
is only within the black rectangle that appears on the start.
Only ONE rectanle will be remembered.
Most of the code has been provided for drawing the rectangles. The
first problem to solve is that currently if a rectangle is drawn
AFTER the scrollbars have been moved the rectangle will "jump" before
scrolling. This is due to not taking in consideration WHERE the scrollbar
was during the drawing of the rectangle. 
As you did in the assignment, limit the scrolling to the area
100,100, cxClient-100, cyClient-100

You need to fix the jumping during scrolling. 

The second problem is to deal with the demagnification area.
You need to create a 2nd rectangle (the blue one) which is at
300,300, cxClient-300, cyClient-300. The rect2 struct would be
useful for this.
You need to cause a SECOND WM_PAINT to occur. To do this, add another
InvalidateRect() and UpdateWindow() after the first one for Horizontal scrolling
In this case you will use the rect2 to define a different clipping area
You will need to use the "mag" boolean variable (static) to indicate if this
is the 2nd WM_PAINT message or not.
In WM_PAINT you need to check if this is the 2nd WM_PAINT via "mag" variable
If it is, then set the mapping mode so you can change the origin and extents

Set the origin to the centre of the rectangle:
(curpt.x-  startpt.x)/2-iHorzPos+startpt.x,(curpt.y-startpt.y)/2-iVertPos+startpt.y

Set the extents to shrink the rectangle in size by 1/2 in both width and height

Draw the rectangle based on the centre of the rectangle being the origin so use:
-(curpt.x-startpt.x)/2-iHorzPos/2,-(curpt.y-startpt.y)/2-iVertPos/4, (curpt.x-startpt.x)/2-iHorzPos/2,(curpt.y-startpt.y)/2-iVertPos/4);

Otherwise draw the rectangle as you did for the first part.



HELPFUL STUFF:

There is a simple and proper way to accomplish these goals. How you accomplish
these tasks will indicate your understanding of scrolling.
Marks will be lost for inappropriate solutions or solutions that fail to 
demonstrate proper scrolling (i.e. just getting it
to seemingly work as the example is NOT enough).


Marking Guide

scrolling within limited area										2 
rectangle not jumping when scrolled, drawn, scrolled again			2
blue rectangle area prevents normal rectangle from appearing		2
rectangles that pass in the blue rectangle are scaled correctly		2
rectangles that pass in blue rectangle continue to scroll properly	2
*******************************************************************/

#include <windows.h>
#include <mmsystem.h>
#define SCROLLSTARTX 0
#define SCROLLSTARTY 100

LRESULT CALLBACK HelloWndProc (HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, 
					PSTR szCMLine, int iCmdShow){
	static TCHAR szAppName[] = TEXT ("HelloApplication");//name of app
	HWND	hwnd;//holds handle to the main window
	MSG		msg;//holds any message retrieved from the msg queue
	WNDCLASS wndclass;//wnd class for registration

	//defn wndclass attributes for this application
	wndclass.style		= CS_HREDRAW | CS_VREDRAW;//redraw on refresh both directions
	wndclass.lpfnWndProc = HelloWndProc;//wnd proc to handle windows msgs/commands
	wndclass.cbClsExtra	= 0;//class space for expansion/info carrying
	wndclass.cbWndExtra = 0;//wnd space for info carrying
	wndclass.hInstance	= hInstance;//application instance handle
	wndclass.hIcon		= LoadIcon (NULL, IDI_APPLICATION);//set icon for window
	wndclass.hCursor	= LoadCursor (NULL, IDC_ARROW);//set cursor for window
	wndclass.hbrBackground = (HBRUSH) GetStockObject (WHITE_BRUSH);//set background
	wndclass.lpszMenuName = NULL;//set menu
	wndclass.lpszClassName = szAppName;//set application name

	//register wndclass to O/S so approp. wnd msg are sent to application
	if (!RegisterClass (&wndclass)){
		MessageBox (NULL, TEXT ("This program requires Windows 95/98/NT"),
					szAppName, MB_ICONERROR);//if unable to be registered
		return 0;
	}

	//create the main window and get it's handle for future reference
	hwnd = CreateWindow(szAppName,		//window class name
						TEXT("Hello World for Windows"), // window caption
						WS_OVERLAPPEDWINDOW| WS_VSCROLL | WS_HSCROLL,	//window style
						CW_USEDEFAULT,		//initial x position
						CW_USEDEFAULT,		//initial y position
						CW_USEDEFAULT,		//initial x size
						CW_USEDEFAULT,		//initial y size
						NULL,				//parent window handle
						NULL,				//window menu handle
						hInstance,			//program instance handle
						NULL);				//creation parameters

	ShowWindow(hwnd, iCmdShow);//set window to be shown
	UpdateWindow(hwnd);//force an update so window is drawn

	//messgae loop
	while (GetMessage(&msg, NULL, 0, 0)){//get message from queue
		TranslateMessage(&msg);//for keystroke translation
		DispatchMessage(&msg);//pass msg back to windows for processing
		//note that this is to put windows o/s in control, rather than this app
	}

	return msg.wParam;
}


/**
Purpose: To handle windows messages for specific cases including when
		 the window is first created, refreshing (painting), and closing
		 the window.

Returns: Long
Notes:	 CALLBACK is defined as __stdcall which defines a calling
		 convention for assembly (stack parameter passing)
**/
LRESULT CALLBACK HelloWndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam){
	HDC		hdc;
	PAINTSTRUCT ps;
	static int  cxClient,cyClient;
	int iVertPos, iHorzPos;//IMPORTANT VARIABLES
	SCROLLINFO si;
	static POINT startpt, curpt, offset;
	static RECT rect;
	static RECT rect2;
	static BOOL mag = FALSE;

	switch (message){
		case WM_CREATE://additional things to do when window is created
			return 0;
		case WM_LBUTTONDOWN:
			startpt.x = LOWORD(lParam);
			startpt.y = HIWORD(lParam);
			curpt.x = startpt.x;
			curpt.y = startpt.y;

			return 0;
		case WM_MOUSEMOVE:
			if (LOWORD(wParam) & MK_LBUTTON){
				hdc = GetDC(hwnd);
				SetROP2(hdc,R2_NOTXORPEN);
				SelectObject(hdc,CreatePen(0,2,RGB(255,0,0)));
				Rectangle(hdc,startpt.x, startpt.y, curpt.x, curpt.y);
				curpt.x = LOWORD(lParam);
				curpt.y = HIWORD(lParam);
				Rectangle(hdc,startpt.x, startpt.y, curpt.x, curpt.y);
				ReleaseDC(hwnd,hdc);
			}
			return 0;
		case WM_LBUTTONUP:
			curpt.x = LOWORD(lParam);
			curpt.y = HIWORD(lParam);
			hdc = GetDC(hwnd);
			SelectObject(hdc,GetStockObject(NULL_BRUSH));
			SelectObject(hdc,CreatePen(0,2,RGB(255,0,0)));
			Rectangle(hdc,startpt.x, startpt.y, curpt.x, curpt.y);
			GetScrollInfo(hwnd, SB_HORZ, &si);
			startpt.x += si.nPos;
			curpt.x += si.nPos;
			GetScrollInfo(hwnd, SB_VERT, &si);
			startpt.y += si.nPos;
			curpt.y += si.nPos;

			ReleaseDC(hwnd,hdc);
			return 0;
		case WM_SIZE:
			cxClient = LOWORD(lParam);
			cyClient = HIWORD(lParam);

			si.cbSize = sizeof(si);
			si.fMask = SIF_RANGE | SIF_PAGE | SIF_POS;
			si.nMin = 1;
			si.nMax = 1000;
			si.nPage = 1;
			si.nPos= 0;
			SetScrollInfo(hwnd, SB_VERT, &si, TRUE);

			si.cbSize = sizeof(si);
			si.fMask = SIF_RANGE | SIF_PAGE | SIF_POS;
			si.nMin = -cxClient;
			si.nMax = cxClient;
			si.nPage = 1;
			si.nPos = SCROLLSTARTX;
			SetScrollInfo(hwnd, SB_HORZ, &si, TRUE);
			rect.left=100;
			rect.right=cxClient-100;
			rect.top=100;
			rect.bottom=cyClient-100;
			rect2.left = 300;
			rect2.right = cxClient-300;
			rect2.top=300;
			rect2.bottom=cyClient-300;
			return 0;
		
		case WM_HSCROLL:
			si.cbSize = sizeof(si);
			si.fMask = SIF_ALL;

			GetScrollInfo(hwnd, SB_HORZ, &si);
			iHorzPos = si.nPos;

			switch (LOWORD(wParam))
			{
			case SB_LINELEFT:
				si.nPos -=1;
				break;

			case SB_LINERIGHT:
				si.nPos += 1;
				break;

			case SB_PAGELEFT:
				si.nPos -= si.nPage;
				break;

			case SB_PAGERIGHT:
				si.nPos += si.nPage;
				break;

			case SB_THUMBTRACK:
				si.nPos = si.nTrackPos;
				break;

			default:
				break;
			}

			si.fMask = SIF_POS;
			SetScrollInfo(hwnd, SB_HORZ, &si, TRUE);
			GetScrollInfo(hwnd, SB_HORZ, &si);
			
			if (si.nPos != iHorzPos)
			{
				mag = false;
				InvalidateRect(hwnd,&rect,TRUE);
				UpdateWindow(hwnd);

				mag = true;
				InvalidateRect(hwnd, &rect2, TRUE);
				UpdateWindow(hwnd);

				
			}
			return 0;

			case WM_VSCROLL:

				si.cbSize = sizeof(si);
				si.fMask = SIF_ALL;
				GetScrollInfo(hwnd, SB_VERT, &si);
				iVertPos = si.nPos;

				switch(LOWORD(wParam)){
				case SB_TOP:
					si.nPos = si.nMin;
					break;

				case SB_BOTTOM:
					si.nPos = si.nMax;
					break;

				case SB_LINEUP:
					si.nPos--;
					break;

				case SB_LINEDOWN:
					si.nPos++;
					break;

				case SB_PAGEUP:
					si.nPos -= si.nPage;
					break;

				case SB_PAGEDOWN:
					si.nPos += si.nPage;
					break;

				case SB_THUMBTRACK:
					si.nPos = si.nTrackPos;
					break;

				default:
					break;
				}

				si.fMask = SIF_POS;
				SetScrollInfo(hwnd, SB_VERT, &si, TRUE);
				GetScrollInfo(hwnd, SB_VERT, &si);

				if (si.nPos != iVertPos){
					//ScrollWindow();
					
					mag = false;
					InvalidateRect(hwnd,&rect,TRUE);
					UpdateWindow(hwnd);

					mag = true;
					InvalidateRect(hwnd, &rect2, TRUE);
					UpdateWindow(hwnd);
				}
				return 0;


		case WM_PAINT://what to do when a paint msg occurs
			hdc = BeginPaint(hwnd, &ps);//get a handle to a device context for drawing

			//scrollbar info
			si.cbSize = sizeof(si);
			si.fMask = SIF_POS;
			GetScrollInfo(hwnd, SB_VERT, &si);
			iVertPos = si.nPos;//current vertical position
			GetScrollInfo(hwnd, SB_HORZ, &si);
			iHorzPos = si.nPos;//current horizontal position
			Rectangle(hdc,rect.left,rect.top,rect.right, rect.bottom);

			SelectObject(hdc, CreatePen(PS_SOLID, 3, RGB(10, 0, 255)));
			Rectangle(hdc, rect2.left, rect2.top, rect2.right, rect2.bottom);
			SelectObject(hdc, CreatePen(PS_SOLID, 2, RGB(0, 255, 0)));
			//blue rect here pls
			if (mag)
			{
				SetMapMode(hdc, MM_ISOTROPIC);
				SetWindowOrgEx(hdc, (curpt.x - startpt.x) / 2 - iHorzPos + startpt.x, (curpt.y - startpt.y) / 2 - iVertPos + startpt.y, NULL);
				SetWindowExtEx(hdc, 2, 2, NULL);
				SetWindowOrgEx(hdc, -(curpt.x - startpt.x) / 2 - iHorzPos / 2, -(curpt.y - startpt.y) / 2 - iVertPos / 4, NULL);
				Rectangle(hdc, startpt.x - iHorzPos, startpt.y - iVertPos, curpt.x - iHorzPos, curpt.y - iVertPos);

			}
			else {

				Rectangle(hdc, startpt.x - iHorzPos, startpt.y - iVertPos, curpt.x - iHorzPos, curpt.y - iVertPos);
			}
			EndPaint(hwnd, &ps);//release the device context
			return 0;

		case WM_DESTROY://how to handle a destroy (close window app) msg
			PostQuitMessage(0);
			return 0;
	}
	//return the message to windows for further processing
	return DefWindowProc(hwnd, message, wParam, lParam);
}