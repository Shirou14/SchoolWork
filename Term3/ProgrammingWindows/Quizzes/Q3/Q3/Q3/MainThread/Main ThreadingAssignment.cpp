/*******************************************************************
Part 1

Description: The code provided creates 10 threads (NTHREADS)
in WM_CREATE
The main thread WAITS until all threads are finished (dead) and
then displays "Finished adding to buffer" in window title bar

Each thread is putting 10 numbers into a common collection (array)
by calling the setCount() function.

Your tasks are as follows:

1. main thread must WAIT until all threads are finished/dead before
changing window title bar and appearing
2. If the BOOL sync is set to FALSE (default) the result MUST
exacerbate the race condition present so that many values are lost
3. If the BOOL sync is set to TRUE the result MUST prevent any
race condition - no values lost

Marks

Task					Mark
1						2
2						2
3						2
*******************************************************************/

#include <windows.h>

#define NUM 100
#define NTHREADS 10

int position = 0;
int* collection;
int* sort(int a[], int n);
LPCRITICAL_SECTION cs;
BOOL sync;
LRESULT CALLBACK HelloWndProc (HWND, UINT, WPARAM, LPARAM);
DWORD WINAPI countProc(PVOID pVoid);
HANDLE deathEvent[10];
void setCount(int value);
TCHAR* createDisplay(int* data, int n);
typedef struct _data {
		BOOL done;
		int start;
} DATA, *PDATA;

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, 
					PSTR szCMLine, int iCmdShow){
	static TCHAR szAppName[] = TEXT ("HelloApplication");//name of app
	HWND	hwnd;//holds handle to the main window
	MSG		msg;//holds any message retrieved from the msg queue
	WNDCLASS wndclass;//wnd class for registration
	
	//defn wndclass attributes for this application
	wndclass.style		= CS_HREDRAW | CS_VREDRAW;//redraw on refresh both directions
	wndclass.lpfnWndProc = HelloWndProc;//wnd proc to handle windows msgs/commands
	wndclass.cbClsExtra	= 0;//class space for expansion/info carrying
	wndclass.cbWndExtra = 0;//wnd space for info carrying
	wndclass.hInstance	= hInstance;//application instance handle
	wndclass.hIcon		= LoadIcon (NULL, IDI_APPLICATION);//set icon for window
	wndclass.hCursor	= LoadCursor (NULL, IDC_ARROW);//set cursor for window
	wndclass.hbrBackground = (HBRUSH) GetStockObject (WHITE_BRUSH);//set background
	wndclass.lpszMenuName = NULL;//set menu
	wndclass.lpszClassName = szAppName;//set application name

	InitializeCriticalSection(cs);
	sync = TRUE;
	//register wndclass to O/S so approp. wnd msg are sent to application
	if (!RegisterClass (&wndclass)){
		MessageBox (NULL, TEXT ("This program requires Windows 95/98/NT"),
					szAppName, MB_ICONERROR);//if unable to be registered
		return 0;
	}

	collection = (int*)calloc(NUM,sizeof(int));
	//create the main window and get it's handle for future reference
	hwnd = CreateWindow(szAppName,		//window class name
						TEXT("Hello World for Windows"), // window caption
						WS_OVERLAPPEDWINDOW,	//window style
						CW_USEDEFAULT,		//initial x position
						CW_USEDEFAULT,		//initial y position
						CW_USEDEFAULT,		//initial x size
						CW_USEDEFAULT,		//initial y size
						NULL,				//parent window handle
						NULL,				//window menu handle
						hInstance,			//program instance handle
						NULL);				//creation parameters
	ShowWindow(hwnd, iCmdShow);//set window to be shown
	UpdateWindow(hwnd);//force an update so window is drawn
	
	//messgae loop
	while (GetMessage(&msg, NULL, 0, 0)){//get message from queue
		TranslateMessage(&msg);//for keystroke translation
		DispatchMessage(&msg);//pass msg back to windows for processing
		//note that this is to put windows o/s in control, rather than this app
	}

	return msg.wParam;
}


/**
Purpose: To handle windows messages for specific cases including when
		 the window is first created, refreshing (painting), and closing
		 the window.

Returns: Long - any error message (see Win32 API for details of possible error messages)
Notes:	 CALLBACK is defined as __stdcall which defines a calling
		 convention for assembly (stack parameter passing)
**/
LRESULT CALLBACK HelloWndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam){
	HDC		hdc;
	PAINTSTRUCT ps;
	RECT	rect;
	static HANDLE hThread[NTHREADS];
	PDATA pdata[NTHREADS];
	DWORD id;
	TCHAR* display;
	int result;

	switch (message){
		case WM_CREATE:
			for (int x=0;x<NTHREADS;x++){
				pdata[x] = (PDATA)malloc(sizeof(DATA));
				pdata[x]->done = FALSE;
				pdata[x]->start = x*10;
				hThread[x] = CreateThread(NULL,0,countProc,pdata[x],0,&id);
				deathEvent[x] = CreateEvent(NULL, FALSE, TRUE, TEXT("ThreadDeath"));
			}

			//THIS SHOULD HAPPEN AFTER ALL THREADS ARE FINISHED!!!
			//result = WaitForMultipleObjects(10, deathEvent, true, 0);
			//if(result = WAIT_OBJECT_0)
				SetWindowText(hwnd,TEXT("Finished adding to buffer"));
			return 0;

		case WM_PAINT://what to do when a paint msg occurs
			hdc = BeginPaint(hwnd, &ps);//get a handle to a device context for drawing
			GetClientRect(hwnd, &rect);//define drawing area for clipping
			collection = sort(collection,NUM);
			display = createDisplay(collection,100);
			DrawText(hdc,display, -1, &rect,
					 DT_CENTER | DT_VCENTER);//write text to the context
			
			EndPaint(hwnd, &ps);//release the device context
			return 0;

		case WM_DESTROY://how to handle a destroy (close window app) msg
			PostQuitMessage(0);
			return 0;
	}
	//return the message to windows for further processing
	return DefWindowProc(hwnd, message, wParam, lParam);
}
TCHAR* createDisplay(int* data, int n){
	TCHAR* result = (TCHAR*)malloc(sizeof(TCHAR)*n*3+100);
	TCHAR* temp = result;
	for (int i=1;i<=n;i++){
		result += wsprintf(result,TEXT("%2d "),data[i-1]);
		if (i%10 == 0) result += wsprintf(result,TEXT("\n"));
	}

	return temp;
}
//THREAD PROC - DO NOT ALTER!
DWORD WINAPI countProc(PVOID pVoid){
	PDATA pdata = (PDATA)pVoid;

	while(!(pdata->done)){
		for (int i = pdata->start; i< pdata->start+10;i++){
			setCount(i);
		for (int i=0;i<99999;i++);
		}
		pdata->done = TRUE;
	}
	return 0;
}
//STORES A VALUE IN GLOBAL STORE
//YOUR CODE GOES IN HERE!!
void setCount(int value){
	
	if(sync)
	{
		EnterCriticalSection(cs);
		if (position <NUM){
			collection[position] = value;
			position++;
		}
		LeaveCriticalSection(cs);
	}else{
		if (position <NUM){
			Sleep(100);
			collection[position] = value;
			position++;
		}
	}
}

int* sort(int a[], int n){
	int temp;
	for(int i=0;i<n;i++){
		for(int j=i+1;j<n;j++){
			if(a[i]>a[j]){
				temp=a[i];
				a[i]=a[j];
				a[j]=temp;
			}
		}
	}
	return a;	
}
