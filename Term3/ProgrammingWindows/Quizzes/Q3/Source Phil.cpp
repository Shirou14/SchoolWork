#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <conio.h>

/*****************************************************************************************************************************************************************
PART 2

Description: The Philosophers problem is an old concurrency problem with a number of solutions. The premise is: there are 5 philosophers sitting at
a dinner table. They are there to think and eat. There is a bowl of rice in the middle of the table for all to share. There are 5 chopsticks to eat
with, one chopstick placed between each pair of philosophers.
Each philosopher needs two chopsticks to eat the rice - the chopstick on his/her left is picked up first if it's available, then once that is
achieved the one on the right is picked up if it's available. Once both are picked up the philosopher eats some rice then puts the chopsticks back on
the table.
At this point the philosopher then does some thinking.
After some heavy thinking the philosopher gets hungry again and the process repeats.

The code below simulates the above. Each chopstick is "protected" by a mutex (in our case the chopstick IS a mutex) as only one philosopher can hold
a chopstick at a time.

The problem is that the current solution, although prevents a race condition, creates a DEADLOCK!

To prevent the deadlock there are a variety of solutions. The solution you are to implement is to allow only 4 philosophers into a room at any one time.
Thus you need to use a synchronization technique that would keep count and only allow 4 into the "room". There is only one such object type!
The solution requires ONLY 2 LINES OF CODE (plus 1 line to create object)! Any more and you are doing it wrong :)
Note that there will be EXACTLY 4 in the room (or at the dinner table if you prefer) at any time.
Once a philosopher has eaten, they should "leave the room", allowing another to enter.
The philosopher would then "loop back up" to re-enter the room when possible.
This repeats until the main thread tells them they are done.

The example deadlock version runs for a bit then several philosophers grab a chopstick and no one eats. The program stops displaying
things (DEADLOCKED) until eventually the PROGRAM FINISHED _ PRESS ANY KEY appears. 

The solution version runs displaying all the time until PROGRAM FINISHED _ PRESS ANY KEY appears - it never deadlocks so no pause in
output until 5 seconds pass to end program.

Marks

4 marks for getting the correct solution. 2 marks for getting the correct lines of code, 2 for putting it together so it works correctly!

*****************************************************************************************************************************************************************/

DWORD WINAPI Thread(PVOID pVoid);
DWORD id, id2;
HANDLE h1,h2;
HANDLE fork[5];
HANDLE room;//THIS IS USED ON THE OBJECT THAT CONTROLS THE ACCESS TO THE ROOM
int pos[5];
HANDLE phils[5];

int getPos(int id){

	for (int p=0; p<5; p++)
		if (id == pos[p])
			return p;
	return -1;

}

int _tmain(int argc, _TCHAR* argv[])
{
	BOOL done = FALSE;
	for (int i=0; i<5; i++){
		fork[i] = CreateMutex(NULL,FALSE,NULL);
		phils[i] = CreateThread(NULL,0,Thread,&done,CREATE_SUSPENDED,(LPDWORD)&pos[i]);
	}
	//YOU NEED TO CREATE THE ROOM OBJECT HERE
	room = CreateSemaphore(NULL, 4, 4, TEXT("DoorKey"));

	for (int i=0; i<5; i++)
		ResumeThread(phils[i]);
	printf("starting\n");
	Sleep(5000);
	done = TRUE;
	printf("FINISHED PROGRAM _ PRESS ANY KEY TO EXIT\n");
	_getch();
	return 0;
}

//PHILOSOPHERS CODE
//NEED TO MAKE ADJUSTMENT (ADD 2 LINES OF CODE) IN THIS PROC
DWORD WINAPI Thread(PVOID pVoid){
	BOOL* done = ((BOOL *)pVoid);
	int x;
	while(!(*done)){
		WaitForSingleObject(room, 0);
			printf("%d is thinking\n",getPos(GetCurrentThreadId()));
			WaitForSingleObject(fork[getPos(GetCurrentThreadId())],INFINITE);
			printf("%d got left fork\n",GetCurrentThreadId());
			WaitForSingleObject(fork[(getPos(GetCurrentThreadId())+1)%5], INFINITE);
			printf("%d is eatting\n",getPos(GetCurrentThreadId()));
			ReleaseMutex(fork[(getPos(GetCurrentThreadId())+1)%5]);
			ReleaseMutex(fork[getPos(GetCurrentThreadId())]);
		ReleaseSemaphore(room, 1, NULL);
	}
	return 0;
}