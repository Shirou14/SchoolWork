#include "Header.h"
#define INFINITY 0

LRESULT CALLBACK RaceProc(HWND, UINT, WPARAM, LPARAM);
//void Race(bool);
void AdvanceButton(HWND, bool);
DWORD WINAPI Thread(LPVOID pVoid);

//int i = 0;
bool race = false;
HWND car1, car2, car3, car4, car5;
DWORD ThreadId;
HANDLE carThread[5], hEvent[2], hRace[2], hSyncRace;;
    
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   PSTR szCMLine, int iCmdShow)
{
    static TCHAR szAppName[] = TEXT("HelloApplication"); //name of app
    HWND hwnd;                                           //holds handle to the main window
    MSG msg;                                             //holds any message retrieved from the msg queue
    WNDCLASS wndclass;                                   //wnd class for registration
    //int w;
    
    HBRUSH hbrush = CreateSolidBrush(RGB(200, 055, 000));

    hEvent[0] = CreateEvent(NULL, true, false, TEXT("bob"));
    hEvent[1] = CreateEvent(NULL, true, false, TEXT("Phil"));
    //hSyncRace
    hRace[0] = CreateEvent(NULL, true, false, TEXT("SyncGo"));
    hRace[1] = CreateEvent(NULL, true, false, TEXT("UnSyncGo"));

    //WaitForSingleObject(hEvent,INFINITE);

    //defn wndclass attributes for this application
    wndclass.style = CS_HREDRAW | CS_VREDRAW;                    //redraw on refresh both directions
    wndclass.lpfnWndProc = RaceProc;                         //wnd proc to handle windows msgs/commands
    wndclass.cbClsExtra = 0;                                     //class space for expansion/info carrying
    wndclass.cbWndExtra = 0;                                     //wnd space for info carrying
    wndclass.hInstance = hInstance;                              //application instance handle
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);            //set icon for window
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);              //set cursor for window
    wndclass.hbrBackground = hbrush; //set background
    wndclass.lpszMenuName = NULL;                                //set menu
    wndclass.lpszClassName = szAppName;                          //set application name

    //register wndclass to O/S so approp. wnd msg are sent to application
    if (!RegisterClass(&wndclass))
    {
        MessageBox(NULL, TEXT("This program requires Windows 95/98/NT"),
                   szAppName, MB_ICONERROR); //if unable to be registered
        return 0;
    }

    //create the main window and get it's handle for future reference
    hwnd = (HWND)CreateWindow(szAppName,                       //window class name
                        TEXT("Hello World for Windows"), // window caption
                        WS_OVERLAPPEDWINDOW,             //window style
                        CW_USEDEFAULT,                   //initial x position
                        CW_USEDEFAULT,                   //initial y position
                        CW_USEDEFAULT,                   //initial x size
                        CW_USEDEFAULT,                   //initial y size
                        NULL,                            //parent window handle
                        NULL,                            //window menu handle
                        hInstance,                       //program instance handle
                        NULL);                           //creation parameters

    car1 = CreateWindow(
        "BUTTON",                                             // Predefined class; Unicode assumed
        "Blue Car",                                          // Button text
        WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, // Styles
        0,                                                     // x position
        300,                                                   // y position
        100,                                                   // Button width
        25,                                                    // Button height
        hwnd,                                                  // Parent window
        NULL,                                                  // No menu.
        GetModuleHandle(NULL),
        NULL); // Pointer not needed.

    car2 = CreateWindow(
        "BUTTON",                                             // Predefined class; Unicode assumed
        "Red Car",                                           // Button text
        WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, // Styles
        0,                                                     // x position
        100,                                                   // y position
        100,                                                   // Button width
        25,                                                    // Button height
        hwnd,                                                  // Parent window
        NULL,                                                  // No menu.
        GetModuleHandle(NULL),
        NULL); // Pointer not needed.

    car3 = CreateWindow(
        "BUTTON",                                             // Predefined class; Unicode assumed
        "Green Car",                                         // Button text
        WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, // Styles
        0,                                                     // x position
        200,                                                   // y position
        100,                                                   // Button width
        25,                                                    // Button height
        hwnd,                                                  // Parent window
        NULL,                                                  // No menu.
        GetModuleHandle(NULL),
        NULL);

    car4 = CreateWindow(
        "BUTTON",                                             // Predefined class; Unicode assumed
        "Orange Car",                                        // Button text
        WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, // Styles
        0,                                                     // x position
        400,                                                   // y position
        100,                                                   // Button width
        25,                                                    // Button height
        hwnd,                                                  // Parent window
        NULL,                                                  // No menu.
        GetModuleHandle(NULL),
        NULL);

    car5 = CreateWindow(
        "BUTTON",                                             // Predefined class; Unicode assumed
        "Black Car",                                        // Button text
        WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, // Styles
        0,                                                     // x position
        0,                                                     // y position
        100,                                                   // Button width
        25,                                                    // Button height
        hwnd,                                                  // Parent window
        NULL,                                                  // No menu.
        GetModuleHandle(NULL),
        NULL);
    
    int result = WaitForSingleObject(hEvent, INFINITE);
    switch(result)
    {
    case WAIT_OBJECT_0 + 0:
        SetWindowText(hwnd, "StartOne");
		break;

	case WAIT_OBJECT_0 + 1:
        SetWindowText(hwnd, "StartTwo");
		break;

	default:
		printf("Wait error: %d\n", GetLastError());
		ExitProcess(0);
    }

    carThread[0] = CreateThread(NULL, 0, Thread, &car1, 0, &ThreadId);
    carThread[1] = CreateThread(NULL, 0, Thread, &car2, 0, &ThreadId);
    carThread[2] = CreateThread(NULL, 0, Thread, &car3, 0, &ThreadId);
    carThread[3] = CreateThread(NULL, 0, Thread, &car4, 0, &ThreadId);
    carThread[4] = CreateThread(NULL, 0, Thread, &car5, 0, &ThreadId);

    ShowWindow(hwnd, iCmdShow); //set window to be shown
    UpdateWindow(hwnd);         //force an update so window is drawn

    //messgae loop
    while (GetMessage(&msg, NULL, 0, 0))
    {                           //get message from queue
        TranslateMessage(&msg); //for keystroke translation
        DispatchMessage(&msg);  //pass msg back to windows for processing
                                //note that this is to put windows o/s in control, rather than this app
    }

    return msg.wParam;
}


LRESULT CALLBACK RaceProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//HDC         hdc;
	PAINTSTRUCT ps;
	RECT        rect;
    //HWND hwndButton;	

	switch (message)
	{
	case WM_CREATE:
    //I guess something should happen here :T
		return 0;
	case WM_PAINT:
        //hdc = BeginPaint(hwnd, &ps);
        GetClientRect(hwnd, &rect);
        //DrawText(hdc, TEXT("Hello world! What is my porpoise?"), -1, &rect, 
      //      DT_SINGLELINE | DT_CENTER | DT_VCENTER);
        EndPaint(hwnd, &ps);
		return 0;
    case WM_LBUTTONUP:
        SetEvent(hRace[0]);
        return 0;
    case WM_RBUTTONUP:
        SetEvent(hRace[1]);
        return 0;
	case WM_DESTROY:
        PostQuitMessage(0);
		return 0;

	}
    return DefWindowProc(hwnd, message, wParam, lParam);
}

/*void Race(bool sync)
{
    DWORD ThreadId;
    HANDLE carThread[5];
    carThread[0] = CreateThread(NULL, 0, Thread, &car1, 0, &ThreadId);
    carThread[1] = CreateThread(NULL, 0, Thread, &car2, 0, &ThreadId);
    carThread[2] = CreateThread(NULL, 0, Thread, &car3, 0, &ThreadId);
    carThread[3] = CreateThread(NULL, 0, Thread, &car4, 0, &ThreadId);
    carThread[4] = CreateThread(NULL, 0, Thread, &car5, 0, &ThreadId);
}*/

DWORD WINAPI Thread(LPVOID pVoid)
{
    int result = WaitForMultipleObjects(2, hRace, false, INFINITY);
    switch(result)
    {
        case WAIT_OBJECT_0://synced version
            AdvanceButton((HWND)*pVoid, true);
            break;
        case WAIT_OBJECT_0+1://unsynced version
            AdvanceButton((HWND)*pVoid, false);
            break;
        default:
            MessageBox(NULL, TEXT("hRace event has suffered an unfortunate fate."),
            szAppName, MB_ICONERROR); //if unable to be registered
            return 0;
    }
}

void AdvanceButton(HWND& hwnd, bool sync)
{
    if(sync)
    {
        HANDLE sem = CreateSemaphore(NULL, 3, 3, NULL);
        RECT prect = (RECT)malloc(sizeof(RECT));
        GetWindowRect(hwnd, prect);
        while(prect->left <= 350)
        {
            int result = WaitForSingleObject(sem, INFINITE);
            switch(result)
            {
                case WAIT_OBJECT_0:
                    MoveWindow(hwnd, prect->left + 10, prect->top, 100, 25, TRUE);
                break;
                case WAIT_TIMEOUT:
                    printf("something went horribly, horribly wrong.");
                return 0;
            }
        }
        MessageBox(hwnd, TEXT("congratulations to \n PURPLE"), TEXT("THE WINNER IS!!"), MB_OK);
    }else{
        while(prect->left <= 350)
        {
            MoveWindow(hwnd, prect->left + 10, prect->top, 100, 25, TRUE);
        }
        MessageBox(hwnd, TEXT("congratulations to \n BLUE"), TEXT("THE WINNER IS!!"), MB_OK);
    }
    return true;
}