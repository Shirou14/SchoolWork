#include "Header.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevhInstance, PSTR szCmdLine, int iCmdShow)
{

    HWND hwnd;//, hwndButton; //Creating a handle to be assigned for our class
    WNDCLASS startClass; //these are the windows class details. 
    MSG msg; //message to pass to the proc
    CONST int WIDTH = 300;
    CONST int HEIGHT = 200;
    TCHAR szAppName[] = TEXT("StartOne");

    //set up the wndClass
    startClass.style = CS_HREDRAW | CS_VREDRAW;
	startClass.lpfnWndProc = RaceProcc; //adds the callback procc to ... this thing
    //extra memory to allocate at the head of the windows. One shared between classes, one between processes?
	//startClass.cbClsExtra = sizeof(PDETAILS); //currently unnecessary
	//startClass.cbWndExtra = sizeof(PDETAILS);
	startClass.hInstance = hInstance;
	startClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);//stock icon
	startClass.hCursor = LoadCursor(NULL, IDC_ARROW);//stock arrow
	startClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);//stock brush
	startClass.lpszMenuName = NULL;//no menu
	startClass.lpszClassName = szAppName; //name of the class/window

    //We must register the class, if it fails to register we must post a warning. 
	if (!RegisterClass(&startClass)) //WNDCLASS
	{
		MessageBox(NULL, TEXT("This program requires Windows NT!"),
			szAppName, MB_ICONERROR);
		return 0;
	}

    //class is registered, now to create the window and apply the handle.
    hwnd = CreateWindow(szAppName,                  // window class name
    szAppName,  // window caption
    WS_OVERLAPPEDWINDOW,        // window style
    CW_USEDEFAULT,              // initial x position
    CW_USEDEFAULT,              // initial y position
    WIDTH,                      // initial x size
    HEIGHT,                     // initial y size
    NULL,                       // parent window handle
    NULL,                       // window menu handle
    hInstance,                  // program instance handle
    NULL);                      // creation parameters*/

//creating a button. Oh boy~!


    ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);
   // ShowWindow(hwndButton, iCmdShow);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

LRESULT CALLBACK RaceProcc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC         hdc;
	PAINTSTRUCT ps;
	RECT        rect;
    HWND hwndButton;	
    HANDLE hEvent[2];
    hEvent[0] = CreateEvent(NULL, true, false, TEXT("bob")); 
    hEvent[2] = CreateEvent(NULL, true, false, TEXT("Ragnarok"));

	switch (message)
	{
	case WM_CREATE:
    //I guess something should happen here :T
        hwndButton = CreateWindow( 
            "BUTTON",  // Predefined class; Unicode assumed 
            "Go!",      // Button text 
            WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles 
            10,         // x position 
            10,         // y position 
            100,        // Button width
            20,        // Button height
            hwnd,     // Parent window
            NULL,       // No menu.
            GetModuleHandle(NULL), 
            NULL);      // Pointer not needed.
		return 0;
	case WM_PAINT:
        hdc = BeginPaint(hwnd, &ps);
        GetClientRect(hwnd, &rect);
        //DrawText(hdc, TEXT("Hello world! What is my porpoise?"), -1, &rect, 
      //      DT_SINGLELINE | DT_CENTER | DT_VCENTER);
        EndPaint(hwnd, &ps);
		return 0;
	case WM_DESTROY:
        PostQuitMessage(0);
		return 0;
    case WM_COMMAND:
        switch(LOWORD(wParam))
        {
        case BN_CLICKED:
            SetEvent(hEvent);
            return 0;
        }
        return 0;
	}
    return DefWindowProc(hwnd, message, wParam, lParam);
}