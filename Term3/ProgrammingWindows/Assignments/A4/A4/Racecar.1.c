#include "Header.h"

LRESULT CALLBACK RaceProc(HWND, UINT, WPARAM, LPARAM);
static int i = 0;
bool race = 0;
HWND car1 = NULL, car2 = NULL, car3 = NULL, car4 = NULL, hwndButton;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   PSTR szCMLine, int iCmdShow)
{
    static TCHAR szAppName[] = TEXT("HelloApplication"); //name of app
    HWND hwnd;                                           //holds handle to the main window
    MSG msg;                                             //holds any message retrieved from the msg queue
    WNDCLASS wndclass;                                   //wnd class for registration
    int w;
    HANDLE hEvent = CreateEvent(NULL, true, false, TEXT("bob"));
    HBRUSH hbrush = CreateSolidBrush(RGB(200, 055, 000));

    //WaitForSingleObject(hEvent,INFINITE);

    //defn wndclass attributes for this application
    wndclass.style = CS_HREDRAW | CS_VREDRAW;                    //redraw on refresh both directions
    wndclass.lpfnWndProc = RaceProc;                         //wnd proc to handle windows msgs/commands
    wndclass.cbClsExtra = 0;                                     //class space for expansion/info carrying
    wndclass.cbWndExtra = 0;                                     //wnd space for info carrying
    wndclass.hInstance = hInstance;                              //application instance handle
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);            //set icon for window
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);              //set cursor for window
    wndclass.hbrBackground = hbrush; //set background
    wndclass.lpszMenuName = NULL;                                //set menu
    wndclass.lpszClassName = szAppName;                          //set application name

    //register wndclass to O/S so approp. wnd msg are sent to application
    if (!RegisterClass(&wndclass))
    {
        MessageBox(NULL, TEXT("This program requires Windows 95/98/NT"),
                   szAppName, MB_ICONERROR); //if unable to be registered
        return 0;
    }

    //create the main window and get it's handle for future reference
    hwnd = CreateWindow(szAppName,                       //window class name
                        TEXT("Hello World for Windows"), // window caption
                        WS_OVERLAPPEDWINDOW,             //window style
                        CW_USEDEFAULT,                   //initial x position
                        CW_USEDEFAULT,                   //initial y position
                        CW_USEDEFAULT,                   //initial x size
                        CW_USEDEFAULT,                   //initial y size
                        NULL,                            //parent window handle
                        NULL,                            //window menu handle
                        hInstance,                       //program instance handle
                        NULL);                           //creation parameters

    car1 = CreateWindow(
        L"BUTTON",                                             // Predefined class; Unicode assumed
        L"BLUE CAR",                                          // Button text
        WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, // Styles
        0,                                                     // x position
        300,                                                   // y position
        100,                                                   // Button width
        25,                                                    // Button height
        hwnd,                                                  // Parent window
        NULL,                                                  // No menu.
        (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
        NULL); // Pointer not needed.

    car2 = CreateWindow(
        L"BUTTON",                                             // Predefined class; Unicode assumed
        L"RED CAR",                                           // Button text
        WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, // Styles
        0,                                                     // x position
        100,                                                   // y position
        100,                                                   // Button width
        25,                                                    // Button height
        hwnd,                                                  // Parent window
        NULL,                                                  // No menu.
        (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
        NULL); // Pointer not needed.

    car3 = CreateWindow(
        L"BUTTON",                                             // Predefined class; Unicode assumed
        L"GREEN CAR",                                         // Button text
        WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, // Styles
        0,                                                     // x position
        200,                                                   // y position
        100,                                                   // Button width
        25,                                                    // Button height
        hwnd,                                                  // Parent window
        NULL,                                                  // No menu.
        (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
        NULL);

    car4 = CreateWindow(
        L"BUTTON",                                             // Predefined class; Unicode assumed
        L"ORANGE CAR",                                        // Button text
        WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, // Styles
        0,                                                     // x position
        400,                                                   // y position
        100,                                                   // Button width
        25,                                                    // Button height
        hwnd,                                                  // Parent window
        NULL,                                                  // No menu.
        (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
        NULL);

    car5 = CreateWindow(
        L"BUTTON",                                             // Predefined class; Unicode assumed
        L"PURPLE CAR",                                        // Button text
        WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, // Styles
        0,                                                     // x position
        0,                                                     // y position
        100,                                                   // Button width
        25,                                                    // Button height
        hwnd,                                                  // Parent window
        NULL,                                                  // No menu.
        (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
        NULL);

    ShowWindow(hwnd, iCmdShow); //set window to be shown
    UpdateWindow(hwnd);         //force an update so window is drawn

    //messgae loop
    while (GetMessage(&msg, NULL, 0, 0))
    {                           //get message from queue
        TranslateMessage(&msg); //for keystroke translation
        DispatchMessage(&msg);  //pass msg back to windows for processing
                                //note that this is to put windows o/s in control, rather than this app
    }

    return msg.wParam;
}


int CALLBACK RaceProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC         hdc;
	PAINTSTRUCT ps;
	RECT        rect;
    HWND hwndButton;	

	switch (message)
	{
	case WM_CREATE:
    //I guess something should happen here :T
        hwndButton = CreateWindow( 
		return 0;
	case WM_PAINT:
        hdc = BeginPaint(hwnd, &ps);
        GetClientRect(hwnd, &rect);
        //DrawText(hdc, TEXT("Hello world! What is my porpoise?"), -1, &rect, 
      //      DT_SINGLELINE | DT_CENTER | DT_VCENTER);
        EndPaint(hwnd, &ps);
		return 0;
	case WM_DESTROY:
        PostQuitMessage(0);
		return 0;

	}
    return DefWindowProc(hwnd, message, wParam, lParam);
}