/*#include "Header.h"

int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE prevhInstance, PSTR szCmdLine, int iCmdShow)
{
	HWND hwnd;
	MSG msg;
	WNDCLASS raceClass;



	return 0;
}

WNDCLASS SetupClass(HINSTANCE hInstance)//, LRESULT CALLBACK procc)//, TCHAR szAppNameAlpha[])
{
	WNDCLASS wndClass;
	//static TCHAR szAppNameAlpha[] = TEXT("HelloWin");

	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.lpfnWndProc = WndProcAlpha;
	wndClass.cbClsExtra = sizeof(PDETAILS);
	wndClass.cbWndExtra = sizeof(PDETAILS);
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppNameAlpha;

	return wndClass;
}

LRESULT CALLBACK RaceProcc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	HDC         hdc;
	PAINTSTRUCT ps;
	RECT        rect;
	int			count;
	

	switch (message)
	{
	case WM_CREATE:
		return 0;
	case WM_PAINT:
		return 0;
	case WM_DESTROY:
		return 0;

	}
}*/

#include "Header.h"

//HWND CreateButton(char *);

LRESULT CALLBACK CarProcc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevhInstance, PSTR szCmdLine, int iCmdShow)
{

	HWND hwnd, car1;//, car2, car3, car4, car5, car6, car7; //Creating a handle to be assigned for our class
	WNDCLASS startClass; //these are the windows class details. 
	MSG msg; //message to pass to the proc
	CONST int WIDTH = 700;
	CONST int HEIGHT = 500;
	int horipos = 10;
	TCHAR szAppName[] = TEXT("Doggo");
	//HBRUSH hbrush = CreateSolidBrush(RGB(200, 055, 000));

	//set up the wndClass
	startClass.style = CS_HREDRAW | CS_VREDRAW;
	startClass.lpfnWndProc = CarProcc; //adds the callback procc to ... this thing
									   //extra memory to allocate at the head of the windows. One shared between classes, one between processes?
									   //raceClass.cbClsExtra = sizeof(PDETAILS); //currently unnecessary
									   //raceClass.cbWndExtra = sizeof(PDETAILS);
	startClass.hInstance = hInstance;
	startClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);//stock icon
	startClass.hCursor = LoadCursor(NULL, IDC_ARROW);//stock arrow
	startClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);//hbrush;//stock brush
	startClass.lpszMenuName = NULL;//no menu
	startClass.lpszClassName = szAppName; //name of the class/window

										  //We must register the class, if it fails to register we must post a warning. 
	if (!RegisterClass(&startClass)) //WNDCLASS
	{
		MessageBox(NULL, TEXT("This program requires Windows NT!"),
			szAppName, MB_ICONERROR);
		return 0;
	}

	//class is registered, now to create the window and apply the handle.
	hwnd = CreateWindow(szAppName,                  // window class name
		szAppName,  // window caption
		WS_OVERLAPPEDWINDOW,        // window style
		CW_USEDEFAULT,              // initial x position
		CW_USEDEFAULT,              // initial y position
		WIDTH,                      // initial x size
		HEIGHT,                     // initial y size
		NULL,                       // parent window handle
		NULL,                       // window menu handle
		hInstance,                  // program instance handle
		NULL);                      // creation parameters*/

									//creating a button. Oh boy~!
	car1 = CreateWindow(
		L"BUTTON",  // Predefined class; Unicode assumed 
		L"Go!",      // Button text 
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles 
		10,         // x position 
		10,         // y position 
		100,        // Button width
		20,        // Button height
		hwnd,     // Parent window
		NULL,       // No menu.
		(HINSTANCE)GetWindowLong(hwnd, -6),
		NULL);
	// Pointer not needed.
	//horipos += 10;     
	/*car2 = CreateButton("RedCar");
	car3 = CreateButton("GreenCar");
	car4 = CreateButton("OrangeCar");
	car5 = CreateButton("YellowCar");
	car6 = CreateButton("BlackCar");
	car7 = CreateButton("PurpleCar");*/

	ShowWindow(hwnd, iCmdShow);
	ShowWindow(car1, iCmdShow);
	UpdateWindow(hwnd);

	/*ShowWindow(car2, iCmdShow);
	ShowWindow(car3, iCmdShow);
	ShowWindow(car4, iCmdShow);
	ShowWindow(car5, iCmdShow);
	ShowWindow(car6, iCmdShow);
	ShowWindow(car7, iCmdShow);*/


	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

LRESULT CALLBACK CarProcc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//HDC         hdc;
	PAINTSTRUCT ps;
	RECT        rect;

	switch (message)
	{
	case WM_CREATE:
		//I guess something should happen here :T

		return 0;
	case WM_PAINT:
		//hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rect);
		//DrawText(hdc, TEXT("Hello world! What is my porpoise?"), -1, &rect, 
		//      DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		EndPaint(hwnd, &ps);
		return 0;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	}
	return DefWindowProc(hwnd, message, wParam, lParam);
}

/*void CreateButton(char * title, HWND& hwnd)
{
static int horipos = 10;
//HWND hwnd;
hwnd = CreateWindow(
"BUTTON",  // Predefined class; Unicode assumed
title,      // Button text
WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles
10,         // x position
horipos,         // y position
100,        // Button width
20,        // Button height
hwnd,     // Parent window
NULL,       // No menu.
(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
NULL);      // Pointer not needed.
horipos += 10;

//return hwnd;
}

/*
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevhInstance, PSTR szCmdLine, int iCmdShow)
{

HWND hwnd, car1; //Creating a handle to be assigned for our class
WNDCLASS startClass; //these are the windows class details.
MSG msg; //message to pass to the proc
CONST int WIDTH = 300;
CONST int HEIGHT = 200;
TCHAR szAppName[] = TEXT("StartOne");
HBRUSH hbrush = CreateSolidBrush(RGB(200, 055, 000));

//set up the wndClass
startClass.style = CS_HREDRAW | CS_VREDRAW;
startClass.lpfnWndProc = RaceProcc; //adds the callback procc to ... this thing
//extra memory to allocate at the head of the windows. One shared between classes, one between processes?
//startClass.cbClsExtra = sizeof(PDETAILS); //currently unnecessary
//startClass.cbWndExtra = sizeof(PDETAILS);
startClass.hInstance = hInstance;
startClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);//stock icon
startClass.hCursor = LoadCursor(NULL, IDC_ARROW);//stock arrow
startClass.hbrBackground = hbrush;//stock brush
startClass.lpszMenuName = NULL;//no menu
startClass.lpszClassName = szAppName; //name of the class/window

//We must register the class, if it fails to register we must post a warning.
if (!RegisterClass(&startClass)) //WNDCLASS
{
MessageBox(NULL, TEXT("This program requires Windows NT!"),
szAppName, MB_ICONERROR);
return 0;
}

//class is registered, now to create the window and apply the handle.
hwnd = CreateWindow(szAppName,                  // window class name
szAppName,  // window caption
WS_OVERLAPPEDWINDOW,        // window style
CW_USEDEFAULT,              // initial x position
CW_USEDEFAULT,              // initial y position
WIDTH,                      // initial x size
HEIGHT,                     // initial y size
NULL,                       // parent window handle
NULL,                       // window menu handle
hInstance,                  // program instance handle
NULL);                      // creation parameters

//creating a button. Oh boy~!
car1 = CreateWindow(
"BUTTON",  // Predefined class; Unicode assumed
"Go!",      // Button text
WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles
10,         // x position
10,         // y position
100,        // Button width
20,        // Button height
hwnd,     // Parent window
NULL,       // No menu.
(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
NULL);      // Pointer not needed.

ShowWindow(hwnd, iCmdShow);
UpdateWindow(hwnd);
ShowWindow(car1, iCmdShow);

while (GetMessage(&msg, NULL, 0, 0))
{
TranslateMessage(&msg);
DispatchMessage(&msg);
}
return msg.wParam;
}

LRESULT CALLBACK RaceProcc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
HDC         hdc;
PAINTSTRUCT ps;
RECT        rect;

switch (message)
{
case WM_CREATE:
//I guess something should happen here :T
return 0;
case WM_PAINT:
hdc = BeginPaint(hwnd, &ps);
GetClientRect(hwnd, &rect);
//DrawText(hdc, TEXT("Hello world! What is my porpoise?"), -1, &rect,
//      DT_SINGLELINE | DT_CENTER | DT_VCENTER);
EndPaint(hwnd, &ps);
return 0;
case WM_DESTROY:
PostQuitMessage(0);
return 0;

}
return DefWindowProc(hwnd, message, wParam, lParam);
}*/