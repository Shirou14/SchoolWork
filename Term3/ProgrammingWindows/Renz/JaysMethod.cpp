void assembly_brighten(BITMAP* bitmap, INT brighten, BYTE* buffer){
	INT width = bitmap->bmWidth;
	INT height = bitmap->bmHeight;
	INT bitsPerPixel = bitmap->bmBitsPixel;
	//REGISTERS

	//EAX, EBX, ECX, EDX are general purpose registers
	//ESI, EDI, EBP are also available as general purpose registers
	//AX, BX, CX, DX are the lower 16-bit of the above registers (think E as extended)
	//AH, AL, BH, BL, CH, CL, DH, DL are 8-bit high/low registers of the above (AX, BX, etc)
	//Typical use:
	//EAX accumulator for operands and results
	//EBX base pointer to data in the data segment
	//ECX counter for loops
	//EDX data pointer and I/O pointer
	//EBP frame pointer for stack frames
	//ESP stack pointer hardcoded into PUSH/POP operations
	//ESI source index for array operations
	//EDI destination index for array operations [e.g. copying arrays]
	//EIP instruction pointer
	//EFLAGS results flag hardcoded into conditional operations

	//SOME INSTRUCTIONS

	//MOV <destination>, <source>: mov reg, reg; mov reg, immediate; mov reg, memory; mov mem, reg; mov mem, imm
	//INC and DEC on registers or memory
	//ADD destination, source
	//SUB destination, source
	//CMP destination, source : sets the appropriate flag after performing (destination) - (source)
	//JMP label - jumps unconditionally ie. always to location marked by "label"
	//JE - jump if equal, JG/JL - jump if greater/less, JGE/JLE if greater or equal/less or equal, JNE - not equal, JZ - zero flag set
	//LOOP target: uses ECX to decrement and jump while ECX>0
	//logical instructions: AND, OR, XOR, NOT - performs bitwise logical operations. Note TEST is non-destructive AND instruction
	//SHL destination, count : shift left, SHR destination, count :shift right - carry flag (CF) and zero (Z) bits used, CL register often used if shift known
	//ROL - rotate left, ROR rotate right, RCL (rotate thru carry left), RCR (rotate thru carry right)
	//EQU - used to elimate hardcoding to create constants
	//MUL destination, source : multiplication
	//PUSH <source> - pushes source onto the stack
	//POP <destination> - pops off the stack into destination
	__asm
	{
		// save all registers you will be using onto stack
		push eax
		push ebx
		push ecx
		push edx
		push esi
	
		// calculate the number of pixels
		mov eax, width
		mul height 

		xor ebx, ebx //clears ebx

		mov ebx, bitsPerPixel
		shr ebx, 3
		// calculate the number of bytes in image (pixels*bitsperpixel)
		mul ebx


		// store the address of the buffer into a register (e.g. ebx)
		mov ebx, buffer
		//setup counter register
		xor ecx, ecx

		//create a loop
		//loop while still have pixels to brighten
		//jump out of loop if done
		start: cmp eax, ecx
			   jl end 

		//load a pixel into a register
		mov edx, [ebx]
		xor edi, edi
		xor ebp, ebp
		mov ebp, 4
		//need to work with each colour plane: A R G B

		//load same pixel then into 3 more registers
		mov esi, edx
		and esi, 0xff
		add esi, 30
		cmp esi, 255
		jle skip1
			mov esi, 255
		skip1: or edi, esi


		mov esi, edx
		shr esi, 8
		and esi, 0xff
		add esi, 10
		cmp esi, 255
		jle skip2
			mov esi, 255
		skip2: shl esi, 8
		or edi, esi

		mov esi, edx
		shr esi, 16
		and esi, 0xff
		add esi, 10
		cmp esi, 255
		jle skip3
			mov esi, 255
		skip3: shl esi, 16
		or edi, esi

		mov esi, edx
		shr esi, 24
		and esi, 0xff
		add esi, 10
		cmp esi, 255
		jle skip4
			mov esi, 255
		skip4: shl esi, 24
		or edi, esi
		//shift bits down for each channel
		//clear out other bits
		/*and esi, ff00000000
		and edi, 00ff000000
		and ebp, 0000ff0000
		//add brighten value to each pixel
		add esi, 10
		add edi, 10
		add ebp, 10*/
		//check each pixel to see if saturated
		//saturate:
		//if greater than 255, set to 255
		//	mov esi, 255
		//put pixel back together again
		//shift each channel amount needed
		mov [ebx], edi
		//add each channel
		//store back into buffer
	
		//increment loop counter by 4
		add ecx, 4
		cmp ecx, 409508
			jne notequal
			or ecx, ecx
			notequal:

		add ebx, 4
		//loop back up
		jmp start 

		end:
		//restore registers to original values before leaving
		//function

		pop esi
		pop edx
		pop ecx
		pop ebx
		pop eax
	}

}