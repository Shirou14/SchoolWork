#include "SubHeader.h"

LRESULT CALLBACK WndAlpha(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC         hdc;
	PAINTSTRUCT ps;
	RECT        rect;
	TCHAR		classNum[128];// = TEXT("This is class #");


	switch (message)
	{
	case WM_CREATE:
		PlaySound(TEXT("hellowin.wav"), NULL, SND_FILENAME | SND_ASYNC);
		return 0;

	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);

		GetClientRect(hwnd, &rect);

		DrawText(hdc, TEXT("Hello, Windows 98!"), -1, &rect,
			DT_SINGLELINE | DT_CENTER | DT_VCENTER);

		//classNum = "This class is class number";
		wsprintf(classNum, TEXT("This class is class number %d"), (int)GetClassLongPtr(hwnd, 0));

		DrawText(hdc, classNum, -1, &rect, DT_SINGLELINE | DT_BOTTOM);



		EndPaint(hwnd, &ps);
		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(hwnd, message, wParam, lParam);
}