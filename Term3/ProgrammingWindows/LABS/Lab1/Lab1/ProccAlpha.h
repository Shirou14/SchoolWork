#pragma once
#ifndef Header_h
#define Header_h
#include "Header.h"
#endif
 // !Header_h

static CONST TCHAR szAppNameAlpha[] = TEXT("HelloWinAlpha");

LRESULT CALLBACK WndProcAlpha(HWND, UINT, WPARAM, LPARAM);

void SetupDetails(PDETAILS deets)
{
	deets->xPos = 0;
	deets->yPos = 0;
	deets->winNum = 1;
	deets->color = RGB(255, 0, 0);
	deets->msg = (TCHAR*)malloc(sizeof(TCHAR) * 25);
	lstrcpyW(deets->msg, TEXT("Wizardry!"));
	// return newHead;
}

WNDCLASS SetupAlpha(HINSTANCE hInstance)//, LRESULT CALLBACK procc)//, TCHAR szAppNameAlpha[])
{
	WNDCLASS wndClass;
	//static TCHAR szAppNameAlpha[] = TEXT("HelloWin");

	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.lpfnWndProc = WndProcAlpha;
	wndClass.cbClsExtra = sizeof(PDETAILS);
	wndClass.cbWndExtra = sizeof(PDETAILS);
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppNameAlpha;

	return wndClass;
}

LRESULT CALLBACK WndProcAlpha(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC         hdc;
	PAINTSTRUCT ps;
	RECT        rect;
	int			count;
	TCHAR		classNum[128];// = TEXT("This is class #");
//	TCHAR		mesg[128];
	PDETAILS		pdetails;// = (PDETAILS)malloc(sizeof(DETAILS));// = (details)GetClassLongPtr(hwnd, 0
	PDETAILS		pWinDetails;// = (PDETAILS)malloc(sizeof(DETAILS));
	COLORREF	txtColor;

	switch (message)
	{
	case WM_CREATE:
		PlaySound(TEXT("hellowin.wav"), NULL, SND_FILENAME | SND_ASYNC);
		return 0;

	case WM_PAINT:
		//pdetails = (details)GetClassLongPtr(hwnd, 0);
		//pdetails = (PDETAILS)malloc(sizeof(DETAILS));
		pdetails = (PDETAILS)GetClassLongPtr(hwnd, 0);
		pWinDetails = (PDETAILS)GetWindowLongPtr(hwnd, 0);
		hdc = BeginPaint(hwnd, &ps);

		txtColor = pWinDetails->color;
		GetClientRect(hwnd, &rect);

		SetTextColor(hdc, txtColor);
//		wsprintf(mesg, TEXT("%s"), (TCHAR)pdetails->msg);
		DrawText(hdc, pWinDetails->msg, -1, &rect,
			DT_SINGLELINE | DT_CENTER | DT_VCENTER);

		count = (int)GetClassLongPtr(hwnd, 0);
		
		//pdetails = (details)GetClassLongPtr(hwnd, 0);
		//pdetails->winNum = count;
		//SetClassLongPtr(hwnd, pdetails->winNum, count);

		//classNum = "This class is class number";
		wsprintf(classNum, TEXT("This class is class number %d"), (int)GetClassLongPtr(hwnd, 0));
		count++;
		SetClassLongPtr(hwnd, 0, count);
		DrawText(hdc, classNum, -1, &rect, DT_SINGLELINE | DT_BOTTOM);

		EndPaint(hwnd, &ps);
		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(hwnd, message, wParam, lParam);
}