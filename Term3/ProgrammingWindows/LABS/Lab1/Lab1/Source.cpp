/*------------------------------------------------------------
HELLOWIN.C -- Displays "Hello, Windows 98!" in client area
(c) Charles Petzold, 1998
------------------------------------------------------------*/

#include "ProccAlpha.h"
#include "ProccBeta.h"

//LRESULT CALLBACK WndProcAlpha(HWND, UINT, WPARAM, LPARAM);
//WNDCLASS SetupAlpha(HWND, HINSTANCE, LRESULT, TCHAR);
//void SetupDetails(head*);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	PSTR szCmdLine, int iCmdShow)
{
	//static TCHAR szAppNameAlpha[] = TEXT("HelloWin");
	HWND         hwnd;
	HWND         hwnd2;
	HWND		 hwnd3;
	HWND		 hwnd4;
	MSG          msg;
	WNDCLASS     wndclassAlpha;
	WNDCLASS	 wndclassBeta;
	CONST int WIDTH = 300;
	CONST int HEIGHT = 200;

	PDETAILS		pdetails;// = (details)GetClassLongPtr(hwnd, 0);


	if (!(pdetails = (PDETAILS)malloc(sizeof(DETAILS))))
	{
		MessageBox(NULL, TEXT("MALLOC failure!"),
			szAppNameAlpha, MB_ICONERROR);
		return 0;
	}
	SetupDetails(pdetails);

	wndclassAlpha = SetupAlpha(hInstance);

	if (!RegisterClass(&wndclassAlpha))
	{
		MessageBox(NULL, TEXT("This program requires Windows NT!"),
			szAppNameAlpha, MB_ICONERROR);
		return 0;
	}
	wndclassBeta = SetupBeta(hInstance);

	if (!RegisterClass(&wndclassBeta))
	{
		MessageBox(NULL, TEXT("This program requires Windows NT!"),
			szAppNameBeta, MB_ICONERROR);
		return 0;
	}


	hwnd = CreateWindow(szAppNameAlpha,                  // window class name
		TEXT("The Hello Program"), // window caption
		WS_OVERLAPPEDWINDOW,        // window style
		CW_USEDEFAULT,              // initial x position
		CW_USEDEFAULT,              // initial y position
		WIDTH,              // initial x size
		HEIGHT,              // initial y size
		NULL,                       // parent window handle
		NULL,                       // window menu handle
		hInstance,                  // program instance handle
		NULL);                     // creation parameters*/

	SetClassLongPtr(hwnd, 0, pdetails->winNum);// (LONG_PTR)&details);
	pdetails = (PDETAILS)malloc(sizeof(DETAILS));
	SetupDetails(pdetails);
	pdetails->color = COLORREF RGB(0, 255, 0);
	lstrcpyW(pdetails->msg, TEXT("WIZARDRY!"));
	SetWindowLongPtr(hwnd, 0, (LONG)pdetails);

	SetWindowPos(hwnd, HWND_TOP, pdetails->xPos, pdetails->yPos, -1, -1, SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED); 
	pdetails->xPos = WIDTH + pdetails->xPos;
	//pdetails->winNum = 1 + pdetails->winNum;
	//SetClassLongPtr(hwnd, 0, pdetails->winNum);

	hwnd2 = CreateWindow(szAppNameAlpha,                  // window class name
		TEXT("The Hello Program"), // window caption
		WS_OVERLAPPEDWINDOW,        // window style
		CW_USEDEFAULT,              // initial x position
		CW_USEDEFAULT,               // initial y position
		WIDTH,              // initial x size
		HEIGHT,              // initial y size
		NULL,                       // parent window handle
		NULL,                       // window menu handle
		hInstance,                  // program instance handle
		NULL);    //CreateAlphaWin(hInstance);

	SetWindowPos(hwnd2, HWND_TOP, pdetails->xPos, pdetails->yPos, -1, -1, SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED); 
	pdetails->xPos = WIDTH + pdetails->xPos;
	SetClassLongPtr(hwnd2, 0, pdetails->winNum);// (LONG_PTR)&details);
	pdetails = (PDETAILS)malloc(sizeof(DETAILS));
	pdetails->color = COLORREF RGB(255, 0, 100);
	lstrcpyW(pdetails->msg, TEXT("SORCERY!!"));
	SetWindowLongPtr(hwnd2, 0, (LONG)pdetails);
	/*
	
		SETTING UP BETA HERE
	
	*/

	SetupDetails(pdetails);

	hwnd3 = CreateWindow(szAppNameBeta,                  // window class name
		TEXT("The Hello Program"), // window caption
		WS_OVERLAPPEDWINDOW,        // window style
		CW_USEDEFAULT,              // initial x position
		CW_USEDEFAULT,              // initial y position
		WIDTH,              // initial x size
		HEIGHT,              // initial y size
		NULL,                       // parent window handle
		NULL,                       // window menu handle
		hInstance,                  // program instance handle
		NULL);                     // creation parameters*/

	SetClassLongPtr(hwnd3, 0, pdetails->winNum);// (LONG_PTR)&details);
	pdetails->yPos = HEIGHT;

	SetWindowPos(hwnd3, HWND_TOP, pdetails->xPos, pdetails->yPos, -1, -1, SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);
	pdetails->xPos = WIDTH + pdetails->xPos;
	pdetails->winNum = 1 + pdetails->winNum;
	SetClassLongPtr(hwnd3, 0, pdetails->winNum);

	hwnd4 = CreateWindow(szAppNameBeta,                  // window class name
		TEXT("The Hello Program"), // window caption
		WS_OVERLAPPEDWINDOW,        // window style
		CW_USEDEFAULT,              // initial x position
		CW_USEDEFAULT,               // initial y position
		WIDTH,              // initial x size
		HEIGHT,              // initial y size
		NULL,                       // parent window handle
		NULL,                       // window menu handle
		hInstance,                  // program instance handle
		NULL);    //CreateAlphaWin(hInstance);

	SetWindowPos(hwnd4, HWND_TOP, pdetails->xPos, pdetails->yPos, -1, -1, SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);
	//pdetails->xPos = WIDTH + pdetails->xPos;

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	ShowWindow(hwnd2, iCmdShow);
	UpdateWindow(hwnd2);

	ShowWindow(hwnd3, iCmdShow);
	UpdateWindow(hwnd3);

	ShowWindow(hwnd4, iCmdShow);
	UpdateWindow(hwnd4);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

