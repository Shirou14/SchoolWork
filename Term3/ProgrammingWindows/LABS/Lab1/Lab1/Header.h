#pragma once
#include <windows.h>
#include <string.h>
#include <stdio.h>


typedef struct
{
	int winNum;
	int xPos;
	int yPos;
	COLORREF color;
	TCHAR* msg;
}DETAILS;

typedef DETAILS *PDETAILS;
