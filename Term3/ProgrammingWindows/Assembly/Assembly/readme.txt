Left click is Assembly Brighten
Middle Click is regular C brighten
Right Click is mmx Brighten

I could not figure out how to print the benchmarked values to the screen, as it's 1 am and I am very tired. The code that benchmarks them is still there if you want to debug it (I doubt it), but the values I got were 281ms for C, 28ms for assembly, and 2ms for mmx, which is an odd coincidence, but whatever works I suppose.