package Lab3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;;

public class BruteForce 
{
	static Scanner scan;
	
	public static void main(String[] args)
	{
		List<String> text = ReadFile("src/dict.txt");
		BruteForceOne(text);
		BruteForceTwo(text);
		BruteForceThree(text);
	}
	
	static List<String> ReadFile(String path)
	{
		List<String> s = new ArrayList<String>();
		try {
			scan = new Scanner(new File(path));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.err.println("CANNOT READ FILE FROM " + path);
			e.printStackTrace();
		}

		while(scan.hasNext())
		{
			s.add(scan.next());
		}
		return s;
	}
	
	public static void BruteForceOne(List<String> txt)
	{
		System.out.println("We are beginning Method one of Lab 3; Elimination Check");
		int tmp = 0, max = 0;
		String maxWord = "";
		long systemStart = System.currentTimeMillis(); 
		
		for(int iii = 0; iii < txt.size(); iii++)
		{
			for (int jjj = 0; jjj < txt.size(); jjj++)
			{
				if(ElimCheck(txt.get(iii), txt.get(jjj)))
					tmp++;
			}
			if (tmp > max)
			{
				max = tmp;
				maxWord = txt.get(iii);
			}
			tmp = 0;
		}
		
		System.out.println("The word " + maxWord + " has the largest amount of anagrams clocking in at "+ max + " total anagrams.");
		System.out.println("This function took " + (System.currentTimeMillis() - systemStart) + " Milliseconds to complete");
	}
	
	public static void BruteForceTwo(List<String> txt)
	{
		System.out.println("\n\nWe are beginning Method two of Lab 3; Sorted Check");
		int tmp = 0, max = 0;
		String maxWord = "";
		long systemStart = System.currentTimeMillis(); 
		
		for(int iii = 0; iii < txt.size(); iii++)
		{
			for (int jjj = 0; jjj < txt.size(); jjj++)
			{
				if(SortedCheck(txt.get(iii), txt.get(jjj)))
					tmp++;
			}
			if (tmp > max)
			{
				max = tmp;
				maxWord = txt.get(iii);
			}
			tmp = 0;
		}
		
		System.out.println("The word " + maxWord + " has the largest amount of anagrams clocking in at "+ max + " total anagrams.");
		System.out.println("This function took " + (System.currentTimeMillis() - systemStart) + " Milliseconds to complete");
	}
	
	public static void BruteForceThree(List<String> txt)
	{
		System.out.println("\n\nWe are beginning Method three of Lab 3; Vector Check");
		int tmp = 0, max = 0;
		String maxWord = "";
		long systemStart = System.currentTimeMillis(); 
		
		for(int iii = 0; iii < txt.size(); iii++)
		{
			for (int jjj = 0; jjj < txt.size(); jjj++)
			{
				if(VectorCheck(txt.get(iii), txt.get(jjj)))
					tmp++;
			}
			if (tmp > max)
			{
				max = tmp;
				maxWord = txt.get(iii);
			}
			tmp = 0;
		}
		
		System.out.println("The word " + maxWord + " has the largest amount of anagrams clocking in at "+ max + " total anagrams.");
		System.out.println("This function took " + (System.currentTimeMillis() - systemStart) + " Milliseconds to complete");
	}
	
	public static boolean ElimCheck(String w1, String w2)
	{
		StringBuilder c1 = new StringBuilder(w1);
		StringBuilder c2 = new StringBuilder(w2);
		int idx;
		
		if(w1.length() != w2.length())
			return false;
		
		for(int lll = 0; lll < c1.length(); lll++)
		{
			if(c2 != null)
			{
				idx = c2.indexOf(c1.substring(lll, lll+1));
				//System.out.println("Word: " + c1 + "; Substring: " + c1.substring(lll, lll+1) + "; lll = " + lll);
				if(idx != -1)
				{
					c2.deleteCharAt(idx);
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		return true;
	}
	
	public static boolean SortedCheck(String w1, String w2)
	{
		char[] c1 = w1.toCharArray();
		char[] c2 = w2.toCharArray();
		
		if(c1.length != c2.length)
			return false;
		
		Arrays.sort(c1);
		Arrays.sort(c2);
		
		for(int nnn = 0; nnn < c1.length; nnn++)
		{
			if(c1[nnn] != c2[nnn])
				return false;
		}
		
		return true;
	}
	
	//V is 128 for method 3
	public static boolean VectorCheck(String w1, String w2)
	{
		int[] v = new int[128];
		char[]c1 = w1.toCharArray();
		char[] c2 = w2.toCharArray();
		
		if(c1.length != c2.length)
			return false;
		
		for(int ooo = 0; ooo < c1.length; ooo++)
		{
			v[(int)c1[ooo]] ++;
			v[(int)c2[ooo]] --;
		}
		
		for(int ppp = 0; ppp < v.length; ppp ++)
		{
			if(v[ppp] != 0)
				return false;
		}
		
		return true;
	}
}
