package lab8;

import java.util.Arrays;
import java.util.PriorityQueue;

public class AdjGraph {

    private int V;
    public boolean directed = false;
    private int[][] matrix;
    
    public AdjGraph(int V) {
        this.V = V;
        matrix = new int[V][V];
    }
    
    public boolean isDirected() {
        return directed;
    }
    
    public void addEdge(int u, int v){
        matrix[u][v] = 1;
        if (!directed) {
            matrix[v][u] = 1;
        }
    }
    
    public int degree(int t) {
        return (directed) ? inDegree(t) + outDegree(t) : Arrays.stream(matrix[t]).sum();
    }
    
    public int inDegree(int t) {
        int rtn = 0;
        for (int[] col : matrix) {
            rtn += col[t];
        }
        return rtn;
    }
    
    public int outDegree(int t) {
        return Arrays.stream(matrix[t]).sum();
    }
    
    public void DFS() {
        boolean[] visited = new boolean[V];
        for (boolean val : visited) {
            val = false;
        }
        for (int i = 0; i < V; ++i) {
            if (!visited[i]) {
                dfs(i, visited);
            }
        }
    }
    
    private void dfs(int v, boolean[] visited) {
        visited[v] = true;
        System.out.println("Visiting vertex: " + v);
        for (int i = 0; i < V; ++i) {
            if (i == v) {
                continue;
            }
            if (matrix[v][i] == 1 && !visited[i]) {
                dfs(i, visited);
            }
        }
    }
    
    public void BFS() {
        boolean[] visited = new boolean[V];
        for (boolean val : visited) {
            val = false;
        }
        for (int i = 0; i < V; ++i) {
            if (!visited[i]) {
                bfs(i, visited);
            }
        }
    }
    
    public void bfs(int v, boolean[] visited) {
        visited[v] = true;
        System.out.println("Visiting vertex: " + v);
        PriorityQueue<Integer> qu = new PriorityQueue<>();
        qu.add(v);
        int currHead = v;
        while (!qu.isEmpty()) {
            currHead = qu.peek();
            for (int i = 0; i < V; ++i) {
                if (i == currHead) {
                    continue;
                }
                if (matrix[currHead][i] == 1) {
                    if (!visited[i]) {
                        visited[i] = true;
                        System.out.println("Visiting vertex: " + i);
                        qu.add(i);
                    }
                }
            }
            qu.poll();
        }
    }
    
    @Override
    public String toString() {
        StringBuilder rtn = new StringBuilder();
        for (int[] col : matrix) {
            for (int val : col) {
                rtn.append(val);
                rtn.append(' ');
            }
            rtn.append('\n');
        }
        return rtn.toString();
    }
}
