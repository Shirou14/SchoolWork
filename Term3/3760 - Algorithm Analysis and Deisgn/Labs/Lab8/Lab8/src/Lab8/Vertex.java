package Lab8;

import java.util.Arrays;

public class Vertex 
{
	Edge[] friends;
	int x, y;
	
	
	Vertex(int xPos, int yPos)
	{
		x = xPos;
		y = yPos;
	}
	
	Vertex(int xPos, int yPos, Edge e)
	{
		x = xPos;
		y = yPos;
		
		friends = new Edge[1];
		friends[0] = e;
	}
	
	void AddEdge(Edge e)
	{
		Edge[] temp = Arrays.copyOf(friends, friends.length);
		friends = new Edge[friends.length];
		System.arraycopy(temp, 0, friends, 0, temp.length - 1);
		friends[friends.length - 1] = e;
	}
}
