package Lab8;

public class Edge 
{
	private Vertex nodeA, nodeB;
	Edge(Vertex start, Vertex end)
	{
		nodeA = start;
		nodeB = end;
	}
	
	Vertex GetNodeA()
	{
		return nodeA;
	}
	
	Vertex GetNodeB()
	{
		return nodeB;
	}
	
	void SetNewPath(Vertex start, Vertex end)
	{
		nodeA = start;
		nodeB = end;
	}
}
