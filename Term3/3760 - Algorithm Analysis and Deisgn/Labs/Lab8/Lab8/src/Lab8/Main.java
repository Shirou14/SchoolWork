package Lab8;

public class Main 
{
	static AdjGraph graph = new AdjGraph(8);
	
	public static void main(String[] args)
	{
		graph.AddEdge(0, 1);
		graph.AddEdge(0, 2);
		graph.AddEdge(0, 4);
		graph.AddEdge(1, 3);
		graph.AddEdge(1, 5);
		graph.AddEdge(2, 3);
		graph.AddEdge(2, 6);
		graph.AddEdge(3, 7);
		graph.AddEdge(4, 5);
		graph.AddEdge(4, 6);
		graph.AddEdge(5, 7);
		graph.AddEdge(6, 7);
		
		System.out.println(graph.Degree(4));
		
		System.out.println(graph.ToString());
		
		graph.DFS();
		System.out.println();
		graph.BFS();
	}
}
