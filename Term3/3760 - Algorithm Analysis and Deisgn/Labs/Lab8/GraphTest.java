package lab8;

public class GraphTest {

    public static void main(String[] args) {
        AdjGraph firstGraph = new AdjGraph(5);
        AdjGraph secondGraph = new AdjGraph(4);
        AdjGraph dfsGraph = new AdjGraph(8);
        
        firstGraph.addEdge(0, 1);
        firstGraph.addEdge(0, 3);
        firstGraph.addEdge(0, 4);
        firstGraph.addEdge(1, 2);
        firstGraph.addEdge(1, 4);
        firstGraph.addEdge(2, 3);
        firstGraph.addEdge(3, 4);
        
        secondGraph.addEdge(0, 1);
        secondGraph.addEdge(1, 2);
        secondGraph.addEdge(2, 3);
        
        System.out.println("First graph: \n" + firstGraph.toString());
        System.out.println("Second graph: \n" + secondGraph.toString());
        
        System.out.printf("Degree for first graph example with vertex 0: %d\n", firstGraph.degree(0));
        System.out.println();
        
        AdjGraph dir = new AdjGraph(5);
        dir.directed = true;
        
        dir.addEdge(0, 1);
        dir.addEdge(0, 4);
        dir.addEdge(1, 2);
        dir.addEdge(1, 4);
        dir.addEdge(2, 3);
        dir.addEdge(3, 0);
        dir.addEdge(3, 4);
        
        System.out.println("Directed graph is Directed: " + dir.directed);
        System.out.printf("Directed example degrees for vertex 0:\nIn: %d\nOut: %d\nTotal: %d\n", 
                dir.inDegree(0), dir.outDegree(0), dir.degree(0));
        
        dfsGraph.addEdge(0, 1);
        dfsGraph.addEdge(0, 2);
        dfsGraph.addEdge(0, 4);
        dfsGraph.addEdge(1, 3);
        dfsGraph.addEdge(1, 5);
        dfsGraph.addEdge(2, 3);
        dfsGraph.addEdge(2, 6);
        dfsGraph.addEdge(3, 7);
        dfsGraph.addEdge(4, 5);
        dfsGraph.addEdge(4, 6);
        dfsGraph.addEdge(5, 7);
        dfsGraph.addEdge(6, 7);
        
        System.out.println();
        
        System.out.println("DFS + BFS search graph: \n" + dfsGraph.toString());
        System.out.println("DFS: ");
        dfsGraph.DFS();
        System.out.println();
        System.out.println("BFS: ");
        dfsGraph.BFS();
    }

}
