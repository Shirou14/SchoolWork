import java.util.Comparator;
import java.util.PriorityQueue; 

public class Lab6 {
	public static void main(String args[]){
		
		int a[] = { 2, 24, 33, 65, 999, 2, 545, 380, 32, 2454, 665, 5444};
		
		int k = 3; 
		
		PriorityQueue test = TreeSorter(a, k);
		int size = test.size(); 
		
		System.out.print("The length of the array is: ");
		for ( int iii = 0; iii < a.length; iii++)
			System.out.print(a[iii] + " ");
		
		System.out.println("");
		System.out.print("The " + k + " smallest numbers are: " );
		for ( int jjj = 0; jjj < size; jjj++)
			System.out.print(test.poll() + " ");
		
	}


	public static PriorityQueue<Integer> TreeSorter(int a[], int k){
		
		PriorityQueue<Integer> heap = new PriorityQueue<Integer>(k, new cmp()); 
		
		for ( int lll = 0; lll < a.length ; lll++){
			if ( heap.size() < k )
				heap.offer(a[lll]);
			else {
				if ( heap.peek() > a[lll] ){
					heap.remove();
					heap.offer(a[lll]);
				}
			}
		}
		
		return heap;
		
	}
	
	static class cmp implements Comparator<Integer>{
		
		public int compare(Integer o1, Integer o2) {
			// TODO Auto-generated method stub
			return o2 - o1;
		}
	}
}