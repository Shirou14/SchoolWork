package Main;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;;

public class Main {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = null;
		ArrayList<String> test = new ArrayList<String>();
		ArrayList<String> check = new ArrayList<String>();
		
		try {
			scan = new Scanner(new FileReader("Main/lab4_wordlist.txt"));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//System.out.println("");

		while(scan.hasNextLine())
		{
			check.add(scan.nextLine());
		}
		scan.close();
		try {
			scan = new Scanner(new FileReader("Main/lab4_testdata.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		while(scan.hasNext())
		{
			test.add(scan.next());
		}
		scan.close();
		
		seqSearch(check, test);
		binSearch(check, test);
	}
	
	public static void seqSearch(ArrayList<String> check, ArrayList<String> test)
	{
		System.out.println("\nWe are now beginning the brute for check method");
		int found = 0;
		long systemStart = System.currentTimeMillis(); 
		
		for(int i = 0; i < test.size(); i++)
		{
			for(int j = 0; j < check.size(); j++)
			{
				if(check.get(j).equalsIgnoreCase(test.get(i)))
				{
					found++;
					break;
				}
			}
		}
		
		System.out.println("We have found " + (test.size() - found) + " words that do not match.");
		System.out.println("This method took " + (System.currentTimeMillis() - systemStart) + " milliseconds to complete");
	}

	public static void binSearch(ArrayList<String> check, ArrayList<String> test)
	{
		System.out.println("\n\nWe are now beginning the binary for check method");
		int found = 0;
		long systemStart = System.currentTimeMillis(); 
		int mid = check.size()/2;
		
		for(int i = 0; i < test.size(); i++)
		{
			if(srch(test.get(i), check))
				found++;
		}
		
		System.out.println("We have found " + (test.size() - found) + " words that do not match.");
		System.out.println("This method took " + (System.currentTimeMillis() - systemStart) + " milliseconds to complete");
	
	}
	
	public static boolean srch(String a, List<String> b)
	{
		int mid = b.size()/2;
		boolean found = false;

		if(b.size()>1)
		{
			if(a.compareToIgnoreCase(b.get(mid)) != 0)
			{
				if(a.compareToIgnoreCase(b.get(mid)) < 0)
				{
					found = srch(a, b.subList(0, mid));
				}
				else if(a.compareToIgnoreCase(b.get(mid)) > 0)
				{
					found = srch(a, b.subList(mid, b.size() - 1));
				}
				//found = true;
			}else{
				found = true;
			}
		}else if(b.size() == 1){
			if(a.equalsIgnoreCase(b.get(0)))
				found = true;
			else
				found = false;
		}else{
			found = false;
		}
		return found;
	}
}
