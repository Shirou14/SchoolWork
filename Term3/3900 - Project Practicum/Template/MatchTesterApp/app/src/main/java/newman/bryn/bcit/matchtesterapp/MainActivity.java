package newman.bryn.bcit.matchtesterapp;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity{
    private static final String  TAG              = "OCVSample::Activity";
    private Bitmap bitmap;
    private MatchTester tester;
    private FileOutputStream stream;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this){
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    // Create and set View
                    //mView = new puzzle15View(mAppContext);
                    setContentView(R.layout.activity_main);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }//

    protected void onStart() {
        tester = new MatchTester();
        try {
            stream = openFileOutput("Testfile.jpg", Context.MODE_WORLD_READABLE);
            File hound = new File("Android.Resource://app/drawable/houndstooth.jpg");
            int sz = (int) hound.length();
            byte[] b = new byte[sz];
            stream.write(b);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // bitmap = getBitmapFromAssets("C:\\Users\\Brandon\\AndroidStudioProjects\\MatchTesterApp\\app\\src\\main\\res\\drawable\\houndstooth.jpg");
        if (OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6, this, mLoaderCallback)) {
            //Log.e(TAG, "Cannot connect to OpenCV Manager");

            tester.run(MainActivity.this.getFilesDir().getAbsolutePath() + "/Testfile.jpg",
                    "Android.Resource://app/drawable/croppedhoundstooth.jpg",
                    "E:\\~Users\\Projects\\School\\SchoolWork\\Term3\\3900 - Project Practicum\\Template\\MatchTesterApp\\app\\src\\main\\res\\drawable\\teste.jpg", Imgproc.TM_CCOEFF);
        }
    }

    public Bitmap getBitmapFromAssets(String filename){
        AssetManager assetManager = getAssets();
        InputStream istr = null;
        try {
            istr = assetManager.open(filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(istr);
        return bitmap;

    }
}
