The clients tool of choice was Nantworks, which is an library containing an API connecting to Nantworks server that hosts its image recognition software. 
At first glance it looked like it would achive most things on our scope.

The first issue we've had with the library is that it would not work on emulators. 
Although minor it did mean we've only tested it using our devices which work off of Android 6.0.1 and 7.0.0. 
Nantworks is listed as compatible with android 4.1 and above, but we've had no way of testing whether our tests would have worked on that version.

There was no problem adding the library to an empty project and the initial setup.

Our first big wall was trying to instantiate the recognition engine. Our tests showed the app would authenticate with Nantwork's server using 
the client ID/Secret but once it created an instance of the recognition engine it would crash. 
We would run into our favorite error " Your app has stopped working " < maybe picture of app crashing > 

In trying to get the recognition engine to work we've tried initializing it using both a remote image and a local image. Also tried different using different image formats both .jpeg and .png
" Your app has stopped working "

Nantworks image recognition works by comparing an image against a collection of images uploaded on their server called entities. 
Uploading an entity is done through their website.
Attempting to retrieve on antity also proved to not work. < " Your app has stopped working " >

The last two things that led us to look into alternatives were
Once we've integrated a working camera app with a project that had the library the whole app would crash. This was likely from the library needing permissions to use the camera, and another part of the application using the camera at the same time. We did not spend anymore time further investigating.

Lastly looking further into nantworks we found out that it would have trouble with repeating patterns. This led us to finally abandon it. 

