//Variation of review1.cpp (private member is now in B class)
//Albert's review exercise, front side

#include <iostream>

using namespace std;

class X {
public:
	X() { cerr << "X()|"; }
	X(const X&) { cerr << "X(const X&)|"; }
	~X() { cerr << "~X()|"; }

	X& operator=(const X&) { cerr << "X::op=|"; return *this; }
};


class B {
public:
	B() { cerr << "B()|"; }
	B(const B&) { cerr << "B(const B&)|"; }
	virtual ~B() { cerr << "~B()|";}

	B& operator=(const B& b) { 
        cerr << "B::op=|";
		x_ = b.x_;
        return *this ;
    }
private:
	X x_;
};

class D : public B {
public:
	D() { cerr << "D()|"; }
	D(const D& d): B(d) { cerr << "D(const D&)|"; }
	virtual ~D() { cerr << "~D()|"; }
		
	D& operator =(const D& d) {
		cerr << "D::op=|";
		B::operator=(d);
		return *this;
	}
};

int main(){
	
	//1
	cout << "Q1: " << endl;
	B b[2];
	cout<<endl<<endl;
	
	//2
	cout << "Q2: " << endl;
	B b1(b[0]);
	cout<<endl<<endl;
	
	//3
	cout << "Q3: " << endl;
	B *pb = new D;
	cout<<endl<<endl;
	
	//4
	cout << "Q4: " << endl;
	B b2 = *pb;
	cout<<endl<<endl;
	
	//5
	cout << "Q5: " << endl;
	*pb = b[0];
	cout<<endl<<endl;
	
	//6
	cout << "Q6 :" << endl;
	delete pb;
	cout<<endl<<endl;
	
	
	//7
	cout << "Q7 :" << endl;
	D d1;
	cout<<endl<<endl;
	
	
	//8
	cout << "Q8: " << endl;
	D a[3];
	cout<<endl<<endl;
	
	//9
	cout << "Q9: " << endl;
	D d2(a[0]);
	cout<<endl<<endl;
	
	//10
	cout << "Q10: " << endl;
	D d3 = d1;
	cout<<endl<<endl;
	
	//11
	cout << "Q11: " << endl;
	D *pd = new D;
	cout<<endl<<endl;
	
	//12
	cout<< "Q12: " << endl;
	D *p = pd;
	cout<<endl<<endl;
	
	//13
	cout<< "Q13: " << endl;
	d1 = a[0];
	cout<<endl<<endl;
	
	//14
	cout<< "q14: " << endl;
	delete p;
	cout<<endl<<endl;
	
	//15 EXTRASSS
	cout<< "Q15 :" << endl;
	B *pbb = new B;
	cout<<endl<<endl;
	
	//16 EXTRASSS
	cout<< "Q16 :" << endl;
	delete pbb;
	cout<<endl<<endl;
	
	
	
	
	cout<<"exiting..."<<endl;
	
	
}
