//Albert's review exercise, back side

#include <iostream>

using namespace std;

class A{
public:
	void f1(){ cerr << "A::f1()" << endl; }
	virtual void f2(){ cerr << "A::f2()" << endl; }
	virtual void f3(){ cerr << "A::f3()" << endl; }
	virtual void f4(){ cerr << "A::f4()" << endl; }
	virtual void f5(int n=1){ cerr << "A::f5(" << n << ")" << endl; }
};


class B: public A{
public:
	virtual void f1(){ cerr << "B::f1()" << endl; }
	virtual void f2(){ cerr << "B::f2()" << endl; }
	//no f3();
	virtual void f4(int n){ cerr << "B::f4(" << n << ")" << endl; }
	virtual void f5(int n=2){ cerr << "B::f5(" << n << ")" << endl; }
};

class C: public B {
public:
	virtual void f1() { cerr << "C::f1()" << endl; }
	virtual void f2() { cerr << "C::f2()" << endl; }
	virtual void f3() { cerr << "C::f3()" << endl; }
	virtual void f4(int n=1) { cerr << "C::f4(" << n << ")" << endl; }
	virtual void f5() { cerr << "C::f5()" << endl; }
	virtual void f5(int n) { cerr << "C::f5(" << n << ")" << endl; }
};

int main(){
	
	A a; B b; C c;
	
	A& rab = b;
	A& rac = c;
	B& rbc = c;
	
	//1
	cout << "Q1: " << endl;
	rab.f1(); rac.f1(); rab.f1();
	cout<<endl<<endl;
	
	//2
	cout << "Q2: " << endl;
	rab.f2(); rac.f2(); rbc.f2();
	cout<<endl<<endl;
	
	//3
	cout << "Q3: " << endl;
	rab.f3(); rac.f3(); rbc.f3();
	cout<<endl<<endl;
	
	//4
	cout << "Q4: " << endl;
	rab.f4(); rac.f4(); // rbc.f4(); // doesn't compile
	cout<<endl<<endl;
	
	//5
	cout << "Q5: " << endl;
	rab.f5(); rac.f5(); rbc.f5();
	cout<<endl<<endl;
	
	
	
}
