#include <iostream>
using namespace std;

class X{
    public:
    X() {cerr<<"X()|";}
    X(const X&){cerr<<"X(const X&)|";}
    ~X() {cerr<<"~X()|";}
    X& operator=(const X&) {cerr<<"X::op=|"; return *this;}
};

class B{
    public:
    B(){cerr<<"B()|";}
    B(const B&) {cerr << "B(const B&)|";} 
    virtual ~B() {cerr<<"~B()|";}
    B& operator=(const B&) {cerr <<"B::op=|";}
};

class D: public B{
    public:
    D(){cerr<<"D()|";}
    D(const D& d): B(d), x_(d.x_){cerr<<"D(const D&)|";}
    virtual ~D(){cerr<<"~D()|";}
    D& operator=(const D& d)
    {
        cerr << "D::op=|";
        B::operator=(d);
        x_ = d.x_;
        return *this;
    }
    private:
    X x_;
};

int main()
{
    /*B b[2]; //B()|B()|~B()|~B()|
    B b1(b[0]); //B(const B&)|~B()|
    B *pb = new D;
    B b2 = *pb;
    delete pb;*/
    D d;
    D a[3];
    D d2(a[0]);
    D d3 = d;
    D *pd = new D;
    D *p = pd;
    d = a[0];
    delete p;
}