//Albert's example on the behaviour of virtual, given during lecture

#include <iostream>

using namespace std;

class A{
public:
	void f(){ cerr << "A::f()" << endl; }
	virtual void g(){ cerr << "A::g()" << endl; }
	void h(){ cerr << "A::h()" << endl; }
	// no i();
	virtual void j(){ cerr << "A::j()" << endl; }
	virtual void k() { cerr << "A::k()" << endl; }
	virtual void l() { cerr << "A::l()" << endl; }
	virtual void m(int n=1){ cerr << "A::m(" << n << ")" << endl; }
	virtual void n() { cerr << "A::n()" << endl; }
};


class B: public A{
public:
	virtual void f(){ cerr << "B::f()" << endl; }
	virtual void g(){ cerr << "B::g()" << endl; }
	//no h();
	void i(){ cerr << "B::i()" << endl; }
	//no j();		
	//void k(){ cerr << "B::k()" << endl; }
	virtual void l(int) { cerr << "B::l()"  << endl; }				
	virtual void m(int n=2){ cerr << "B::m(" << n << ")" << endl; }
	
private:
	virtual void n(){ cerr << "B::n()" << endl; }
};

class C: public B {
public:
	// no f();
	virtual void g() { cerr << "C::g()" << endl; }
	void h() { cerr << "C::h()" << endl; }
	//no i();
	virtual void j() { cerr << "C::j()" << endl; }
	void k() { cerr << "C::k()" << endl;}
	//no l();
	//no m();
	//no n();
};

class D: public C{
	void k() { cerr << "D::k()" << endl;}
};

int main(){
	
	A a; B b; C c; D d;
	
	A& rab = b;
	A& rac = c;
	B& rbc = c;
	C& rcd = d;
	
	//1
	cout << "Q1: " << endl;
	rab.f(); // compiles, early binding - static type determines binding, calls A::f();
	cout<<endl<<endl;
	
	//2
	cout << "Q2A: " << endl;
	rab.j(); //compiles, late binding, calls A::j();
	cout<<endl<<endl;
	
	//3
	cout << "Q3: " << endl;
	rac.j(); // compiles, late binding, calls C:: j();
	cout<<endl<<endl;
	
	//4
	cout << "Q4: " << endl;
	rbc.k(); // compiles, late binding - virtual is inherited, calls C::k();
	cout<<endl<<endl;
	
	//5
	cout << "Q5: " << endl;
	//rbc.l(); // doesn't compile
	cout<<endl<<endl;
	
	//6
	cout << "Q6: " << endl;
	rab.m(); // compiles, late binding, rewritten to m(1), calls B::m(1)  
	//default values and access are determined at compile time
	cout<<endl<<endl;
	
	//7
	cout<< "Q7: " << endl;
	rab.n(); // compiles, calls B::n()
	//access is determined at compile time and determined by static type
	cout<<endl<<endl;
	
	//8
	cout<< "Q8" << endl;
	rcd.k(); //complies, late binding - inherited, calls D::k();
	cout <<endl<<endl;
	
}
