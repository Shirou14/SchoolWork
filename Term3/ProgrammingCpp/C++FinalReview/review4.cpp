//Slight variation of review1.cpp (different calls in main)
//Legend of class names: M = member, P = parent, C = child

#include <iostream>

using namespace std;

class M {
public:
	M() {  cerr << "M()|"; }
	M(const M&) { cerr << "M(const M&)|"; }
	~M() {cerr << "~M()|"; }
	
	M& operator=(const M&) { cerr << "M::op=|"; return *this; }
};


class P {
public:
	P() { cerr << "P()|"; }
	P(const P&) { cerr << "P(const P&)|"; }
	virtual ~P() { cerr << "~P()|";}
	
	P& operator=(const P&) { cerr << "P::op=|"; return *this; }
};

class C : public P {
public:
	C() { cerr << "C()|"; }
	C(const C& c): P(c), m_(c.m_) { cerr << "C(const C&)|"; }
	virtual ~C() { cerr << "~C()|"; }
		
	C& operator =(const C& c) {
		cerr << "C::op=|";
		P::operator=(c);
		m_ = c.m_;
		return *this;
	}
private:
	M m_;
};

int main(){
	
	//1
	cout << "Q1: " << endl;
	P *p1 = new P; // new returns a pointer (the pointer calls the base constructor)
	cout<<endl<<endl;
	
	//2
	cout << "Q2: " << endl;
	P *p2 = p1;
	cout<<endl<<endl;
	
	//3
	cout << "Q3: " << endl;
	P p3 = *p2;
	cout<<endl<<endl;

	//4
	cout << "Q4: " << endl;
	P a[4];
	cout<<endl<<endl;
	
	//5
	cout << "Q5: " << endl;
	P p4 = a[0];
	cout<<endl<<endl;
	
	//6
	cout << "Q6: " << endl;
	P *pP = new C;
	cout<<endl<<endl;
	
	//7
	cout << "Q7: " << endl;
	*pP = a[2];
	cout<<endl<<endl;
	
	//8
	cout << "Q8: " << endl;
	delete pP;
	cout<<endl<<endl;
	
	//9
	cout << "Q9: " << endl;
	C c1;
	cout<<endl<<endl;
	
	
	//10
	cout << "Q10: " << endl;
	C b[3];
	cout<<endl<<endl;
	
	//11
	cout << "Q11: " << endl;
	C c2(b[0]);
	cout<<endl<<endl;
	
	//12
	cout << "Q12: " << endl;
	C c3 = c1;
	cout<<endl<<endl;
	
	//13
	cout << "Q13: " << endl;
	C *pC = new C;
	cout<<endl<<endl;
	
	//14
	cout<< "Q14: " << endl;
	C *p = pC;
	cout<<endl<<endl;
	
	//15
	cout<< "Q15: " << endl;
	c1 = b[0];
	cout<<endl<<endl;
	
	//16
	cout<< "q16: " << endl;
	delete p;
	cout<<endl<<endl;
	
	//17
	cout<< "Q17: " << endl;
	P *ppP = new P;
	cout<<endl<<endl;
	
	//18
	cout<< "Q18: " << endl;
	delete ppP;
	cout<<endl<<endl;
	
	
	
	
	cout<<"exiting..."<<endl;
	
	
}
