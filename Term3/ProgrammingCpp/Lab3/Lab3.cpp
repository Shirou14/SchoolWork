#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

void InputRecord(string);
void OutputRecord(string);
bool input_int(const string&, int&, bool(*is_valid)(const int&));
bool input_float(const string&, int&, bool(*is_valid)(const float&));
bool is_valid(const float& entry);
bool is_valid(const int& entry);
//void set_precision(int);

int main(int argc, char *argv[])
{
    //Check to make sure the user has appropriately input a filename
    if(argc != 2)
    {
        cerr<<"No Filename detected. Please input a filename to be read."<<endl;
        cout<<"Usage: "<<argv[0]<<" <Filename>"<<endl;
        return 0;
    }
    string line;
    int opt;
    cout<<"Please enter an option, or the end-of-file character to exit"<<endl<<"1. Input Record"<<endl<<"2. Display Records"<<endl;

    while(cin>>opt)
    {
        switch(opt)
        {
            case 1:
            InputRecord(argv[1]);
            break;

            case 2:
            cout<<"Which record would you like to read?"<<endl;
            OutputRecord(argv[1]);
            break;

            default:
            cout<<"That is not a valid option, please enter 1, 2, or the eof character"<<endl;
        }
        //ifs.clear();
        cin.clear();

        cout<<"\nPlease enter an option, or the end-of-file character to exit\n1. Input Record\n2. Display Records"<<endl;
    }
    cout<<"\nThank you for using our program, goodbye"<<endl;
    return 0;
}

void InputRecord(string s)
{
    int id;
    float score;
    cout<<"Please input a student ID followed by a score"<<endl;
    fstream out(s, ios_base::out | ios_base::binary | ios_base::trunc);
    while(cin>>id>>score)
    {
        if(is_valid(id) && is_valid(score))
        {
            cout<<"The record has been accepted"<<endl;
            
            out<<setfill('0')<<setw(8)<<id<<" "<<fixed<<setprecision(2)<<score<<" ";
            cin.clear();
        }

        cout<<"Please enter another record, or press the end of file character to return to the menu"<<endl;
    }
}

void OutputRecord(string s)
{
    cout<<"How many records would you like to see?"<<endl;
    int numb, fsize, start, end;
    float score;
    string id;

    fstream out(s, ios_base::in | ios_base::binary);
    out.seekp(0, out.end);
    fsize = out.tellg();

    while(cin>>numb)
    {
        if(numb > 0)
        {
            //we want to show that -particular- record
            start = ((8+5+2)*(numb-1));
            end = ((8+5+2)*numb);
            //cout<<"seek = "<<start<<" fsize="<<fsize<<endl;
            if(end <= fsize)
            {
                //getline(out, id);
                out.seekp(start);
                out>>id>>score;
                cout<<"A"<<setfill('0')<<setw(8)<<id<<" Grade:"<<fixed<<setprecision(2)<<score<<endl;
                
            }else{
                cout<<"You have chosen an invalid record. please try again."<<endl;
            }

        }else if(numb < 0)
        {
            //show that -many- records
            //cout<<"Displaying records"<<endl;
            numb = (-1)*numb;
            end = ((8+5+2)*numb);
            //cout<<"seek = "<<end<<" fsize="<<fsize<<endl;
            if(end <= fsize)
            {
                //getline(out, id);
                out.seekp(0);
                for(int iii = 0; iii < numb; iii++)
                {
                    out>>id>>score;
                    cout<<"A"<<setfill('0')<<setw(8)<<id<<" Grade:"<<fixed<<setprecision(2)<<score<<endl;
                }
                
            }else{
                cout<<"You have chosen an invalid number of records. please try again."<<endl;
            }

        }else{
            cout<<"You have opted to look at zero records. Interesting choice."<<endl;
        }
        cout<<"Enter another number, or end of file to exit"<<endl;
        cin.clear();
    }
}

//verify the validity
bool is_valid(const int& m)
{
    if(m >= 10000 && m <= 99999999)
        return true;
    else{
        cout<<"The student ID was invalid"<<endl;
        return false;
    }
        
        
}

bool is_valid(const float& m)
{
    if(m <= 100.00 && m >= 0.00)
        return true;
    else
        return false;
}