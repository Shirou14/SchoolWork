//MinSecView

#include "MinSecView.h"
#include <iomanip>
#include <iostream>

using namespace std;

void MinSecView::display(ostream& os) const 
{
    
	os << (timer_->get()/60) <<":"<<setw(2) << setfill('0')<< (timer_->get()%60) << endl;
}