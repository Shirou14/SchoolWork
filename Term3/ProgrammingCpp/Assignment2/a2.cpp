#include "keyboardcontroller.h"
#include "timer.h"
#include "minsecview.h"
#include "tickview.h"
#include "secview.h"
int main()
{
    Timer tmr;
    KeyboardController* kbc = new KeyboardController();
    MinSecView msv(&tmr);
    TickView tv(&tmr);
    SecView sv(&tmr);
    
    kbc->subscribe(&tmr);
    tmr.subscribe(&msv);
    tmr.subscribe(&tv);
    tmr.subscribe(&sv);
    kbc->start();
}