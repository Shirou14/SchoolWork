#include "KeyboardController.h"

void KeyboardController::start()
{
    std::string input;
    auto first = input.begin();

    std::cout<<"Please input a command"<<std::endl;
    while(std::cin>>input)
    {
        first = input.begin();
        command_ = tolower(*first);
        if(command_ == 'r' || command_ == 's' || command_ == 'h')
        {
            notify();
        }else if(command_ == 'x')
        {
            return;
        }else
        {
             std::cout<<"Command not recognized. Please input a proper command. (s, h, r, x)"<<std::endl;
        }
    }
}

char KeyboardController::getCommand() const
{
    return command_;
}