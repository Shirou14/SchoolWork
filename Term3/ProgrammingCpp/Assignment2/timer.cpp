#include "timer.h"
#include "keyboardcontroller.h"

Timer::Timer():sec_(0), ticking_(false), Observer(), Subject()
{
    sec_ = 0;
    std::thread th_(&Timer::run, this);
    th_.detach();
}

void Timer::start()
{
    ticking_ = true;
}

void Timer::stop()
{
    ticking_ = false;
}

void Timer::reset()
{
    sec_ = 0;
}

unsigned long Timer::get() const
{
    return sec_;
}

void Timer::run()
{
    while(1)
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        if(ticking_)
        {
            ++sec_;
            notify();
        }
    }
}

void Timer::update(Subject *s)
{
    if(KeyboardController *keyboard = dynamic_cast<KeyboardController *>(s)){
        switch(keyboard->getCommand())
        {
            case 's':
                start();
                break;
            case 'h':
                stop();
                break;
            case 'r':
                reset();
                break;
        }
    }
}