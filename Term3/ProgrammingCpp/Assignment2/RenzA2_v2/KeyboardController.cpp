#include <iostream>
#include <sstream>
#include "KeyboardController.h"
#include "Timer.h"

using namespace std;

void KeyboardController::start() {
	while(1) {
		string line;
		if(!getline(cin, line)) {
			cin.clear();
			break;
		}
		istringstream iss(line);
		
		
		string command;
		
		//Gets and sets commands from user and notifies subject
		if (iss >> command) {
			if (command[0] == 's' || command[0] == 'h' || command[0] == 'r') {
				setCommand(command[0]);
				//cout << "Notified observers" << endl;
				notify();
			} else if (command[0] == 'x') {
				break;
			} else {
				cout << "Enter a valid command" << endl;
			}
		}
	}
}