#include <iostream>
#include "SecView.h"

using namespace std;

//Gets the seconds and displays it
void SecView::display(ostream& os) const {
	os << timer_->get() << endl;
}
