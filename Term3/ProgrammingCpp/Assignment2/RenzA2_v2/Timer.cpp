#include <iostream>
#include <thread>
#include <chrono>
#include "KeyboardController.h"
#include "Timer.h"

using namespace std;

/*
Constructor for the Timer
Initializes seconds to 0 and ticking to false
and runs the thread
*/
Timer::Timer()
: sec_(0), ticking_(false) {
	std::thread t(&Timer::run, this);
	t.detach();	
}

//Sets ticking to true
void Timer::start() {
	ticking_ = true;
}

//Sets ticking to false
void Timer::stop() {
	ticking_ = false;
}

//Resets timer(seconds) to 0
void Timer::reset() {
	sec_ = 0;
}

//Gets the seconds of Timer
unsigned long Timer::get() const {
	return sec_;
}

//Updates timer according to command
void Timer::update(Subject *s) {
	KeyboardController* kb = (KeyboardController*) s;
	char command = kb->getCommand();
	
	if (command == 's') {
		start();
	} else if (command == 'h') {
		stop();
	} else if (command == 'r') {
		reset();
	}
}

/*
Method used for the thread.
Notifies all of the observers.
*/
void Timer::run() {
	while(1) {
		std::this_thread::sleep_for(std::chrono::seconds(1));
		if (ticking_) {
			++sec_;
			cout << "Notified observers" << endl;
			notify(); //notify to update
		}
	}
}