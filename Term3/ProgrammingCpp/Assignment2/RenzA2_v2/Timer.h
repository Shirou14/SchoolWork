#ifndef TIMER_H
#define TIMER_H
#include <iostream>
#include <string>
#include <atomic>
#include "Subject.h"
#include "Observer.h"


class Timer : public Subject, public Observer {
public:
	Timer();						//ctor
	
	void start();					//start the timer
	void stop();					//stop the timer
	void reset();					//reset timer to 0
	unsigned long get() const;		//returns number of seconds elapsed
	
	Timer(const Timer&) = delete;				// (*)
	Timer& operator=(const Timer&) = delete;	// (*)
	
	virtual void update(Subject *s);
private:
	std::atomic<unsigned long> 	sec_;		//number of seconds elapsed
	std::atomic<bool>			ticking_;	//is timer ticking or not?
	void run();
};

#endif
