#include <iostream>
#include <iomanip>
#include "MinSecView.h"

using namespace std;

//Displays time in '-:--' format
void MinSecView::display(ostream& os) const {
	unsigned long min = timer_->get() / 60;
	unsigned long sec = timer_->get() - min * 60;

	os << min << ":" << setfill('0') << setw(2) << sec << endl;
}