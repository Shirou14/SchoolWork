#ifndef KEYBOARDCONTROLLER_H
#define KEYBOARDCONTROLLER_H
#include <atomic>
#include "Subject.h"

class KeyboardController : public Subject {
public:
	KeyboardController() : Subject() {}
	
	void start();					//start the loop to get user commands
	char getCommand() const {		//return the "command"
		return command_;
	}
	void setCommand(char command) {	//sets the "command"
		command_ = command;
	}
private:
	std::atomic<char> command_;		//store the "command" the user enters
};

#endif