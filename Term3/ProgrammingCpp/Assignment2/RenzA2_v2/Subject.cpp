#include <iostream>
#include <sstream>
#include <algorithm>
#include <string>
#include "Subject.h"
#include "Observer.h"

using namespace std;

//Subscribes observers
void Subject::subscribe(Observer *obs) {
	observers_.push_back(obs);
}

//Unsubscribes observers
void Subject::unsubscribe(Observer *obs) {
	observers_.erase(remove(observers_.begin(), observers_.end(), obs)
					, observers_.end());
}

//Updates all observers
void Subject::notify() {
	for (auto x : observers_) {
		cout << "Updated" << endl;
		x->update(this);
	}
}