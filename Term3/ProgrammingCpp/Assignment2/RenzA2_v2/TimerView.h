#ifndef TIMERVIEW_H
#define TIMERVIEW_H
#include <iostream>
#include <sstream>
#include "Timer.h"
#include "Subject.h"
#include "Observer.h"

class TimerView : public Observer {
public:
	TimerView(Timer* timer) : timer_(timer){
		std::cout << "Subscribed" << std::endl;
		timer_->subscribe(this);
	}
	
	~TimerView() {
		std::cout << "Unsubscribed" << std::endl;
		timer_->unsubscribe(this);
	}
	
	virtual void update(Subject *s) {
		if (s == timer_) {
			display(std::cout);
		}
	}
	
	virtual void display(std::ostream& os) const = 0;
protected:
	Timer *timer_;
};

#endif