#include "KeyboardController.h"
#include "SecView.h"
#include "MinSecView.h"
#include "TickView.h"

using namespace std;

int main() {
	Timer* t = new Timer();
	
	KeyboardController* kb = new KeyboardController();
	
	kb->subscribe(t);		//subscribe the timer to the keyboard controller
	
	TickView tv(t);			//create TickView
	MinSecView msv(t);		//create MinSecView
	SecView sv(t);			//create SecView
	
	kb->start();			//controller start
	
	kb->unsubscribe(t);
	
	cout << "End of program" << endl;
}