#include "subject.h"
#include "observers.h"
#include <iostream>
#include <algorithm>

using namespace std;

void Subject::subscribe(Observer *obs)
{
    observers_.push_back(obs);
}

void Subject::unsubscribe(Observer *obs)
{
    observers_.erase(remove(observers_.begin(), observers_.end(), obs), observers_.end());
}

void Subject::notify()
{
	for (auto x : observers_) 
	{
		x->update(this);
	}
}