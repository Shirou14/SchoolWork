//Timer class
#ifndef TIMER_H
#define TIMER_H
#include <atomic>
#include <thread>
#include <chrono>
#include "observers.h"
#include "subject.h"

class Timer:public Subject, public Observer 
{
    public:
        Timer(); //ctor
        void start(); //start the timer
        void stop(); //stop the timer
        void reset(); //reset the timer
        unsigned long get() const;

        //These constructors are to stop copy ctors
        Timer(const Timer&) = delete;
        Timer& operator=(const Timer&) = delete;
        virtual void update(Subject *) override;

        //inherited methods not shown
        private:
            std::atomic<unsigned long> sec_; //number of seconds elapsed
            std::atomic<bool> ticking_;
            void run();
};

#endif