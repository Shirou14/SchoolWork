//SecView.h
#ifndef SECVIEW_H
#define SECVIEW_H
#include "TimerView.h"
#include <iostream>

class SecView: public TimerView
 {
    public:
        SecView(Timer *timer): TimerView(timer) {}
        virtual void display(std::ostream& os) const;
};

#endif