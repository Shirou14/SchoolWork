//MinSecView.h
#ifndef MINSECVIEW_H
#define MINSECVIEW_H
#include "TimerView.h"
#include <iostream>

class MinSecView: public TimerView {
public:
	MinSecView(Timer *timer): TimerView(timer) {}
	virtual void display(std::ostream& os) const;
};
#endif