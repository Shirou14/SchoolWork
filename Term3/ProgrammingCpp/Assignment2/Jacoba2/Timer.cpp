#include <chrono>
#include <iostream>
#include "Timer.h"
#include "KeyboardController.h"
using namespace std;


//Main constructor
Timer::Timer (unsigned long sec, bool ticking): Observer(), Subject(), sec_(sec), ticking_(ticking) {
	thread timerThread(&Timer::run, this);
	timerThread.detach();
}

// Timer::Timer (): Observer(), Subject(), sec_(0), ticking_(false) {
// 	thread timerThread(&Timer::run, this);
// 	timerThread.detach();
// }

// Timer::Timer(const Timer& timer): Observer(), Subject(), sec_(timer.sec_), ticking_(timer.ticking_) {
// 	thread timerThread(&Timer::run, this);
// 	timerThread.detach();
// }

void Timer::start() {
	ticking_ = true;
}

void Timer::stop() {
	ticking_ = false;
}

void Timer::reset() {
	sec_ = 0;
}

unsigned long Timer::get() const {
	return sec_;
}

void Timer::run() {
	while(1) {
		if(ticking_) {
			std::this_thread::sleep_for(std::chrono::seconds(1));
			sec_++;
			notify();
		}
	}
}

void Timer::update(Subject *s) {
	if(KeyboardController *kc = dynamic_cast<KeyboardController *>(s)){
		switch(kc->getCommand()){
			case 's':
				start();
				break;
			case 'h':
				stop();
				break;
			case 'r':
				reset();
				break;
			case 'x':
				break;
		}
	}
}