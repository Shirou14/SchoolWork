//TimerView.h
#ifndef TIMERVIEW_H
#define TIMERVIEW_H

#include "Observer.h"
#include "Timer.h"
#include "Subject.h"

class TimerView: public Observer {
public:
	TimerView(Timer *timer);
	virtual void update(Subject *s) = 0; //from observer class
	virtual void display(std::ostream& os) const = 0;

protected:
	Timer *timer_;
};
#endif
