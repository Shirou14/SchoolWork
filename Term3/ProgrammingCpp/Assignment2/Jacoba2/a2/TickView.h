//TickView.h
#ifndef TICKVIEW_H
#define TICKVIEW_H
#include "TimerView.h"
#include <iostream>

class TickView: public TimerView {
public:
	TickView(Timer *timer): TimerView(timer) {}
	virtual void display(std::ostream& os) const;
};

#endif