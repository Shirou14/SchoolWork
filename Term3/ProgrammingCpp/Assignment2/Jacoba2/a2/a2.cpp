#include "Timer.h"
#include "Subject.h"
#include "KeyboardController.h"
#include "SecView.h"
#include "MinSecView.h"
#include "TickView.h"

int main() {
	KeyboardController kc;
	Timer timer;
	SecView secView(&timer);
	MinSecView minSecView(&timer);
	TickView tickView(&timer);

	kc.subscribe(&timer);
	timer.subscribe(&secView);
	timer.subscribe(&minSecView);
	timer.subscribe(&tickView);

	kc.start();
}