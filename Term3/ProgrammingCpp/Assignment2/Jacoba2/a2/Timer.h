//Timer.h
#ifndef TIMER_H
#define TIMER_H

#include <thread>
#include <atomic>
#include "Observer.h"
#include "Subject.h"

class Timer: public Observer, public Subject {
public:
	//ctors
	Timer (unsigned long sec = 0, bool ticking = false); //ctor create thread
	Timer(const Timer&) = delete;			 			//(*)

	Timer& operator=(const Timer&) = delete;			//(*)
	void start();										//start timer ticking
	void stop();										//stop timer from ticking
	void reset();										//Reset timer to 0
	unsigned long get() const;							//returns number of seconds elapsed
	virtual void update(Subject *) override; 			//from observer class

private:
	std::atomic<unsigned long> 	sec_;		//number of seconds elapsed
	std::atomic<bool>			ticking_;	//is timer ticking
	void run();								//Function executed by thread
};
#endif