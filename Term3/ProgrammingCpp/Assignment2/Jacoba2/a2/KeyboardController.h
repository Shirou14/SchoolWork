//KeyboardController.h
#ifndef KEYBOARDCONTROLLER_H
#define KEYBOARDCONTROLLER_H
#include <atomic>
#include <string>
#include "Subject.h"

class KeyboardController: public Subject {
public:
	void start();									// start the loop to get users commands
	char getCommand() const {return command_;};		// return the command
private:
	std::atomic<char> command_;						//Store the command the user enters
};

#endif