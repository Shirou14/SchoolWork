//Subject.cpp
#include "Subject.h"

void Subject::subscribe(Observer *obs){
	observer_.push_back(obs);
}

void Subject::unsubscribe(Observer *obs){
	observer_.remove(obs);
}

void Subject::notify(){
	for(auto x: observer_){
		x->update(this);
	}
}