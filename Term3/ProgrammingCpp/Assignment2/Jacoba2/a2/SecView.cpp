//SecView.cpp
#include "SecView.h"


//Display the number of seconds elapsed from the beginning of the timer
void SecView::display(std::ostream& os) const {
	unsigned long secondsElapsed = timer_->get();

	os << secondsElapsed << std::endl;
}