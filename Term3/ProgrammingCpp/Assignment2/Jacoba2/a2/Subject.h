//Subject.h
#ifndef SUBJECT_H
#define SUBJECT_H
#include <list>
#include "Observer.h"
class Observer;

class Subject { //abstract base class
public:
	virtual ~Subject(){}
	virtual void subscribe(Observer *obs); //Subscribe an observer
	virtual void unsubscribe(Observer *obs); //unsubscribe an observer
	virtual void notify(); //ask each observer to update itself
protected:
	Subject(){}
private:
	std::list<Observer *> observer_; //list of observers
};

#endif