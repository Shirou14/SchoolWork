//MinSecVeiw.cpp
#include "MinSecView.h"
#include <iomanip>


//Display the number of minutes and seconds elapsed from the beginning of the timer
//Formatted as:
//12:05 or 1:15
void MinSecView::display(std::ostream& os) const {
	unsigned long secondsElapsed = timer_->get();
	unsigned long minutesElaped = (secondsElapsed/60);
	unsigned long secondsInTheMinute = (secondsElapsed%60);

	os << minutesElaped << ":" << std::setw(2) << std::setfill('0') << secondsInTheMinute << std::endl;
}