//KeyboardController.cpp
#include "KeyboardController.h"
#include <iostream>
#include <sstream>


void KeyboardController::start() {
	std::string line;
    std::string word;

	while (getline(std::cin,line)) {
		std::istringstream iss(line);

        //check if word is read
        if(!(iss >> word))
            break;

        //get the first character from the word and set the command
        switch(word.front()) {
        case 's':
        	command_ = 's';
           	notify();
        	break;
        case 'h':
        	command_ = 'h';
           	notify();
        	break;
        case 'r':
        	command_ = 'r';
            notify();
        	break;
        case 'x':
            return;  
        }
	}
}