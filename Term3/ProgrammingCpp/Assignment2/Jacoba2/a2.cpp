#include "Timer.h"
#include "Subject.h"
#include "KeyboardController.h"

int main() {
	KeyboardController kc;
	Timer timer;
	kc.subscribe(&timer);
	kc.start();
}