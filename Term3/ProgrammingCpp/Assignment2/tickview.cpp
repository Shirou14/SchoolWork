//tickView.cpp
#include "TickView.h"

//Display an asterix whenever the timer ticks (every second)
void TickView::display(std::ostream& os) const
{
	os << "*" << std::endl;
}