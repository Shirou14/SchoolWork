#ifndef TRIANGLE_H
#define TRIANGLE_H
#include <iostream>
#include <string>
#include <sstream>
#include <memory>
#include "Shape.h"
#include "Point.h"
#include "GC.h"

class Triangle: public Shape {
public:
  explicit Triangle(const Point& v1 = Point(), const Point& v2 = Point(1,0),
                    const Point& v3 = Point(0,1), 
                    GC::Colour colour = GC::Colour::Default)
  : Shape(colour), v1_(v1), v2_(v2), v3_(v3) {
  }

  explicit Triangle(std::istream& is): Shape(is) {
    is >> v1_ >> v2_ >> v3_;
  }

  virtual ~Triangle() {}

  virtual void draw(GC& gc) const {
    Shape::draw(gc);
    gc.draw(to_str());
  }

  virtual void save(std::ostream& os) const {
    // os << "Triangle" << std::endl;
    Shape::save(os);
    os << v1_ << ' ' << v2_ << ' ' << v3_ << std::endl;
  }

  virtual std::shared_ptr<Shape> clone() const {
    return std::make_shared<Triangle>(*this);
  }

private:
  Point  v1_, v2_, v3_;

  std::string to_str() const {
    std::ostringstream  oss;
    oss << "[T: " << v1_ << ' ' << v2_ << ' ' << v3_ << "]" 
        << std::endl;
    return oss.str();
  }
};
#endif      

