#ifndef CIRCLE_H
#define CIRCLE_H
#include <iostream>
#include <string>
#include <sstream>
#include <memory>
#include "Shape.h"  // need to include header for base class
#include "Point.h"
#include "GC.h"

class Circle: public Shape {
public:
  // derived class ctor needs to call base class ctor
  explicit Circle(const Point& centre = Point(), int radius = 1, 
                  GC::Colour colour = GC::Colour::Default)
  : Shape(colour), centre_(centre), radius_(radius) {
    if (radius_ < 0) {
      throw "Circle(const Point&, int, int): negative radius not allowed";
    }
  }

  explicit Circle(std::istream& is): Shape(is) {
   is >> centre_ >> radius_;
  } 

  virtual ~Circle() {}

  virtual void draw(GC& gc) const {
    Shape::draw(gc);
    gc.draw(to_str());
  }

  virtual void save(std::ostream& os) const {
    // os << "Circle" << std::endl;
    Shape::save(os);  // call Shape's save to save the colour
    os << centre_ << " " << radius_ << std::endl;
  }

  virtual std::shared_ptr<Shape> clone() const {
    return std::make_shared<Circle>(*this);
  }

private:
  Point  centre_;
  int    radius_;  
  std::string to_str() const {
    std::ostringstream  oss;
    oss << "[C: " << centre_ << ' ' << radius_ << "]" 
        << std::endl;
    return oss.str();
  }
};
#endif

#if 0
int main() {
  try {
    Circle (Point(1,1), -1, 1);
  } catch (const char *s) {
    cerr << s << endl;
  }
}
#endif
