#include <vector>
#include <memory>
#include "Shape.h"
#include "Circle.h"
#include "Triangle.h"
#include "GC.h"
using namespace std;

int main() {
  GC              gc(cout);
  vector<shared_ptr<Shape>>  v;
  Circle          c;
  Triangle        t;
  Shape *shapes[] = { &c, &t };  // prototypes
  int             choice;
  while (cin >> choice)
    if (0 <= choice && choice < (int) (sizeof(shapes)/sizeof(shapes[0])))
      v.push_back(shapes[choice]->clone());

  for (auto p: v)
    p->draw(gc);
}
