#ifndef SHAPE_H
#define SHAPE_H
#include <iostream>
#include <typeinfo>
#include <memory>
#include "GC.h"

class Shape {  // actually shapes with colours
               // abstract base class (ABC): class with a least 1 
               //                            pure virtual function
               // an abstract class cannot be instantiated
private:
  GC::Colour  colour_;
public:
  explicit Shape(GC::Colour colour = GC::Colour::Default): colour_(colour) {}
  explicit Shape(std::istream& is) {
    int colour;
    is >> colour;
    colour_ = static_cast<GC::Colour>(colour);
  }

  virtual ~Shape() {}

  // implementation of a pure virtual function is optional
  virtual void draw(GC&) const = 0;  // pure virtual function
    
  virtual void save(std::ostream&) const = 0; 

  virtual std::shared_ptr<Shape> clone() const = 0;
};

inline void  // virtual not repeated
Shape::save(std::ostream& os) const {
  os << typeid(*this).name() << std::endl;
  os << static_cast<int>(colour_) << std::endl;
}

inline void 
Shape::draw(GC& gc) const {
  gc.setColour(colour_);
}
#endif
