#ifndef POINT_H
#define POINT_H
#include <iostream>
// header files should not be "using namespace std;"

class Point {  // class definition
public:
  explicit Point(int x = 0, int y = 0): x_(x), y_(y) {}  // implicitly inline
  // compiler-generated copy ctor, dtor etc

  // + and += are related; += is a member function; + can be a outside
  // function or a member function

  Point& operator+=(const Point& p) {
    x_ += p.x_;  // same as: this->x_ += p.x_;
    y_ += p.y_;
    return *this;
  }
  
  friend std::ostream& operator<<(std::ostream&, const Point&);
  friend std::istream& operator>>(std::istream&, Point&);
private:
  int  x_, y_;
}; 

inline Point  // note: no reference
operator+(const Point& lhs, const Point& rhs) {
  Point p(lhs);
  return p += rhs;
}

inline std::ostream&  // functions defined in header outside class
                      // definition should be declared "inline"
operator<<(std::ostream& os, const Point& p) {
  return os << '(' << p.x_ << ',' << p.y_ << ')';
}

inline std::istream& 
operator>>(std::istream& is, Point& p) {
  // weak vs strong guarantee
  char c1, c2, c3;
  // return is >> c1 >> p.x_ >> c2 >> p.y_ >> c3;
  int  x, y;
  if (is >> c1 >> x >> c2 >> y >> c3 && c1 == '(' && c2 == ',' &&
      c3 == ')')
    p.x_ = x, p.y_ = y;
  else
    is.setstate(std::ios_base::failbit);
  return is;
}
#endif
