#ifndef SHAPEFACTORY_H
#define SHAPEFACTORY_H
#include <iostream>
#include <memory>
#include "Shape.h"
#include "Circle.h"
#include "Triangle.h"

class ShapeFactory {
private:
  std::istream *pis_;
public:
  ShapeFactory(std::istream& is): pis_(&is) {}  
  std::shared_ptr<Shape> create() {
    std::string  type;
    if (!(*pis_ >> type))
      return 0;

    if (type == typeid(Circle).name())
      return std::shared_ptr<Circle>(new Circle(*pis_));

    if (type == typeid(Triangle).name())
      return std::shared_ptr<Triangle>(new Triangle(*pis_));

    return std::shared_ptr<Shape>();  
  }
};
#endif
