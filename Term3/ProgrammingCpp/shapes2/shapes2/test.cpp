#include <iostream>
#include <memory>
using namespace std;

int main() {
  shared_ptr<int>  p;
  shared_ptr<int>  p2(new int(5));
  cout << p2.use_count() << endl;
  *p2 = 3;
  cout << *p2 << endl;
  {
  shared_ptr<int>  p3(p2);
  cout << p2.use_count() << endl;
  cout << p3.use_count() << endl;
  cout << *p3 << endl;
  *p3 = 10;
  cout << *p2 << endl;
  }
  cout << p2.use_count() << endl;

}
