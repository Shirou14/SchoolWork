#ifndef GC_H
#define GC_H
#include <iostream>
#include <string>
#include <map>

class GC {
public:
  enum class Colour {
    Black, Red, Green, Brown, Blue, Purple, Cyan, Grey, Default 
  };

  GC(std::ostream& os): pos_(&os) {}

  ~GC() { *pos_ << defaultColour; }

  void setColour(Colour c) {
    auto it = colours_.find(c);
    if (it != colours_.end())
      *pos_ << it->second;
  }

  void draw(const std::string& s) {
    *pos_ << s;
  }

private:
  std::ostream *pos_;

  static std::map<Colour, const char *>  colours_;  // declaration only
  static constexpr const char *defaultColour = "\033[39;49m";
};  
#endif
