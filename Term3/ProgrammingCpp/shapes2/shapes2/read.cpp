#include <iostream>
#include <vector>
#include <memory>
#include "Shape.h"
#include "Circle.h"
#include "Triangle.h"
#include "ShapeFactory.h"
#include "GC.h"
using namespace std;

int main() {
  GC            gc(cout);
  ShapeFactory  sf(cin);
  vector<shared_ptr<Shape>> v;
  shared_ptr<Shape>         p ;

  while ((p = sf.create()))
    v.push_back(p);

  for (auto p: v)
    p->draw(gc);
}

