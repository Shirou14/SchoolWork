#include <vector>
#include "Shape.h"
#include "Circle.h"
#include "Triangle.h"
#include "GC.h"
using namespace std;

int main() {
  GC              gc(cout);
  vector<Shape*>  v;
  v.push_back(new Circle(Point(1,1), 3, GC::Colour::Red));
  v.push_back(new Triangle(Point(0,0), Point(1,0), Point(0,1), GC::Colour::Blue));

  for (auto p: v)
    p->draw(gc);

  for (auto p: v)
    delete p;
}
