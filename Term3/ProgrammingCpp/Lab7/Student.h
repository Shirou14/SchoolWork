#include <iostream>
#include <string>
#include <map>


typedef std::pair<std::string, std::string> Name;

class Student{
    public:
        //provide ctor
        //Overload operators <<, >>
        virtual ~Student(){}
        Student(std::string id, std::string firstN, std::string lastN):name_(firstN, lastN)
        {
            try{
                if(!isValidId(id))
                {
                    throw "Student (string, string, string) did not have a valid ID";
                }else{
                    id_ = id;
                }
            }catch(const char *s){
                std::cerr<<s<<std::endl;
            }
        }

        friend std::ostream operator << (std::ostream&, Student);

    private:
        std::string id_; //e.g. a11111111
        Name name_; //e.g. Homer Simpson

        static bool isValidId(const std::string& id)
        {
            auto beg = id.begin();
            int iii = 0;
            if(*beg != 'a')
                return false;
            ++beg;
            for(; beg != id.end(); ++beg, ++iii)
            {
                if(!isdigit(*beg))
                {
                    return false;
                }
            }
            if(iii == 8)
                return true;

            return false;
        }
        //additional helpers if necessary
};

std::istream operator >> (std::istream& is, Student st)
{
    std::string id, fn, ln;

    is>>id>>fn>>ln;

    st = Student(id, fn, ln);
}

std::ostream operator << (std::ostream& os, Student st)
{
    os<<st.id_<<' '<<st.name_.first<<' '<<st.name_.second<<std::endl;
}