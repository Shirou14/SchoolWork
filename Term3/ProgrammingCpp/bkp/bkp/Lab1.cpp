#include <iostream>
#include <iomanip>


int m, n;
char o[];
int main()
{
	while (1)
	{
		std::cout << "Enter your input:" << std::endl;
		std::cin >> m >> n >> o;
		std::cout << std::setw(n) << std::setfill(' ') << m << std::endl;
		clear(cin);
	}
}