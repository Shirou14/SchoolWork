#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <string>

using namespace std; 

struct Grades {
	string           id;     // Student ID
	map<string, int> scores; // Key = course, value = score
};

std::ostream& operator << (ostream& os, const Grades& s ){
	os << s.id << '\n' << s.scores.size() << endl;
	for(auto it = s.scores.begin(); it != s.scores.end(); it++){
		os << it->first << ' ' << it->second << endl;
	}
	return os;
}

std::istream& operator >> (istream& is, Grades& s){
	string course;
	size_t size;
	int grade;
	if(!(is >> s.id))
		return is;
	is >> size;
	for(auto i = 0; i < size; i++){
		is >> course >> grade;
		s.scores[course] = grade;
	}
	return is; 
}

struct Cmp{
	string course;
	Cmp(const string& c): course(c){}
	bool operator()(Grades& lhs, Grades& rhs) const{
		if(lhs.scores.find(course) != lhs.scores.end())
			return true;
		if(rhs.scores.find(course) != rhs.scores.end())
			return false;
		return lhs.scores[course] > rhs.scores[course];
	}
};

struct MaxFinder {
	string course;
	vector<string>	v;
	int max = 0;
	MaxFinder(const string& c): course(c){}
	void operator()(const Grades& g){
		auto it = g.scores.find(course);
		if(it != g.scores.end()){
			if(max < it->second){
				max = it->second;
				v.push_back(g.id);
			}	
			if(max == it->second){
				v.push_back(g.id);	
			}
		}
	}
};

int main()
{
	Grades a;
	vector<Grades> v;
	while(cin >> a){
		v.push_back(a);
	}
	sort(v.begin(), v.end(), Cmp("COMP3512"));
	for(auto& a: v){
		cout << a << endl;
	}
	MaxFinder m = for_each(v.begin(), v.end(), MaxFinder("COMP3512"));
	cout <<  m.course << ' ' << m.max << ' ' ;
	for (auto it = m.v.begin();it!=m.v.end();++it)
		cout << *it << ' ' ;
	return 0;
}