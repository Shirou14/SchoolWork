#include <iostream>
#include <iomanip>
#include <fstream>
//#include <string.h>

using namespace std;

string ltrim(const string&, int(*is_junk)(int));
string rtrim(const string&, int(*is_junk)(int));
string cdelete(const string&, int(*is_junk)(int));
string sdelete(const string&, const string&);
string squeeze(const string&, char);

int main()
{
    string s;
    cout<<"enter a word"<<endl;
    getline(cin, s);
    cout<<ltrim(s, isspace)<<endl;
    cin.clear();
    cout<<"hahahahahaha\t     "<<endl;
    //getline(cin, s);
    cout<<rtrim("hahahahahaha\t     ", isspace)<<endl;
    cin.clear();
    cout<<"enter yet another word"<<endl;
    getline(cin, s);
    cout<<cdelete(s, isspace)<<endl;
    cin.clear();
    cout<<"hoomnmer simpsoomnn"<<endl;
    cout<<sdelete("hoomnmer simpsoomnn", "omn")<<endl;
    cin.clear();
}

string ltrim(const string& s, int (*is_junk)(int))
{
    int count = 0;
    //string tempStr;
    while(is_junk(s[count]))
    {
        count++;
    }

    string tempStr(s, count);

    return tempStr;
}

string rtrim(const string& s, int (*is_junk)(int))
{
    int count = s.length();
    //string tempStr;
    while(is_junk(s[count]) && count > 0)
    {
        count--;
    }

    string tempStr(s, 0, count);

    return tempStr;
}

string cdelete(const string& s, int(*is_junk)(int))
{
    int count = 0;
    char tmpstr[128];

    for(int iii = 0; iii < s.length(); iii++)
    {
        if(!is_junk(s[iii]))
        {
            tmpstr[count++] = s[iii];
        }
    }
    tmpstr[count] = '\0';

    string temp(tmpstr);
    return temp;
}

string sdelete(const string& s, const string& delim)
{
    //int count = 0;
    size_type fnd = 0;
    int iii = 0;
    string tmp;
    char tmpstr[128];

    while((fnd = s.find(iii)) != 'npos')
    {
        tmp.append(s, iii, fnd);
        iii = fnd+delim.length();
    }

    //tmpstr[count] = '\0';

    //string temp(tmpstr);
    return tmp;
}