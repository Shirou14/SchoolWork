#include <iostream>
#include <iomanip>

using namespace std;
//using namespace ios_base;

int m, n;
char o;

int main()
{
    //cout << "Enter your input:" << endl;
    while(cin >> m >> n >> o)
    {
        if(o == 'd')
        {
            cout<<dec;
        }else if (o == 'x'){
            cout<<nouppercase<<hex;
        }else if (o == 'X'){
            cout<<uppercase<<hex;
        }else if(o == 'o'){
            cout<<oct;
        }

        cout << setw(n) << setfill('.') << m << endl;
    }
}