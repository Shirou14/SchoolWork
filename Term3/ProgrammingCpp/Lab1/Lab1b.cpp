#include <iostream>
#include <iomanip>

using namespace std;

float m;
int n, o;
char p;
int main()
{
    //cout << "Enter your input:" << endl;
    while(cin >> m >> n >> o >> p)
    {
        if(p == 'f')
        {
            cout<<fixed;
        }else if (p == 'e'){
            cout<<scientific<<nouppercase;
        }else if (p == 'E'){
            cout<<scientific<<uppercase;
        }
        cout << setprecision(o) << setw(n) << setfill('.') << m << endl;
        cout << "Enter your input:" << endl;
        //cin.clear();
    }
}