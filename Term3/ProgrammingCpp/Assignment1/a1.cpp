#include "a1Header.h"

using namespace std;

int main(int argc, char* argv[])
{
    string config = "a1tests\\config", parse, tmpStr;

    if(argc >= 2)
    {
        config = argv[1];
    }

    ColourMap mods(config);

    while(getline(cin, tmpStr)){
        parse.insert(parse.end(), tmpStr.begin(), tmpStr.end());
        parse.insert(parse.end(), '\n');
    }
    parse.pop_back();
    ParseString(parse, mods);
    cout << parse << DEFAULT_MODE<<endl;
}




void ParseString(string& s, const ColourMap& cm)
{
    static size_t j = 0, k;
    bool newline = false;
    string out, command;
    vector<string> vColours;
    char lDelim = '(', rDelim = ')', EXIT = 0x1b;

    for(auto strt = s.begin(); strt < s.end(); ++strt)
    {
       if(*strt == rDelim || *strt == lDelim)
       {
           auto delimMark = strt;
           //cout<<*strt<<" is the delim detected"<<endl;
           if(*strt == *(strt + 1))
           {
               //cout<<"double detected. now adding in new one"<<endl;
               s.replace(strt - 1, strt + 1, strt, strt + 1);
           }else if(*strt == lDelim){
               //so far do nothing, but escape sequence will be here soon.
               //cout<<"no double detected"<<endl;
               //copying the strt iterator so we can jump ahead and check for the end of the word
               
               ++strt;
               while(*strt == ' ' && strt != s.end()) 
                    ++strt;
               
               auto tmp = strt;
               while(*strt != ' ' && strt != s.end()) 
                {
                    if(*strt == lDelim && *(strt+1) == *strt)
                    {
                        s.erase(strt);
                        //++strt;
                    }
                    else if(*strt == rDelim && *(strt+1) == *strt)
                        s.erase(strt);
                    else if(*strt == '\n')
                    {
                        //cout<<*strt<<" hello"<<endl;
                        s.erase(strt);
                        newline = true;
                    }
                    ++strt;
                    //cout<<*strt<<endl;
                }
               command.insert(command.begin(), tmp, strt);
               //cout<<"blah"<<command<<endl;
               while(*(strt) == ' ' && strt != s.end())
                   ++strt;
               s.erase(delimMark, strt);
               strt = delimMark;
               //check the colour code to insert
               if(newline)
               {
                   s.insert(strt, '\n');
                   ++strt;
               }
               auto col = cm.colour.find(command);
               if(col != cm.colour.end())
               {
                   vColours.push_back(col->second);
                   s.insert(strt, vColours.back().begin(), vColours.back().end());
               }else{
                   //cout<<"boo"<<command<<endl;
                   vColours.push_back(DEFAULT_MODE);
                   s.insert(strt, vColours.back().begin(), vColours.back().end());
               }
           }else if(*strt == rDelim){
               if(vColours.begin() != vColours.end() - 1)
               {
                   vColours.pop_back();
                   s.erase(strt);
                   s.insert(strt, vColours.back().begin(), vColours.back().end());
               }else{
                   string temporary;
                   temporary.insert(temporary.begin(), strt + 1, s.end());
                   if(temporary.find(rDelim) != temporary.npos)
                   { 
                       cerr<<"INSUFFICIENT BRACKETS"<<endl;
                       strt = s.end();
                   }else{
                       s.erase(strt);
                       vColours.pop_back();
                   }
               }
           }
       }else{
           if(vColours.begin() == vColours.end())
           {
               k = s.end() - strt;
               cerr<<DEFAULT_MODE<<"Incorrect number of delimiters at line "<<j<<" char "<<k<<endl;
               break;
           }
       }
       //empty out the command to make sure the delims don't pile on
       command = "";
    }
    j++;
}
