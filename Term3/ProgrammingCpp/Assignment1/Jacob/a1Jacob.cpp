#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <map>
#include <vector>
using namespace std;

#ifndef DEBUG
#define DEBUG false
#endif

string readInput();
void processString(string& input, map<string,string>& m, vector<string>& v);
void trimLeadingWhitespace(string& s);
void trimTrailingWhitespace(string& s);
bool isFirstCharOpenBrace(string& s);
bool isLastCharCloseBrace(string& s);
void commandMapCreator(fstream& file, map<string,string>& map);
bool commandFinder(string& s, size_t location);
size_t findFirstWhitespaceAfterCommand(string& s, size_t pos);
size_t findLastWhitespaceAfterCommand(string& s, size_t pos);
bool closingBracestoEndOfString(string& s, size_t pos);
size_t closingBraceCounter(string& s, size_t pos);
size_t openingBraceCounter(string& s, size_t pos);
bool inputBeginsWithCommand(string& input);
void replaceEscapeCodeWithHexChar(string& s, size_t pos);
void removeDoubleBracesFromCommand(string& s);
void returnToPreviousCommand(vector<string>& v, string& s, map<string,string>& m, size_t pos);

string 	OPEN_BRACE_STRING 	= "(";
char 	OPEN_BRACE_CHAR 	= '(';
string 	CLOSE_BRACE_STRING 	= ")";
char 	CLOSE_BRACE_CHAR 	= ')';
char 	ESCAPE 				= 0x1b;
string 	DEFAULT_MODE 		= "[39;49m";


int main(int argc, char *argv[]) {

	map<string,string> commandMap;
	vector<string> commands;

	string commandFileName = "config";
	if(argc > 1) {
		commandFileName = argv[1];
	}
	//Open the file for reading in
	fstream commandFile(commandFileName, fstream::in);

	//Create map of commands
	commandMapCreator(commandFile, commandMap);

	//If debug is defined, display the used commands and their respective colors
	if(DEBUG) {
		for(const auto& m: commandMap) {
			string code = m.second;
			replaceEscapeCodeWithHexChar(code, 0);
			cout << code << OPEN_BRACE_STRING << m.first << CLOSE_BRACE_STRING << ESCAPE << DEFAULT_MODE << endl;
		}
		return 0;
	}

	//store input as String
	string inputRead = readInput();

	//Trim the leading and trailing whitespace from the input
	trimLeadingWhitespace(inputRead);
	trimTrailingWhitespace(inputRead);

	//Ensure that the first and last character are a brace
	if(isFirstCharOpenBrace(inputRead)) {
		if(inputBeginsWithCommand(inputRead)) {
			processString(inputRead, commandMap, commands);
		} else {
			cout << ESCAPE << DEFAULT_MODE;
			cerr << "Error on line 1" << endl;
			cerr << "String does not begin with a command" << endl;
		}
	} else {
		cout << ESCAPE << DEFAULT_MODE;
		cerr << "Error on line 1" << endl;
		cerr << "first char is not a \"(\"" << endl;
	}
	commandFile.close();
}

// main function to process and output the string
void processString(string& input, map<string,string>& m, vector<string>& v) {
	size_t firstWhitespace = 0;
	size_t lastWhitespace = 0;
	size_t commandLength = 0;
	size_t substringLength = 0;
	size_t lineCounter = 1;
	string command;

	for(size_t i = 0; i < input.length()-1; i++) {		

		if(input[i] == OPEN_BRACE_CHAR) {
			
			//if an opening brace is found where the next position is not an opening brace
			if(commandFinder(input, i)) {

				size_t j = i;
				//returns the position of the first char in the command word
				//this allows for having whitespace between the 
				//right opening brace and the command word
				j = findLastWhitespaceAfterCommand(input, i+1);

				//The position of the first whitespace after the command word
				firstWhitespace = findFirstWhitespaceAfterCommand(input, j);
				//The position of the first non-whitespace char after the command word
				lastWhitespace = findLastWhitespaceAfterCommand(input, firstWhitespace);

				commandLength = (lastWhitespace - j);
				substringLength = (firstWhitespace - j);

				command = input.substr(j,substringLength);

				//reformats (( and )) command words to ( and ), respectively
				removeDoubleBracesFromCommand(command);

				//find if command exists in map
				if(m.find(command) == m.end()) {
					cout << ESCAPE << DEFAULT_MODE;
					cerr << "Error on line " << lineCounter << endl;
					cerr << "Command not found in Map\n";
					return;
				} else {
					//replace command word and trailing whitespace in string with cammand value
					v.push_back(command);
					input.replace(j,commandLength, m.find(command)->second);

					//replaces escape character sequence with Hex char for ESC
					replaceEscapeCodeWithHexChar(input, j);

					//Replaces the ( at the begining of the command with an empty string
					input.replace(i,1,"");
				}

			} else {
				//replaces two (( with a single (
				input.replace(i,2,OPEN_BRACE_STRING);
			}		
		//If the character found was a )	
		} else if(input[i] == CLOSE_BRACE_CHAR) {
			size_t closeBraceCounter = closingBraceCounter(input, i);
			size_t difference = closingBraceCounter(input, i) - v.size();

			if(closingBracestoEndOfString(input, i)) {

				//If the next char is also a )
				if(closeBraceCounter != v.size() && difference > 1) {
					//replaces two )) with a single )
					input.replace(i,2,CLOSE_BRACE_STRING);

				} else {
					//replaces the closing brace with the formatting that was previously
					//being applied to the string
					returnToPreviousCommand(v, input, m, i);
				}

			} else {
				if(!(closeBraceCounter % 2 == 0)) {
					if(closeBraceCounter > 1) {
						//replaces two )) with a single )
						input.replace(i,2,CLOSE_BRACE_STRING);
					} else {
						//replaces the closing brace with the formatting that was previously
						//being applied to the string
						returnToPreviousCommand(v, input, m, i);
					}					
				} else {
					//replaces two )) with a single )
					input.replace(i,2,CLOSE_BRACE_STRING);
				}
			}		
		}
		//to ensure that proper information can be given to the end user if an 
		//error is made, the lineCounter is incrememnted whenever a new line char is seen
		if(input[i] == '\n') {
			lineCounter++;
		}
		//print current char to terminal
		cout << input[i];
	}
}

//Checks to ensure that the input begins with a command
bool inputBeginsWithCommand(string& input) {
	if(commandFinder(input, 0)) {
		return true;
	} else {
		return false;
	}
}

//replaces the closing brace with the formatting that was previously
//being applied to the string
void returnToPreviousCommand(vector<string>& v, string& s, map<string,string>& m, size_t pos) {
	//Remove the current command from the stack
	v.pop_back();

	//If the stack is empty (no more commands) replace closing brace with empty string
	if(v.empty()) {
		s.replace(pos,1, "");
	} else {
		string previousCommand = v.back();
		s.replace(pos,1, m.find(previousCommand)->second);

		replaceEscapeCodeWithHexChar(s, pos);
	}
}

//reformats (( and ))  in command words to ( and ), respectively
void removeDoubleBracesFromCommand(string& s) {
	for(size_t i = 0; i < s.length(); i++) {
		while(s[i] == OPEN_BRACE_CHAR) {
			if(s[i+1] == OPEN_BRACE_CHAR) {
				s.replace(i, 2, OPEN_BRACE_STRING);
			}
			i++;
		}
		while(s[i] == CLOSE_BRACE_CHAR) {
			if(s[i+1] == CLOSE_BRACE_CHAR) {
				s.replace(i, 2, CLOSE_BRACE_STRING);
			}
			i++;
		}
	}
}

//returns true if the current char is an opening brace and the next in the string char is not
bool commandFinder(string& s, size_t location) {
	if(s[location] == OPEN_BRACE_CHAR && s[location + 1] != OPEN_BRACE_CHAR) {
		return true;
	} else {
		return false;
	}
}

//Replaces the escape code for formatting with the HEX char ESC
//so that formatting can be read by stdout.
void replaceEscapeCodeWithHexChar(string& s, size_t pos) {
	size_t i = pos;
	while(s[i] != '[') {
		i++;
	}
	s.replace(pos, (i-pos), &ESCAPE);
}

//Counts the number of opening braces that occur in a row from a given point
size_t openingBraceCounter(string& s, size_t pos) {
	size_t counter = 0;
	while(s[pos] == OPEN_BRACE_CHAR) {
		pos++;
		counter++;
	}
	return counter;
}

//Counts the number of closing braces that occur in a row from a given point
size_t closingBraceCounter(string& s, size_t pos) {
	size_t counter = 0;
	while(s[pos] == CLOSE_BRACE_CHAR) {
		pos++;
		counter++;
	}
	return counter;
}

//Returns true if the rest of teh characters in the string are closing braces
bool closingBracestoEndOfString(string& s, size_t pos) {
	while(s[pos] == CLOSE_BRACE_CHAR) {
		pos++;
	}
	if(s[pos] == '\0') {
		return true;
	} else {
		return false;
	}
}

//The position of the first whitespace after the command word
size_t findFirstWhitespaceAfterCommand(string& s, size_t pos) {
	while(s[pos] != ' ' && s[pos] != '\n' && s[pos] != '\t') {
			pos++;
	}
	return pos;
}

//The position of the first non-whitespace char after the command word
size_t findLastWhitespaceAfterCommand(string& s, size_t pos) {
	while(s[pos] == ' ' || s[pos] == '\n' || s[pos] == '\t') {
		pos++;
	}
	return pos;
}


//Generates the unordered map of commands and values to be used in formatting the text.
void commandMapCreator(fstream& file, map<string,string>& map) {
	string line;
	stringstream ss;
	string command;
	string valueCode;

	while(getline(file, line)) {
		ss.str(line);
		if((ss >> command >> valueCode)) {
			if(map.find(command) == map.end()) {
				map.emplace(command, valueCode);
			} else {
				continue;
			}
		}
	}
}

//Checks to ensure that the final char of the inputted string is a closing brace
bool isLastCharCloseBrace(string& s) {
	size_t lastChar = s.length()-1;
	if(s[lastChar] == CLOSE_BRACE_CHAR) {
		return true;
	} else {
		return false;
	}
}

//Checks to ensure that the first char of the inputted string is a opening brace
bool isFirstCharOpenBrace(string& s) {
	size_t firstChar = 0;
	if(s[firstChar] == OPEN_BRACE_CHAR) {
		return true;
	} else {
		return false;
	}
}

//trims trailing whitespace from string
void trimTrailingWhitespace(string& s) {
	size_t p = s.find_last_not_of(" \t\n");

	if (string::npos != p) {
  		s.erase(p+1);
	}
}

//trims leading whitespace from string
void trimLeadingWhitespace(string& s) {
	size_t p = s.find_first_not_of(" \t\n");
	s.erase(0,p);
}

//Concatonates entire input as a single string and returns it.
string readInput() {
	string input;
	string concat;

	//Read input and store in a String.
	while(cin) {
		getline(cin,input);
		concat.append(input);
		concat.append("\n");
	}
	return concat;
}