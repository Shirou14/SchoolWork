#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <STRING>
#include <sstream>

#ifndef STRING
#define STRING std::string
#endif

#ifndef DEFAULT_MODE
#define DEFAULT_MODE ""
#endif

#ifndef DEBUG
#define DEBUG false
#endif

struct ColourMap{
    public:
        std::unordered_map<STRING, STRING> colour;
        STRING location;
        ColourMap(STRING f = "config") : location(f)
        {
            STRING p1, p2, line;
            std::fstream fin(f, std::ios_base::in | std::ios_base::binary);
            while(getline(fin, line))
            {
                std::stringstream ss(line);
                if(ss>>p1>>p2)
                {
                    size_t iii;
                    if((iii = p2.find("\\")) != std::string::npos)
                    {
                        //std::cout<<"part 1"<<std::endl;
                        if(p2[iii + 1] != '\\' && p2[iii + 1] == 'e')
                            {
                                p2.replace(iii, 2, "\033");
                            }
                    }
                    colour.emplace(p1, p2);
                }
            }

            if(DEBUG)
                Debug();
        }

        void Debug()
        {
            for(auto beg = colour.begin(); beg != colour.end(); ++beg)
            {
                std::cout<<beg->second<<'('<<beg->first<<')'<<DEFAULT_MODE<<std::endl;
            }
        } 
};

void ParseString(STRING&, const ColourMap&);
