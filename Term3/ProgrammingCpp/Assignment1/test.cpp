#include "a1Header.h"

using namespace std;

int main()
{
    string test;
    cout<<"begin"<<endl;
    while(getline(cin, test))
    {}
    cout<<"end"<<endl;
}

string ParseString(const string& s, const ColourMap& cm)
{
    size_t j;
    string out, tempStr;
    vector<string> vColours;
    char lDelim = '(', rDelim = ')';

    for(auto strt = s.begin(); strt < s.end(); ++strt)
    {
       if(*strt == rDelim || *strt == lDelim)
       {
           //cout<<*strt<<" is the delim detected"<<endl;
           if(*strt == *(strt + 1))
           {
               //cout<<"double detected. now adding in new one"<<endl;
               out.append(strt, (++strt)+1);
           }else if(*strt == lDelim){
               //so far do nothing, but escape sequence will be here soon.
               //cout<<"no double detected"<<endl;
               //copying the strt iterator so we can jump ahead and check for the end of the word
               auto tmp = strt;

               while(*strt == ' ' && strt != s.end()) 
                    out.append(strt, ++strt);
               while(*strt != ' ' && strt != s.end()) 
                {
                    if(*strt == lDelim && *(strt+1) == *strt)
                        ++strt;
                    else if(*strt == rDelim && *(strt+1) == *strt)
                        ++strt;
                    else if(*strt == '\n')
                        out.append(strt, ++strt);
                    ++strt;
                }
               tempStr.append((tmp + 1), strt);
               while(*(strt+1) == ' ' && strt != s.end())
                   ++strt;
               //check the colour code to insert
               auto col = cm.colour.find(tempStr);
               if(col != cm.colour.end())
               {
                   vColours.push_back(col->second);
                   j = out.size();
                   out.append(vColours.back());
                   replaceEscapeCodeWithHexChar(out, j);
               }else{
                   vColours.push_back(DEFAULT_MODE);
                   j = out.size();
                   out.append(vColours.back());
                   replaceEscapeCodeWithHexChar(out, j);
               }
               //cout<< temp << "::" <<endl;
           }else if(*strt == rDelim){
               //cout<<"found rDelim"<<endl;
               if(vColours.begin() != vColours.end() - 1)
               {
                   vColours.pop_back();
                   j = out.size();
                   out.append(vColours.back());
                   replaceEscapeCodeWithHexChar(out, j);
               }else{
                   string tmp;
                   tmp.insert(tmp.begin(), strt + 1, s.end());
                   if(tmp.find(rDelim) != tmp.npos)
                   { 
                       cerr<<"INSUFFICIENT BRACKETS"<<endl;
                       strt = s.end();
                       return out;
                   }else{
                       vColours.pop_back();
                   }
               }
           }
       }else{
           out.append(strt, strt + 1);
       }
       //empty out the tempStr to make sure the delims don't pile on
       tempStr = "";
    }
    return out;
}
