#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

using namespace std;

int kth_selection(const vector<int>&, size_t);
vector<string> subsets(size_t);

int main()
{
    vector<int> v = {10, 5, 6, 2, 3, 11, 19, 4, 40};
    vector<string> s;
    int k = 5, n = 4;
    cout << kth_selection(v, k) << endl;
    s = subsets(n);
    for(int iii = 0; iii < s.size(); iii++)
    {
        cout<<s[iii]<<endl;
    }
}

//Precondition: v.size() > 0 && 1<= k && k  <= v.size_()
int kth_selection(const vector<int>& v, size_t k)
{
    vector<int> left, mid, right;
    int result;

    if(v.size() <= 1){return v[0];}

    for(int iii = 0; iii < v.size(); iii++)
    {
        if(v[iii] < v[0])
        {
            left.push_back(v[iii]);
        }else if(v[iii] > v[0]){
            right.push_back(v[iii]);
        }else{
            mid.push_back(v[iii]);
        }
    }
    if(left.size() >= k)
    {
        return kth_selection(left, k);
    }else if(left.size() + mid.size() == k){
        return mid[0];
    }else{
        return kth_selection(right, k - (left.size() + mid.size()));
    }
    return v[k];
}

vector<string> subsets(size_t n)
{
    vector<string> permutations;
    int size = permutations.size();

    if(n <= 1)
    {
        return permutations = {"0", "1"};
    }
    permutations = subsets(n--);
    for(int iii = 0; iii > size; iii++)
    {
        permutations.push_back("1" + permutations[iii]);
        permutations[iii] = ("0" + permutations[iii]);
    }

    return permutations;
}