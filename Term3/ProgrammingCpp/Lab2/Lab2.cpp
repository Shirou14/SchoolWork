#include <iostream>
#include <iomanip>

using namespace std;

bool input_int(const string&, int&, bool(*is_valid)(const int&));
bool input_float(const string&, int&, bool(*is_valid)(const float&));
bool is_valid(const float& entry);
bool is_valid(const int& entry);

int id;
float score;

int main()
{
    cout<<"Please input a student ID number"<<endl;
    while(getline(cin, id)){
        if(input_int("Please input a score", id, is_valid))
        {
            //cout<<"Please enter the student score"<<endl;
            if(getline(cin, score))
            {
                if(input_float("Please an input a student ID", score, is_valid))
                {
                    //file input here
                }
            }else{
                break;
            }
        }
    }
    cin.clear();
    cout<<"End of File detected"<<endl;
}

bool input_int(const string& prompt, int& n, bool(*is_valid)(const int&))
{
    if(is_valid(n)){
        cout<<"User ID accepted:"<<setw(8)<<n<<endl;
        cout<<prompt<<endl;
        return true;
    }else{
        cout<<"User ID is invalid"<<endl;
        return false;
    }
    cout<<prompt<<endl;
    return false;
}

bool input_float(const string& prompt, int& n, bool(*is_valid)(const float&))
{
    if(is_valid(n)){
        cout<<"User score accepted:"<<set_precision(2)<<n<<endl;
        return true;
    }else{
        cout<<"User score is invalid"<<endl;
        return false;
    }
    cout<<prompt<<endl;
    return false;
}

bool is_valid(const int& m)
{
    if(m >= 10000 && m <= 99999999)
        return true;
    else
        return false;
}

bool is_valid(const float& m)
{
    if(m <= 100.00 && m >= 0.00)
        return true;
    else
        return false;
}
