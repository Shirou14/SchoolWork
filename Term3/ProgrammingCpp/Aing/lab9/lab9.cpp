#include <iostream>
#include <sstream>
#include "Timer.h"

using namespace std;

int main() {
    Timer t;
    string line;
    string word;

    while (getline(cin,line)) { 

        istringstream iss(line);
        //check if word is read
        if(!(iss >> word))
            break;

        switch(word.front()) {
            case 's':
                t.start();
            break;
            case 'h':
                t.stop();
            break;
            case 'x':
                t.stop();
                return 0;
            break;
    
        }
    }
}