#include <chrono>
#include <iostream>
#include "Timer.h"
using namespace std;

//ctor
Timer::Timer (unsigned long sec, bool ticking): sec_(sec), ticking_(ticking) {
    thread timerThread(&Timer::run, this);
    timerThread.detach();
}

void Timer::start() {
    ticking_ = true;
}

void Timer::stop() {
    ticking_ = false;
}

void Timer::run() {
    while(1) {
        if(ticking_) {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            sec_++;
            cout << sec_ << endl;
        }
    }
}