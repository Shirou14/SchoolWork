

#include <thread>
#include <atomic>
class Timer {
public:
    Timer (unsigned long sec = 0, bool ticking = false); 	//ctor create thread
    void start();               			            	//start timer ticking
    void stop();                            				//stop timer from ticking
private:
    std::atomic<unsigned long>    sec_;       				//number of seconds elapsed
    std::atomic<bool>         ticking_;   					//is timer ticking
    void run();                             				//Function executed by thread
};