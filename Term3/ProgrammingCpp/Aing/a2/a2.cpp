#include <iostream>
#include <sstream>
#include "Timer.h"
// #include "Observer.h"
#include "Subject.h"
#include "KeyboardController.h"
#include "SecView.h"
#include "MinSecView.h"


using namespace std;


int main() {
    KeyboardController kc;
    Timer t;
    SecView secView(&t);
    MinSecView minSecView(&t);
    
    //make subscriptions
    kc.subscribe(&t);
    t.subscribe(&secView);
    t.subscribe(&minSecView);

    //start user input and output
    kc.start();


    // Timer t(0, true);

    // while(1){}
    // t.start();



/*
    string line;
    string word;

    while (getline(cin,line)) { 

        istringstream iss(line);
        //check if word is read
        if(!(iss >> word))
            break;

        switch(word.front()) {
            case 's':
                t.start();
            break;
            case 'h':
                t.stop();
            break;
            case 'x':
                t.stop();
                return 0;
            break;
    
        }
    }
*/

}


//main thread
    //create a timer object
    //create a keyboard controller 
    //create all 3 views
        //how do we pick the view?
    
    //while loop
        //cin to keyboard controller list, line by line
            //get first non-whitespace character
                //anything else on that line is ignored 
        
//timer thread
    //called by the keyboard controller
