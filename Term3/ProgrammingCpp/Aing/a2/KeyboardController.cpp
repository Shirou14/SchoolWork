//KeyboardController
#include <istream>
#include <iostream>

#include "KeyboardController.h"

using namespace std;

string ltrim(const string& s, int(*is_junk)(int));

void KeyboardController::start(){
	string input;
	// while(cin >> input){
	while(getline(cin, input)){	
		input = ltrim(input, ::isspace);
		switch(input[0]){
			case 's':
				cout << "start" << endl;
				command_ = 's';
				notify();
				break;
			case 'h':
				cout << "hault" << endl;
				command_ = 'h';
				notify();
				break;
			case 'r':
				cout << "reset" << endl;
				command_ = 'r';
				notify();
				break;
			case 'x':
				//exit the program
				cout << "exit" << endl;
				command_ = 'x';
				notify();
				break;
			default:
				cout << "error: not a valid command" << endl;
		}
	}
}

string ltrim(const string& s, int (*is_junk)(int)){
	string str;

	for(int i = 0; i < (int)s.length(); i++){
		if(is_junk(s[i]) == 0){
			//str.resize(i+1);
			str = s.substr(i, s.length());
			return str;
		}
	}
		
	return str;
}