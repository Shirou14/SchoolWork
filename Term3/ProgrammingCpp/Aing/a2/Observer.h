//Observer.h
#ifndef OBSERVER_H
#define OBSERVER_H

#include "Subject.h"
class Subject;

class Observer { //abstract base class
public:
	virtual ~Observer(){}
	virtual void update(Subject *) = 0;	//called by notify in Subject.h
										//abstract: to be specified by views or timer
protected:
	Observer(){}
};

#endif