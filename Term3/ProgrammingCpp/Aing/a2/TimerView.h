//TimerView.h
#ifndef TIMERVIEW_H
#define TIMERVIEW_H

#include <ostream>
#include "Observer.h"
#include "Timer.h"


class TimerView: public Observer {
public:
	TimerView(Timer *timer);
	virtual void update(Subject *s); //from observer class
	virtual void display(std::ostream& os) const = 0;	//pure virtual because of the =0

protected:
	Timer *timer_;
};

#endif