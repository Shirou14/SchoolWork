//TickView.h

class TickView: public TimerView {
public:
	TickView(Timer *timer): TimerView(timer) {}
	virtual void display(std::ostream& os) const;
};