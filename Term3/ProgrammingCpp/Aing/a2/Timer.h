#ifndef TIMER_H
#define TIMER_H

#include <thread>
#include <atomic>
#include "Subject.h"
#include "KeyboardController.h"

class Timer: public Subject, public Observer {
// class Timer{	
public:
	//Timer();
	void start();							//start timer ticking
	void stop();							//stop timer from ticking
	void reset();							//Reset timer to 0
	unsigned long get() const;				//returns number of seconds elapsed
	// virtual void update(Subject *s); 		//from observer class

	Timer(const Timer&) = delete;			 	//(*)
	Timer& operator=(const Timer&) = delete;	//(*)
	virtual void update(Subject *) override;

//Inherited methods not shown....

	//The line below was not provided in the assignment instructions
	//copied from lab 9
	Timer (unsigned long sec = 0, bool ticking = false); //ctor create thread

private:
	std::atomic<unsigned long> 	sec_;		//number of seconds elapsed
	std::atomic<bool>			ticking_;	//is timer ticking
	void run();								//Function executed by thread
	//additional members if needed
};

#endif