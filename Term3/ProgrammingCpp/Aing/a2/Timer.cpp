#include <chrono>
#include <iostream>
#include "Timer.h"


using namespace std;


Timer::Timer (unsigned long sec, bool ticking): sec_(sec), ticking_(ticking) {
	thread timerThread(&Timer::run, this);
	timerThread.detach();
}

/*
Timer::Timer (): sec_(0), ticking_(false) {
	thread timerThread(&Timer::run, this);
	timerThread.detach();
}

Timer::Timer(const Timer& timer): sec_(timer.sec_), ticking_(timer.ticking_) {
	thread timerThread(&Timer::run, this);
	timerThread.detach();
}
*/
void Timer::start() {
	ticking_ = true;
}

void Timer::stop() {
	ticking_ = false;
}

void Timer::reset() {
	sec_ = 0;
}

unsigned long Timer::get() const{
	return sec_;
}

void Timer::update(Subject *s){
	cerr << "updating" << endl;
	if(KeyboardController *keyboard = dynamic_cast<KeyboardController *>(s)){
		switch(keyboard->getCommand()){
			case 's':
				start();
				break;
			case 'h':
				stop();
				break;
			case 'r':
				reset();
				break;
			case 'x':
				//exit the program
				break;
		}
	}
}

void Timer::run() {
	while(1) {
		if(ticking_) {
			std::this_thread::sleep_for(std::chrono::seconds(1));
			sec_++;
			// cout << sec_ << endl;
			//update observers with notify??
			notify();
		}
	}
}