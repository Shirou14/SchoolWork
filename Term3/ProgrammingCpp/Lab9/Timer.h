#ifndef TIMER_H
#define TIMER_H
#include <iostream>
#include <string>
#include <atomic>
#include <thread>
#include <chrono>
#include <sstream>


class Timer
{
    public:
        Timer(unsigned long sec = 0, bool ticking = false);
        void start();
        void stop();

    private:
        std::atomic<unsigned long> sec_;
        std::atomic<bool> ticking_;
        void run();
        //what's that mysterious ticking sound...?
};
#endif