#include "Timer.h"

using namespace std;

int main()
{
    Timer t;
    string line, word;
    while(getline(cin, line))
    {
        istringstream iss(line);

        if(!(iss >> word)) {
            break;
        } else {
            switch(word.front())
            {
                case 's':
                    t.start();
                    break;
                case 'x':
                    t.stop();
                    return 0;
                    break;
                case 'h':
                    t.stop();
                    break;
            }
        }
    }
}