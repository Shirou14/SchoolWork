#include "Timer.h"

Timer::Timer(unsigned long sec, bool ticking):sec_(sec), ticking_(ticking)
    {
        std::thread t_(&Timer::run, this);
        t_.detach();
    }

void Timer::start()
    {
        ticking_ = true;
    }

void Timer::stop()
    {
        ticking_ = false;
    }

void Timer::run()
    {
        
        while(1){
       // std::cout<< "Hey! Listen!" << std::endl;
            if(ticking_)
            {
                std::this_thread::sleep_for(std::chrono::seconds(1));
                ++sec_;
                std::cout << sec_ << std::endl;
            }
        }
    }

