#include "Basic.h"
#ifndef STUDENTFACTORY_H
#define STUDENTFACTORY_H
#include "Student.h"

class StudentFactory
{
    private:
        std::istream *pis_;

    public:
        StudentFactory(std::istream& is):*pis_(&is){}
        std::shared_ptr<Student> create()
        {
            std::string type;
            if(!(*pis_ >> type))
                return 0;
            
            if(type == typeid(Student).name())
                return std::shared_ptr<Student>(new Student(*pis_));
        }

}

#endif