#include "Student.h"

using namespace std;

//size_t OptionStudent::count_ = 0;

void Student::display(ostream& os) const
{
    os<<"ID: "<<id_<<endl;
    os<<"Name: "<<name_.first<<" "<<name_.second<<endl;
}

Student::Student(istream& is)
{
    string id, fname, lname;

    if(!(is >> id >> fname >> lname))
        throw"EHHHHHH couldn't student.'";

    name_ = make_pair(fname, lname);
    id_ = id;

    std::cout<<"We have made a student, id: " << id_ <<", Name: " <<name_.first<<" "<<name_.second<<std::endl;
}

std::istream operator >> (std::istream& is, Student st)
{
    std::string id, fn, ln;

    is>>id>>fn>>ln;

    st = Student(id, fn, ln);
}

std::ostream operator << (std::ostream& os, Student st)
{
    os<<st.id_<<' '<<st.name_.first<<' '<<st.name_.second<<endl;
}

/*void OptionStudent::display(ostream& os) const
{
    Student::display(os);
    os<<"Term: "<<term_<<endl;
    os<<"Option: "<<option_<<endl;
}

OptionStudent::OptionStudent(string fname, string lname, string id, int term, string option):Student(id, fname, lname),
                                                                                                         term_(term),
                                                                                                         option_(option)
{
    ++count_;
}

OptionStudent::OptionStudent(const OptionStudent& os):Student(os.id_, os.name_.first, os.name_.second), 
                                                      term_(os.term_), 
                                                      option_(os.option_)
                                                       //name_(os.name_), id_(os.id_)
{
    ++count_;
}

OptionStudent::OptionStudent(OptionStudent&& os):Student(os.id_, os.name_.first, os.name_.second),
                                                  term_(os.term_),
                                                  option_(os.option_)
{}  


std::istream operator >> (std::istream& is, OptionStudent ost)
{
   return is>>(Student&)ost>>ost.term_>>ost.option_;
}

std::ostream operator << (std::ostream& os, OptionStudent ost)
{
    os<<(const Student&)ost<<' '<<ost.term_<<' '<<ost.option_<<endl;
}*/