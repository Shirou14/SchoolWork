#include "Basic.h"

class Name
{
    private:
        std::string first_, last_;
        bool isValidName(std::string name)
        {
            return true;
        }
    public:
        explicit Name(const std::string& first ="John", const std::string& last = "Doe")
        :first_(first), last_(last)
        {
            if(!isValidName(first_, last_))
                throw"Name Constructor: (const std::string&, const std::string&) This name is invalid, although it should never hit this.";
        }

        std::string getFirst() const {return first_}
        std::string getLast() const {return last_}

        bool setFirst(const std::string& fname)
        {
            if(!isValid(fname))
                return false;

            fname_ = fname;
            return true;
        }
}