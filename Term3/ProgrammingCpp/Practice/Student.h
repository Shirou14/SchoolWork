
#ifndef STUDENT_H
#define STUDENT_H
#include <iostream>
#include <string>
#include <map>


typedef std::pair<std::string, std::string> Name;

class Student{
    public:
        //provide ctor
        //Overload operators <<, >>
        virtual ~Student(){}
        Student(std::string id, std::string firstN, std::string lastN):name_(firstN, lastN)
        {
            try{
                if(!isValidId(id))
                {
                    throw "Student (string, string, string) did not have a valid ID";
                }else{
                    id_ = id;
                }
            }catch(const char *s){
                std::cerr<<s<<std::endl;
            }
        }

        Student(std::istream& is);

        virtual void display(std::ostream& os) const;

        friend std::ostream operator << (std::ostream&, Student);

    protected:
        std::string id_; //e.g. a11111111
        Name name_; //e.g. Homer Simpson

    private:
        static bool isValidId(const std::string& id)
        {
            auto beg = id.begin();
            int iii = 0;
            if(*beg != 'a')
                return false;
            ++beg;
            for(; beg != id.end(); ++beg, ++iii)
            {
                if(!isdigit(*beg))
                {
                    return false;
                }
            }
            if(iii == 8)
                return true;

            return false;
        }
        //additional helpers if necessary
};



/*class OptionStudent: public Student
{
public:
    //provide suitable ctors and dtors 
    OptionStudent(std::string fname, std::string lname, std::string id, int term, std::string option);
    OptionStudent(const OptionStudent& os);
    OptionStudent(OptionStudent&& os);

    static size_t getCount() {return count_;}
    virtual void display(std::ostream&) const; //for pretty printing

    friend std::ostream& operator<<(std::ostream& os, const OptionStudent& s);
    friend std::istream& operator>>(std::istream& is, const OptionStudent& s);

protected:
    int term_;
    std::string option_;
    static size_t count_;
};*/
#endif