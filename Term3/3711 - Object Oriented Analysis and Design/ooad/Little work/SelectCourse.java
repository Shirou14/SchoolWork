//Source file: C:\\Users\\Student\\Desktop\\SelectCourse.java


public class SelectCourse 
{
   public Course theCourse[];
   
   /**
    * @roseuid 5707CB940245
    */
   public SelectCourse() 
   {
    
   }
   
   /**
    * @return Void
    * @roseuid 570584730361
    */
   public Void AddCourse() 
   {
	   System.out.println("this Class: " + this.getClass().getSimpleName() + " method: " + Thread.currentThread().getStackTrace()[1].getMethodName());
    return null;
   }
   
   /**
    * @return Void
    * @roseuid 5705847C01C2
    */
   public Void DeleteCourse() 
   {
	   System.out.println("this Class: " + this.getClass().getSimpleName() + " method: " + Thread.currentThread().getStackTrace()[1].getMethodName());
    return null;
   }
   
   /**
    * @return Void
    * @roseuid 5705848402B7
    */
   public Void Review() 
   {
	   System.out.println("this Class: " + this.getClass().getSimpleName() + " method: " + Thread.currentThread().getStackTrace()[1].getMethodName());
    return null;
   }
   
   /**
    * @return Void
    * @roseuid 5705848C01AF
    */
   public Void Print() 
   {
	   System.out.println("this Class: " + this.getClass().getSimpleName() + " method: " + Thread.currentThread().getStackTrace()[1].getMethodName());
    return null;
   }
   
   /**
    * @return Void
    * @roseuid 57058496014A
    */
   public Void Quit() 
   {
	   System.out.println("this Class: " + this.getClass().getSimpleName() + " method: " + Thread.currentThread().getStackTrace()[1].getMethodName());
    return null;
   }
}
