import java.util.Date;

//Source file: C:\\Users\\Student\\Desktop\\Course.java


public class Course 
{
   private Integer CreditNumber;
   private String CourseDiscription;
   private Date StartDate;
   private Date EndDate;
   private String CourseOutline;
   public Schedule theSchedule;
   public CourseSection theCourseSection;
   public Semester theSemester;
   
   /**
    * @roseuid 5707CFAC01C3
    */
   public Course() 
   {
    
   }
   
   /**
    * @return Void
    * @roseuid 570587F5039C
    */
   public Void getAvailableCourses() 
   {
	   System.out.println("this Class: " + this.getClass().getSimpleName() + " method: " + Thread.currentThread().getStackTrace()[1].getMethodName());
    return null;
   }
   
   /**
    * @return String
    * @roseuid 5705897303D0
    */
   public String getCourseDiscription() 
   {
	   System.out.println("this Class: " + this.getClass().getSimpleName() + " method: " + Thread.currentThread().getStackTrace()[1].getMethodName());
    return null;
   }
   
   /**
    * @return Void
    * @roseuid 5705898602E0
    */
   public Void getStartandEndDate() 
   {
	   System.out.println("this Class: " + this.getClass().getSimpleName() + " method: " + Thread.currentThread().getStackTrace()[1].getMethodName());
    return null;
   }
}
