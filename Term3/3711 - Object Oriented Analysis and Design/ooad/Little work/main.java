
public class main {
	public static void main(String [] args){
		// course
		new Course().getAvailableCourses();
		new Course().getCourseDiscription();
		new Course().getStartandEndDate();
		// Course section
		new CourseSection().getCourseDayOfWeek();
		new CourseSection().getCourseTime();
		new CourseSection().getCourse();
		//semester
		new Semester().GetCurrentSemester();
		new Semester().GetFutureSemester();
		//select course
		new SelectCourse().AddCourse();
		new SelectCourse().DeleteCourse();
		new SelectCourse().Review();
		new SelectCourse().Quit();
		new SelectCourse().Print();
		//schedule
		new Schedule().BuildSchedule();
		new Schedule().getCourses();
		//instructor
		new Instructor().GetInstructorID();
		new Instructor().GetInstructorPassword();
		new Instructor().SubmitRegestration();
		//dbaccess
		new DBAccess().update();
		new DBAccess().Login();
		new DBAccess().getInstructorInfo();
		new DBAccess().getCourseInfo();
		new DBAccess().getAvailableCourses();
		//registration
		new Registration().getCourseID();
		new Registration().register();
		//registrar
		new Registrar().updateRegistration();
		new Registrar().getRegistrarID();
		new Registrar().getRegistrarPassword();
		//course catalogue
		new CourseCatalogue().buildCatalogue();
		
	}
}
