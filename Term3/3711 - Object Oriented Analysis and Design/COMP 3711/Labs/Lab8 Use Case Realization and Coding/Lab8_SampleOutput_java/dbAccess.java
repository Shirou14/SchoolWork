//Source file: H:\\MyCourses\\COMP3711\\LAB8\\dbAccess.java


public class dbAccess 
{
   public OptionalProducts theOptionalProducts;
   public VehicleInventory theVehicleInventory[];
   public Vehicle theVehicle;
   public Location theLocation[];
   public AwardProgram theAwardProgram[];
   public CustomerProfile theCustomerProfile[];
   public Reservation theReservation;
   
   /**
    * @roseuid 550C451701F6
    */
   public dbAccess() 
   {
    
   }
   
   /**
    * @roseuid 54DE328D03C6
    */
   public void sendVehicleOptions(String[] options) 
   {
	   options = new String[5];
	   options[0] = "compact";
	   options[1] = "intermediate";
	   options[2] = "full-size";
	   options[3] = "mini-van";
	   options[4] = "SUV";
	   
	   System.out.println("Message");
	   System.out.println("className: dbAccess ");
	   System.out.println("methodName: sendVehicleOptions()\n");
   }
   
   /**
    * @roseuid 54DE327E0235
    */
   public void sendVehicleDetails(String details) 
   {
	   System.out.println("Message");
	   System.out.println("className: dbAccess ");
	   System.out.println("methodName: sendVehicleDetails()\n");
   }
   
   /**
    * @roseuid 54DE32A00317
    */
   public void sendOptionalProd(String[] optionalProds) 
   {
	   optionalProds = new String[5];
	   optionalProds[0] = "childSeats";
	   optionalProds[1] = "GPS";
	   optionalProds[2] = "fuelService-size";
	   optionalProds[3] = "accInsurance";
	   optionalProds[4] = "lossDamWaiver";
	   
	   System.out.println("Message");
	   System.out.println("className: dbAccess ");
	   System.out.println("methodName: sendOptionalProd()\n");
   }
   
   /**
    * @roseuid 54DE32B700C0
    */
   public void checkCustomerProf(String check) 
   {
	   System.out.println("Message");
	   System.out.println("className: dbAccess ");
	   System.out.println("methodName: checkCustomerProf()\n");
   }
   
   /**
    * @roseuid 54DE32F10103
    */
   public void sendExistingProfile(String send) 
   {
	   System.out.println("Message");
	   System.out.println("className: dbAccess ");
	   System.out.println("methodName: sendExistingProfile()\n");
   }
   
   /**
    * @roseuid 54DE32FC0362
    */
   public void sendDiscounts(String discounts) 
   {
	   System.out.println("Message");
	   System.out.println("className: dbAccess ");
	   System.out.println("methodName: sendDiscounts()\n");
   }
   
   /**
    * @roseuid 54DE32C30280
    */
   public void addReservationID(int id) 
   {
	   System.out.println("Message");
	   System.out.println("className: dbAccess ");
	   System.out.println("methodName: addReservationID()\n");
   }
}
