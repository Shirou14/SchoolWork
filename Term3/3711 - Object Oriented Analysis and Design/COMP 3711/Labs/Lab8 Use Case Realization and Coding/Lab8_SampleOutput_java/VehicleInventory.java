//Source file: H:\\MyCourses\\COMP3711\\LAB8\\VehicleInventory.java


public class VehicleInventory 
{
   private String compact;
   private String intermediate;
   private String fullSize;
   private String miniVan;
   private String SUV;
   public Location theLocation;
   
   /**
    * @roseuid 550C48EB003A
    */
   public VehicleInventory() 
   {
    
   }
   
   /**
    * @roseuid 54DE33300174
    */
   public void sendVehicleOptions(String[] options) 
   {
	   options = new String[5];
	   options[0] = compact;
	   options[1] = intermediate;
	   options[2] = fullSize;
	   options[3] = miniVan;
	   options[4] = SUV;
	   
	   System.out.println("Message");
	   System.out.println("className: VehicleInventory ");
	   System.out.println("methodName: sendVehicleOptions()\n");
   }
}
