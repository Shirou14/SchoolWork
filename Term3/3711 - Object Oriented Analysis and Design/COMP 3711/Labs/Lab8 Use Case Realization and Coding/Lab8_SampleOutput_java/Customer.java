//Source file: H:\\MyCourses\\COMP3711\\LAB8\\Customer.java


public class Customer 
{
   private String firstName;
   private String lastName;
   private int telephone;
   private String email;
   private String contactInfo = firstName + lastName + telephone + email;
   public AwardProgram theAwardProgram;
   public UCcontroller theUCcontroller[];
   
   /**
    * @roseuid 550C45170052
    */
   public Customer() 
   {
    
   }
   
   /**
    * @roseuid 54DE30CB035F
    */
   public void navigateWeb(String url) 
   {
	   System.out.println("Message");
	   System.out.println("className: Customer ");
	   System.out.println("methodName: navigateWeb()\n");
   }
   
   /**
    * @roseuid 54DE30D50289
    */
   public void enterLocationPref(String location) 
   {
	   System.out.println("Message");
	   System.out.println("className: Customer ");
	   System.out.println("methodName: enterLocationPref()\n");
   }
   
   /**
    * @roseuid 54DE30E700D4
    */
   public void enterDatePref(String date) 
   {
	   System.out.println("Message");
	   System.out.println("className: Customer ");
	   System.out.println("methodName: enterDatePref()\n");
   }
   
   /**
    * @roseuid 54DE310B0080
    */
   public void submitPref(String confirm) 
   {
	   System.out.println("Message");
	   System.out.println("className: Customer ");
	   System.out.println("methodName: submitPref()\n");
   }
   
   /**
    * @roseuid 54DE31170031
    */
   public void requestVehicleDetails(String req) 
   {
	   System.out.println("Message");
	   System.out.println("className: Customer ");
	   System.out.println("methodName: requestVehicleDetails()\n");
   }
   
   /**
    * @roseuid 54DE31260044
    */
   public void enterVehiclePref(String vehicle) 
   {
	   System.out.println("Message");
	   System.out.println("className: Customer ");
	   System.out.println("methodName: enterVehiclePref()\n");
   }
   
   /**
    * @roseuid 54DE31340056
    */
   public void enterOptionalProd(String opProd) 
   {
	   System.out.println("Message");
	   System.out.println("className: Customer ");
	   System.out.println("methodName: enterOptionalProd()\n");
   }
   
   /**
    * @roseuid 54DE313E035F
    */
   public void enterContactInfo(String cInfo) 
   {
	   cInfo = contactInfo;
	   System.out.println("Message");
	   System.out.println("className: Customer ");
	   System.out.println("methodName: enterContactInfo()\n");
   }
   
   /**
    * @roseuid 54DE314E03D2
    */
   public void enterAwardsID(int id) 
   {
	   System.out.println("Message");
	   System.out.println("className: Customer ");
	   System.out.println("methodName: enterAwardsID()\n");
   }
   
   /**
    * @roseuid 54DE31730038
    */
   public void confirmReservation(String confirm) 
   {
	   System.out.println("Message");
	   System.out.println("className: Customer ");
	   System.out.println("methodName: confirmReservation()\n");
   }
}
