public class Main {

	public static void main(String[] args) {

		// String[] programs = new String[3];
		String[] vehicleOps = new String[5];
		String[] opProds = new String[5];

		/*
		 * AwardProgram ap = new AwardProgram(); ap.sendAwardsChoices(12345,
		 * programs);
		 */

		Customer c = new Customer();
		c.navigateWeb("www.budget.com");
		c.enterLocationPref("Vancouver");
		c.enterDatePref("March 22, 2015");
		c.submitPref("Confirmed");
		c.requestVehicleDetails("Request");
		c.enterVehiclePref("Subaru Impreza");
		c.enterOptionalProd("Childseats");
		c.enterContactInfo("Julia Yiu 6041234567 rental@rental.com");
		c.enterAwardsID(12345);
		c.confirmReservation("Confirmed");

		/*
		 * CustomerProfile cp = new CustomerProfile();
		 * cp.sendCustProfile("Previous", "Preferences");
		 */

		dbAccess db = new dbAccess();
		db.sendVehicleOptions(vehicleOps);
		db.sendVehicleDetails("Vehicle Details");
		db.sendOptionalProd(opProds);
		db.checkCustomerProf("Check profile");
		db.sendExistingProfile("Send Exisiting Profile");
		db.sendDiscounts("Discounts");
		db.addReservationID(12345);

		Location l = new Location();
		l.sendLocationDetails("Vancouver");

		/*
		 * OptionalProducts op = new OptionalProducts();
		 * op.sendOptionalProd(opProds);
		 */

		Reservation r = new Reservation();
		r.sendReservationEmail("Vancouver", "March 22, 2015", "Vancouver",
				"December 22, 2015", "Confirm");

		/*
		 * UCcontroller ucc = new UCcontroller(); ucc.promptLocationPref();
		 * ucc.promptDatePref(); ucc.addReservationDetails();
		 * ucc.requestVehicleOptions(); ucc.presentVehicleOptions();
		 * ucc.requestVehicleDetails(); ucc.presentVehicleDetails();
		 * ucc.requestOptionalProd(); ucc.presentOptionalProd();
		 * ucc.promptContactInfo(); ucc.requestCustomerProf();
		 * ucc.promptAwardsID(); ucc.requestAwardsStanding();
		 * ucc.presentAwardsOptions(); ucc.applyDiscount();
		 * ucc.presentReservationSummary(); ucc.generateReservationID();
		 * ucc.sendReservationEmail(); ucc.storeReservationID();
		 */

		Vehicle v = new Vehicle();
		v.sendVehicleDetails("Subaru 4 seater", 90);

		VehicleInventory vi = new VehicleInventory();
		vi.sendVehicleOptions(vehicleOps);
	}

}
