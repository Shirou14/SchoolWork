//Source file: H:\\MyCourses\\COMP3711\\LAB8\\UCcontroller.java


public class UCcontroller 
{
   public dbAccess theDbAccess;
   public Customer theCustomer;
   
   /**
    * @roseuid 550C47C90083
    */
   public UCcontroller() 
   {
    
   }
   
   /**
    * @roseuid 54DE3397023F
    */
   public void promptLocationPref() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: promptLocationPref()\n");

   }
   
   /**
    * @roseuid 54DE33A602F8
    */
   public void promptDatePref() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: promptDatePref()\n");

   }
   
   /**
    * @roseuid 54DE33AE00A1
    */
   public void addReservationDetails() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: addReservationDetails()\n");

   }
   
   /**
    * @roseuid 54DE33BA0341
    */
   public void requestVehicleOptions() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: sendReservationEmail()\n");

   }
   
   /**
    * @roseuid 54DE33C90319
    */
   public void presentVehicleOptions() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: presentVehicleOptions()\n");

   }
   
   /**
    * @roseuid 54DE33FD03DC
    */
   public void requestVehicleDetails() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: requestVehicleDetails()\n");

   }
   
   /**
    * @roseuid 54DE340800D4
    */
   public void presentVehicleDetails() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: presentVehicleDetails()\n");

   }
   
   /**
    * @roseuid 54DE3454002E
    */
   public void requestOptionalProd() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: requestOptionalProd()\n");

   }
   
   /**
    * @roseuid 54DE345B0356
    */
   public void presentOptionalProd() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: presentOptionalProd()\n");

   }
   
   /**
    * @roseuid 54DE34680127
    */
   public void promptContactInfo() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: promptContactInfo()\n");

   }
   
   /**
    * @roseuid 54DE34760028
    */
   public void requestCustomerProf() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: requestCustomerProf()\n");

   }
   
   /**
    * @roseuid 54DE348901F9
    */
   public void promptAwardsID() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: promptAwardsID()\n");
   }
   
   /**
    * @roseuid 54DE348E00D9
    */
   public void requestAwardsStanding() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: requestAwardsStanding()\n");
   }
   
   /**
    * @roseuid 54DE34AE01FA
    */
   public void presentAwardsOptions() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: presentAwardsOptions()\n");
   }
   
   /**
    * @roseuid 54DE34C20002
    */
   public void applyDiscount() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: applyDiscount()\n");
   }
   
   /**
    * @roseuid 54DE34D802A5
    */
   public void presentReservationSummary() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: presentReservationSummary()\n");
   }
   
   /**
    * @roseuid 54DE34F20035
    */
   public void generateReservationID() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: generateReservationID()\n");
   }
   
   /**
    * @roseuid 54DE3508029D
    */
   public void sendReservationEmail() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: sendReservationEmail()\n");
   }
   
   /**
    * @roseuid 54DE351A01EE
    */
   public void storeReservationID() 
   {
	   System.out.println("Message");
	   System.out.println("className: UCcontroller ");
	   System.out.println("methodName: storeReservationID()\n");
   }
}
