class main{

  public static void main(String[] args){

    ATM atm = new ATM();

    atm.readSerialNo();
    atm.requestPassword();
    atm.displayMenu();
    atm.ejectCard();
    atm.displayAccountData();
    atm.dispenseCash();
    atm.requestAccountData();


    User user = new User();
    user.requestAccount();

    Account acct = new Account();
    acct.currentBalance();
    acct.getAccount();

    BankSystem bs = new BankSystem();
    bs.getAccountData();
    bs.getWithdrawalLimit();
    bs.verifyPassword();

    Transaction ts = new Transaction();
    ts.deposit();
    ts.withdraw();
    ts.transfer();

    Session ss = new Session();
    ss.selectAccount();
    ss.createTransaction();
    ss.processTransaction();
    ss.getCurrentSession();

  }

}