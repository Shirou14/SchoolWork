class Account
{
    public Account()
    {
        System.out.println("Class: Account;\t\t Method: Constructor;\t\t Accessing Account");
    }

    public void currentBalance(){
        System.out.println("Class: Account;\t\t Method: currentBalance;\t\t");
    }

    public void getAccount(){
        System.out.println("Class: Account;\t\t Method: getAccount;\t\t retrieving account variable");
    }
}