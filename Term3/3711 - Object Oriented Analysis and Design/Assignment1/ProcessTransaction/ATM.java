class ATM
{
    public Account accnt;
    public Transaction trans;

    public ATM()
    {
        System.out.println("Class: ATM;\t\t Method: Constructor;\t\t Accessing ATM");
        // accnt = new Account();
        // beginTransaction();
    }

    public void beginTransaction()
    {
        System.out.println("Class: ATM;\t\t Method: beginTransaction;\t Initiating a new Transaction");
        // trans = new Transaction();
        // selectAccount();
        // promptForAmount();
        // trans.verifyTransactionAmount();
        // trans.verifyAccountBalance();
    }

    public void selectAccount()
    {
        System.out.println("Class: ATM;\t\t Method: selectAccount;\t\t Selecting account");
    }

    public void promptForAmount()
    {
        System.out.println("Class: ATM;\t\t Method: promptForAmount;\t Prompting user for Transaction amount");
    }

    public void dispenseCash()
    {
        System.out.println("Class: ATM;\t\t Method: dispenseCash;\t\t Dispensing funds requested by User");
    }

    public void readSerialNo(){
        System.out.println("Class: ATM;\t\t Method: readSerialNo;\t\t reading Serial Number");   
    }

    public void requestPassword(){
        System.out.println("Class: ATM;\t\t Method: requestPassword;\t\t requesting passwod from bank");      
    }

    public void displayMenu(){
        System.out.println("Class: ATM;\t\t Method: displayMenu;\t\t Displaying Menu to user");         
    }

    public void ejectCard(){
        System.out.println("Class: ATM;\t\t Method: ejectCard;\t\t removing Card");         
    }

    public void displayAccountData(){
        System.out.println("Class: ATM;\t\t Method: displayAccountData;\t\t displaying account Data to user");            
    }

    public void requestAccountData(){
        System.out.println("Class: ATM;\t\t Method: requestAccountData;\t\t requestAccountData account Data from bank");               
    }

}