class BankSystem
{
    public BankSystem()
    {
        System.out.println("Class: BankLink;\t Method: Constructor;\t\t Contacting Bank");
    }

    public void getAccountData()
    {
        System.out.println("Class: BankSystem;\t Method: getAccountData;\t\t Getting Account Information");
    }

    public void getWithdrawalLimit(){
        System.out.println("Class: BankSystem;\t Method: getWithdrawalLimit;\t Getting withdrawal limit"); 
    }

    public void verifyPassword(){
        System.out.println("Class: BankSystem;\t Method: verifyPassword;\t verification"); 
    }
}