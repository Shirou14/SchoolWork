class Session
{
    public BankLink bLink;

    public Session()
    {
        System.out.println("Class: Session;\t\t Method: Constructor;\t\t ");
    }

    public void selectAccount()
    {
        System.out.println("Class: Session;\t\t Method: selectAccount;\t ");
    }

    public void createTransaction()
    {
        System.out.println("Class: Session;\t\t Method: createTransaction;\t ");
    }

    public void processTransaction()
    {
        System.out.println("Class: Session;\t\t Method: processTransaction;\t ");
    }

    public void getCurrentSession()
    {
        System.out.println("Class: Session;\t\t Method: getCurrentSession;\t ");
    }
}