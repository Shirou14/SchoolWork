package QueryAccount;

class BankLink
{
    public BankLink()
    {
        System.out.println("Class: BankLink;\t Method: Constructor;\t Contacting Bank");
    }

    public void getData()
    {
        System.out.println("Class: BankLink;\t Method: getData;\t Getting Account Information");
    }
}