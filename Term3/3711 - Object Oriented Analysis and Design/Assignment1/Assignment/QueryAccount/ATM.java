package QueryAccount;

class ATM
{
    public Account accnt;
    public ConsortiumLink cLink;

    public ATM()
    {
        System.out.println("Class: ATM;\t\t Method: Constructor;\t Accessing ATM");
        accnt = new Account();
        getAccountInfo();
    }

    public void getAccountInfo()
    {
        cLink = new ConsortiumLink();
        System.out.println("Class: ATM;\t\t Method: getAccountInfo; Getting account Information");
        cLink.getData();
    }
}