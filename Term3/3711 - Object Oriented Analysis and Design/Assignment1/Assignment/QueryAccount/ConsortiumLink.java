package QueryAccount;

class ConsortiumLink
{
    public BankLink bLink;

    public ConsortiumLink()
    {
        System.out.println("Class: ConsortiumLink;\t Method: Constructor;\t Contacting Consortium");
    }

    public void getData()
    {
        System.out.println("Class: ConsortiumLink;\t Method: getData;\t Contacting Bank for data");
        bLink = new BankLink();
        bLink.getData();
    }
}