package ProcessTransaction;

class BankLink
{
    public BankLink()
    {
        System.out.println("Class: BankLink;\t Method: Constructor;\t\t Contacting Bank");
    }

    public void verifyAccountBalance()
    {
        System.out.println("Class: BankLink;\t Method: verifyAccountBalance;\t Getting Account Information");
    }
}