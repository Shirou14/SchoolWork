package ProcessTransaction;

class ConsortiumLink
{
    public BankLink bLink;

    public ConsortiumLink()
    {
        System.out.println("Class: ConsortiumLink;\t Method: Constructor;\t\t Contacting Consortium");
    }

    public void verifyAccountBalance()
    {
        System.out.println("Class: ConsortiumLink;\t Method: verifyAccountBalance;\t Contacting Bank for data");
        bLink = new BankLink();
        bLink.verifyAccountBalance();
    }
}