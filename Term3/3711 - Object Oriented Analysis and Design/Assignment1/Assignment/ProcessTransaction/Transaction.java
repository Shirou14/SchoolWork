package ProcessTransaction;

class Transaction
{
    public ConsortiumLink cLink;

    public Transaction()
    {
        System.out.println("Class: Transaction;\t Method: Constructor;\t\t Creating new Transaction");
    }

    public void verifyTransactionAmount()
    {
        System.out.println("Class: Transaction;\t Method: verifyTransactionAmount;Verifying amount matches policy");
    }

    public void verifyAccountBalance()
    {
        System.out.println("Class: Transaction;\t Method: verifyAccountBalance;\t Verifying Account contains needed funds.");
        cLink = new ConsortiumLink();
        cLink.verifyAccountBalance();
    }
}