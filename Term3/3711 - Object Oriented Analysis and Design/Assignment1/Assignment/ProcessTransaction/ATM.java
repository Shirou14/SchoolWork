package ProcessTransaction;

class ATM
{
    public Account accnt;
    public Transaction trans;

    public ATM()
    {
        System.out.println("Class: ATM;\t\t Method: Constructor;\t\t Accessing ATM");
        accnt = new Account();
        beginTransaction();
    }

    public void beginTransaction()
    {
        System.out.println("Class: ATM;\t\t Method: beginTransaction;\t Initiating a new Transaction");
        trans = new Transaction();
        selectAccount();
        promptForAmount();
        trans.verifyTransactionAmount();
        trans.verifyAccountBalance();
        dispenseFunds();
    }

    public void selectAccount()
    {
        System.out.println("Class: ATM;\t\t Method: selectAccount;\t\t Selecting account");
    }

    public void promptForAmount()
    {
        System.out.println("Class: ATM;\t\t Method: promptForAmount;\t Prompting user for Transaction amount");
    }

    public void dispenseFunds()
    {
        System.out.println("Class: ATM;\t\t Method: dispenseFunds;\t\t Dispensing funds requested by User");
    }
}