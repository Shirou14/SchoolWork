﻿using System;
using System.Collections.Generic;
using System.Text;

namespace asgn5v1
{
    public static class Calculations
    {
        public enum AXIS { X, Y, Z};

        public static double[,] ctrans = new double[4,4];
        public static double symbolHeight, symbolWidth;
        public static double[] symbolCenter = new double[3];

        public static void translateMatrix(double newX, double newY, double newZ = 0)
        {
            //new translate matrix
            double[,] tMatrix = { { 1, 0, 0, 0 }, { 0, 1, 0, 0 }, { 0, 0, 1, 0 }, { newX, newY, newZ, 1 } };
            //applies to ctrans
            multiplyMatrices(tMatrix);
        }

        public static void reflectMatrix(AXIS rAxis)
        {
            //create new matrix
            double[,] reMatrix = { {1,0,0,0 }, {0,1,0,0 }, {0,0,1,0 }, {0,0,0,1 } };

            //based on the desired reflection vector, apply the reflection
            switch(rAxis)
            {
                case AXIS.X:
                    {
                        reMatrix[0, 0] = -1;
                        break;
                    }
                case AXIS.Y:
                    {
                        reMatrix[1, 1] = -1;
                        break;
                    }
                case AXIS.Z:
                    {
                        reMatrix[2, 2] = -1;
                        break;
                    }
            }

            //apply to ctrans
            multiplyMatrices(reMatrix);
        }

        public static void rotationMatrix(AXIS rAxis, double angle)
        {
            double[,] roMatrix = new double[4,4];
            setIdentity(roMatrix, 4, 4);

            switch(rAxis)
            {
                case AXIS.X:
                    {
                        roMatrix[1, 1] = Math.Cos(angle);
                        roMatrix[1, 2] = -Math.Sin(angle);
                        roMatrix[2, 1] = Math.Sin(angle);
                        roMatrix[2, 2] = Math.Cos(angle);
                        break;
                    }
                case AXIS.Y:
                    {
                        roMatrix[0, 0] = Math.Cos(angle);
                        roMatrix[0, 2] = -Math.Sin(angle);
                        roMatrix[2, 0] = Math.Sin(angle);
                        roMatrix[2, 2] = Math.Cos(angle);
                        break;
                    }
                case AXIS.Z:
                    {
                        roMatrix[0, 0] = Math.Cos(angle);
                        roMatrix[0, 1] = -Math.Sin(angle);
                        roMatrix[1, 0] = Math.Sin(angle);
                        roMatrix[1, 1] = Math.Cos(angle);
                        break;
                    }
            }
            multiplyMatrices(roMatrix);
        }

        public static void scaleMatrix(double xScale, double yScale = 1, double zScale = 1)
        {
            //create new transform matrix
            double[,] scMatrix = { { xScale, 0, 0, 0 }, { 0, yScale, 0, 0 }, { 0, 0, zScale, 0 }, { 0, 0, 0, 1 } };
            //apply it to ctrans
            multiplyMatrices(scMatrix);
        }

        public static void shearMatrix(double shear)
        {
            double[] offset = findShapeCenterOffset();

            double[] yOffset = new double[3];
            double[,] shMatrix = new double[4, 4];

            setIdentity(shMatrix, 4, 4);

            yOffset[1] = -symbolCenter[0] * ctrans[0, 1] + -symbolCenter[2] * ctrans[1, 1] + -symbolCenter[2]* ctrans[2, 1];

            shMatrix[1, 0] = shear;

            translateMatrix(-offset[0], -offset[1], -offset[2]);
            translateMatrix(-yOffset[0], -yOffset[1], -yOffset[2]);

            //Perform the shear
            multiplyMatrices(shMatrix);
            //Translate the object back to it's original position
            translateMatrix(yOffset[0], yOffset[1], yOffset[2]);
            translateMatrix(offset[0], offset[1], offset[2]);
        }

        public static void center(double height, double width)
        {
            //get the size of the symbol
            double originalScale = height / 2 / symbolHeight;
            //get the middle of the screen
            double[] screenCenter = { width / 2, height / 2, 0 };
            //get the scale
            double[] originalScaleVector = {originalScale, -originalScale, originalScale };

            //set ctrans to empty, no transformation
            //createEmptyMatrix(ctrans);
            setIdentity(ctrans, 4, 4);

            //we scale to the new scale
            scaleMatrix(originalScaleVector[0], originalScaleVector[1], originalScaleVector[2]);

            //find how far off we are from the center
            double[] currentOffset = findShapeCenterOffset();

            //move to the center
            translateMatrix(screenCenter[0], screenCenter[1], screenCenter[2]);
            //move back based on the original offset
            translateMatrix(-currentOffset[0], -currentOffset[1], -currentOffset[2]);

        }

        public static void setIdentity(double[,] A, int nrow, int ncol)
        {
            for (int i = 0; i < nrow; i++)
            {
                for (int j = 0; j < ncol; j++) A[i, j] = 0.0d;
                A[i, i] = 1.0d;
            }
        }// end of setIdentity

        public static void multiplyMatrices(double[,] m1)
        {
            double[,] matrix = new double[4,4];

            for (int iii = 0; iii < 4; iii++)
            {
                for (int jjj = 0; jjj < 4; jjj++)
                {
                    for (int lll = 0; lll < 4; lll++)
                    {
                        matrix[iii, jjj] += (ctrans[iii, lll] * m1[lll, jjj]);
                    }
                }
            }
            ctrans = matrix;
        }

        public static double[] findShapeCenterOffset()
        {
            double[] offsetFromCentre = new double[3];

            offsetFromCentre[0] = symbolCenter[0] * ctrans[0, 0] + symbolCenter[1] * ctrans[1, 0] + symbolCenter[2] * ctrans[2, 0] + ctrans[3, 0];
            offsetFromCentre[1] = symbolCenter[0] * ctrans[0, 1] + symbolCenter[1] * ctrans[1, 1] + symbolCenter[2] * ctrans[2, 1] + ctrans[3, 1];
            offsetFromCentre[2] = symbolCenter[0] * ctrans[0, 2] + symbolCenter[1] * ctrans[1, 2] + symbolCenter[2] * ctrans[2, 2] + ctrans[3, 2];

            return offsetFromCentre;
        }

    }
}
