﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Assignment2
{
    public partial class Form1 : Form
    {
        SolidBrush bGreen, bBlue, bYellow, bRed;

        public Form1()
        {
            bGreen = new SolidBrush(Color.Green);
            bBlue = new SolidBrush(Color.Blue);
            bYellow = new SolidBrush(Color.Yellow);
            bRed = new SolidBrush(Color.Red);

            Text = "C4560:  Assignment 2, Exercise 1 (Jay Coughlan 4B/2017)";
            BackColor = Color.Black;
            ResizeRedraw = true;
            // enable double-buffering to avoid flicker
            // copied from http://www.publicjoe.f9.co.uk/csharp/card09.html
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.DoubleBuffer, true);

            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs pea)
        {
            Graphics grfx = pea.Graphics;

            grfx.SmoothingMode = SmoothingMode.HighQuality;
            grfx.PixelOffsetMode = PixelOffsetMode.HighQuality;

            int cx = ClientSize.Width;
            int cy = ClientSize.Height;
            int s = Math.Min(cx, cy), R, r, Mx, My;
            Mx = cx / 2;
            My = cy / 2;
            R = s / 6;
            r = R / 2;

            //here is where all of your drawing code would go.  For example, the following
            // statements produce a blue filled circle of diameter 50 pixels, with the upper 
            // hand corner of the bounding box of the circle being at x = 100, y = 200.

            SolidBrush brush = new SolidBrush(Color.Blue);
            grfx.FillEllipse(bBlue, (Mx - 5 * r), (My - 5 * r), R*2, R*2);
            grfx.FillEllipse(bBlue, (Mx - 5 * r), (My + r), R * 2, R * 2);
            grfx.FillEllipse(bYellow, (Mx + r), (My - 5 * r), R * 2, R * 2);
            grfx.FillEllipse(bYellow, (Mx + r), (My + r), R * 2, R * 2);
            grfx.FillEllipse(bGreen, (Mx - 2 * R), (My - 2 * R), R, R);
            grfx.FillEllipse(bRed, (Mx - 2 * R), (My + R), R, R);
            grfx.FillEllipse(bGreen, (Mx + R), (My - 2 * R), R, R);
            grfx.FillEllipse(bRed, (Mx + R), (My + R), R, R);
            
            //well this was fun. Who's up for chinese?
        }

    }
}
