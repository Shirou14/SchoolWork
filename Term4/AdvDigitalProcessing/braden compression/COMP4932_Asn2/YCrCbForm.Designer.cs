﻿namespace COMP4932_Asn2 {
    partial class YCrCbForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.YBox = new System.Windows.Forms.PictureBox();
            this.CrBox = new System.Windows.Forms.PictureBox();
            this.CbBox = new System.Windows.Forms.PictureBox();
            this.YLabel = new System.Windows.Forms.Label();
            this.CrLabel = new System.Windows.Forms.Label();
            this.CbLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.YBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CrBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // YBox
            // 
            this.YBox.Location = new System.Drawing.Point(0, 0);
            this.YBox.Margin = new System.Windows.Forms.Padding(0);
            this.YBox.Name = "YBox";
            this.YBox.Size = new System.Drawing.Size(250, 250);
            this.YBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.YBox.TabIndex = 0;
            this.YBox.TabStop = false;
            // 
            // CrBox
            // 
            this.CrBox.Location = new System.Drawing.Point(250, 0);
            this.CrBox.Margin = new System.Windows.Forms.Padding(0);
            this.CrBox.Name = "CrBox";
            this.CrBox.Size = new System.Drawing.Size(250, 250);
            this.CrBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.CrBox.TabIndex = 1;
            this.CrBox.TabStop = false;
            // 
            // CbBox
            // 
            this.CbBox.Location = new System.Drawing.Point(500, 0);
            this.CbBox.Margin = new System.Windows.Forms.Padding(0);
            this.CbBox.Name = "CbBox";
            this.CbBox.Size = new System.Drawing.Size(250, 250);
            this.CbBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.CbBox.TabIndex = 2;
            this.CbBox.TabStop = false;
            // 
            // YLabel
            // 
            this.YLabel.AutoSize = true;
            this.YLabel.Location = new System.Drawing.Point(6, 9);
            this.YLabel.Name = "YLabel";
            this.YLabel.Size = new System.Drawing.Size(14, 13);
            this.YLabel.TabIndex = 3;
            this.YLabel.Text = "Y";
            // 
            // CrLabel
            // 
            this.CrLabel.AutoSize = true;
            this.CrLabel.Location = new System.Drawing.Point(256, 9);
            this.CrLabel.Name = "CrLabel";
            this.CrLabel.Size = new System.Drawing.Size(17, 13);
            this.CrLabel.TabIndex = 3;
            this.CrLabel.Text = "Cr";
            // 
            // CbLabel
            // 
            this.CbLabel.AutoSize = true;
            this.CbLabel.Location = new System.Drawing.Point(507, 9);
            this.CbLabel.Name = "CbLabel";
            this.CbLabel.Size = new System.Drawing.Size(20, 13);
            this.CbLabel.TabIndex = 3;
            this.CbLabel.Text = "Cb";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.YLabel);
            this.groupBox1.Controls.Add(this.CbLabel);
            this.groupBox1.Controls.Add(this.CrLabel);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 250);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(750, 25);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // YCrCbForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 275);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CbBox);
            this.Controls.Add(this.CrBox);
            this.Controls.Add(this.YBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "YCrCbForm";
            this.Text = "YCrCbForm";
            this.Resize += new System.EventHandler(this.YCrCbFormResize);
            ((System.ComponentModel.ISupportInitialize)(this.YBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CrBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox YBox;
        private System.Windows.Forms.PictureBox CrBox;
        private System.Windows.Forms.PictureBox CbBox;
        private System.Windows.Forms.Label YLabel;
        private System.Windows.Forms.Label CrLabel;
        private System.Windows.Forms.Label CbLabel;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}