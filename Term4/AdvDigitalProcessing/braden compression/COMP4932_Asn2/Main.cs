﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace COMP4932_Asn2 {
    public partial class Main : Form {

        private ImageData leftImageData, rightImageData;

        public Main() {
            InitializeComponent();
        }

        /**
         * sets the and right PictureBoxes to be half of the screen each
         * 
         * @param sender - The object that is calling this function
         * @param e - the event arguments
         */
        private void mainFormResize(object sender, EventArgs e) {
            int width = this.ClientSize.Width / 2,
                height = this.ClientSize.Height - this.MenuStrip.Height - this.StatusBar.Height;

            LeftImage.SetBounds(0, this.MenuStrip.Height, width, height);
            RightImage.SetBounds(width, this.MenuStrip.Height, width, height);

            Invalidate();
        }

        /**
         * Opens the file selection dialog so the user can select a file to open
         * 
         * @param sender - The object that is calling this function
         * @param e - the event arguments
         */
        private void openSourceToolStripMenuItem_Click(object sender, EventArgs e) {
            this.OpenSourceDialog.ShowDialog();
        }

        /**
         * Opens the file selection dialog so the user can select a compressed file to open
         * 
         * @param sender - The object that is calling this function
         * @param e - the event arguments
         */
        private void openCompressedToolStripMenuItem_Click(object sender, EventArgs e) {
            this.OpenCompressedDialog.ShowDialog();
        }

        /**
         * 
         * Code for getting the pixel data: http://csharpexamples.com/fast-image-processing-c/
         * 
         * @param sender - The object that is calling this function
         * @param e - the event arguments
         */
        private void OpenSourceDialog_FileOk(object sender, CancelEventArgs e) {
            Image newImage = Image.FromStream(OpenSourceDialog.OpenFile());

            if (leftImageData == null) {
                leftImageData = new ImageData(newImage);
                LeftImage.Image = leftImageData.getBitmap();
            } else {
                rightImageData = new ImageData(newImage);
                RightImage.Image = rightImageData.getBitmap();
            }
            
            Invalidate();
        }

        /**
         * 
         * 
         * @param sender - The object that is calling this function
         * @param e - the event arguments
         */
        private void OpenCompressedDialog_FileOk(object sender, CancelEventArgs e) {
            BinaryReader reader = new BinaryReader(OpenCompressedDialog.OpenFile());

            if (leftImageData == null) {
                leftImageData = new ImageData(reader);
                LeftImage.Image = leftImageData.getBitmap();
            } else {
                rightImageData = new ImageData(reader);
                RightImage.Image = rightImageData.getBitmap();
            }

        }

        /**
         * starts the JPG compression (converts RBG image to YCrCb)
         * 
         * @param sender - The object that is calling this function
         * @param e - the event arguments
         */
        private void startJPGCompressToolStripMenuItem_Click(object sender, EventArgs e) {
            YCrCbForm YCrCbView;
            Bitmap YImage = new Bitmap(leftImageData.source.Width, leftImageData.source.Height),
                CrImage = new Bitmap(YImage),
                CbImage = new Bitmap(YImage);
            ColourYCrCb[,] values = ColourYCrCb.convertToYCrCb(leftImageData.getPixels());


            for (int x = 0; x < YImage.Width; x++) {
                for (int y = 0; y < YImage.Height; y++) {
                    YImage.SetPixel(x, y, values[x, y].getMonoY());
                    CrImage.SetPixel(x, y, values[x, y].getMonoCr());
                    CbImage.SetPixel(x, y, values[x, y].getMonoCb());
                }
            }

            YCrCbView = new YCrCbForm(YImage, CrImage, CbImage);
            YCrCbView.Show();
        }

        /**
         * Opens save file dialog
         * 
         * @param sender - The object that is calling this function
         * @param e - the event arguments
         */
        private void saveToolStripMenuItem_Click(object sender, EventArgs e) {
            SaveCompressedDialog.ShowDialog();
        }

        /// <summary>
        /// Displays the motion vectors on the original image
        /// </summary>
        /// <param name="sender">The object that is calling this function</param>
        /// <param name="e">the event arguments</param>
        private void viewMotionVectorsToolStripMenuItem_Click(object sender, EventArgs e) {
            Pen pen = new Pen(Color.LightGreen);
            pen.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
            Graphics g = RightImage.CreateGraphics(),
                     gg = LeftImage.CreateGraphics();
            double ratio = 0;
            int offsetX = 0, offsetY = 0;
            
            //get Imagebox scaling and offset
            if (LeftImage.Width / (double)leftImageData.source.Width < LeftImage.Height / (double)leftImageData.source.Height) {
                ratio = LeftImage.Width / (double)leftImageData.source.Width;
                offsetY = (LeftImage.Height - (int)(leftImageData.source.Height * ratio)) / 2;
            } else {
                ratio = LeftImage.Height / (double)leftImageData.source.Height;
                offsetX = (LeftImage.Width - (int)(leftImageData.source.Width * ratio)) / 2;
            }

            ColourYCrCb[,] leftColourValues = ColourYCrCb.convertToYCrCb(leftImageData.getPixels()),
                           rightColourValues = ColourYCrCb.convertToYCrCb(rightImageData.getPixels());

            double[,] leftYValues = new double[leftImageData.source.Width, leftImageData.source.Height],
                      rightYValues = new double[rightImageData.source.Width, rightImageData.source.Height];

            for (int x = 0; x < leftYValues.GetLength(0); x++) {
                for (int y = 0; y < leftYValues.GetLength(1); y++) {
                    leftYValues[x, y] = leftColourValues[x, y].y;
                    rightYValues[x, y] = rightColourValues[x, y].y;
                }
            }

            leftYValues = ImageData.getPaddedArray(leftYValues);
            rightYValues = ImageData.getPaddedArray(rightYValues);

            MotionVector[,] mv = new MotionVector[(int)Math.Ceiling(leftYValues.GetLength(0) / 8.0),
                                                  (int)Math.Ceiling(leftYValues.GetLength(1) / 8.0)];

            for (short x = 0, u = 0; x < leftYValues.GetLength(0); x += 8, u++) {
                for (short y = 0, v = 0; y < leftYValues.GetLength(1); y += 8, v++) {
                    mv[u, v] = new MotionVector(x, y);
                    mv[u, v].getSequentialPrediction(leftYValues, rightYValues, 15);

                    g.DrawLine(pen, offsetX + (int)(x * ratio), offsetY + (int)(y * ratio), offsetX + (int)((x + mv[u, v].predX) * ratio), offsetY + (int)((y + mv[u, v].predY) * ratio));
                    gg.DrawLine(pen, offsetX + (int)(x * ratio), offsetY + (int)(y * ratio), offsetX + (int)((x + mv[u, v].predX) * ratio), offsetY + (int)((y + mv[u, v].predY) * ratio));
                }
            }
        }

        /// <summary>
        /// Saves the ImageData to a new .bdc file
        /// </summary>
        /// <param name="sender">The object that is calling this function</param>
        /// <param name="e">the event arguments</param>
        private void SaveCompressedDialog_FileOk(object sender, CancelEventArgs e) {
            System.IO.FileStream fp = new System.IO.FileStream(SaveCompressedDialog.FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            System.IO.BinaryWriter writer = new System.IO.BinaryWriter(fp);

            leftImageData.saveCompressedImage(writer);
            
            displayCompressionRatio(leftImageData, 1, fp);

            if (writer != null) {
                writer.Close();
            }
            if (fp != null) {
                fp.Close();
            }
        }

        private void saveVideoToolStripMenuItem_Click(object sender, EventArgs e) {
            if (rightImageData.source.Width != leftImageData.source.Width ||
                rightImageData.source.Height != leftImageData.source.Height) {
                MessageBox.Show("Incompatable images, please make sure both images have the same dimentions");
                return;
            }
            SaveCompressedVideoDialog.ShowDialog();
        }

        private void SaveCompressedVideoDialog_FileOk(object sender, CancelEventArgs e) {
            new SearchRangeForm(this).ShowDialog();
        }

        public void saveVideo(int p) {
            System.IO.FileStream fp = new System.IO.FileStream(SaveCompressedVideoDialog.FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            System.IO.BinaryWriter writer = new System.IO.BinaryWriter(fp);
            ColourYCrCb[,] compressedValues;

            compressedValues = leftImageData.saveCompressedImage(writer);
            rightImageData.saveMotionVectors(writer, compressedValues, 15);

            displayCompressionRatio(leftImageData, 2, fp);

            if (writer != null) {
                writer.Close();
            }
            if (fp != null) {
                fp.Close();
            }
        }

        private void openCompressedVideoToolStripMenuItem_Click(object sender, EventArgs e) {
            OpenCompressedVideoDialog.ShowDialog();
        }

        private void OpenCompressedVideoDialog_FileOk(object sender, CancelEventArgs e) {
            BinaryReader reader = new BinaryReader(OpenCompressedVideoDialog.OpenFile());

            leftImageData = new ImageData(reader);
            LeftImage.Image = leftImageData.getBitmap();

            rightImageData = ImageData.readFromMotionVectors(reader, leftImageData);
            RightImage.Image = rightImageData.getBitmap();
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e) {
            leftImageData = rightImageData = null;
            LeftImage.Image = RightImage.Image = null;
        }

        private void displayCompressionRatio(ImageData image1, int numOfFrames, System.IO.FileStream fileStream) {
            Console.WriteLine("File Length: " + fileStream.Length + "bytes");
            CompressionStatusLabel.Text = "Compression Ratio: " + ((image1.source.Width * image1.source.Height * 3.0 * numOfFrames) / fileStream.Length);
        }
    }
}
