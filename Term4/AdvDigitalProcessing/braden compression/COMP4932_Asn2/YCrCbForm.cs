﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace COMP4932_Asn2 {
    public partial class YCrCbForm : Form {
        public YCrCbForm(Image Y, Image Cr, Image Cb) {
            InitializeComponent();

            YBox.Image = Y;
            CrBox.Image = Cr;
            CbBox.Image = Cb;
        }

        /**
         * sets the and right PictureBoxes to be half of the screen each
         * 
         * @param sender - The object that is calling this function
         * @param e - the event arguments
         */
        private void YCrCbFormResize(object sender, EventArgs e) {
            int width = this.ClientSize.Width / 3,
                height = this.ClientSize.Height - 25;

            YBox.SetBounds(0, 0, width, height);
            CrBox.SetBounds(width, 0, width, height);
            CbBox.SetBounds(2 * width, 0, width, height);

            YLabel.Left = 5;
            CrLabel.Left = width + 5;
            CbLabel.Left = 2 * width + 5;

            Invalidate();
        }
    }
}
