﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMP4932_Asn2 {
    class MotionVector {
        public short srcX, srcY;
        public sbyte predX, predY;
        private static int N = 8;

        public MotionVector(short x, short y) {
            srcX = x;
            srcY = y;
        }


        /// <summary>
        /// Sequential approach for finding motion vectors, uses the top left instead of the middle of the block
        /// </summary>
        /// <param name="targetImage">the newest frame</param>
        /// <param name="refImage">the previous I-frame</param>
        /// <param name="p">the distance to look for a match</param>
        /// <returns></returns>
        public double[,] getSequentialPrediction(double[,] refImage, double[,] targetImage, int p) {
            double minMAD = double.MaxValue,
                   curMAD;

            for (int u = -p; u <= p; u++) {
                if (srcX + u < 0)
                    continue;
                if (srcX + u + N - 1 >= refImage.GetLength(0))
                    break;
                for (int v = -p; v <= p; v++) {
                    if (srcY + v < 0)
                        continue;
                    if (srcY + v + N - 1 >= refImage.GetLength(1))
                        break;
                    curMAD = getMeanDifference(refImage, targetImage, u, v);

                    if (curMAD < minMAD) {
                        minMAD = curMAD;
                        predX = (sbyte)u;
                        predY = (sbyte)v;
                    }
                }
            }
            //check if all are the same
            curMAD = getMeanDifference(refImage, targetImage, 0, 0);

            if (curMAD == minMAD) {
                minMAD = curMAD;
                predX = 0;
                predY = 0;
            }

            return getMacroBlockDifference(refImage, targetImage, predX, predY);
        }

        /// <summary>
        /// returns the mean absolute difference between two macroblocks specified by the offsets
        /// </summary>
        /// <param name="targetImage">the JPGed frame</param>
        /// <param name="refImage">The reference frame that you are searching through</param>
        /// <param name="i">horizontal offset</param>
        /// <param name="j">vertical offset</param>
        /// <returns>the Mean Absolute Difference (MAD) of the two macroblocks</returns>
        private double getMeanDifference(double[,] refImage, double[,] targetImage, int i, int j) {
            double meanDifference = 0;

            for (int x = 0; x < N; x++)
                for (int y = 0; y < N; y++)
                    meanDifference += Math.Abs(targetImage[srcX + x, srcY + y]
                                - refImage[srcX + i + x, srcY + j + y]);
            meanDifference *= 1 / Math.Pow(N, 2);

            return meanDifference;
        }

        private double[,] getMacroBlockDifference(double[,] refImage, double[,] targetImage, int i, int j) {
            double[,] macro = new double[N, N];

            for (int x = 0; x < N; x++)
                for (int y = 0; y < N; y++)
                    macro[x, y] = targetImage[srcX + x, srcY + y]
                                - refImage[srcX + i + x, srcY + j + y];

            return macro;
        }
    }
}
