﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMP4932_Asn2 {
    class ImageData {
        public Bitmap source;
        private static double[,] luminousTable =
            {
                { 16, 11, 10, 16, 24, 40, 51, 61 },
                { 12, 12, 14, 19, 26, 58, 60, 55},
                { 14, 13, 16, 24, 40, 57, 69, 56},
                { 14, 17, 22, 29, 51, 87, 80, 62},
                { 18, 22, 37, 56, 68, 109, 103, 77},
                { 24, 35, 55, 64, 81, 104, 113, 92},
                { 49, 64, 78, 87, 103, 121, 120, 101},
                { 72, 92, 95, 98, 112, 100, 103, 99}
            },
            chrominanceTable =
            {
                { 17, 18, 24, 47, 99, 99, 99, 99 },
                { 18, 21, 26, 66, 99, 99, 99, 99 },
                { 24, 26, 56, 99, 99, 99, 99, 99 },
                { 47, 66, 99, 99, 99, 99, 99, 99 },
                { 99, 99, 99, 99, 99, 99, 99, 99 },
                { 99, 99, 99, 99, 99, 99, 99, 99 },
                { 99, 99, 99, 99, 99, 99, 99, 99 },
                { 99, 99, 99, 99, 99, 99, 99, 99 }
            },
            motionVectorTable =
            {
                { 16, 16, 16, 16, 16, 16, 16, 16 },
                { 16, 16, 16, 16, 16, 16, 16, 16 },
                { 16, 16, 16, 16, 16, 16, 16, 16 },
                { 16, 16, 16, 16, 16, 16, 16, 16 },
                { 16, 16, 16, 16, 16, 16, 16, 16 },
                { 16, 16, 16, 16, 16, 16, 16, 16 },
                { 16, 16, 16, 16, 16, 16, 16, 16 },
                { 16, 16, 16, 16, 16, 16, 16, 16 }

            };
        private static int[,] zigZag =
            {
                {0, 0},
                {1, 0}, {0, 1},
                {0, 2}, {1, 1}, {2, 0},
                {3, 0}, {2, 1}, {1, 2}, {0, 3},
                {0, 4}, {1, 3}, {2, 2}, {3, 1}, {4, 0},
                {5, 0}, {4, 1}, {3, 2}, {2, 3}, {1, 4}, {0, 5},
                {0, 6}, {1, 5}, {2, 4}, {3, 3}, {4, 2}, {5, 1}, {6, 0},
                {7, 0}, {6, 1}, {5, 2}, {4, 3}, {3, 4}, {2, 5}, {1, 6}, {0, 7},
                {1, 7}, {2, 6}, {3, 5}, {4, 4}, {5, 3}, {6, 2}, {7, 1},
                {7, 2}, {6, 3}, {5, 4}, {4, 5}, {3, 6}, {2, 7},
                {3, 7}, {4, 6}, {5, 5}, {6, 4}, {7, 3},
                {7, 4}, {6, 5}, {5, 6}, {4, 7},
                {5, 7}, {6, 6}, {7, 5},
                {7, 6}, {6, 7},
                {7, 7},
            };

        /**
         * Constructor that sets the image to its source image
         */
        public ImageData(Image image) {
            source = new Bitmap(image);
        }

        /**
         * Constructor that reads the compressed file from a IO sream
         */
        public ImageData(BinaryReader reader) {
            ColourYCrCb[,] YCrCbValues = readFromFile(reader);

            if (YCrCbValues != null) {
                source = new Bitmap(YCrCbValues.GetLength(0), YCrCbValues.GetLength(1));

                Console.WriteLine("setting pixels");
                setPixels(ColourYCrCb.convertToColor(YCrCbValues));
            } else {
                Console.WriteLine("Unable to read file");
            }
        }

        /**
         * returns the source bitmap
         * @return the source bitmap
         */
        public Bitmap getBitmap() {
            return source;
        }

        /**
         * Temperary getting of pixel data until working with bytes
         * 
         * @return Array of pixel values as colors
         */
        public Color[,] getPixels() {
            Color[,] pixels = new Color[source.Width, source.Height];

            for (int x = 0; x < source.Width; x++)
                for (int y = 0; y < source.Height; y++)
                    pixels[x, y] = source.GetPixel(x, y);

            return pixels;
        }

        /**
         * Temperary setting of pixels until working with bytes
         * 
         * @param newColours the Color array of the new bitmap
         */
        public void setPixels(Color[,] newColours) {
            for (int x = 0; x < source.Width; x++)
                for (int y = 0; y < source.Height; y++)
                    source.SetPixel(x, y, newColours[x, y]);
        }

        /// <summary>
        /// Writes a run to the file
        /// </summary>
        /// <param name="run"></param>
        /// <param name="val"></param>
        public void writeRun(BinaryWriter writer, sbyte run, sbyte val) {
            writer.Write((sbyte)0); //special character
            writer.Write(run);      //run length
            writer.Write(val);      //the number
        }

        /// <summary>
        /// applies a run length encoding to the block of data.  The special character is 0.
        /// </summary>
        /// <param name="writer">the binary writer that the data will be written to</param>
        /// <param name="block">the block of data from DCT</param>
        public void applyRunlengthEncoding(BinaryWriter writer, double[,] block) {
            sbyte prev = 0, cur, run = 1;

            for (int x = 0; x < zigZag.GetLength(0); x++) {
                if (x == 0) { //first time in for loop
                    prev = checkSByteRange(block[zigZag[x, 0], zigZag[x, 1]]);
                    continue;
                } else if (x == zigZag.GetLength(0) - 1) { //the last go
                    cur = checkSByteRange(block[zigZag[x, 0], zigZag[x, 1]]);

                    if (cur == prev) {
                        if (++run >= 3) {
                            writeRun(writer, run, prev);
                        } else {
                            if (prev == 0) {
                                writeRun(writer, run, prev);
                            } else {
                                for (int i = 0; i < run; i++)
                                    writer.Write(prev);
                            }
                        }
                    } else if (run >= 3) {
                        writeRun(writer, run, prev);

                        if (cur == 0) {
                            writeRun(writer, 1, cur);
                        } else {
                            writer.Write(cur);
                        }
                    } else {
                        if (prev == 0) {
                            writeRun(writer, run, prev);
                        } else {
                            for (int i = 0; i < run; i++)
                                writer.Write(prev);
                        }

                        if (cur == 0) {
                            writeRun(writer, 1, cur);
                        } else
                            for (int i = 0; i < 1; i++)
                                writer.Write(cur);
                    }
                    continue;
                }

                cur = checkSByteRange(block[zigZag[x, 0], zigZag[x, 1]]);
                if (cur == prev) {
                    run++;
                } else if (run >= 3) {
                    writeRun(writer, run, prev);

                    prev = cur;
                    run = 1;
                } else {
                    if (prev == 0) {
                        writeRun(writer, run, prev);
                    } else {
                        for (int i = 0; i < run; i++)
                            writer.Write(prev);
                    }
                    prev = cur;
                    run = 1;
                }
            }
        }

        /// <summary>
        /// undoes the run length encoding from a file input
        /// </summary>
        /// <param name="reader">the file to be read</param>
        /// <returns>the 8x8 DCT block from the file</returns>
        public static double[,] undoRunlengthEncoding(BinaryReader reader) {
            sbyte curVal, run; 
            double[,] values = new double[8, 8];

            for (int x = 0; x < zigZag.GetLength(0); x++) {
                curVal = reader.ReadSByte();
                if (curVal == 0) {
                    run = reader.ReadSByte();
                    curVal = reader.ReadSByte();

                    for (sbyte i = 0; i < run; i++, x++)
                        values[zigZag[x, 0], zigZag[x, 1]] = curVal;
                    x--;
                } else {
                    values[zigZag[x, 0], zigZag[x, 1]] = curVal;
                }
            }
            
            return values;
        }

        /// <summary>
        /// Saves the Image data to the specified binary writer file pointer
        /// 
        /// Header:
        ///     Int:    width
        ///     Int:    height
        /// Data: (with RLE)
        ///     bytes:  all Y data(as 8x8 blocks)
        ///     bytes:  Subsampled Cr, then Cb (as 8x8 blocks)
        /// </summary>
        /// <param name="writer">the binary file writer</param>
        public ColourYCrCb[,] saveCompressedImage(System.IO.BinaryWriter writer) {
            ColourYCrCb[,] values = ColourYCrCb.convertToYCrCb(getPixels());
            List<double[,]> yBlocks, crBlocks, cbBlocks;
            int subWidth = (int)Math.Ceiling(values.GetLength(0) / 2.0), 
                subHeight = (int)Math.Ceiling(values.GetLength(1) / 2.0);
            double[,] yArray = new double[values.GetLength(0), values.GetLength(1)],
                      crArray = new double[subWidth, subHeight],
                      cbArray = new double[subWidth, subHeight];
            
            //put Y values into an array
            for (int y = 0; y < yArray.GetLength(1); y++)
                for (int x = 0; x < yArray.GetLength(0); x++)
                    yArray[x, y] = values[x, y].y;
            
            //put Cr and Cb values into an array
            for (int y = 0, v = 0; y < subHeight; y++, v += 2) {
                for (int x = 0, u = 0; x < subWidth; x++, u += 2) {
                    crArray[x, y] = values[u, v].cr;
                    cbArray[x, y] = values[u, v].cb;
                }
            }

            //break them up into 8x8 blocks
            yBlocks = get8x8Blocks(yArray);
            crBlocks = get8x8Blocks(crArray);
            cbBlocks = get8x8Blocks(cbArray);

            //perform DCT on the 8x8 blocks
            //also divides the DCT values by the luminous and chrominance tables
            for (int i = 0; i < yBlocks.Count; i++) {
                yBlocks[i] = DCT(yBlocks[i]);
                divideTables(yBlocks[i], luminousTable);
            }
                

            for (int i = 0; i < crBlocks.Count; i++) {
                crBlocks[i] = DCT(crBlocks[i]);
                divideTables(crBlocks[i], chrominanceTable);

                cbBlocks[i] = DCT(cbBlocks[i]);
                divideTables(cbBlocks[i], chrominanceTable);
            }

            //write width and height
            writer.Write(source.Width);
            writer.Write(source.Height);

            for (int i = 0; i < yBlocks.Count; i++)
                applyRunlengthEncoding(writer, yBlocks[i]);

            for (int i = 0; i < crBlocks.Count; i++)
                applyRunlengthEncoding(writer, crBlocks[i]);

            for (int i = 0; i < cbBlocks.Count; i++)
                applyRunlengthEncoding(writer, cbBlocks[i]);


            //apply IDCT to return new values created

            for (int i = 0; i < yBlocks.Count; i++) {
                multiplyTables(yBlocks[i], luminousTable);
                yBlocks[i] = IDCT(yBlocks[i]);
            }

            for (int i = 0; i < crBlocks.Count; i++) {
                multiplyTables(crBlocks[i], chrominanceTable);
                multiplyTables(cbBlocks[i], chrominanceTable);
                crBlocks[i] = IDCT(crBlocks[i]);
                cbBlocks[i] = IDCT(cbBlocks[i]);
            }

            return blocksToYCrCb(yBlocks, crBlocks, cbBlocks, source.Width, source.Height);
        }

        /// <summary>
        /// checks if the rounded double value is within -128 and 127
        /// </summary>
        /// <param name="d">the value to be checked</param>
        /// <returns>the rounded number, within the sbyte value range</returns>
        public sbyte checkSByteRange(double d) {
            sbyte r = (sbyte)Math.Min(sbyte.MaxValue, Math.Round(d));
            return Math.Max(r, sbyte.MinValue);
        }

        /// <summary>
        /// arranges the source array into 8x8 blocks, padds out of range values with 0s
        /// </summary>
        /// <param name="src">The source array of doubles</param>
        /// <returns>List of 8x8 blocks</returns>
        public List<double[,]> get8x8Blocks(double[,] src) {
            List<double[,]> blocks = new List<double[,]>();
            double[,] curArray;

            int verticalBlocks = (int)Math.Ceiling(src.GetLength(0) / 8.0),
                horizontalBlocks = (int)Math.Ceiling(src.GetLength(1) / 8.0);

            for (int i = 0; i < horizontalBlocks; i++) { //each row
                for (int j = 0; j < verticalBlocks; j++) { //each column
                    blocks.Add(new double[8, 8]);
                    curArray = blocks[blocks.Count - 1];

                    for (int x = 0; x < 8; x++)
                        for (int y = 0; y < 8; y++)
                            if (j * 8 + x >= src.GetLength(0))
                                curArray[x, y] = 0;
                            else if (i * 8 + y >= src.GetLength(1))
                                curArray[x, y] = 0;
                            else 
                                curArray[x, y] = src[j * 8 + x, i * 8 + y];
                }
            }

            return blocks;
        }

        /// <summary>
        /// Pads the array so that i can be broken into 8x8 blocks
        /// </summary>
        /// <param name="srcArray">the original array</param>
        /// <returns>the padded array</returns>
        public static double[,] getPaddedArray(double[,] srcArray) {
            int width = 8 * (int)Math.Ceiling(srcArray.GetLength(0) / 8.0),
                height = 8 * (int)Math.Ceiling(srcArray.GetLength(1) / 8.0);
            double[,] paddedArray = new double[width, height];

            for (int x = 0; x < srcArray.GetLength(0); x++) {
                for (int y = 0; y < srcArray.GetLength(1); y++) {
                    paddedArray[x, y] = srcArray[x, y];
                }
            }

            return paddedArray;
        }

        /// <summary>
        /// Divides each value in the source table by the corresponding value in the table
        /// </summary>
        /// <param name="src">the source table to be divided</param>
        /// <param name="table">the table to be divided by, either luminous or chrominance</param>
        public static void divideTables(double[,] src, double[,] table) {
            for (int x = 0; x < src.GetLength(0); x++)
                for (int y = 0; y < src.GetLength(1); y++)
                    src[x, y] /= table[y, x];
        }

        /// <summary>
        /// Multiplies each value in the source table by the corresponding value in the table
        /// </summary>
        /// <param name="src">the source table to be multiplied</param>
        /// <param name="table">the table to be multiplied by, either luminous or chrominance</param>
        public static void multiplyTables(double[,] src, double[,] table) {
            for (int x = 0; x < src.GetLength(0); x++)
                for (int y = 0; y < src.GetLength(1); y++)
                    src[x, y] *= table[y, x];
        }

        /// <summary>
        /// Reads the compressed image file from a given file stream
        /// </summary>
        /// <param name="fileStream">the stream to the file</param>
        /// <returns>2D array of YCrCb values of the compressed image</returns>
        public ColourYCrCb[,] readFromFile(BinaryReader reader) {
            int width, height;
            List<double[,]> yBlocks = new List<double[,]>(),
                            crBlocks = new List<double[,]>(), 
                            cbBlocks = new List<double[,]>();
            ColourYCrCb[,] values = null;

            width = reader.ReadInt32();
            height = reader.ReadInt32();


            values = new ColourYCrCb[width, height];

            int verticalBlocks = (int)Math.Ceiling(width / 8.0),
                horizontalBlocks = (int)Math.Ceiling(height / 8.0),
                subWidthBlocks = (int)Math.Ceiling(Math.Ceiling(width / 2.0) / 8),
                subHeightBlocks = (int)Math.Ceiling(Math.Ceiling(height / 2.0) / 8);

            for (int i = 0; i < horizontalBlocks * verticalBlocks; i++) {
                yBlocks.Add(undoRunlengthEncoding(reader));

                //multiply by the quantization table
                multiplyTables(yBlocks[i], luminousTable);

                //perform IDCT on the array
                yBlocks[i] = IDCT(yBlocks[i]);
            }

            for (int i = 0; i < subWidthBlocks * subHeightBlocks; i++) {
                crBlocks.Add(undoRunlengthEncoding(reader));

                //multiply by the quantization table
                multiplyTables(crBlocks[i], chrominanceTable);

                //perform IDCT on the array
                crBlocks[i] = IDCT(crBlocks[i]);
            }
               
            for (int i = 0; i < subWidthBlocks * subHeightBlocks; i++) {
                cbBlocks.Add(undoRunlengthEncoding(reader));

                //multiply by the quantization table
                multiplyTables(cbBlocks[i], chrominanceTable);

                //perform IDCT on the array
                cbBlocks[i] = IDCT(cbBlocks[i]);
            }
            
            return blocksToYCrCb(yBlocks, crBlocks, cbBlocks, width, height);
        }

        public static ColourYCrCb[,] blocksToYCrCb(List<double[,]> YBlocks, List<double[,]> CrBlocks, List<double[,]> CbBlocks, int width, int height) {
            ColourYCrCb[,] newImage = new ColourYCrCb[width, height];
            int blocksWidth = (int)Math.Ceiling(width / 8.0),
                srcX, srcY;
            double[,] Cr = expandCrCbBlocks(CrBlocks, width, height),
                      Cb = expandCrCbBlocks(CbBlocks, width, height);

            for (int u = 0; u < blocksWidth; u++) {
                for (int v = 0; v < YBlocks.Count / blocksWidth; v++) {

                    for (int x = 0; x < 8 && u * 8 + x < width; x++) {
                        for (int y = 0; y < 8 && v * 8 + y < height; y++) {
                            srcX = u * 8 + x;
                            srcY = v * 8 + y;
                            newImage[srcX, srcY] = new ColourYCrCb(ColourYCrCb.checkRGBValue(YBlocks[v * blocksWidth + u][x, y]),
                                                                             ColourYCrCb.checkRGBValue(Cr[srcX, srcY]),
                                                                             ColourYCrCb.checkRGBValue(Cb[srcX, srcY]));
                        }
                    }

                }
            }
            return newImage;
        }

        public static double[,] expandCrCbBlocks(List<double[,]> blocks, int width, int height) {
            double[,] values = new double[width, height];
            int blocksWidth = (int)Math.Ceiling((width / 2.0) / 8.0),
                blocksHeight = (int)Math.Ceiling((height / 2.0) / 8.0);
            int srcX, srcY;

            for (int u = 0; u < blocksWidth; u++) {
                for (int v = 0; v < blocksHeight; v++) {

                    for (int x = 0; x < 8 && u * 16 + (x * 2) < width; x++) {
                        srcX = u * 16 + x * 2;
                        for (int y = 0; y < 8 && v * 16 + (y * 2) < height; y++) {
                            srcY = v * 16 + y * 2;
                            values[srcX, srcY] = blocks[v * blocksWidth + u][x, y];

                            if (srcX + 1 < width) {
                                values[srcX + 1, srcY] = blocks[v * blocksWidth + u][x, y];
                                if (srcY + 1 < height) {
                                    values[srcX, srcY + 1] = blocks[v * blocksWidth + u][x, y];

                                    values[srcX + 1, srcY + 1] = blocks[v * blocksWidth + u][x, y];
                                }
                            } else if (srcY + 1 < height) {
                                values[srcX, srcY + 1] = blocks[v * blocksWidth + u][x, y];
                            }
                        }
                    }
                }
            }
            return values;
        }

        /// <summary>
        /// precondition: the reference frame and target frame need to be the same dimentions
        /// </summary>
        /// <param name="writer">the file writer</param>
        /// <param name="referenceFrame">the reference inamge / I-frame</param>
        /// <param name="p">the search range</param>
        public void saveMotionVectors(BinaryWriter writer, ColourYCrCb[,] referenceFrame, int p) {
            int width = 8 * (int)(Math.Ceiling(source.Width / 8.0)),
                height = 8 * (int)(Math.Ceiling(source.Height / 8.0)),
                subArrayWidth = 8 * (int)Math.Ceiling(source.Width / 2.0 / 8.0),
                subArrayHeight = 8 * (int)Math.Ceiling(source.Height / 2.0 / 8.0),
                subWidth = (int)Math.Ceiling(source.Width / 2.0),
                subHeight = (int)Math.Ceiling(source.Height / 2.0);

            ColourYCrCb[,] colourValues = ColourYCrCb.convertToYCrCb(getPixels());

            double[,] targetYValues = new double[width, height],
                      targetCrValues = new double[subArrayWidth, subArrayHeight],
                      targetCbValues = new double[subArrayWidth, subArrayHeight],
                      refYValues = new double[width, height],
                      refCrValues = new double[subArrayWidth, subArrayHeight],
                      refCbValues = new double[subArrayWidth, subArrayHeight];

            //copy the colour array into its own array
            for (int x = 0; x < source.Width; x++) {
                for (int y = 0; y < source.Height; y++) {
                    targetYValues[x, y] = colourValues[x, y].y;
                    refYValues[x, y] = referenceFrame[x, y].y;
                }
            }

            //subsample cr and cb
            for (int y = 0, v = 0; y < subHeight; y++, v += 2) {
                for (int x = 0, u = 0; x < subWidth; x++, u += 2) {
                    targetCrValues[x, y] = colourValues[u, v].cr;
                    targetCbValues[x, y] = colourValues[u, v].cb;

                    refCrValues[x, y] = referenceFrame[u, v].cr;
                    refCbValues[x, y] = referenceFrame[u, v].cb;
                }
            }

            MotionVector mv;
            double[,] difference;

            //Write the Y block values
            for (short y = 0; y < height; y += 8) {
                for (short x = 0; x < width; x += 8) {
                    //get motion vector at the x, y location
                    mv = new MotionVector(x, y);
                    difference = mv.getSequentialPrediction(refYValues, targetYValues, p);

                    //apply DCT
                    difference = DCT(difference);

                    //divide by table/quantize
                    divideTables(difference, motionVectorTable);

                    //write x and y of the motion vector
                    writer.Write(mv.predX);
                    writer.Write(mv.predY);

                    //apply run length encoding for the difference
                    applyRunlengthEncoding(writer, difference);
                }
            }

            //Write the Cr block values
            for (short y = 0, v = 0; y < subArrayHeight; y += 8, v++) {
                for (short x = 0, u = 0; x < subArrayWidth; x += 8, u++) {
                    //get motion vector at the x, y location
                    mv = new MotionVector(x, y);
                    difference = mv.getSequentialPrediction(refCrValues, targetCrValues, p);

                    //apply DCT
                    difference = DCT(difference);

                    //divide by table/quantize
                    divideTables(difference, motionVectorTable);

                    //write x and y of the motion vector
                    writer.Write(mv.predX);
                    writer.Write(mv.predY);

                    //apply run length encoding for the difference
                    applyRunlengthEncoding(writer, difference);
                }
            }

            //Write the Cb block values
            for (short y = 0, v = 0; y < subArrayHeight; y += 8, v++) {
                for (short x = 0, u = 0; x < subArrayWidth; x += 8, u++) {
                    //get motion vector at the x, y location
                    mv = new MotionVector(x, y);
                    difference = mv.getSequentialPrediction(refCbValues, targetCbValues, p);

                    //apply DCT
                    difference = DCT(difference);

                    //divide by table/quantize
                    divideTables(difference, motionVectorTable);

                    //write x and y of the motion vector
                    writer.Write(mv.predX);
                    writer.Write(mv.predY);

                    //apply run length encoding for the difference
                    applyRunlengthEncoding(writer, difference);
                }
            }

            return;
        }

        public static ImageData readFromMotionVectors(BinaryReader reader, ImageData refImage) {
            //get subSampled YCrCb
            ColourYCrCb[,] refColourValues = ColourYCrCb.convertToYCrCb(refImage.getPixels());
            int width = refImage.source.Width,
                height = refImage.source.Height,
                subArrayWidth = 8 * (int)Math.Ceiling(refImage.source.Width / 2.0 / 8.0),
                subArrayHeight = 8 * (int)Math.Ceiling(refImage.source.Height / 2.0 / 8.0),
                subWidth = (int)Math.Ceiling(refImage.source.Width / 2.0),
                subHeight = (int)Math.Ceiling(refImage.source.Height / 2.0),
                N = 8;

            double[,] refYValues = new double[8 * (int)(Math.Ceiling(refImage.source.Width / 8.0)),
                                              8 * (int)(Math.Ceiling(refImage.source.Height / 8.0))],
                      refCrValues = new double[subArrayWidth, subArrayHeight],
                      refCbValues = new double[subArrayWidth, subArrayHeight];
            Bitmap newBitmap = new Bitmap(width, height);

            //copy the colour array into its own array
            for (int x = 0; x < refImage.source.Width; x++)
                for (int y = 0; y < refImage.source.Height; y++)
                    refYValues[x, y] = refColourValues[x, y].y;

            //subsample cr and cb
            for (int y = 0, v = 0; y < subHeight; y++, v += 2) {
                for (int x = 0, u = 0; x < subWidth; x++, u += 2) {
                    refCrValues[x, y] = refColourValues[u, v].cr;
                    refCbValues[x, y] = refColourValues[u, v].cb;
                }
            }

            short curX, curY;
            List<double[,]> yBlocks = new List<double[,]>(),
                            crBlocks = new List<double[,]>(),
                            cbBlocks = new List<double[,]>();

            int verticalBlocks = (int)Math.Ceiling(width / 8.0),
                horizontalBlocks = (int)Math.Ceiling(height / 8.0),
                subWidthBlocks = (int)Math.Ceiling(Math.Ceiling(width / 2.0) / 8),
                subHeightBlocks = (int)Math.Ceiling(Math.Ceiling(height / 2.0) / 8);

            //Read in the Y block values
            int i = 0;
            for (int y = 0; y < height; y+= N) {
                for (int x = 0; x < width; x += N) {
                    curX = reader.ReadSByte();
                    curY = reader.ReadSByte();
                    yBlocks.Add(undoRunlengthEncoding(reader));

                    //multiply by encoding scale
                    multiplyTables(yBlocks[i], motionVectorTable);

                    //perform IDCT on the array
                    yBlocks[i] = IDCT(yBlocks[i]);

                    for (int u = 0; u < N; u++) {
                        for (int v = 0; v < N; v++) {
                            yBlocks[i][u, v] = yBlocks[i][u, v] + refYValues[curX + x + u, curY + y + v];
                        }
                    }
                    i++;
                }
            }

            //Read in the Cr block values
            i = 0;
            for (int y = 0; y < subHeight; y += N) {
                for (int x = 0; x < subWidth; x += N) {
                    curX = reader.ReadSByte();
                    curY = reader.ReadSByte();
                    crBlocks.Add(undoRunlengthEncoding(reader));

                    //multiply by encoding scale
                    multiplyTables(crBlocks[i], motionVectorTable);

                    //perform IDCT on the array
                    crBlocks[i] = IDCT(crBlocks[i]);

                    for (int u = 0; u < N; u++) {
                        for (int v = 0; v < N; v++) {
                            crBlocks[i][u, v] = crBlocks[i][u, v] + refCrValues[curX + x + u, curY + y + v];
                        }
                    }
                    i++;
                }
            }

            //Read in the Cb block values
            i = 0;
            for (int y = 0; y < subHeight; y += N) {
                for (int x = 0; x < subWidth; x += N) {
                    curX = reader.ReadSByte();
                    curY = reader.ReadSByte();
                    cbBlocks.Add(undoRunlengthEncoding(reader));

                    //multiply by encoding scale
                    multiplyTables(cbBlocks[i], motionVectorTable);

                    //perform IDCT on the array
                    cbBlocks[i] = IDCT(cbBlocks[i]);

                    for (int u = 0; u < N; u++) {
                        for (int v = 0; v < N; v++) {
                            cbBlocks[i][u, v] = cbBlocks[i][u, v] + refCbValues[curX + x + u, curY + y + v];
                        }
                    }
                    i++;
                }
            }

            ColourYCrCb[,] YCrCbValues = blocksToYCrCb(yBlocks, crBlocks, cbBlocks, width, height);
            Color[,] newColors = ColourYCrCb.convertToColor(YCrCbValues);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    newBitmap.SetPixel(x, y, newColors[x, y]);
                }
            }

            return new ImageData(newBitmap);
        }


        /// <summary>
        /// performes DCT on the array of values passed in. DCT values M and N are both 8
        /// precondition: values must be an 8x8 array
        /// </summary>
        /// <param name="f">the values to perform DCT on</param>
        /// <returns>the change frequencies of the values (DCT)</returns>
        public static double[,] DCT(double[,] f) {
            int N = 8;
            double[,] F = new double[N, N];

            for (int u = 0; u < N; u++) {
                for (int v = 0; v < N; v++) {
                    F[u, v] = 0;
                    for (int x = 0; x < N; x++) {
                        for (int y = 0; y < N; y++) {
                            F[u, v] += f[x, y]
                                     * Math.Cos((2 * x + 1) * u * Math.PI / 16)
                                     * Math.Cos((2 * y + 1) * v * Math.PI / 16)
                                     ;
                        }
                    }
                    F[u, v] = F[u, v] * ((C(u) * C(v)) / 4);
                }
            }
            return F;
        }

        /// <summary>
        /// performs inverse DCT on the values.  Makes the frequencies of changes back into
        /// their original values.
        /// </summary>
        /// <param name="F">the DCT 8x8 block</param>
        /// <returns>the original values from the DCT transformation</returns>
        public static double[,] IDCT(double[,] F) {
            int N = 8;
            double[,] f = new double[N, N];

            for (int x = 0; x < N; x++) {
                for (int y = 0; y < N; y++) {
                    f[x, y] = 0;
                    for (int u = 0; u < N; u++) {
                        for (int v = 0; v < N; v++) {
                            f[x, y] += C(u) * C(v)
                                * Math.Cos((2 * x + 1) * u * Math.PI / 16)
                                * Math.Cos((2 * y + 1) * v * Math.PI / 16)
                                * F[u, v];
                        }
                    }
                    f[x, y] = f[x, y] / 4;
                }
            }

            return f;
        }

        /// <summary>
        /// function used in DCT
        /// </summary>
        /// <param name="xi">the value to check</param>
        /// <returns>1/root2, or 1</returns>
        private static double C(int xi) {
            return xi == 0 ? 1 / Math.Sqrt(2) : 1;
        }
    }
}
