﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMP4932_Asn2 {
    class ColourYCrCb {
        public double y, cr, cb;

        public ColourYCrCb(double y, double cr, double cb) {
            this.y = y;
            this.cr = cr;
            this.cb = cb;
        }

        /**
         * converts an array of Color values (digital RGB) into digital ColourYCrCb values
         * 
         * @param srcColours - the colours to be converted
         * @return the converted colour values
         */
        public static ColourYCrCb[,] convertToYCrCb(Color[,] srcColours) {
            ColourYCrCb[,] YCrCb = new ColourYCrCb[srcColours.GetLength(0), srcColours.GetLength(1)];

            for (int y = 0; y < srcColours.GetLength(1); y++)
                for (int x = 0; x < srcColours.GetLength(0); x++)
                    YCrCb[x, y] = new ColourYCrCb(
                        16 + (65.738 * srcColours[x, y].R / 256) + (129.057 * srcColours[x, y].G / 256) + (25.064 * srcColours[x, y].B / 256),
                        128 + (112.439 * srcColours[x, y].R / 256) - (94.154 * srcColours[x, y].G / 256) - (18.285 * srcColours[x, y].B / 256), 
                        128 - (37.945 * srcColours[x, y].R / 256) - (74.494 * srcColours[x, y].G / 256) + (112.439 * srcColours[x, y].B / 256)
                        );

            return YCrCb;
        }

        /**
         * Converts the digital YCrCb values into colour values
         * 
         * @param srcColours - the sourc YCrCb values
         * @return the converted colors
         */
        public static Color[,] convertToColor(ColourYCrCb[,] srcColours) {
            Color[,] colors = new Color[srcColours.GetLength(0), srcColours.GetLength(1)];
            double newR, newG, newB, yValue;

            for (int y = 0; y < srcColours.GetLength(1); y++) {
                for (int x = 0; x < srcColours.GetLength(0); x++) {
                    yValue = 255 / 219.0 * (srcColours[x, y].y - 16);

                    newR = yValue + 1.596026785714286 * (srcColours[x, y].cr - 128);
                    newG = yValue - 0.391762290094913 * (srcColours[x, y].cb - 128) - 0.812967647237771 * (srcColours[x, y].cr - 128);
                    newB = yValue + 2.017232142857143 * (srcColours[x, y].cb - 128);

                    colors[x, y] = Color.FromArgb(
                            checkRGBValue(newR),
                            checkRGBValue(newG),
                            checkRGBValue(newB)
                        );
                    //Console.WriteLine("RGB = " + colors[x, y].R + " " + colors[x, y].G + " " + colors[x, y].B + " ");
                }
            }

            return colors;
        }

        /**
         * checks if the number is within 0..255
         * @param n - the number to be checked
         * @return the number within the range
         */
        public static int checkRGBValue(double n) {
            double r = Math.Min(n, 255);
            return (int) Math.Max(r, 0);
        }

        public Color getMonoY() {
            return Color.FromArgb((int)y, (int)y, (int)y);
        }

        public Color getMonoCr() {
            return Color.FromArgb((int)cr, (int)cr, (int)cr);
        }

        public Color getMonoCb() {
            return Color.FromArgb((int)cb, (int)cb, (int)cb);
        }
    }
}
