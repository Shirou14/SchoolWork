﻿namespace COMP4932_Asn2 {
    partial class SearchRangeForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SearchRange = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.SearchRange)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(53, 64);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Please enter the search range (p)\r\nfor the motion vectors:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SearchRange
            // 
            this.SearchRange.Location = new System.Drawing.Point(40, 38);
            this.SearchRange.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.SearchRange.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SearchRange.Name = "SearchRange";
            this.SearchRange.Size = new System.Drawing.Size(104, 20);
            this.SearchRange.TabIndex = 2;
            this.SearchRange.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // SearchRangeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(188, 93);
            this.Controls.Add(this.SearchRange);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SearchRangeForm";
            this.Text = "Enter Search Range";
            ((System.ComponentModel.ISupportInitialize)(this.SearchRange)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown SearchRange;
    }
}