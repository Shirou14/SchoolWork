﻿namespace COMP4932_Asn2 {
    partial class Main {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.OpenSourceDialog = new System.Windows.Forms.OpenFileDialog();
            this.MenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openSourceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openCompressedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openCompressedVideoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveVideoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compressionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startJPGCompressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMotionVectorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenCompressedDialog = new System.Windows.Forms.OpenFileDialog();
            this.LeftImage = new System.Windows.Forms.PictureBox();
            this.RightImage = new System.Windows.Forms.PictureBox();
            this.SaveCompressedDialog = new System.Windows.Forms.SaveFileDialog();
            this.StatusBar = new System.Windows.Forms.StatusStrip();
            this.CompressionStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.SaveCompressedVideoDialog = new System.Windows.Forms.SaveFileDialog();
            this.OpenCompressedVideoDialog = new System.Windows.Forms.OpenFileDialog();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LeftImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightImage)).BeginInit();
            this.StatusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // OpenSourceDialog
            // 
            this.OpenSourceDialog.FileName = "OpenSourceDialog";
            this.OpenSourceDialog.Filter = "Images | *.jpg; *.png; *.bmp| All Files | *.*";
            this.OpenSourceDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenSourceDialog_FileOk);
            // 
            // MenuStrip
            // 
            this.MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.compressionToolStripMenuItem});
            this.MenuStrip.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip.Name = "MenuStrip";
            this.MenuStrip.Size = new System.Drawing.Size(800, 24);
            this.MenuStrip.TabIndex = 0;
            this.MenuStrip.Text = "MenuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openSourceToolStripMenuItem,
            this.openCompressedToolStripMenuItem,
            this.openCompressedVideoToolStripMenuItem,
            this.toolStripSeparator2,
            this.saveToolStripMenuItem,
            this.saveVideoToolStripMenuItem,
            this.toolStripSeparator1,
            this.resetToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openSourceToolStripMenuItem
            // 
            this.openSourceToolStripMenuItem.Name = "openSourceToolStripMenuItem";
            this.openSourceToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.openSourceToolStripMenuItem.Text = "Open Source";
            this.openSourceToolStripMenuItem.Click += new System.EventHandler(this.openSourceToolStripMenuItem_Click);
            // 
            // openCompressedToolStripMenuItem
            // 
            this.openCompressedToolStripMenuItem.Name = "openCompressedToolStripMenuItem";
            this.openCompressedToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.openCompressedToolStripMenuItem.Text = "Open Compressed JPG";
            this.openCompressedToolStripMenuItem.Click += new System.EventHandler(this.openCompressedToolStripMenuItem_Click);
            // 
            // openCompressedVideoToolStripMenuItem
            // 
            this.openCompressedVideoToolStripMenuItem.Name = "openCompressedVideoToolStripMenuItem";
            this.openCompressedVideoToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.openCompressedVideoToolStripMenuItem.Text = "Open Compressed Video";
            this.openCompressedVideoToolStripMenuItem.Click += new System.EventHandler(this.openCompressedVideoToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(202, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.saveToolStripMenuItem.Text = "Save JPG";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveVideoToolStripMenuItem
            // 
            this.saveVideoToolStripMenuItem.Name = "saveVideoToolStripMenuItem";
            this.saveVideoToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.saveVideoToolStripMenuItem.Text = "Save Video";
            this.saveVideoToolStripMenuItem.Click += new System.EventHandler(this.saveVideoToolStripMenuItem_Click);
            // 
            // compressionToolStripMenuItem
            // 
            this.compressionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startJPGCompressToolStripMenuItem,
            this.viewMotionVectorsToolStripMenuItem});
            this.compressionToolStripMenuItem.Name = "compressionToolStripMenuItem";
            this.compressionToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.compressionToolStripMenuItem.Text = "View";
            // 
            // startJPGCompressToolStripMenuItem
            // 
            this.startJPGCompressToolStripMenuItem.Name = "startJPGCompressToolStripMenuItem";
            this.startJPGCompressToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.startJPGCompressToolStripMenuItem.Text = "View YCrCb";
            this.startJPGCompressToolStripMenuItem.Click += new System.EventHandler(this.startJPGCompressToolStripMenuItem_Click);
            // 
            // viewMotionVectorsToolStripMenuItem
            // 
            this.viewMotionVectorsToolStripMenuItem.Name = "viewMotionVectorsToolStripMenuItem";
            this.viewMotionVectorsToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.viewMotionVectorsToolStripMenuItem.Text = "View Motion Vectors";
            this.viewMotionVectorsToolStripMenuItem.Click += new System.EventHandler(this.viewMotionVectorsToolStripMenuItem_Click);
            // 
            // OpenCompressedDialog
            // 
            this.OpenCompressedDialog.FileName = "OpenCompressedDialog";
            this.OpenCompressedDialog.Filter = "Compressed Image | *.bdc";
            this.OpenCompressedDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenCompressedDialog_FileOk);
            // 
            // LeftImage
            // 
            this.LeftImage.BackColor = System.Drawing.SystemColors.Control;
            this.LeftImage.Location = new System.Drawing.Point(0, 24);
            this.LeftImage.Margin = new System.Windows.Forms.Padding(0);
            this.LeftImage.Name = "LeftImage";
            this.LeftImage.Size = new System.Drawing.Size(400, 376);
            this.LeftImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LeftImage.TabIndex = 2;
            this.LeftImage.TabStop = false;
            // 
            // RightImage
            // 
            this.RightImage.BackColor = System.Drawing.SystemColors.Control;
            this.RightImage.Location = new System.Drawing.Point(400, 24);
            this.RightImage.Margin = new System.Windows.Forms.Padding(0);
            this.RightImage.Name = "RightImage";
            this.RightImage.Size = new System.Drawing.Size(400, 376);
            this.RightImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.RightImage.TabIndex = 3;
            this.RightImage.TabStop = false;
            // 
            // SaveCompressedDialog
            // 
            this.SaveCompressedDialog.Filter = "Compressed|*.bdc";
            this.SaveCompressedDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.SaveCompressedDialog_FileOk);
            // 
            // StatusBar
            // 
            this.StatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CompressionStatusLabel});
            this.StatusBar.Location = new System.Drawing.Point(0, 378);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Size = new System.Drawing.Size(800, 22);
            this.StatusBar.TabIndex = 0;
            this.StatusBar.Text = "Compression Rate: ";
            // 
            // CompressionStatusLabel
            // 
            this.CompressionStatusLabel.Name = "CompressionStatusLabel";
            this.CompressionStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // SaveCompressedVideoDialog
            // 
            this.SaveCompressedVideoDialog.Filter = "Compressed Video|*.bdv";
            this.SaveCompressedVideoDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.SaveCompressedVideoDialog_FileOk);
            // 
            // OpenCompressedVideoDialog
            // 
            this.OpenCompressedVideoDialog.FileName = "OpenCompressedVideoDialog";
            this.OpenCompressedVideoDialog.Filter = "Compressed Video|*.bdv";
            this.OpenCompressedVideoDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenCompressedVideoDialog_FileOk);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(202, 6);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.resetToolStripMenuItem.Text = "Reset";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(800, 400);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.RightImage);
            this.Controls.Add(this.LeftImage);
            this.Controls.Add(this.MenuStrip);
            this.MainMenuStrip = this.MenuStrip;
            this.Name = "Main";
            this.Text = "Braden D\'Eith - Comp4932 Assignment 2";
            this.Resize += new System.EventHandler(this.mainFormResize);
            this.MenuStrip.ResumeLayout(false);
            this.MenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LeftImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightImage)).EndInit();
            this.StatusBar.ResumeLayout(false);
            this.StatusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog OpenSourceDialog;
        private System.Windows.Forms.MenuStrip MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openSourceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openCompressedToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog OpenCompressedDialog;
        private System.Windows.Forms.PictureBox LeftImage;
        private System.Windows.Forms.PictureBox RightImage;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compressionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startJPGCompressToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog SaveCompressedDialog;
        private System.Windows.Forms.StatusStrip StatusBar;
        private System.Windows.Forms.ToolStripMenuItem viewMotionVectorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel CompressionStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem saveVideoToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog SaveCompressedVideoDialog;
        private System.Windows.Forms.ToolStripMenuItem openCompressedVideoToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog OpenCompressedVideoDialog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
    }
}

