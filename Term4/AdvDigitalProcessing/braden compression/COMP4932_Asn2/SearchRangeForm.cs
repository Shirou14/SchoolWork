﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace COMP4932_Asn2 {
    public partial class SearchRangeForm : Form {
        private Main parent;

        public SearchRangeForm(Main p) {
            InitializeComponent();
            parent = p;
        }

        private void button1_Click(object sender, EventArgs e) {
                parent.saveVideo((int)SearchRange.Value);
                this.Dispose();
                this.Close();
        }
    }
}
