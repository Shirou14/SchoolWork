This is Jay Coughlan's Image Warping App:
Completed features-
>Image selection
>Image Warping
>Multiple frames
>cross-dissolve (kind of)
>Frame step-through
>Line drawing and editing

Incomplete features-
>Frame playback (play button is inoperative)
>Cross dissolve
>loading bar
>saving

When you open the app:
>Pressing the 1 or 2 button will allow you to select an image from your gallery to place in the top or bottom view respectively to Warping
>drawing on either view will allow you to draw a Line
>pressing on a line circle will let you edit that line
>pressing the eraser will delete either the last selected line, or if none is selected the last drawn
>pressing the swirl button will prompt the user to enter how many frames they want (between 0 and 20) and pressing generate will cause the screen to freeze while it processes.

In the play view:
>Pressing next will show the next frame
>pressing prev will show the last frame
>pressing play will do nothing
>pressing the dedicated return button on your device will return you to the main screen