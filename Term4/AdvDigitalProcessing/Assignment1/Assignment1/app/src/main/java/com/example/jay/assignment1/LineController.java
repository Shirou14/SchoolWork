package com.example.jay.assignment1;

/**
 * Created by Jay on 2/1/2017.
 */

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;

import java.lang.reflect.Array;
import java.nio.IntBuffer;
import java.util.ArrayList;

public class LineController
{
    //private ArrayList<LinePair> lines = new ArrayList<LinePair>();
    public static ArrayList<AdjustableLine> fatherLines = new ArrayList<AdjustableLine>();
    public static ArrayList<AdjustableLine> sonLines = new ArrayList<AdjustableLine>();
    private static DrawingView father, son;
    private AdjustableLine selectedF, selectedS;
    private int dormantColour = Color.rgb(8,84,17);
    private int selectedColour = Color.rgb(13,196,179);
    private int[][] pixels;

    public LineController(DrawingView d1, DrawingView d2)
    {
        father = d1; son = d2;
        pixels = new int[father.getWidth()][father.getHeight()];
    }

    public void addNewLine(AdjustableLine line, DrawingView dv)
    {
        AdjustableLine partner = new AdjustableLine(line.getX1(), line.getY1(), line.getX2(), line.getY2());
        partner.lineColour=dormantColour;
        line.setWarp(new fPoint(dv.getWidth()/2, dv.getHeight()/2));
        partner.setWarp(new fPoint(dv.getWidth()/2, dv.getHeight()/2));
        if(dv.equals(father))
        {
            line.lineColour = dormantColour;
            fatherLines.add(line);
            sonLines.add(partner);
        }
        else if(dv.equals(son))
        {
            line.lineColour = dormantColour;
            sonLines.add(line);
            fatherLines.add(partner);
        }

        father.invalidate();
        son.invalidate();
    }

    public void calcLineWarp()
    {
        float a = 0.01f, b = 2, weight = 0;
        fPoint newWarp, avgWarp = new fPoint();

        for (AdjustableLine line : fatherLines)
        {
            float dist = WarpingComputations.distance(line, line.getWarp());
            float fracDist = WarpingComputations.fractionalDistance(WarpingComputations.vector(new fPoint(line.getX1(), line.getY1()), line.getWarp()), WarpingComputations.vector(line));
            AdjustableLine prtnr = findPartner(line, father);
            newWarp = WarpingComputations.newWarpPoint(prtnr, dist, fracDist);

            avgWarp.x += (newWarp.x - line.getWarp().x) * Math.pow(1/(a + dist), b);
            avgWarp.y += (newWarp.y - line.getWarp().y) * Math.pow(1/(a + dist), b);
            weight +=Math.pow(1 /(a + dist), b);

            prtnr.setWarp(newWarp);
        }

        avgWarp.x = avgWarp.x/weight + fatherLines.get(0).getWarp().x;
        avgWarp.y = avgWarp.y/weight + fatherLines.get(0).getWarp().y;
        if(avgWarp.x != son.warpPoint.x && avgWarp.y != son.warpPoint.y)
            son.warpPoint = avgWarp;

        father.invalidate();
        son.invalidate();
    }

    public AdjustableLine getLine(AdjustableLine line, DrawingView dv)
    {
        if(dv.equals(father))
        {
            for (AdjustableLine lp : fatherLines)
            {
                if(lp.equals(line))
                    return lp;
            }
        }else if (dv.equals(son))
        {
            for (AdjustableLine lp : sonLines)
            {
                if(lp.equals(line))
                    return lp;
            }
        }/*else
        {
            throw new
        }*/
        return null;
    }

    public boolean findLine(AdjustableLine line, DrawingView dv)
    {
        if(dv.equals(father))
        {
            for(AdjustableLine l : fatherLines)
            {
                if(l.equals(line))
                    return true;
            }
        }
        else if (dv.equals(son))
        {
            for(AdjustableLine l : sonLines)
            {
                if(l.equals(line))
                    return true;
            }
        }
        else
            return false;
        return false;
    }

    public AdjustableLine findPartner(AdjustableLine l, DrawingView dv)
    {
        int index;
        if(dv.equals(father))
        {
            index = fatherLines.indexOf(l);
            return sonLines.get(index);
        }else if(dv.equals(son))
        {
            index = sonLines.indexOf(l);
            return fatherLines.get(index);
        }
        return null;
    }

    public void selectLine(AdjustableLine l, DrawingView dv)
    {
        if(selectedF != null)
            selectedF.setColour(dormantColour);
        if(selectedS != null)
            selectedS.setColour(dormantColour);

        if(dv.equals(father))
        {
            selectedF = l;
            selectedS = findPartner(l, dv);
        }
        else if(dv.equals(son))
        {
            selectedS = l;
            selectedF = findPartner(l, dv);
        }

        selectedF.setColour(selectedColour);
        selectedS.setColour(selectedColour);

        father.invalidate();
        son.invalidate();
    }

    public ArrayList<AdjustableLine> getMyList(DrawingView dv)
    {
        if(dv.equals(father))
        {
            return fatherLines;
        }
        else if (dv.equals(son))
        {
            return sonLines;
        }

        return null;
    }

    public int lineIndex(AdjustableLine line, DrawingView dv)
    {
        if(dv.equals(father))
        {
            return fatherLines.indexOf(line);
        }
        else if(dv.equals(son))
        {
            return sonLines.indexOf(line);
        }
        return -1;
    }

    public void eraseSelected()
    {
        if(selectedS != null && selectedF != null)
        {
            sonLines.remove(selectedS);
            fatherLines.remove(selectedF);
            selectedF = null;
            selectedS = null;
        }
        else if(fatherLines.size() > 0 && sonLines.size() > 0)
        {
            fatherLines.remove(fatherLines.size() - 1);
            sonLines.remove(sonLines.size() - 1);
        }

        father.invalidate();
        son.invalidate();
    }

    public static AdjustableLine[] getFatherArray()
    {
        AdjustableLine[] fa = new AdjustableLine[fatherLines.size()];
        for(int fff = 0; fff < fa.length; fff++)
        {
            fa[fff] = fatherLines.get(fff);
        }
        return fa;
    }

    public static AdjustableLine[] getSonArray()
    {
        AdjustableLine[] sa = new AdjustableLine[sonLines.size()];
        for(int sss = 0; sss < sa.length; sss++)
        {
            sa[sss] = sonLines.get(sss);
        }
        return sa;
    }
}
