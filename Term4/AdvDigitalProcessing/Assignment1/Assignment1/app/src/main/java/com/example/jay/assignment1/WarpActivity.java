package com.example.jay.assignment1;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class WarpActivity extends AppCompatActivity {

    private int frames;
    ImageView iView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warp_details);

        iView = (ImageView) findViewById(R.id.mainView);

        String s = getIntent().getStringExtra("EXTRA_TEXT");

        Toast toast = Toast.makeText(this.getBaseContext(), "Generating " + s + " Images", Toast.LENGTH_LONG);
        toast.show();

        /*frames = Math.max(Math.min(Integer.parseInt(s), 20), 3);
        WarpManager.generateBitmaps(frames, LineController.getFatherArray(), LineController.getSonArray());*/

        iView.setImageBitmap(WarpManager.getCurrent());
        iView.invalidate();
    }

    public void getNext(View view)
    {
        iView.setImageBitmap(WarpManager.getNext());
        iView.invalidate();
    }

    public void getPrev(View view)
    {
        iView.setImageBitmap(WarpManager.getPrev());
        iView.invalidate();
    }
}
