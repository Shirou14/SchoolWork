package com.example.jay.assignment1;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.view.PixelCopy;

import java.util.ArrayList;

/**
 * Created by Jay on 2/8/2017.
 */

public class WarpManager
{
    private static AdjustableLine[][] inBetweens;
    private static Bitmap[] pics;
    public static Bitmap fatherPic, sonPic;
    public static int index = 0;

    public static void generateLines(int frames, AdjustableLine[] father, AdjustableLine[] son)
    {
        inBetweens = new AdjustableLine[frames][father.length];
        int fullDex = frames - 1;

        for (int iii = 0; iii < father.length; iii++)
        {
            inBetweens[0][iii] = father[iii];
            inBetweens[frames-1][iii] = son[iii];
        }
        for(float t = 1; t < frames - 1; t++)
        {
            for(int jjj = 0; jjj < inBetweens[(int)t].length; jjj++)
            {
                float x1 = (inBetweens[0][jjj].getX1() + (t/(float)frames)*((inBetweens[fullDex][jjj].getX1() - inBetweens[0][jjj].getX1())));
                float y1 = (inBetweens[0][jjj].getY1() + (t/(float)frames)*((inBetweens[fullDex][jjj].getY1() - inBetweens[0][jjj].getY1())));
                float x2 = (inBetweens[0][jjj].getX2() + (t/(float)frames)*((inBetweens[fullDex][jjj].getX2() - inBetweens[0][jjj].getX2())));
                float y2 = (inBetweens[0][jjj].getY2() + (t/(float)frames)*((inBetweens[fullDex][jjj].getY2() - inBetweens[0][jjj].getY2())));
                inBetweens[(int)t][jjj] = new AdjustableLine(x1, y1, x2, y2);
            }
        }
    }

    public static void generateBitmaps(int frames, AdjustableLine[] fatherlines, AdjustableLine[] sonlines)
    {
        pics = new Bitmap[frames];
        generateLines(frames, fatherlines, sonlines);
        float t = 0;
        for (int iii = 0; iii < pics.length; iii++)
        {
            if(iii == 0)
            {
                pics[iii] = fatherPic;
            }
            else if(iii != pics.length-1)
            {
                pics[iii] = calcWarp(inBetweens[0], inBetweens[iii], inBetweens[frames - iii], inBetweens[inBetweens.length-1], frames, iii);
            }
            else
            {
                pics[iii] = sonPic;
            }
        }
    }

    public static Bitmap calcWarp(AdjustableLine[] str1, AdjustableLine[] end1,AdjustableLine[] str2, AdjustableLine[] end2,  int totalFrames, int frame)
    {
        //son.bmpPixels = new int[father.bmpPixels.length];
        fPoint sonTarget, fatherTarget;
        int w = fatherPic.getWidth(), h = fatherPic.getHeight(), fuse;
        float frat, srat;
        Bitmap fusion, sonScaled, fatherScaled;// = Bitmap.createBitmap()
        PixelCopy pxl;
        sonScaled = Bitmap.createScaledBitmap(sonPic, w, h, false);
        fatherScaled = Bitmap.createScaledBitmap(fatherPic, w, h, false);
        fusion = Bitmap.createScaledBitmap(sonPic, w, h, false);

        //son.scaledBitmap = Bitmap.createBitmap(father.scaledBitmap.getWidth(), father.scaledBitmap.getHeight(), father.scaledBitmap.getConfig());
        for(int x = 0; x < w; x++)
        {
            for(int y = 0; y < h; y++)
            {
                sonTarget = WarpingComputations.getTargetPoint(str2, end2, new fPoint(x,y));
                sonTarget.x = Math.min(Math.max(sonTarget.x, 0), w - 1);
                sonTarget.y = Math.min(Math.max(sonTarget.y, 0), h - 1);


                fatherTarget = WarpingComputations.getTargetPoint(str1, end1, new fPoint(x,y));
                fatherTarget.x = Math.min(Math.max(fatherTarget.x, 0), w - 1);
                fatherTarget.y = Math.min(Math.max(fatherTarget.y, 0), h - 1);
                fatherScaled.setPixel(x,y, sonPic.getPixel((int)sonTarget.x,(int)sonTarget.y));
                sonScaled.setPixel(x,y, fatherPic.getPixel((int)fatherTarget.x,(int)fatherTarget.y));
                frat = (fatherPic.getPixel((int)sonTarget.x,(int)sonTarget.y));
                srat = (sonPic.getPixel((int)fatherTarget.x,(int)fatherTarget.y));

                double leftWeight = ((frame) / totalFrames), rightWeight = ((totalFrames - frame) / totalFrames);

                //int leftPixel = leftWarps[i].getPixel(x,y);
                float lRed = Color.red((int)frat) + (float)leftWeight;
                float lGreen = Color.green((int)frat) + (float)leftWeight;
                float lBlue = Color.blue((int)frat) + (float)leftWeight;

                //int rightPixel = rightWarps[totalFrames - i - 1].getPixel(x,y);
                float rRed = Color.red((int)srat) + (float)rightWeight ;
                float rGreen = Color.green((int)srat) + (float)rightWeight ;
                float rBlue = Color.blue((int)srat) + (float)rightWeight ;

                int oRed = (int)(lRed + rRed) / totalFrames;
                int oGreen = (int)(lGreen + rGreen) / totalFrames;
                int oBlue = (int)(lBlue + rBlue) / totalFrames;

                //finalMorph[i].setPixel(x, y, Color.rgb(oRed, oGreen, oBlue));

                fusion.setPixel(x, y, Color.rgb(oRed, oGreen, oBlue));
            }
        }
        return fusion;
    }

    public static Bitmap getNext()
    {
        if(index < pics.length-1)
        {
            Log.d("index", " " + index);
            return pics[++index];
        }
        else
        {
            Log.d("index", " " + index);
            return pics[index];
        }
    }

    public static Bitmap getPrev()
    {
        if(index > 0)
        {
            Log.d("index", " " + index);
            return pics[--index];
        }
        else
        {
            Log.d("index", " " + index);
            return pics[index];
        }
    }

    public static Bitmap getCurrent()
    {
        Log.d("index", " " + index);
        return pics[index];
    }
}
