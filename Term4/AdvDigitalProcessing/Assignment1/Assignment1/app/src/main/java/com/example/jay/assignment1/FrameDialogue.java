package com.example.jay.assignment1;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

/**
 * Created by Jay on 2/8/2017.
 */

public class FrameDialogue extends DialogFragment
{
    EditText numb = null;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.frame_num);
        //LayoutInflater inf = getActivity().getLayoutInflater();

        //View view = inf.inflate(R.layout.frame_selector, null);
        final Context context = this.getContext();
        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setRawInputType(Configuration.KEYBOARD_12KEY);
        builder.setView(input);
        //numb = (EditText)view.findViewById(R.id.enterFrames);
        /*numb.setMaxValue(20);
        numb.setMinValue(5);
        numb.setWrapSelectorWheel(true);*/

        builder.setMessage(R.string.dialogTxt)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                       // Toast toast = Toast.makeText(context, "Generating Images", Toast.LENGTH_SHORT);
                        //toast.show();
                        String frames = null;
                        Log.d("numb maybe", input.getText().toString());
                        if(input.getText() != null)
                        {
                            frames = input.getText().toString();
                        }
                        int f = Math.max(Math.min(Integer.parseInt(frames), 20), 3);
                        WarpManager.generateBitmaps(f, LineController.getFatherArray(), LineController.getSonArray());

                        Intent intent = new Intent(getContext(), WarpActivity.class);
                        //EditText editText = (EditText) findViewById(R.id.warpActivity);
                        //String message = editText.getText().toString();
                        intent.putExtra("EXTRA_TEXT", frames);

                        startActivity(intent);


                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });

        // Create the AlertDialog object and return it
        return builder.create();
    }

    /*@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // R.layout.my_layout - that's the layout where your textview is placed
        View view = inflater.inflate(R.layout.frame_selector, container, false);
        NumberPicker numb = (NumberPicker) view.findViewById(R.id.numberPicker1);
        // you can use your textview.
        return view;
    }*/
}
