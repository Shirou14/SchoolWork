package com.example.jay.assignment1;
//https://code.tutsplus.com/tutorials/android-sdk-create-a-drawing-app-interface-creation--mobile-19021
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.io.IOException;
import java.lang.Math;
import android.view.View;
import android.content.Context;
import android.util.AttributeSet;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Shiro on 1/15/2017.
 */



public class DrawingView extends ImageView
{
    public Bitmap scaledBitmap;

    public enum TOUCHMODE {POINT1, POINT2, NEWLINE}
    TOUCHMODE mode = TOUCHMODE.NEWLINE;
    public static final int FATHER_MODE = 101, SON_MODE = 102;
    //private  Path drawPath;
    private Paint drawPaint, canvasPaint, circlePaint, warpPaint;
    private int paintColor = 0xFF660000;
    public int[] bmpPixels;
    private Canvas drawCanvas;
    public Bitmap canvasBitmap, imageBitmap = null;


    private ArrayList<AdjustableLine> lines = new ArrayList<AdjustableLine>();
    private AdjustableLine lineHolder = null;
    private LineController lController;

    //private final Paint mPaint;
    private float startX, startY, endX, endY;
    //warp point location
    public fPoint warpPoint;
    private int[] viewLocation = new int[2];//bounds are left, top, right, bottom


    public DrawingView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setupDrawing();
        warpPoint = new fPoint(this.getWidth()/2, this.getHeight()/2);
        WarpingComputations.viewHeight = this.getHeight();
    }

    public void setController(LineController p)
    {
        lController = p;
    }

    public void addLine(AdjustableLine line)
    {
        lines.add(line);
    }

    public void removeLine(AdjustableLine line)
    {
        lines.remove(line);
    }


    public void setupDrawing()
    {
        //we will set up our interactions with the drawing view here
        drawPaint = new Paint();
        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeWidth(10);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);

        canvasPaint = new Paint();
        canvasPaint.setColor(Color.rgb(154,253,135));
        canvasPaint.setAntiAlias(true);
        canvasPaint.setStyle(Paint.Style.STROKE);
        canvasPaint.setStrokeWidth(10);
        canvasPaint.setStrokeJoin(Paint.Join.ROUND);
        canvasPaint.setStrokeCap(Paint.Cap.ROUND);

        circlePaint = new Paint();
        circlePaint.setColor(Color.CYAN);

        warpPaint = new Paint();
        warpPaint.setColor(Color.GREEN);


        //canvasPaint = new Paint(Paint.DITHER_FLAG);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        super.onSizeChanged(w, h, oldw, oldh);

        this.getLocationOnScreen(viewLocation);

        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        drawCanvas = new Canvas(canvasBitmap);
        warpPoint = new fPoint(this.getWidth()/2, this.getHeight()/2);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        int iii = 0;

        //if (scaledBitmap != null) {
            if(scaledBitmap != null)
            {
                canvas.drawBitmap(scaledBitmap, null, new Rect(0, 0, this.getWidth(), this.getHeight()), null);
                //scaledBitmap = canvas.
            }
      //  }

        if(lController.getMyList(this).size() > 0)
        {
            for (AdjustableLine line : lController.getMyList(this)) {
                drawPaint.setColor(line.lineColour);
                canvas.drawCircle(line.getX1(), line.getY1(), line.DETECT_RADIUS, canvasPaint);
                canvas.drawCircle(line.getX2(), line.getY2(), line.DETECT_RADIUS, canvasPaint);
                canvas.drawLine(line.getX1(), line.getY1(), line.getX2(), line.getY2(), drawPaint);

                if(line.getWarp().x != warpPoint.x && line.getWarp().y != warpPoint.y)
                {
                    //Log.d("WarpPoint:", "X: " + warpPoint.x + ", Y: " + warpPoint.y);
                    //Log.d("Linepoint " + lController.lineIndex(line, this), "X: " + line.getWarp().x + ", Y: " + line.getWarp().y);
                    canvas.drawCircle(line.getWarp().x, line.getWarp().y, 5, warpPaint);
                }
                //Log.d("line", this.getId() + "; Line " + iii + " X1: " + line.getX1() + ", Y1: " + line.getY1() + " X2: " + line.getX2() + ", Y2: " + line.getY2());
                //iii++;
            }
        }
        if(lineHolder != null && mode == TOUCHMODE.NEWLINE) {
            drawPaint.setColor(lineHolder.lineColour);
            canvas.drawCircle(lineHolder.getX1(), lineHolder.getY1(), lineHolder.DETECT_RADIUS, canvasPaint);
            canvas.drawCircle(lineHolder.getX2(), lineHolder.getY2(), lineHolder.DETECT_RADIUS, canvasPaint);
            canvas.drawLine(lineHolder.getX1(), lineHolder.getY1(), lineHolder.getX2(), lineHolder.getY2(), drawPaint);

            //Log.d("lineHolder", this.getId() + "; X1: " + lineHolder.getX1() + ", Y1: " + lineHolder.getY1() + " X2: " + lineHolder.getX2() + ", Y2: " + lineHolder.getY2());
        }
        Log.d("WarpPoint:", "X: " + warpPoint.x + ", Y: " + warpPoint.y);
        canvas.drawCircle(warpPoint.x, warpPoint.y, 5, circlePaint);
    }

     @Override
    public boolean onTouchEvent(MotionEvent event)
    {
         //detect user touch
         float touchX = event.getX();
         float touchY = event.getY();

         switch(event.getAction())
         {
             case MotionEvent.ACTION_DOWN:
                 startX = event.getX();
                 startY = event.getY();

                 mode = TOUCHMODE.NEWLINE;

                 for (AdjustableLine line : lController.getMyList(this))
                 {
                     if(line.getPoint1().contains(startX, startY))
                     {
                         lineHolder = line;
                         lController.selectLine(line, this);
                         mode = TOUCHMODE.POINT1;
                         break;
                     }
                     else if(line.getPoint2().contains(startX, startY))
                     {
                         lineHolder = line;
                         lController.selectLine(line, this);
                         mode = TOUCHMODE.POINT2;
                         break;
                     }
                     else
                     {
                         mode = TOUCHMODE.NEWLINE;
                     }
                 }
                 if(mode == TOUCHMODE.NEWLINE)
                    lineHolder = new AdjustableLine(startX, startY, startX, startY);

                 break;
             case MotionEvent.ACTION_MOVE:
                 endX = event.getX();
                 endY = event.getY();

                 if(mode == TOUCHMODE.POINT2 || mode == TOUCHMODE.NEWLINE)
                 {
                     lineHolder.setX2(Math.min(Math.max(endX, 0), this.getWidth()));
                     lineHolder.setY2(Math.min(Math.max(endY, 0), this.getWidth()));
                     //Log.d("line", this.getId() + "; line X2: " + lineHolder.getX2() + ", line Y2: " + lineHolder.getY2());
                 }else if(mode == TOUCHMODE.POINT1)
                 {
                     lineHolder.setX1(Math.min(Math.max(endX, 0), this.getWidth()));
                     lineHolder.setY1(Math.min(Math.max(endY, 0), this.getWidth()));
                    // Log.d("line", this.getId() + "; line X1: " + lineHolder.getX1() + ", line Y1: " + lineHolder.getY1());
                 }
                 break;
             case MotionEvent.ACTION_UP:
                 endY = event.getY();
                 endX = event.getX();

                 if(mode == TOUCHMODE.POINT2)
                 {
                     lineHolder.setX2(Math.min(Math.max(endX, 0), this.getWidth()));
                     lineHolder.setY2(Math.min(Math.max(endY, 0), this.getWidth()));
                 }
                 else if(mode == TOUCHMODE.POINT1)
                 {
                     lineHolder.setX1(Math.min(Math.max(endX, 0), this.getWidth()));
                     lineHolder.setY1(Math.min(Math.max(endY, 0), this.getWidth()));
                 }
                 else if(mode == TOUCHMODE.NEWLINE)
                 {
                     lineHolder.setX2(Math.min(Math.max(endX, 0), this.getWidth()));
                     lineHolder.setY2(Math.min(Math.max(endY, 0), this.getWidth()));
                     lController.addNewLine(lineHolder, this);
                 }

                 lController.calcLineWarp();

                 if(lineHolder != null)
                   lineHolder = null;
                 break;
             default:
                 return false;
         }
         invalidate();
         return true;
     }

    public void setColor(String newColor)
    {
        invalidate();
        paintColor = Color.parseColor(newColor);
        drawPaint.setColor(paintColor);
    }

    public void setBitmap(Bitmap bmp, int mode)
    {
        scaledBitmap = Bitmap.createScaledBitmap(bmp, this.getWidth(), this.getHeight(), false);
        switch (mode)
        {
            case FATHER_MODE:
                WarpManager.fatherPic = this.scaledBitmap;
                break;
            case SON_MODE:
                WarpManager.sonPic = this.scaledBitmap;
                break;
        }
        this.invalidate();
    }


}