package com.example.jay.assignment1;

/**
 * Created by Shiro on 2/5/2017.
 */

public class fPoint
{
    //these are float points, because android points are only ints, not floats.
    public float x;
    public float y;

    public fPoint()
    {
        x = 0;
        y = 0;
    }

    public fPoint(float newX, float newY)
    {
        x = newX;
        y = newY;
    }
}
