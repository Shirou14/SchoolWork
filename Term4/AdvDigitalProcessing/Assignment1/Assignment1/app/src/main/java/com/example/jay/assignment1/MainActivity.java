package com.example.jay.assignment1;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.io.IOException;
import java.util.UUID;
import android.provider.MediaStore;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View.OnClickListener;
import android.widget.Toast;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity implements OnClickListener{

    private static final int  SET_IMG_ONE = 1;
    private static final int  SET_IMG_TWO = 2;

    private Spinner imgSelect;
    private ImageView imgWindow;
    private DrawingView drawingParent, drawingChild;
    private LineController lController;
    private ImageButton currPaint, drawBtn;
    private Intent intent;
    private DialogFragment fDialogue;
    Uri imgLink;

    private float smallBrush;

   // private Button bttn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //sets view to main
        setContentView(R.layout.activity_main);
        //gets drawing view from main
        drawingParent = (DrawingView)findViewById(R.id.drawingParent);
        drawingChild = (DrawingView)findViewById(R.id.drawingChild);

        lController = new LineController(drawingParent, drawingChild);

        drawingChild.setController(lController);
        drawingParent.setController(lController);

        intent = new Intent();
        //Uri imgLink = null;
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        fDialogue = new FrameDialogue();
    }

    @Override
    public void onClick(View v)
    {
    }

    public void delete(View v)
    {
        lController.eraseSelected();
    }

    public void setImage1(View v)
    {
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SET_IMG_ONE);
    }

    public void setImage2(View view)
    {
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SET_IMG_TWO);
    }

    public void warp(View v)
    {

       // if(drawingParent.bmpPixels != null)
            fDialogue.show(getFragmentManager(), "frames");
            //lController.calcWarp();

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            Uri imageUri = data.getData();
            if(resultCode == RESULT_OK) {
                try {
                    if (imageUri != null) {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                        switch(requestCode)
                        {
                            case SET_IMG_ONE:
                                drawingParent.setBitmap(bitmap, DrawingView.FATHER_MODE);
                                break;
                            case SET_IMG_TWO:
                                drawingChild.setBitmap(bitmap, DrawingView.SON_MODE);
                                break;
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                //... do nothing
            }
        }
    }
}
