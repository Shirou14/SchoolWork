package com.example.jay.assignment1;

/**
 * Created by Jay on 2/1/2017.
 */

public class LinePair
{
    private AdjustableLine first, second;

    LinePair(AdjustableLine l1, AdjustableLine l2)
    {
        first = l1; second = l2;
    }

    public LinePair (AdjustableLine line)
    {
        AdjustableLine temp = new AdjustableLine(line.getX1(), line.getY1(), line.getX2(), line.getY2());
        first = line; second = temp;
    }

    public AdjustableLine getFirst()
    {
        return first;
    }

    public AdjustableLine getSecond()
    {
        return second;
    }

    public boolean compareFirst(AdjustableLine line)
    {
        return first.equals(line);
    }

    public boolean compareSecond(AdjustableLine line)
    {
        return second.equals(line);
    }

    public void setFirst(AdjustableLine line)
    {
        first = line;
    }

    public void setSecond(AdjustableLine line)
    {
        second = line;
    }
}
