package com.example.jay.assignment1;

import android.graphics.Point;

import java.util.ArrayList;

/**
 * Created by Shiro on 2/4/2017.
 */

public class WarpingComputations
{
    public static int viewHeight;

    //there are a lot of pieces we have to put together here, so lets see if we can figure some things out.

    //first we need to project the point from father onto the lines drawn upon him.
    //to do this, we'll need a method to calculate the percentage down the first line to the second line.
    public static float fractionalDistance(fPoint PW, fPoint PQ)
    {
        float d = dotProduct(PW, PQ)/(float)Math.sqrt((PQ.x * PQ.x) + (PQ.y * PQ.y));
        float fd = d/(float)Math.sqrt((PQ.x * PQ.x) + (PQ.y * PQ.y));

        return fd;
    }

    //to do this we need to calculate the projection of the warpPoint onto the line, and we need to do that for every single line

    public static float distance (AdjustableLine line, fPoint warp)
    {
        fPoint P = new fPoint(line.getX1(), line.getY1());
        //fPoint Q = new fPoint(line.getX2(), line.getY2());
        fPoint PQ = vector(line);
        fPoint n = normal(PQ);

        fPoint WP = vector(warp, P);

        float d = dotProduct(WP, n)/(float)Math.sqrt((n.x * n.x) + (n.y * n.y));

        return d;
    }

    //we need to find the normal of the vector
    public static fPoint normal(fPoint P, fPoint Q)
    {
        fPoint PQ = vector(P, Q);
        fPoint n = new fPoint(-1 * PQ.y, PQ.x);
        return n;
    }

    public static fPoint normal(fPoint PQ)
    {
        fPoint n = new fPoint(-1 * PQ.y, PQ.x);
        return n;
    }

    public static fPoint vector (fPoint P, fPoint Q)
    {
        fPoint PQ = new fPoint (Q.x - P.x, Q.y - P.y);
        return PQ;
    }

    public static fPoint vector (AdjustableLine line) {
        fPoint PQ = new fPoint(line.getX2() - line.getX1(), line.getY2() - line.getY1());
        return PQ;
    }


    public static float dotProduct(fPoint P, fPoint Q)
    {
        float dp = (P.x * Q.x) + (P.y * Q.y);
        return dp;
    }

    //new warp point, trying to calculate where X from the original lines appears on the secondary lines.
    public static fPoint newWarpPoint(AdjustableLine newLine, float d, float fd)
    {
        fPoint X = new fPoint();
        fPoint P = new fPoint(newLine.getX1(), newLine.getY1());
        fPoint PQ = vector(newLine);
        fPoint N = normal(PQ);
        //P + fraction * PQ - d(n/|n|)
        X.x = P.x + (fd * PQ.x) - (d*(N.x/(float)Math.sqrt((N.x * N.x) + (N.y * N.y))));
        X.y = P.y + (fd * PQ.y) - (d*(N.y/(float)Math.sqrt((N.x * N.x) + (N.y * N.y))));

        return X;
    }
    public static fPoint getTargetPoint(AdjustableLine[] parent, AdjustableLine[]  child, fPoint X)
    {
        float a = 0.14f, b = 2, weight = 0;
        fPoint newWarp = new fPoint(), avgWarp = new fPoint();
        AdjustableLine prtnr, line;

        for (int iii = 0; iii < parent.length; iii++)
        {
            line = parent[iii];
            prtnr = child[iii];
            float dist = WarpingComputations.distance(line, X);
            float fracDist = fractionalDistance(vector(new fPoint(line.getX1(), line.getY1()), X), WarpingComputations.vector(line));

            newWarp = newWarpPoint(prtnr, dist, fracDist);

            avgWarp.x += (newWarp.x - X.x) * Math.pow(1/(a + dist), b);
            avgWarp.y += (newWarp.y - X.y) * Math.pow(1/(a + dist), b);
            weight +=Math.pow(1 /(a + dist), b);
        }

        avgWarp.x = avgWarp.x/weight + X.x;// + fatherLines.get(0).getWarp().x;
        avgWarp.y = avgWarp.y/weight + X.y;// + fatherLines.get(0).getWarp().y;

        return avgWarp;
    }
}
