package com.example.jay.assignment1;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;

/**
 * Created by Jay on 1/19/2017.
 */

public class AdjustableLine
{
    public int DETECT_RADIUS = 30;
    public int lineColour = Color.rgb(20,137,170);

    private float p1x, p1y, p2x, p2y;
    //warp point location
    private fPoint wp;
    /*private Paint lPaint;
    private Bitmap lBit;
    private Canvas lCanvas;
    */
    private RectF p1, p2;


    public AdjustableLine(float x1, float y1, float x2, float y2)//, Bitmap bitmap, Canvas canvas, Paint paint)
    {
        p1x = x1; p2x = x2; p1y = y1; p2y = y2;
        p1 = new RectF(p1x - DETECT_RADIUS, p1y - DETECT_RADIUS, p1x + DETECT_RADIUS, p1y + DETECT_RADIUS);
        p2 = new RectF(p2x - DETECT_RADIUS, p2y - DETECT_RADIUS, p2x + DETECT_RADIUS, p2y + DETECT_RADIUS);
    }

    public float getX1()
    {
        return p1x;
    }

    public float getX2()
    {
        return p2x;
    }

    public float getY1()
    {
        return p1y;
    }

    public float getY2()
    {
        return p2y;
    }

    public void setX1(float x)
    {
        p1x = x;
        p1.set(p1x - DETECT_RADIUS, p1y - DETECT_RADIUS, p1x + DETECT_RADIUS, p1y + DETECT_RADIUS);
    }

    public void setX2(float x)
    {
        p2x = x;
        p2.set(p2x - DETECT_RADIUS, p2y - DETECT_RADIUS, p2x + DETECT_RADIUS, p2y + DETECT_RADIUS);
    }

    public void setY1(float y)
    {
        p1y = y;
        p1.set(p1x - DETECT_RADIUS, p1y - DETECT_RADIUS, p1x + DETECT_RADIUS, p1y + DETECT_RADIUS);
    }

    public void setY2(float y)
    {
        p2y = y;
        p2.set(p2x - DETECT_RADIUS, p2y - DETECT_RADIUS, p2x + DETECT_RADIUS, p2y + DETECT_RADIUS);
    }

    public RectF getPoint1()
    {
        return p1;
    }

    public RectF getPoint2()
    {
        return p2;
    }

    public fPoint getWarp()
    {
        return wp;
    }

    public void setWarp(fPoint n)
    {
        wp = n;
    }


    public void setColour(int colour)
    {
        lineColour = colour;
    }

}
