﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    
    public struct rgbPixel
    {
        int x, y;
        double r, g, b;
    }

    public struct cyPixel
    {
        public int x, y;
        public double cy, cr, cb;
    }

    class Converter
    {

        double kr = 0.299, kg = 0.587, kb = 0.114;

        int imgWidth, imgHeight;
        cyPixel[] cyImg;

        public void toYCrCb(Bitmap img)
        {
            cyImg = new cyPixel[img.Width * imgHeight];

            for ( int i = 0, k = 0; i < img.Height; i++)
                for ( int j = 0; j < img.Width; j++, k++)
                {
                    double y = kr * img.GetPixel(i,j).R + (1 - kr - kb) * img.GetPixel(i,j).G + kb * img.GetPixel(i,j).B;
                    double pb = 0.5 * (img.GetPixel(i, j).B - y) / (1 - kb);
                    double pr = 0.5 * (img.GetPixel(i, j).R - y) / (1 - kr);

                    cyImg[k].x = i;
                    cyImg[k].y = j;
                    cyImg[k].cy = y;
                    cyImg[k].cb = pb;
                    cyImg[k].cr = pr;
                }
        }
    }


}
