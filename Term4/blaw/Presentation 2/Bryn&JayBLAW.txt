Area of the Law: Right of Privacy
The right of privacy has been defined as the right to live one's life in seclusion, 
without being subjected to unwarrented and undesired publicity.

Name of the case: Melvin v Reid

Date: Began July 1925, ended march 25, 1932

Court Level: Court of Appeal of California, Fourth District

Jurisdiction: United States

Facts:

Legal Issues or Legal Questions: 
Basically Can someone's name be used in the production of a film
that recreates or portrays events that have happened in the past
that may cause a harm to that persons reputation be used without 
the permission of said person.

Outcome: 

Judges Reasons:
Quote:
The use of appellant's true name in connection with the incidents 
of her former life in the plot and advertisements was unnecessary 
and indelicate and a wilful and wanton disregard of that charity 
which should actuate us in our social intercourse and which should 
keep us from unnecessarily holding another up to the scorn and contempt 
of upright members of society. 
Upon demurrer the allegations of the complaint must be taken as true. 
We must therefore conclude that eight years before the production of 
"The Red Kimono", appellant had abandoned her life of shame, had 
rehabilitated herself and had taken her place as a respected and 
honored member of society. This change having occurred in her life, 
she should have been permitted to continue its course without having 
her reputation and social standing destroyed by the publication of 
the story of her former depravity with no other excuse than the 
expectation of private gain by the publishers.

3 Questions for the case:

1) If the events of the case had not taken place, Therefore the new
precident had not been set for people have this new set level of privacy. 
How do you think it would effect you today?

2) Do you think that if not for this event, a new precident would
never have been set for us?

3) Do you believe that she should have been aquited of all charges 
for the original murder?
