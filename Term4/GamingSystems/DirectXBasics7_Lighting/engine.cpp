#include "includefiles.h"


int GameInit(){
	HRESULT r = 0;//return values

	g_pD3D = Direct3DCreate9(D3D_SDK_VERSION);//COM object
	if( g_pD3D == NULL){
		SetError("Could not create IDirect3D8 object");
		return E_FAIL;
	}

	r = InitDirect3DDevice(g_hWndMain, 640, 480, FALSE, D3DFMT_A8R8G8B8, g_pD3D, &g_pDevice);
	if(FAILED(r)){//FAILED is a macro that returns false if return value is a failure - safer than using value itself
		SetError("Initialization of the device failed");
		return E_FAIL;
	}

	r= CreateViewport();
	if(FAILED(r)){//FAILED is a macro that returns false if return value is a failure - safer than using value itself
		SetError("Could not create viewport");
		return E_FAIL;
	}

	SetProjectionMatrix();

	D3DXMATRIX ViewMatrix;

	D3DXMatrixLookAtLH(&ViewMatrix, &D3DXVECTOR3(0.0f, 3.0f, -5.0f),
									&D3DXVECTOR3(0.0f, 0.0f, 0.0f),
									&D3DXVECTOR3(0.0f, 1.0f, 0.0f));

	g_pDevice->SetTransform(D3DTS_VIEW, &ViewMatrix);


	g_pDevice->Clear(0,0,D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_ARGB(255, 0, 0, 55), 1.0f, 0);

	r = g_pDevice->GetBackBuffer(0,0, D3DBACKBUFFER_TYPE_MONO,&g_pBackSurface );
	if(FAILED(r)){//FAILED is a macro that returns false if return value is a failure - safer than using value itself
		SetError("Could not get backbuffer");
		return E_FAIL;
	}

	LoadAlphabet("Alphabet vSmall.bmp", 8, 16);

	DRVERTEX DRVertices[] = {
		{ -2.0f,-2.0f, 0.0f, -1.0f,-1.0f, 0.0f },
        {  2.0f,-2.0f, 0.0f, 1.0f,-1.0f, 0.0f },
        {  0.0f, 2.0f, 0.0f, 0.0f,1.0f, 0.0f },
	};

	r= g_pDevice->CreateVertexBuffer(sizeof(struct DRVERTEX)*3, 0, DRVERTEX_FLAGS, D3DPOOL_DEFAULT, &g_pVB,NULL);
	if(FAILED(r)){//FAILED is a macro that returns false if return value is a failure - safer than using value itself
		SetError("Could not create vertex buffer");
	return E_FAIL;
	}

	//Pointer to vertex buffer
	BYTE* pVertices = 0;

	//Lock buffer
	r = g_pVB->Lock(0, sizeof(DRVertices), (void **)&pVertices,0);
	if(FAILED(r)){//FAILED is a macro that returns false if return value is a failure - safer than using value itself
		SetError("Could not lock buffer");
		return E_FAIL;
	}

		// Turn off culling so the back of the triangle does not disapear
     g_pDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );

	//Fill buffer
	CopyMemory(pVertices, &DRVertices, sizeof(DRVertices));

	//Unlock buffer
	g_pVB->Unlock();
//Material
	D3DMATERIAL9 Material;
	ZeroMemory(&Material, sizeof(D3DMATERIAL9));

	//setup material
	Material.Diffuse.r=1.0f;
	Material.Diffuse.g=0.0f;
	Material.Diffuse.b=0.0f;
	Material.Diffuse.a=1.0f;
	Material.Ambient.r=1.0f;
	Material.Ambient.g=1.0f;
	Material.Ambient.b=0.0f;
	Material.Ambient.a=1.0f;

	g_pDevice->SetMaterial(&Material);
		D3DLIGHT9 Light;
	ZeroMemory(&Light, sizeof(D3DLIGHT9));

	//Set up light
	Light.Type       = D3DLIGHT_DIRECTIONAL;
	Light.Diffuse.r = 1.0f;
	Light.Diffuse.g = 1.0f;
	Light.Diffuse.b = 1.0f;
    Light.Range      = 100.0f;
   D3DXVECTOR3 vecDir;
	vecDir = D3DXVECTOR3(1.0f,
                         1.0f,
                         1.0f 
						 );
    D3DXVec3Normalize( (D3DXVECTOR3*)&Light.Direction, &vecDir );
	//attach light structure to a Direct3D Lighting index

	g_pDevice->SetLight(0, &Light);

	//enable light
	g_pDevice->LightEnable(0,TRUE);

// Turn on lighting
    g_pDevice->SetRenderState( D3DRS_LIGHTING, TRUE );

	// Set the ambient light level
	g_pDevice->SetRenderState( D3DRS_AMBIENT, D3DCOLOR_XRGB( 25, 25, 25 ) );

	srand(GetTickCount());
	InitTiming();

	return S_OK;
}
int GameLoop(){

	FrameCount();
	Render();

	if (GetAsyncKeyState(VK_ESCAPE))
		PostQuitMessage(0);

	return S_OK;
}

int GameShutdown(){

	UnloadAlphabet();

	if(g_pVB)
		g_pVB->Release();

	//release resources. First display adapter because COM object created it, then COM object
	if (g_pBackSurface)
		g_pBackSurface->Release();

	if(g_pDevice)
		g_pDevice->Release();

	if(g_pD3D)
		g_pD3D->Release();

	return S_OK;
}

int Render(){
	HRESULT r;

	if(!g_pDevice){
		SetError("Cannot render because there is no device");
		return E_FAIL;
	}

	g_pDevice->Clear(0,0,D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 100), 1.0f, 0);

	r=ValidateDevice();
	if(FAILED(r)){
		return E_FAIL;
	}

	D3DLOCKED_RECT Locked;

	g_pBackSurface->LockRect(&Locked, 0, 0);

	PrintFrameRate(550, 400, TRUE, D3DCOLOR_ARGB(255,255,0,255), (DWORD*)Locked.pBits, Locked.Pitch);

	g_pBackSurface->UnlockRect();

	//animate object
	D3DXMATRIX RotationX, RotationY, WorldMatrix;

	D3DXMatrixRotationY( &WorldMatrix, timeGetTime()/500.0f );
	//D3DXMatrixRotationX( &RotationX, timeGetTime()/800.0f );
	//D3DXMatrixMultiply( &WorldMatrix, &RotationX, &RotationY );

	g_pDevice->SetTransform( D3DTS_WORLD, &WorldMatrix );

	//Start to render in 3D
	g_pDevice->BeginScene();

	//set rendering stream
	g_pDevice->SetStreamSource(0, g_pVB, 0,sizeof(DRVERTEX));//order of last two params changed from d3d8 to d3d9

	//set vertex shading
	g_pDevice->SetFVF(DRVERTEX_FLAGS);//DirectX9 version

	//draw from stream
	g_pDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);

	//finish rendering
	g_pDevice->EndScene();

	g_pDevice->Present(NULL, NULL, NULL, NULL);//swap over buffer to primary surface
	return S_OK;
}

void Draw(int Pitch, DWORD* pData){
	DWORD Offset = 100*Pitch/4 + 200;
	pData[Offset] = D3DCOLOR_XRGB(255,0,0);

	//SimpleBitmapDraw();
}
void SimpleBitmapDraw(char* PathName, LPDIRECT3DSURFACE9 pBackSurf, int dpx, int dpy){
	LPDIRECT3DSURFACE9 pSurface = 0;

	LoadBitmapToSurface(PathName, &pSurface, g_pDevice);

	D3DSURFACE_DESC d3dsd;
	pSurface->GetDesc(&d3dsd);//get info about surface

	POINT DestPoint = {dpx, dpy};
	RECT rect = {0,0, d3dsd.Width, d3dsd.Height};//source dimensions

	g_pDevice->StretchRect(pSurface,&rect,pBackSurf,NULL, D3DTEXF_NONE);//replaces copyrects for D3D9

//	pSurface->Release();
//	pSurface = 0;

//	pBackSurf->Release();
//	pBackSurf = 0;
//
//	g_pDevice->Present(NULL, NULL, NULL, NULL);//put it on the primary surface
}

int LoadBitmapToSurface(char* PathName, LPDIRECT3DSURFACE9* ppSurface, LPDIRECT3DDEVICE9 pDevice){
	HRESULT r;
	HBITMAP hBitmap;
	BITMAP Bitmap;

	hBitmap = (HBITMAP)LoadImage(NULL, PathName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
	if(hBitmap == NULL){
		SetError("Unable to load bitmap");
		return E_FAIL;
	}

	GetObject(hBitmap, sizeof(BITMAP), &Bitmap);
	DeleteObject(hBitmap);//we only needed it for the header info to create a D3D surface

	//create surface for bitmap
	r=pDevice->CreateOffscreenPlainSurface(Bitmap.bmWidth, Bitmap.bmHeight, D3DFMT_A8R8G8B8,D3DPOOL_SCRATCH, ppSurface, NULL);
	if(FAILED(r)){
		SetError("Unable to create surface for bitmap load");
		return E_FAIL;
	}

	//load bitmap onto surface
	r = D3DXLoadSurfaceFromFile(*ppSurface, NULL, NULL, PathName, NULL, D3DX_FILTER_NONE, 0, NULL);
	if(FAILED(r)){
		SetError("Unable to laod file to surface");
		return E_FAIL;
	}

	return S_OK;
}
