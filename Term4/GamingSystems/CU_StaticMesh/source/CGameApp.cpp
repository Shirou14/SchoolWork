/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

Title : WinMain.cpp
Author : Chad Vernon
URL : http://www.c-unit.com

Description : Static Mesh

Created :  08/26/2005
Modified : 12/05/2005

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "..\include\stdafx.h"
#include "..\include\CGameApp.h"

char* g_instructions = "Esc: Quit\r\nF5: Toggle fullscreen\r\nF6: Toggle wireframe";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Default constructor
Parameters:
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
CGameApp::CGameApp()
{
    m_pFramework = NULL;
    m_pTemple = NULL;
    m_showInstructions = FALSE;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Clean up resources
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void CGameApp::Release()
{
    SAFE_RELEASE( m_pFramework );
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Sets the CFramework instnace of the application.
Parameters:
[in] pFramework - Pointer to a CFramework instance
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void CGameApp::SetFramework( CFramework* pFramework )
{
    m_pFramework = pFramework;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Initialize application-specific resources and states here.
Returns: TRUE on success, FALSE on failure
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
BOOL CGameApp::Initialize()
{
    // Create the light
    ZeroMemory( &m_light, sizeof( D3DLIGHT9 ) );
    m_light.Type = D3DLIGHT_DIRECTIONAL;
    m_light.Diffuse.r = 1.0f;
    m_light.Diffuse.g = 1.0f;
    m_light.Diffuse.b = 1.0f;
    m_light.Direction.x = -1.0f;
    m_light.Direction.y = -1.0f;
    m_light.Direction.z = 1.0f;
    return TRUE;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: 
This callback function will be called immediately after the Direct3D device has been created. This is 
the best location to create D3DPOOL_MANAGED resources. Resources created here should be released in 
the OnDestroyDevice callback. 
Parameters:
[in] pDevice - Pointer to a DIRECT3DDEVICE9 instance
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void CGameApp::OnCreateDevice( LPDIRECT3DDEVICE9 pDevice )
{
    // Load meshes
    m_mesh.Load( pDevice, "temple.x" );
    if ( m_pTemple )
    {
        for ( int i = 0; i < 2; i++ )
        {
            m_pTemple[i].Release();
        }
    }
    m_pTemple = new CMeshInstance[2];
    m_pTemple[0].SetMesh( &m_mesh );
    m_pTemple[1].SetMesh( &m_mesh );


    // Create sprite for batching text calls
    D3DXCreateSprite( pDevice, &m_pTextSprite );

    // Create 2D text
    m_font.Initialize( pDevice, "Arial", 12 );
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: 
This callback function will be called immediately after the Direct3D device has been created. This is 
the best location to create D3DPOOL_DEFAULT resources. Resources created here should be released in 
the OnLostDevice callback. 
Parameters:
[in] pDevice - Pointer to a DIRECT3DDEVICE9 instance
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void CGameApp::OnResetDevice( LPDIRECT3DDEVICE9 pDevice )
{
    m_pTextSprite->OnResetDevice();
    m_font.OnResetDevice();

    // Set transforms
    D3DXVECTOR3 cameraPosition( 0.0f, 8.0f, -15.0f );
    D3DXVECTOR3 cameraTarget( 0.0f, 0.0f, 5.0f );
    D3DXVECTOR3 cameraUp( 0.0f, 1.0f, 0.0f );
    D3DXMATRIX viewMatrix;
    D3DXMatrixLookAtLH( &viewMatrix, &cameraPosition, &cameraTarget, &cameraUp );
    pDevice->SetTransform( D3DTS_VIEW, &viewMatrix );
    D3DXMATRIX projection;
    float aspect = (float)m_pFramework->GetWidth() / (float)m_pFramework->GetHeight();
    D3DXMatrixPerspectiveFovLH( &projection, D3DX_PI / 3.0f, aspect, 0.1f, 1000.0f );
    pDevice->SetTransform( D3DTS_PROJECTION, &projection );

    // Set render states
    pDevice->SetRenderState( D3DRS_FILLMODE, m_pFramework->GetFillMode() );      
    pDevice->SetRenderState( D3DRS_SHADEMODE, D3DSHADE_GOURAUD );  
    pDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
    pDevice->SetRenderState( D3DRS_AMBIENT, D3DCOLOR_XRGB( 80, 80, 80 ) );
    pDevice->LightEnable( 0, TRUE );
    pDevice->SetLight( 0, &m_light );
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: 
This callback function will be called immediately after the Direct3D device has entered a lost state 
and before IDirect3DDevice9::Reset is called. Resources created in the OnResetDevice callback should 
be released here, which generally includes all D3DPOOL_DEFAULT resources.
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void CGameApp::OnLostDevice()
{
    m_pTextSprite->OnLostDevice();
    m_font.OnLostDevice();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: 
This callback function will be called immediately after the Direct3D device has been destroyed. 
Resources created in the OnCreateDevice callback should be released here, which generally includes 
all D3DPOOL_MANAGED resources.
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void CGameApp::OnDestroyDevice()
{
    SAFE_RELEASE( m_pTextSprite );
    m_font.Release();

    if ( m_pTemple )
    {
        for ( int i = 0; i < 2; i++ )
        {
            m_pTemple[i].Release();
        }
    }
    m_mesh.Release();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Updates the current frame.
Parameters:
[in] pDevice - Pointer to a DIRECT3DDEVICE9 instance
[in] elapsedTime - Time elapsed since last frame
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void CGameApp::OnUpdateFrame( LPDIRECT3DDEVICE9 pDevice, float elapsedTime )
{
    m_pTemple[0].SetXPosition( -5.0f );
    m_pTemple[0].ScaleAbs( 0.7f, 0.7f, 0.7f );
    m_pTemple[0].RotateRel( 0.0f, D3DXToRadian( 10.0f ) * elapsedTime, 0.0f );

    m_pTemple[1].SetXPosition( 5.0f );
    m_pTemple[1].ScaleAbs( 0.7f, 0.7f, 0.7f );
    m_pTemple[1].RotateRel( 0.0f, D3DXToRadian( 10.0f ) * elapsedTime, 0.0f );
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Renders the current frame.
Parameters:
[in] pDevice - Pointer to a DIRECT3DDEVICE9 instance
[in] elapsedTime - Time elapsed since last frame
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void CGameApp::OnRenderFrame( LPDIRECT3DDEVICE9 pDevice, float elapsedTime )
{
    sprintf( m_fps, "%.2f fps", m_pFramework->GetFPS() );

    pDevice->Clear( 0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB( 0, 0, 0 ), 1.0f, 0 ); 
    pDevice->BeginScene();

    for ( int i = 0; i < 2; i++ )
    {
        m_pTemple[i].Render( pDevice );
    }

    // Display framerate and instructions
    m_pTextSprite->Begin( D3DXSPRITE_ALPHABLEND | D3DXSPRITE_SORT_TEXTURE );
    m_font.Print( m_fps, 5, 5, D3DCOLOR_XRGB( 255, 0, 0 ), m_pTextSprite );
    if ( m_showInstructions )
    {
        m_font.Print( g_instructions, 5, 20, D3DCOLOR_XRGB( 255, 255, 255 ), m_pTextSprite );
    }
    else
    {
        m_font.Print( "Hit F1 to view the instructions.", 5, 20, D3DCOLOR_XRGB( 255, 255, 255 ), m_pTextSprite );
    }
    m_pTextSprite->End();

    pDevice->EndScene();
    pDevice->Present( 0, 0, 0, 0 );
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Responds to key presses
Parameters:
[in] wParam - Key down argument
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void CGameApp::OnKeyDown( WPARAM wParam )
{
    switch ( wParam )
    {
    case VK_ESCAPE:
        PostQuitMessage( 0 );
        break;
    case VK_F1:
        m_showInstructions = !m_showInstructions;
        break;
    case VK_F5:
        if ( m_pFramework != NULL )
        {
            m_pFramework->ToggleFullscreen();
        }
        break;
    case VK_F6:
        if ( m_pFramework != NULL )
        {
            m_pFramework->ToggleWireframe();
        }
        break;
    }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Application entry point
Parameters:
[in] hInstance - Application instance
[in] hPrevInstance - Junk
[in] lpCmdLine - Command line arguments
[in] nCmdShow - Window display flags
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow )
{ 
    CGameApp* pApplication = new CGameApp();
    CFramework* pFramework = new CFramework( (CBaseApp*)pApplication );

    pApplication->SetFramework( pFramework );

    // Initialize any application resources
    if ( !pApplication->Initialize() )
    {
        return 0;
    }

    // Initialize the Framework
    if ( !pFramework->Initialize( "Static Mesh", hInstance, 640, 480, TRUE ) )
    {
        return 0;
    }

    // Rock and roll
    pFramework->Run();
    
    // Clean up resources
    SAFE_RELEASE( pApplication );

    return 0;
}