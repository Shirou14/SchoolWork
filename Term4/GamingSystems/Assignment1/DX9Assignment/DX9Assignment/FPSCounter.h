#pragma once
#ifndef FPSCOUNTER_H
#define FPSCOUNTER_H

#include <windows.h>

//this class is designed to keep track of the frames per second
class FPSCounter
{
public:
	FPSCounter();
	//increase the frame count
	void increment();
	//set frame count to zero
	void reset();
	//return the frames per second
	int fps();
private:
	int frames_;
	int fps_;
	LARGE_INTEGER freq_;
	LARGE_INTEGER startTime_;
	LARGE_INTEGER currTime_;
};

#endif