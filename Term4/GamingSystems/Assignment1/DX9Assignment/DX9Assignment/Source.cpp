#define WIN32_LEAN_AND_MEAN

#include "Header.h"
#include "FPSCounter.h"
#include "Line.h"

/*
	The purpose of this assignment is to create a full-screen application using win32 and DX9
	The user should be able to load a bitmap from the constant, and draw a line. 
	Everytime the use draws a line, the application reloads the scene with the new line segment.
*/

FPSCounter counter;
ID3DXFont *pfont;
wchar_t buffer[200];
RECT fRect;
Line line;
POINT ps;

//this is the proc for creating the window
//Requires and hinstance, the previous instance, not sure what a pstr is, and an int cmdshow
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pstrCmdLine, int iCmdShow)
{
	HWND hwnd;
	MSG msg;
	WNDCLASSEX wc;

	fRect.top = 0;
	fRect.left = 0;
	fRect.bottom = 50;
	fRect.right = 100;

	static WCHAR strAppName[] = TEXT("Jay's Magnificient Application");

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.lpfnWndProc = WndProc;
	wc.hInstance = hInstance;
	wc.hbrBackground = (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hIconSm = LoadIcon(NULL, IDI_HAND);
	wc.hCursor = LoadCursor(NULL, IDC_CROSS);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = strAppName;

	RegisterClassEx(&wc);

	hwnd = CreateWindowEx(
		NULL,
		strAppName,
		strAppName,
		WS_EX_TOPMOST | WS_POPUP,
		0,
		0,
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	g_hwndMain = hwnd; //Set the global handle instance

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	if (FAILED(GameInit(hwnd))) {
		MessageBox(hwnd, TEXT("Initialization Failed"), NULL, NULL);
		GameShutdown();
		return E_FAIL;
	}
	
	while (TRUE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) //we peek at the message to see if it is relevant to us. PM_REMOVE removes it from the queue.
		{
			if (msg.message == WM_QUIT)
				break;
			//translates and sends the message to wndProc
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			GameLoop(hwnd);
		}
		//GameLoop(hwnd);
		//Render_Frame();
	}
	GameShutdown();//clean up the game here
	return msg.wParam;

}

//running the window
long CALLBACK WndProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{

	switch (uMessage) {
		case WM_CREATE:
		{
			return 0;
		}
		case WM_PAINT:
		{
			ValidateRect(hWnd, NULL);//basically saying - yeah we took care of any paint msg without any overhead
			return 0;
		}
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}
		case WM_LBUTTONDOWN: //If we get the mouse down, we want to start the line and set p1
		{
			GetCursorPos(&ps);
			line.setP1(ps.x, ps.y);
		}
		case WM_MOUSEMOVE:
		{
			if (wParam == MK_LBUTTON)//if the mouse moves while lbutton down, we want to adjust P2
			{
				GetCursorPos(&ps);
				line.setP2(ps.x, ps.y);
			}
		}
		default:
		{
			return DefWindowProc(hWnd, uMessage, wParam, lParam);
		}
	}
}

int GameInit(HWND hwnd)
{
	HRESULT r = 0;//return values
	HBITMAP hBitmap;
	BITMAP bitmap;
	mainSurface = 0;
	//we make sure the counter is zero on init
	counter.reset();

	g_pD3D = Direct3DCreate9(D3D_SDK_VERSION);//com object
	if (g_pD3D == NULL)
	{
		MessageBox(hwnd, TEXT("Could not create IDirect3D9 object"), NULL, NULL);
		return E_FAIL;
	}
	//parameters object
	D3DPRESENT_PARAMETERS d3dpp;

	ZeroMemory(&d3dpp, sizeof(d3dpp)); //clear out the struct for use
	d3dpp.Windowed = TRUE;//we make it windowed
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD; //Discards old frames
	d3dpp.hDeviceWindow = hwnd; //sets the window used by DX9
	d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8; //set back buffer to 32 bit
	d3dpp.BackBufferWidth = SCREEN_WIDTH;
	d3dpp.BackBufferHeight = SCREEN_HEIGHT;
	/*
	g_pD3D->CreateDevice
	(
		D3DADAPTER_DEFAULT,
		D3DDEVTYPE_HAL,
		hwnd,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING,
		&d3dpp,
		&g_pDevice
	);*/


	r = InitDirect3DDevice(hwnd, 640, 480, FALSE, D3DFMT_X8R8G8B8, g_pD3D, &g_pDevice);
	if (FAILED(r))//FAILED is a macro that returns false if the return value is a failure - safer than using the value itself
	{
		//SetError("Initialization of the device failed");
		return E_FAIL;
	}

	pfont = NULL;//we make sure the the pFont is empty
	//set up the font
	r = D3DXCreateFont(g_pDevice, 50, 0, FW_NORMAL, 1, TRUE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Arial"), &pfont);


	/*hBitmap = (HBITMAP)LoadImage(NULL, TEXT("Otter.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
	GetObject(hBitmap, sizeof(BITMAP), &bitmap);
	DeleteObject(hBitmap);*/

	r = g_pDevice->CreateOffscreenPlainSurface(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), D3DFMT_X8R8G8B8, D3DPOOL_SYSTEMMEM, &mainSurface, NULL);

	r = D3DXLoadSurfaceFromFile(mainSurface, NULL, NULL, TEXT("D:\\~User Data\\Projects\\SchoolWork\\Term4\\GamingSystems\\Assignment1\\DX9Assignment\\DX9Assignment\\Debug\\Otter.bmp"), NULL, D3DX_DEFAULT, 0, NULL);

	return S_OK;
}

int Render_Frame(HWND hwnd)//We do all the calculactions and functions necessary to do every frame
{
	HRESULT r;
	D3DLOCKED_RECT plockedRect;//locked area of display memory (buffer in actuality)
	LPDIRECT3DSURFACE9 pBackSurf = 0;

	//clear window to a deep blue (actually pink but whatevs)
	g_pDevice->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(255, 40, 100), 1.0f, 0); 

	//we get the backbuffer and set it to pBackSurf
	r = g_pDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &pBackSurf);
	//update the main surface to be the background surface (I think)
	g_pDevice->UpdateSurface(mainSurface, NULL, pBackSurf, NULL);
	if (FAILED(r))
	{
		MessageBox(hwnd, TEXT("Could not get Back Buffer"), NULL, NULL);
		return E_FAIL;
	}

	//r = D3DXLoadSurfaceFromSurface(pBackSurf, NULL, NULL, mainSurface, NULL, NULL, D3DX_FILTER_TRIANGLE, 0);

	r = pBackSurf->LockRect(&plockedRect, NULL, 0);	//lock the backSurf rect

	if (FAILED(r))
	{
		MessageBox(hwnd, TEXT("Could not lock Back Buffer"), NULL, NULL);
		return E_FAIL;
	}

	//set the pData
	DWORD* pData = (DWORD*)(plockedRect.pBits);

	//render on the back buffer here pls
	Draw(plockedRect.Pitch, pData);

	g_pDevice->BeginScene(); //begin the 3D scene

	//put the frame counter into the buffer
	_stprintf_s(buffer, 200, TEXT("FPS: %d\n"), counter.fps());
	r = pfont->DrawText(NULL, buffer, -1, &fRect, DT_LEFT | DT_NOCLIP, 0xFF0000FF);//display the FPS

	g_pDevice->EndScene(); //end the 3D scene
	pBackSurf->UnlockRect();//unlock the rect

	pData = 0;//clear out the pData

	pBackSurf->Release();//release the back surface

	pBackSurf = 0;//set to 0

	g_pDevice->Present(NULL, NULL, NULL, NULL);//present the surface
	return S_OK;
}

//this loop runs every possible time when we're not peaking at something
int GameLoop(HWND hwnd)
{
	//increase fps
	counter.increment();
	//render the frame
	Render_Frame(hwnd);

	//if escape, do that
	if (GetAsyncKeyState(VK_ESCAPE))
	{
		PostQuitMessage(0);
	}

	return S_OK;
}

//release everything created to properly shut down
int GameShutdown()
{
	//releases the resources. First display adapter, then the COM object
	//this is because the COM object created the display adapter
	if (g_pDevice)
		g_pDevice->Release(); //POSSIBLY C, MAY NEED TO BE CHANGED 

	if (g_pD3D)
		g_pD3D->Release();

	return S_OK;
}

//runs the calculations to draw the line
void Draw(int Pitch, DWORD* pData)
{
	if (line.getX1() > 0 || line.getX2() > 0 || line.getY1() > 0 || line.getY2() > 0)
	{
		int length = abs(line.getX2() - line.getX1()) * abs(line.getY2() - line.getY1());
		for (double i = 0; i < length; i++) {
			int x = line.getX1() + (i / length) * (line.getX2() - line.getX1());
			int y = line.getY1() + (i / length) * (line.getY2() - line.getY1());
			DWORD Offset = y * Pitch / 4 + x;
			pData[Offset] = D3DCOLOR_XRGB(255, 0, 100);
		}
	}
}