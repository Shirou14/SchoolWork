#include "line.h"

//if we create a line with desires co-ordinates, we input them here
Line::Line(int x1, int x2, int y1, int y2):X1_(x1), X2_(x2), Y1_(y1), Y2_(y2)
{}

//without any input, we initiaize the line to zero
Line::Line() : X1_(0), X2_(0), Y1_(0), Y2_(0) 
{}

//these functions return the co-ordinates of the points
int Line::getX1()
{
	return X1_;
}
int Line::getX2()
{
	return X2_;
}
int Line::getY1()
{
	return Y1_;
}
int Line::getY2()
{
	return Y2_;
}

//we set the points with these functions
void Line::setP1(int x, int y)
{
	X1_ = x;
	Y1_ = y;
}
void Line::setP2(int x, int y)
{
	X2_ = x;
	Y2_ = y;
}