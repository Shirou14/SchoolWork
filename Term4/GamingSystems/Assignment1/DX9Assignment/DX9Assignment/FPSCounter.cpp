#include "FPSCounter.h"

//constructor
FPSCounter::FPSCounter() : frames_(0), fps_(0)
{
	//grab the current frequency
	QueryPerformanceFrequency((LARGE_INTEGER *)&freq_);
	//grab the current time
	QueryPerformanceCounter((LARGE_INTEGER *)&startTime_);
	//set the current time to be the start time because no time has passed
	currTime_ = startTime_;
}

//return fps
int FPSCounter::fps()
{
	return fps_;
}

//increment the fps counter
void FPSCounter::increment()
{
	QueryPerformanceCounter((LARGE_INTEGER *)&currTime_);//pull the current time
	if ((currTime_.QuadPart - startTime_.QuadPart) / (freq_.QuadPart) >= 1)//if the time has passed a second
	{
		reset();//reset the counter
	}
	else {
		frames_++;//increase the counter
	}
}

//clears the fps
void FPSCounter::reset()
{
	fps_ = frames_;//set the current fps to the latest frames count
	frames_ = 0;//set frames to 0
	QueryPerformanceFrequency((LARGE_INTEGER *)&freq_);//grab current frequency
	QueryPerformanceCounter((LARGE_INTEGER *)&startTime_);//get the current time
	currTime_ = startTime_;//time hasn't passed again
}