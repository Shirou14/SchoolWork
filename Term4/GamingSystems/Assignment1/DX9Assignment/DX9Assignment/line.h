#pragma once

class Line{
	//the line class holds and provides the point co-ordinates
public:
	Line(int, int, int, int);
	Line();
	//these functions return the point co-ordinates
	int getX1();
	int getX2();
	int getY1();
	int getY2();
	//sets the points
	void setP1(int, int);
	void setP2(int, int);

private:
	//co-ordinates
	int X1_;
	int X2_;
	int Y1_;
	int Y2_;
};