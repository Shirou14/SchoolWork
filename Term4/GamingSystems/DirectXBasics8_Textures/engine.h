#ifndef __engine__h
#define __engine__h
//#include "includefiles.h"

/*Forward Declarations*/
int GameInit();
int GameLoop();
int GameShutdown();
void SetError(char*,...);
int Render();
//void Draw(int, DWORD*);
int LoadBitmapToSurface(char*, LPDIRECT3DSURFACE9*, LPDIRECT3DDEVICE9);
//void SimpleBitmapDraw(char*, LPDIRECT3DSURFACE9, int, int);
HRESULT InitTiming();
int InitDirect3DDevice(HWND, int, int, BOOL, D3DFORMAT, LPDIRECT3D9, LPDIRECT3DDEVICE9*);



#endif