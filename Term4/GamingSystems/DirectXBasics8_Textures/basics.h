#ifndef __basics_h__
#define __basics_h__
#define DRVERTEX_FLAGS (D3DFVF_XYZ |D3DFVF_DIFFUSE | D3DFVF_TEX1)

struct DRVERTEX {
	float x, y, z;
	DWORD DiffuseColor;
	float tu, tv;
};

HRESULT InitTiming();
HRESULT ValidateDevice();
HRESULT RestoreGraphics();
HRESULT CreateViewport();
void SetProjectionMatrix();
void FrameCount();
int InitDirect3DDevice(HWND hWndTarget, int Width, int Height, BOOL bWindowed, D3DFORMAT FullScreenFormat, LPDIRECT3D9 pD3D, LPDIRECT3DDEVICE9* ppDevice);
void SetError(char* szFormat, ...);
#endif